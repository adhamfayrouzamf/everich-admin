import Link from "next/link"
import { Button } from "reactstrap"
export default function InAccessible ({message}){

    return (
        <div style={{
            width: "100vw",
            height: "100vh",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
        }}>
            <h3>{message || "You Have No Access To This Content"}</h3>
            <Link href="/dashboard" passHref>
                <Button tag="a">Back To Dashboard</Button>
            </Link>
        </div>
    )
}