import { useState, useEffect, useContext } from 'react';
import { useRouter } from 'next/router';
import {UserContext} from "./contexts/AuthContext"
import InAccessible from "./InAccessible"
const authPaths = process.env.authPaths
const privatePaths = process.env.privatePaths

function RouteGuard({ children }) {
    const route = useRouter();
    const [authorized, setAuthorized] = useState(false);
    const [hasAccess, setHasAccess] = useState(true)
    const user = useContext(UserContext)
    
    useEffect(()=>{
        // on route change start - hide page content by setting authorized to false  
        const hideContent = () => {setAuthorized(false);}
        route.events.on('routeChangeStart', hideContent);
        // // on route change complete - run auth check 
        // route.events.on('routeChangeComplete', authCheck)

        // // unsubscribe from events in useEffect return function
        return () => {
            route.events.off('routeChangeStart', hideContent);
            // route.events.off('routeChangeComplete', authCheck);
        }

    },[])
    useEffect(() => {
        authCheck(route.pathname);
    }, [user,route]);

    // useEffect(()=>{
    //     console.log("auth with user")
    //     authCheck(route.pathname);
    // },[user])
    
    const ruleCheck = (url)=>{
        console.log("rule check start")
        if(!user.isLoggedIn) return false
        if(user.type === "ADMIN") return true
        const path = privatePaths.find(p=>p.url === url)
        if(path){
            console.log(path.roleNumber)
            if(path.roleNumber === -1) return true
            if (!user.rules) return false

            const rule = user.rules.find(r=> r.rule.number === path.roleNumber)
            console.log(rule)
            if(rule){
                console.log("has permission",rule.properties.includes(path.property))
                return rule.properties.includes(path.property)
            }
            else
                return false
        }else
            return true
    }

    const authCheck = (url,test)=>{
        // redirect to login page if accessing a private page and not logged in 
        const path = url.split('?')[0];
        console.log("user state",user,url,route.pathname)
        if (!user.isLoggedIn && !authPaths.includes(path)) {
            setAuthorized(false);
            route.push({
                pathname: '/auth/login',
                query: { redirectUrl: route.asPath }
            });
        }else if(user.isLoggedIn && authPaths.includes(path)){
            setAuthorized(false);
            route.push({
                pathname: route.query.redirectUrl || '/dashboard',
            });
        } else if (privatePaths.find(p=>p.url === path)) {
            const hasPermission = ruleCheck(path)
            // if(!hasPermission){
            //     route.push({
            //         pathname:'/dashboard',
            //     });
            // }
            setAuthorized(hasPermission);
            setHasAccess(hasPermission)
        }else{
            setAuthorized(true)
        }
    }
    if(!hasAccess) return <InAccessible/>
    return (authorized && children);
}
export default RouteGuard