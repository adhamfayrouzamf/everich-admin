import React, { useState, useEffect, useContext } from 'react'
import { Table, Button, Input, Form, Spinner } from 'reactstrap';
import TableRow from "./TableRow"
import Link from "next/link"
import ConfirmModal from "../../components/ConfirmModal"
import axios from "axios"
import Loader from "../Loader"
import { UserContext } from '../contexts/AuthContext';
import {useForm} from 'react-hook-form'
import { CheckField } from '../CustomForm/FormFields';
import {useFetch} from "../hooks/fetch-hook"
import { useRouter } from "next/router"
import { FaPlus } from "react-icons/fa"
const CustomTable = props=> {
    const route = useRouter()
    const {options} = props
    const {title,addBtn,fetch_url, roleNumber} = options
    const user = useContext(UserContext)
    const [deleteWish,setDeleteWish] = useState(false)
    const [chosen,setChosen] = useState(null)
    const [controls,setControls] = useState([])
    const [addBtnDisabled,setAddBtnDisabled] = useState(false)
    const [tableData,hasNext,getNext,isLoadingMore,error,isLoading] = useFetch(fetch_url)
    const formMethods = useForm({
        form:"table-form"
    })
    

    const deleteToggle = (id)=>{
        if(id)
            setChosen(id)
        setDeleteWish(prev=>!prev)
    }
    const deleteRequest=()=>{
        console.log(options.api_url+"/"+chosen)
        setTableData(prev=>prev.filter(item=>item.id !== +chosen))
        axios.delete(`${options.api_url}/${chosen}`,{headers:{Authorization:`Bearer ${user.token}`}}).catch(err=>{
            console.log(err.message)
        })
        deleteToggle()
    }
    const selectAll = (e,name)=>{
        console.log(e.target.checked,name)
        if(e.target.checked)
            formMethods.setValue(name,
                tableData.map(t=>t[name].toString())
            )
        else
            formMethods.setValue(name,
                ""
            )
    }
    const renderData = tableData.map((row,index)=>{
        return (
            <TableRow key={row.id} row={row} id={index + 1} options={options} controls={controls} deleteToggle={deleteToggle} formMethods={formMethods}  />
        )
    })
    
    const loadMore = ()=>{
        if(hasNext)
            getNext()
    }
    // useEffect(()=>{
    //     console.log(isLoading)
    //     axios.get(`${fetch_url}`).then(res=>{
    //         const {data, page, pageCount, limit, totalCount, links} = res.data
    //         console.log(data)
    //         setIsLoading(false)
    //         setTableData(data)
    //         setNext(links.next || null)
    //     }).catch(err=>console.error(err))
    // },[])
    useEffect(()=>{
        if(roleNumber && user.type!=="ADMIN"){
            const rule = user.rules.find(r=>r.rule.number === options.roleNumber)
            if(rule){
                setControls(options.controls.map(c=>{
                    return{
                        type:c,disabled:!rule.properties.includes(c)
                    }
                }))
            }else{
                setControls(options.controls.map(c=>{
                    return{
                        type:c,disabled:true
                    }
                }))
            }
        }else
            setControls(options.controls.map(c=>{
                return{
                    type:c,disabled:false
                }
            }))
    },[user,options.controls,options.roleNumber])
    useEffect(()=>{
        if(roleNumber && user.type!=="ADMIN"){
            const rule = user.rules.find(r=>r.rule.number === options.roleNumber)
            if(rule){
                console.log(rule.properties)
                setAddBtnDisabled(!rule.properties.includes("ADD"))
            }else{
                setAddBtnDisabled(true)
            }
        }else
            setAddBtnDisabled(false)
    },[addBtn,options.roleNumber])
    return (
        <>
        <header className="d-flex justify-content-between align-items-center mb-5">
            {title ? <h2 {...title.options}>{title.value[route.locale]}</h2> : null}
            {/* <div style={{direction:"ltr"}}>
                    <div>
                        <Button onClick={prevPage} disabled={page <= 1}>{"<"}</Button>
                        
                        <span>Page : {page} / {pageCount}</span>

                        <Button onClick={nextPage} disabled={page >= pageCount}>{">"}</Button>
                    </div>
                    {/* <span>Items: {tableData.length} / {totalCount}</span> */}
            {/* </div> */}
            <div>
                {addBtn ?
                    <Link href={`${options.url}/add`} passHref>
                        <Button tag="a" disabled={addBtnDisabled} className={addBtnDisabled && "disabled"}>
                            <FaPlus />
                        </Button>
                    </Link>
                    : null
                }
            </div>
        </header>
        <Loader Show={!isLoading} Render={true}>
            <Form>
            <Table responsive>
                <thead>
                    <tr >
                        {
                            options.cols.map((col, index) => {
                                return (
                                    <th key={index} >
                                        {col.check ?
                                            <CheckField type="checkbox" name={col.key+"s"} choices={[{label:"All",id:"all"+col.label}]} methods={formMethods} onChange={(e)=>selectAll(e,col.key)} />
                                            : col.label[route.locale]
                                        }
                                    </th>
                                )
                            })
                        }
                        {options.controls && options.controls.length ?
                            <th>{route.locale== "ar" ? "التحكم" : "Controls"}</th>
                        :null
                        }
                    </tr>
                </thead>
                <tbody>
                    {renderData}
                </tbody>
            </Table>
            </Form>
            <footer className="text-center">
                {hasNext && 
                <>
                {!isLoadingMore ? 
                <Button onClick={loadMore}>Load More</Button> 
                    :
                    <div class="spinner-border text-dark" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                }
                </>
                }
            </footer>
        </Loader>
        <ConfirmModal isOpen={deleteWish} confirm={deleteRequest} toggle={deleteToggle} title="حذف منتج" message="are u sure u want to delete this item ?" />
        </>
    )

}


export default CustomTable