import { useRouter } from 'next/router';
import React, { useState, useEffect, useContext } from 'react'
import { Button, Tooltip } from 'reactstrap';

import {CheckField} from "../CustomForm/FormFields"
import {UserContext} from "../contexts/AuthContext"
import ConfirmModal from "../ConfirmModal"
import axios from "axios"
const Toggler = ({choices,value,row}) => {

        const route = useRouter()
        const user = useContext(UserContext)
        const [chosen, setChosen] = useState()
        const [currentState, setCurrentState] = useState(0)
        const [val, setVal] = useState(value)
        const [modal,setModal] = useState({})
        const [modalIsOpen, setModalIsOpen] = useState(false)
        const [tooltipOpen, setTooltipOpen] = useState(false)

        const handleClick= () =>{
            setModalIsOpen(!modalIsOpen)
            console.log(val, row.id)
            chosen.onClick(choices[chosen.toState].value,row.id).then(res=>{
                console.log("updated",res.data)
                if(chosen.toState !== null){
                    setChosen(choices[chosen.toState])
                    setCurrentState(chosen.toState)
                    setVal(choices[chosen.toState].value)
                    setModal(choices[chosen.toState].modal)
                }
            }).catch(err=>console.error(err))
        }

        const handleReject = ()=>{
            setModalIsOpen(!modalIsOpen)
            chosen.onReject(choices[chosen.toState].value,row.id).then(res=>{
                console.log("updated",res.data)
                setChosen(null)
                setCurrentState(0)
                setVal(value)
                setModal({})
            }).catch(err=>console.error(err))
        }

        useEffect(()=>{
            for(let i =0; i< choices.length; i++){
                // console.log(value)
                if(choices[i].value === value ){
                    // console.log(choices[i])
                    setChosen(choices[i])
                    setCurrentState(i)
                    setVal(choices[i].value)
                    setModal(choices[i].modal)
                    break;
                }
            }
        },[choices,value])

        return (
            <>
            {chosen && chosen.toState !== null ? 
                <>
                <Button onClick={()=>setModalIsOpen(!modalIsOpen)} id={`s${currentState}_${chosen.value}_${row.id}`}>{chosen.label[route.locale]}</Button> 
                {chosen.tooltip && <Tooltip placement="bottom" isOpen={tooltipOpen} toggle={()=>setTooltipOpen(!tooltipOpen)} target={`s${currentState}_${chosen.value}_${row.id}`}>
                    Current State : {chosen.tooltip}
                </Tooltip>}
                </>
            : null}
            {chosen && chosen.toState === null && <span>{chosen.value}</span>}
            <ConfirmModal message ={modal && modal.message }
                title={modal && modal.title} 
                confirm={handleClick}
                isOpen={modalIsOpen}
                toggle={()=>setModalIsOpen(!modalIsOpen)}
                confirmBtn={modal && modal.confirmBtn}
                rejectBtn={modal && modal.rejectBtn}
                reject={chosen && chosen.onReject && handleReject || null}
            />
            </>
        )

}


export default Toggler