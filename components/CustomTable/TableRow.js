import { useRouter } from 'next/router';
import React, { useState, useEffect } from 'react'
import { Table, Button } from 'reactstrap';
import Image from "next/image"
import { FaEdit, FaEye, FaTrash } from 'react-icons/fa';
import Link from "next/link"
import Moment from "react-moment"
import "moment/locale/ar"
import {CheckField} from "../CustomForm/FormFields"
import Toggler from './Toggler';
const TableRow = props=> {

        const {row, options, formMethods, id, controls} = props
        const {cols,url} = options
        const route = useRouter()
        
        const configCol = (col,index)=>{
            let data = row[col.key]
            if(col.childs && col.childs.length){
                if(data){
                    for(let i=0;i<col.childs.length;i++){
                        if(data[col.childs[i]])
                        data = data[col.childs[i]]
                        else
                            break;
                    }
                }
            }
            
            if(data && col.localized)
                data = data[route.locale]
            else if(!data && data !== false)
                data = col.notFound || ""
            
            
            if(col.isImage){
                if(!data.url || !data.id) data=col.notFound || ""
                else
                    return <Image src={data.url} width={100} height={100} alt={data.id} />
            }
            if(col.isDate){
                return <Moment element="span" format="YYYY/MM/DD" locale={route.locale}>{data}</Moment>
            }
            if(col.isToggler){
                // console.log(data)
                return <Toggler choices={col.choices} value={data} row={row} />
            }
            if(col.check){
                const label = col.key === "id" ?  index : data
                const field = {
                    type:"checkbox", name:col.key,choices:[{label:label,value:data,id:"id-"+data}]
                }
                return <CheckField {...field} methods={formMethods} />
            }
            if(col.isLink && data){
                return (
                    <Link href={`${url}/${data}${col.url? "/" + col.url : ""}`} passHref>
                        <Button tag="a">{col.icon}</Button>
                    </Link>
                )
            }
            if(col.key === "id"){
                return <span>{index}</span>
            }
            return <span>{data}</span>
        }
        const configControl = (c)=>{
            if(c.type === "DETAILS"){
                return (
                    <Link href={`${url}/${row.id}`} passHref>
                        <Button className={`mx-1 ${c.disabled ? "disabled" :""}`} disabled={c.disabled} tag="a"><FaEye /></Button>
                    </Link>
                )
            }else if(c.type === "UPDATE"){
                return (
                    <Link href={`${url}/${row.id}/edit`} passHref>
                        <Button className={`mx-1 ${c.disabled ? "disabled" :""}`} disabled={c.disabled} tag="a"><FaEdit /></Button>
                    </Link>
                )
            }else if(c.type === "DELETE"){
                return (
                    <Button className="mx-1" disabled={c.disabled} onClick={()=>props.deleteToggle(row.id)}><FaTrash /></Button>
                )
            }
        }
        return (
            
            <tr >
                {
                    cols.map((col, index) => {
                        return (
                            <td key={index} htmlFor={col.key} >
                                {configCol(col,id)}
                            </td>
                        )
                    })
                }
                {controls && controls.length ? 
                    <td>
                        {
                            controls.map((c,index)=>{
                                return (
                                    <span key={index}>
                                        {configControl(c)}
                                    </span>
                                )
                            })
                        }
                    </td> 
                :null
                }
                {/* {(config.view||config.edit||config.delete)&&<th>&nbsp;</th>}  */}
            </tr>
        )

}


export default TableRow