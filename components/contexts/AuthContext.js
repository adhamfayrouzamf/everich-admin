import {createContext, useState, useEffect} from 'react'
import cookie from "react-cookies"
import axios from "axios"
import { toast } from "react-toastify"
import { useRouter } from "next/router"
import {getMyToken, onMessageListener} from "../../firebaseClient"

export const UserContext = createContext({});
export const LoginContext = createContext(null)
export const TokenContext = createContext(null)
// export const RememberContext = createContext(null)
const UserContextProvider = ({children})=>{
    const route = useRouter()
    const [user, setUser] = useState(()=>{
        const session = typeof window !== 'undefined' ? JSON.parse(sessionStorage.getItem("adminUser")) : null
        // const cookies = cookie.load("adminUser")
        console.log("cookie")
        return session ? {...session, isLoggedIn:true} : {isLoggedIn:false}
    })
    
    const [deviceToken, setDeviceToken] = useState(null)

    

    const logout = () => {
        console.log('logout')
        // cookie.remove('adminUser');
        sessionStorage.removeItem("adminUser")
        if(deviceToken)
            axios.post("/api/user/logout",{token:deviceToken},{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>{
                console.log("logged out")
            }).catch(err=>console.error(err))
            
        setUser({isLoggedIn:false});
        // setIsLoggedIn(false)
        // route.push({
        //     pathname:"/auth/login",
        //     query:{redirectUrl:route.asPath}
        // })
    }
    const login = (values) => {
        axios.post('/api/admin/signin', values).then((res) => {
            const userdata = res.data
            // getMyToken().then(token=>{
            //     console.log(token)
            //     if(token){
            //         console.log("token",token)
            //         setDeviceToken(token)
            //         axios.post("/api/user/addToken",{token:token,type:"web"},{headers:{Authorization:`Bearer ${userdata.token}`}})
            //         .then(res=>{
            //             console.log("token added")
            //         }).catch(err=>console.error(err))
            //     }
            //     updateUser(userdata)
            // }).catch(err=>{
            //     console.error(err)
            //     updateUser(userdata)
            // })
            // route.push(route.query.redirectUrl || "/dashboard")
            updateUser(userdata)
        }).catch((err) => {
            console.log({...err})
            toast.error("login failed")
        })
    }
    const updateUser = (values)=>{
        console.log(values)
        sessionStorage.setItem("adminUser",JSON.stringify(values))
        // cookie.save("adminUser",{...values},{path:'/',maxAge:60*60*24*365})
        // setIsLoggedIn(true)
        setUser({...values,isLoggedIn:true})
    }
    // useEffect(()=>{
        
    // },[deviceToken])

    return(
        <UserContext.Provider value={user.isLoggedIn? {...user,logout} : {...user} } >
        <LoginContext.Provider value ={login}>
        <TokenContext.Provider value ={deviceToken}>
            {children}
        </TokenContext.Provider>
        </LoginContext.Provider>
        </UserContext.Provider>
    )
}

export default UserContextProvider