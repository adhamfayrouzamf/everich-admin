import {createContext, useContext, useEffect} from 'react'
import axios from "axios"
import { toast } from "react-toastify"
import { useRouter } from "next/router"
import { UserContext } from "./AuthContext"
// import {getMyToken, onMessageListener} from "../../firebaseClient"

export const NotificationContext = createContext(null);
export const CountContext = createContext(null)
export const UnseenContext = createContext(null)
const NotifContextProvider = ({children})=>{
    const route = useRouter()
    const user = useContext(UserContext)

    // useEffect(()=>{
    //     if(user){
    //         getMyToken().then(token=>{
    //             console.log(token)
    //             if(token)
    //                 axios.post("/api/user/addToken",{token,type:"web"},{headers:{Authorization:`Bearer ${user.token}`}})
    //                 .then(res=>{
    //                     console.log("token added")
    //                 }).catch(err=>console.error(err))
    //         }).catch(err=>{
    //             console.error(err)
    //         })
    //         onMessageListener().then(payload=>{
    //             console.log(payload)
    //         })
    //     }else
    //         console.log("disconnected from notif socket")
    // },[])
    return(
        <NotificationContext.Provider value={user ? "connected" : "disconnected"} >
        <CountContext.Provider value={user ? 0 : 1} >
            {children}
        </CountContext.Provider>
        </NotificationContext.Provider>
    )
}

export default NotifContextProvider