import {createContext, useContext, useState, useEffect} from 'react'
import axios from "axios"
import { toast } from "react-toastify"
import { useRouter } from "next/router"
import { UserContext } from "./AuthContext"
// import {getMyToken, onMessageListener} from "../../firebaseClient"

export const PageContext = createContext(null)

const PageContextProvider = ({children})=>{
    const route = useRouter()
    const user = useContext(UserContext)
    const [page,setPage] = useState([])
    
    return(
        <PageContext.Provider value={{page,setPage}} >
            {children}
        </PageContext.Provider>
    )
}

export default PageContextProvider