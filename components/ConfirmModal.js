/* style / images */

/* files / packages */
import React from 'react'

/* components */
import {Button,Modal, ModalHeader, ModalFooter, ModalBody, Row, Col, Form, FormGroup, Label, Input, FormText} from 'reactstrap'


const ConfirmModal = (props)=>{


    const {message,title,confirm,isOpen,toggle,modalClass,confirmBtn, rejectBtn, reject} = props
    return (
        <Modal isOpen={isOpen} toggle={props.toggle} className={modalClass} style={{margin: '1.75rem auto'}} centered>
            <ModalHeader className="d-block">
                <div className="d-flex justify-content-between align-items-center">
                    <span>{title}</span>
                    <Button onClick={props.toggle} style={{borderRadius:"50%"}}>X</Button>
                </div>
            </ModalHeader>
            <ModalBody>
                {message}
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={confirm}>{confirmBtn || "تأكيد"}</Button>{' '}
                {reject ? <Button onClick={reject} color="danger">{rejectBtn || "Reject"}</Button> : null}
                <Button color="secondary" onClick={toggle}>الغاء</Button>
            </ModalFooter>
        </Modal>
    );
}


export default ConfirmModal;
