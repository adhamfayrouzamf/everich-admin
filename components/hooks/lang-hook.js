import {useState, useEffect} from "react"
import strings from "../../locales/strings"
import {useRouter} from "next/router"

export function useLang(){
    const route = useRouter()
    if(strings.getLanguage !== route.locale)
        strings.setLanguage(route.locale)

    const t = (key)=>{
        const keys = key.split(".")
        let word = strings[keys[0]] || key
        if(word !== key)
            for(let i=1; i<keys.length && word !== key;i++){
                word = word[keys[i]] || key
            }
        
        return word
    }
    const setLang = (lang)=>{
        route.replace(route.asPath,route.asPath,{locale:lang})
    }


    return {t,setLang}
}
