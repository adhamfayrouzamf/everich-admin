import {useState, useEffect} from "react"
import axios from "axios"
import useSWR, { useSWRConfig} from "swr"
export function useFetch(url,config={}){
    const [isLoading,setIsLoading] = useState(true)
    const [isLoadingMore,setIsLoadingMore] = useState(false)
    const [fetchData,setData] = useState([])
    const [links,setLinks] = useState({})
    const [hasNext,setHasNext] = useState(false)
    const [error, setError] = useState(null)
    const getNext = ()=>{
        setIsLoadingMore(true)
        if(links.next){
            axios.get(links.next,config).then(res=>{
                setIsLoadingMore(false)
                const {data,page,pageCount,totalCount,links} = res.data
                setData([...fetchData,...data])
                setLinks(links)
                setHasNext(links.next ? true : false)
            }).catch(err=>{
                setError(err)
                setIsLoadingMore(false)
            })
        }
    }
    useEffect(()=>{
        if(url){
            setIsLoading(true)
            axios.get(url,config).then(res=>{
                const {data,page,pageCount,totalCount,links} = res.data
                setIsLoading(false)
                setData(data)
                setLinks(links)
                setHasNext(links.next ? true : false)
            }).catch(err=>{
                setIsLoading(false)
                setError(err)
            })
        }
    },[url])

    return [fetchData,hasNext,getNext,isLoadingMore,error,isLoading]
}

export function useFetchOne(url,config={}){
    const [isLoading,setIsLoading] = useState(true)
    const [fetchData,setData] = useState({})
    const [error, setError] = useState(null)
    
    useEffect(()=>{
        if(url){
            setIsLoading(true)
            console.log("isLoading")
            axios.get(url,config).then(res=>{
                const {data} = res
                console.log("data===>",data)
                setIsLoading(false)
                setData(data)
            }).catch(err=>{
                console.log("error===>",{...err})
                setIsLoading(false)
                setError(err)
            })
        }else{
            setError({message:"url is required"})
        }
    },[url])

    return [fetchData,error,isLoading]
}

export function useCache (url, config={},options={}){
    const {cache} = useSWRConfig()
    const {data, error} = useSWR(url,url=>fetch(url).then(res=>res.json()),options)
    const [isLoading,setIsLoading] = useState(true)
    const [isLoadingMore,setIsLoadingMore] = useState(false)
    const [fetchData,setData] = useState([])
    const [links,setLinks] = useState({})
    const [hasNext,setHasNext] = useState(false)
    const [fetchError, setError] = useState(null)
    const [isCached, setIsCached] = useState(false)
    // const checkCache = ()=>{
    //     const res = cache.get(url)
    //     if(res){
            
    //         console.log("cached",res)
    //         const {data,links} = res
    //         setData(data)
    //         setLinks(links)
    //         setHasNext(links.next ? true : false)
    //         setIsCached(true)
    //     }
    //     else
    //         setIsCached(false)
    // }

    const getNext = ()=>{
        setIsLoadingMore(true)
        if(links.next){
            axios.get(links.next,config).then(res=>{
                setIsLoadingMore(false)
                const {data,page,pageCount,totalCount,links} = res.data
                setData([...fetchData,...data])
                setLinks(links)
                setHasNext(links.next ? true : false)
            }).catch(err=>{
                setError(err)
                setIsLoadingMore(false)
            })
        }
    }
    // useEffect(()=>{
    //     checkCache()
    // },[url])
    useEffect(()=>{
        if(data){
            setData(data.data)
            setLinks(data.links)
            setHasNext(data.links.next ? true : false)
            // if(fetchData !== data.data){
            //     console.log("replace or no cache")
            // }
        }else if(error){
            setError(error)
        }
        setIsLoading(!data && !error)
    },[data,error])



    return [fetchData,hasNext,getNext,isLoadingMore,fetchError,isLoading]
}