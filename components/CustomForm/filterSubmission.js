function filterArray(arr){
    for(let i =0; i<arr.length; i++){
        let value = arr[i]
        if(value instanceof Object && !Array.isArray(value)){
            filterSubmitData(value)
            if(!Object.keys(value).length){
                arr.splice(i,1)
            }else{
                arr[i] = value
            }
        }
    }
}

export default function filterSubmitData (values){
    for(let val in values){
        let value = values[val]
        if(value === null || value === "" || value === undefined){
            delete values[val]
        }
        if(Array.isArray(value)){
            filterArray(value)
            if(!value.length){
                delete values[val]
            }
        }
        if(value instanceof Object && !Array.isArray(value)){
            filterSubmitData(value)
            if(!Object.keys(value).length){
                delete values[val]
            }else{
                values[val] = value
            }
        }
    }
}

const values = {
    test1:"",
    test2:null,
    test3:"test",
    test4:[{test:[1,2]},{test2:{}}],
    test5:{test6:"da", test7:0,test8:{test9:{test10:""}}}
}

// filterSubmitData(values)
// console.log(values)