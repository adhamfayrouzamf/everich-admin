import styles from "../../styles/CustomForm/Fields.module.scss"
import {useState, useEffect, useRef} from 'react'
import Loader from '../Loader'
import { FaPlus } from 'react-icons/fa'
import { Button, FormText, FormGroup, Input, Label, Col } from 'reactstrap'
import AsyncSelect from "react-select/async"
import makeAnimated from "react-select/animated"
import {useRouter} from "next/router"
import Image from "next/image"
// import {Controller} from "react-hook-form"
import axios from 'axios'

// /image\/(png|jpg|jpeg|gif|svg\+xml|webp)
const imgTypes = ["image/png","image/jpg","image/jpeg","image/svg+xml","image/webp"]
export const ImageField = ({methods,name,locale, id,label,fieldset,values,formGroup, dir, ...more})=>{
    const {register,setValue, formState:{errors}} = methods
    const {ref,...field} = register(`${name}File`)
    const urlReg = register(`${name}.url`)
    const idReg = register(`${name}.id`)
    const [image, setImage] = useState(null)
    const [showImage,setShowImage] = useState(false)
    const [displayChangeBtn,setDisplayChangeBtn] = useState('none')
    const error = errors[name+"File"]
    const handleImage = (e)=>{
        console.log(e.target.files)
        if(image)
            removeImage()
        else{
            setShowImage(false)
        }
        if(e.target.files.length>0){
            console.log(e.target.files[0].type)
            if(imgTypes.indexOf(e.target.files[0].type)>=0 && e.target.files[0].size <= 10000000){
                console.log('uploading')
                let formData = new FormData()
                formData.append("files",e.target.files[0])
                formData.append("dir",dir)
                
                setShowImage(true)
                axios.post('/api/file/upload',formData).then(res=>{
                    console.log("result",res.data)
                    setImage(res.data[0])
                    setDisplayChangeBtn('inline-block')
                    setValue(urlReg.name,res.data[0].url)
                    setValue(idReg.name,res.data[0].id)
                }).catch(err=>{
                    console.log({...err})
                })
            }
        }
        
    }
    const removeImage=(e=null)=>{
        const Img = values?.image
        console.log(Img)
        setShowImage(Img ? true : false)
        setDisplayChangeBtn(Img ? 'inline-block' : 'none')
        if(e && e.target.dataset.remove){
            console.log('remove')
            document.querySelector('#'+id+"-file").value=""
        }
        if(image && image!==Img){
            const {id} = image
            axios.delete(`/api/file/delete`,{data:{ids:[id]}}).catch(err=>{
                console.log({...err})
            })
        }
        setImage(Img || null)
        console.log('test remove')
        setValue(urlReg.name,Img && Img.url || "")
        setValue(idReg.name,Img && Img.id || "")
    }

    useEffect(()=>{
        if(values?.image && image!==values.image){
            removeImage()
            // setImage(values[name])
            // setShowImage(true)  
            // setDisplayChangeBtn('inline-block')
        }
    },[values])

    return (
        <>
        <FormGroup className={`d-flex justify-content-center ${styles.formGroup}`}>
            <Label className={styles.imageField} id={`${id}-label`} for={!showImage? id+"-file" : ''}>
                {!showImage && 
                    <div>
                        <span>{label}</span> 
                        <FaPlus />
                        {error && !image && <FormText tag="div" style={{width:"100%"}} color={"danger"}>{error ? error.message : null}</FormText>}
                    </div>
                }
                <Loader Show={image} Render={showImage} width={100} height={100} >
                    <Image  src={image && image.url} alt="image" width={400} height={160} />
                </Loader>
                <Button className={styles.changeBtn} tag={Label} for={id+"-file"} style={{display:displayChangeBtn}}>
                Change Image
                </Button>
                {image  &&
                <>
                    <Button className={styles.removeBtn} data-remove={true} onClick={removeImage} color="danger">Remove</Button>
                </>
                }
            </Label>
            <Input {...field} innerRef={ref} type="file" id={id+"-file"} style={{width:0,height:0}} onChange={handleImage} />
            <Input {...urlReg} innerRef={urlReg.ref} type={'hidden'} id={id+"-url"} /* value={image && image.url || ""}  *//>
            <Input {...idReg} innerRef={idReg.ref} type={'hidden'} id={id+"-id"}/*  value={image && image.id || ""}  *//>
        </FormGroup>
        </>
    )
}
export const ImagesField = ({name, label, id , methods,locale, fieldset,formGroup, dir, values,...more})=>{
    const {register, unregister, setValue, formState:{errors}} = methods
    const {ref,...field} = register(`${name}File`)
    const error = errors[name+"File"]
    const [images, setImages] = useState([])
    const [showImage,setShowImage] = useState(false)
    const [displayChangeBtn,setDisplayChangeBtn] = useState('none')

    const handleImage = (e)=>{
        console.log(e.target.files)
        if(images)
            removeImage()
        else{
            setShowImage(false)
        }
        console.log("iteration")
        if(e.target.files.length>0){
            if(imgTypes.indexOf(e.target.files[0].type)>=0 && e.target.files[0].size <= 10000000){
                console.log('uploading')
                let formData = new FormData()
                for(let i=0;i<e.target.files.length;i++){
                    formData.append("files",e.target.files[i])
                }
                console.log(dir)
                formData.append("dir",dir)
                setShowImage(true)
                axios.post('/api/file/upload',formData).then(res=>{
                    console.log(res.data)
                    setImages(res.data)
                    let imgs = res.data
                    setDisplayChangeBtn('inline-block')
                    for (let i=0;i<imgs.length;i++){
                        setValue(`${name}.${i}.url`,imgs[i].url)
                        setValue(`${name}.${i}.id`,imgs[i].id)
                    }
                }).catch(err=>{
                    console.log({...err})
                })
            }
        }
        
    }
    const removeImage=(e=null)=>{
        const Imgs = values?.images || null
        setShowImage(Imgs ? true : false)
        setDisplayChangeBtn(Imgs ? 'inline-block' : 'none')
        if(e && e.target.dataset.remove){
            console.log('remove')
            document.querySelector('#'+id+"-file").value=""
        }
        if(images.length && images!==Imgs){
            let ids = images.map(i=>i.id)
            axios.delete(`/api/file/delete`,{data:{ids}}).catch(err=>{
                console.log({...err})
            })
        }
        
        setImages(Imgs || [])
        for(let i=0;i<images.length;i++){
            unregister(`${name}.${i}`)
        }
    }

    useEffect(()=>{
        if(values?.images && images!==values.images){
            removeImage()
            // setImage(current)
            // setShowImage(true)  
            // setDisplayChangeBtn('inline-block')
        }
    },[values])

    return (
        <>
        <FormGroup className={`d-flex justify-content-center ${styles.formGroup}`}>
            <Label className={styles.imageField} id={`${id}-label`} for={!showImage? id+"-file" : ''}>
                {!showImage && 
                    <div>
                        <span>{label}</span> 
                        <FaPlus />
                        {error && !images.length && <FormText tag="div" style={{width:"100%"}} color={"danger"}>{error ? error.message : null}</FormText>}
                    </div>
                }
                <Loader Show={images && images.length>0} Render={showImage} width={100} height={100}>
                    {images.map((image,index)=>{
                        return(
                            <Image key={index} src={images && images[index] ? images[index].url : ""} alt="image" width={400} height={160} />
                        )
                    })}
                </Loader>
                <Button className={styles.changeBtn} tag={Label} for={id+"-file"} style={{display:displayChangeBtn}}>
                Change Image
                </Button>
                {images.length>0 && images !== values?.images &&
                <>
                    <Button className={styles.removeBtn} data-remove={true} onClick={removeImage} color="danger">Remove</Button>
                </>
                }
            </Label>
            <Input {...field} innerRef={ref} type="file" id={id+"-file"} style={{width:0,height:0}} multiple onChange={handleImage} />
            {[...Array(images.length)].map((val,index)=>{
                const urlReg = register(`${name}.${index}.url`)
                const idReg = register(`${name}.${index}.id`)
                return(
                    <div style={{width:'0',height:'0'}} key={index}>
                        <Input {...urlReg} innerRef={urlReg.ref} type={'hidden'} id={id+"-url"} /* value={image && image.url || ""}  *//>
                        <Input {...idReg} innerRef={idReg.ref} type={'hidden'} id={id+"-id"}/*  value={image && image.id || ""}  *//>
                    </div>
                )
            })}
        </FormGroup>
        </>
    )
}

export const RenderField = ({methods,name,locale,label,fieldset,formGroup, ...more})=>{
    const {register, formState:{errors}} = methods
    const {ref,...field} = register(locale ? `${name}.${locale}` : name)
    const formGroupClass = formGroup ? formGroup.className : ""
    const error = errors[name]
    return (
        <>
        <FormGroup tag={fieldset ? "fieldset" : "div"} {...formGroup} className={`${styles.formGroup} ${formGroupClass}`}>
                {fieldset ? <legend {...fieldset.options}>{fieldset.value}</legend> : null}
                    {label ? <Label>{label}</Label> : null}
                    <Input {...field} innerRef={ref} {...more} />
                    {/* <InputGroupAddon addonType="prepend">
                        <InputGroupText className='h-100'><Icon /></InputGroupText>
                    </InputGroupAddon>
                </InputGroup> */}
                <FormText color="danger" >{error ? error[locale] ? error[locale].message : error.message || null : null}</FormText>
        </FormGroup>
        </>
    )
}
export const SelectField = ({methods,name,locale,label,fieldset,fetch_url,values,option,formGroup, onChange, ...more})=>{
    const {register,formState:{errors},setValue} = methods
    const {ref,...field} = register(locale ? `${name}.${locale}` : name)
    const [chosen, setChosen] = useState()
    const select = useRef()
    const error = errors[name]
    const route = useRouter()
    // const choices = options && options.length ? options.map(o=>{
    //     console.log(o)
    //     const value = o[option.value]
    //     const labels = option.label.map((lab,i)=>{
    //         let val = o[lab.key]
    //         if(lab.childs && lab.childs.length){
    //             if(val){
    //                 for(let i=0;i<lab.childs.length;i++){
    //                     if(val[lab.childs[i]])
    //                     val = val[lab.childs[i]]
    //                     else
    //                         break;
    //                 }
    //             }
    //         }
    //         if(lab.localized)
    //             val = val[route.locale]
    //         return val
    //     })
    //     const label = labels.join(" - ")
    //     return {value,label}
    // }) : null
    const handleChange= (v)=>{
        console.log(v)
        setValue(name,v ? v.length ? v.map(c=>c.value) : v.value : "")
        setChosen(v)
        if(onChange){
            onChange(v)
        }
    }
    const getLabel = (val)=>{
        const labels = option.label.map((lab,i)=>{
            let label = val[lab.key]
            if(lab.childs && lab.childs.length){
                if(label){
                    for(let i=0;i<lab.childs.length;i++){
                        if(label[lab.childs[i]])
                        label = label[lab.childs[i]]
                        else
                            break;
                    }
                }
            }
            if(lab.localized)
                label = label[route.locale]
            return label
        })
        const label = labels.join(" - ")
        return label
    }
    useEffect(()=>{
        if(values && values[name]){
            let data
            if(values[name].length){
                data = values[name].map((val)=>{
                    const value = val[option.value]
                    const label = getLabel(val)
                    return {value,label}
                })
            }else{
                const label = getLabel(values[name])
                data = {value:values[option.value],label}
            }
            setChosen(data)
        }
    },[values])
    const getOptions = (v)=>{
        console.log(select)
        return axios.get(`${fetch_url}${v}`).then(res=>{
            return res.data.data.map(p=>{
                console.log(option.value,p)
                const value = p[option.value]
                console.log(value)
                const label = getLabel(p)
                return {label:label,value:value}
            })
        })
    }

    return (
        <>
        <FormGroup tag={fieldset ? "fieldset" : "div"} {...formGroup} className={`${styles.formGroup}`}>
                {fieldset ? <legend {...fieldset.options}>{fieldset.value}</legend> : null}
                    {label ? <Label>{label}</Label> : null}
                    <AsyncSelect {...field} ref={ref} value={chosen} components={makeAnimated()} cacheOptions defaultOptions={/* choices ||  */true} loadOptions={fetch_url ? getOptions : null} isClearable
                                onChange={handleChange} {...more} />
                <FormText color="danger" >{error ? error[locale] ? error[locale].message : error.message || null : null}</FormText>
        </FormGroup>
        </>
    )
}

export const RadioField = ({methods,name,locale,fieldset, choices, formGroup, ...more})=>{
    const {register, formState:{errors}} = methods
    const {ref,...field} = register(locale ? `${name}.${locale}` : name)
    const error = errors[name]
    return (
        <>
        <FormGroup tag={fieldset ? "fieldset" : "div"} {...formGroup} className={`${styles.formGroup}`}>
                {fieldset ? <legend {...fieldset.options}>{fieldset.value}</legend> : null}
                {choices.map((c,index)=>{
                    return (
                        <Label key={index} for={c.id} className={"mx-2"}>
                            <Input {...field} innerRef={ref} id={c.id} value={c.value} {...more} />
                            {c.label}
                        </Label>
                    )
                })}
                <FormText color="danger" >{error ? error[locale] ? error[locale].message : error.message || null : null}</FormText>
        </FormGroup>
        </>
    )
}
export const CheckField = ({methods,name,locale,fieldset, choices, formGroup, ...more})=>{
    const {register, formState:{errors}} = methods
    const {ref,...field} = register(locale ? `${name}.${locale}` : name)
    const error = errors[name]
    return (
        <>
        <FormGroup tag={fieldset ? "fieldset" : "div"} {...formGroup} className={`${styles.formGroup}`}>
                {fieldset ? <legend {...fieldset.options}>{fieldset.value}</legend> : null}
                {choices.map((c,index)=>{
                    const {id,label,...cmore} = c
                    return (
                        <Label key={index} for={id} className={"mx-2"}>
                            <Input style={{marginLeft:"5px", marginRight:"5px"}} {...field} innerRef={ref} id={id} {...cmore} {...more} />
                            {label}
                        </Label>
                    )
                })}
                <FormText color="danger" >{error ? error[locale] ? error[locale].message : error.message || null : null}</FormText>
        </FormGroup>
        </>
    )
}

export const ConditionalField = ({methods,condition,values,inputs,onValue})=>{
    const [value,setValue] = useState("")
    const handleChange =(e)=>{
        setValue(e.target.value)
        if(e.target.value !== onValue && value === onValue){
            inputs.forEach(i=>{
                methods.unregister(i.name)
            })
        }
    }
    useEffect(()=>{
        if(values){
            const val = values[condition.name]
            setValue(val)
        }
    },[values])
    return (
        <>
        {configField(condition,methods,{onChange:handleChange})}
        {value === onValue ?
            inputs.map((i,index)=>{
                return configField(i,methods,{key:index,values})
            })
            : null
        }
        </>
    )
}

export const HiddenField = ({methods,name,locale})=>{
    const {register, formState:{errors}} = methods
    const {ref,...field} = register(locale ? `${name}.${locale}` : name)
    // const error = errors[name]
    return (
        <Input {...field} innerRef={ref} type="hidden" />
    )
}

export const configField = (field,methods,more)=>{
    const {type, multiple, col} = field
    if(type === "select"){
        return (
            <Col {...col}>
                <SelectField {...field} methods={methods} {...more} />
            </Col>
        )
    }else if(type === "radio"){
        return (
            <Col {...col}>
                <RadioField {...field} methods={methods} {...more} />
            </Col>
        )
    }else if(type === "file" && !multiple){
        return (
            <Col xs="12" className="text-center">
                <ImageField {...field} methods={methods} {...more} />
            </Col>
        )
    }else if(type === "file" && multiple){
        return (
            <Col xs="12">
                <ImagesField {...field} methods={methods} {...more} />
            </Col>
        )
    }
    else if(type === "checkbox"){
        return (
            <Col {...col}>
                <CheckField {...field} methods={methods} {...more} />
            </Col>
        )
    }else if(type==="conditional"){
        return (
            <Col {...col}>
                <ConditionalField {...field} methods={methods} {...more} />
            </Col>
        )
    }else if(type === "hidden"){
        return (
            <HiddenField {...field} methods={methods} {...more} />
        )
    }
    return(
        <Col {...col}>
            <RenderField {...field} methods={methods} {...more} />
        </Col>
    )
}

