/* style / images */

/* files / packages */
import React, {useState, useEffect, useContext} from 'react'
import styles from "../../styles/CustomForm/Fields.module.scss"
/* components */
import {Button, Row, Col, Form } from 'reactstrap'
import {useForm} from 'react-hook-form'
import {yupResolver} from "@hookform/resolvers/yup"
import {configField} from "./FormFields"

const CustomForm = ({config, fields, validation, values, submit})=>{


    const {form, title, submitBtn,...more} = config

    const formMethods = validation ? useForm({
        form:form,
        resolver: yupResolver(validation),
        mode:"all"
    }) : useForm({form:form})

    const {handleSubmit, reset} = formMethods
    const renderFields = fields.length ? fields.map((field,index)=>{
        return (
            configField(field,formMethods,{key:index,values})
            )
    }) : null;
    const titleClass = title && title.options && title.options.className ? title.options.className : ""
    const configFieldData = (field)=>{
        const {name, type, option, condition, inputs} = field
        if(type === "select" && values[name] && option){
            return values[name].length ? values[name].map(val=>val[option.value]) : values[name][option.value]
        }else if(type === "conditional" && condition){
            let data = {}
            data[condition.name] = configFieldData(condition)
            if(inputs && inputs.length){
                inputs.forEach(i=>{
                    const {name,type, condition} = i
                    if(type === "conditional" && condition){
                        data = {...data,...configFieldData(i)}
                    }else{
                        data[name] = configFieldData(i)
                    }
                })
            }
            return data
        }

        return values[name]
    }
    useEffect(()=>{
        if(values){
            let data = {}
            fields.forEach((field)=>{
                const {name, type, condition} = field
                if(type === "conditional" && condition){
                    data = {...data,...configFieldData(field)}
                }else{
                    data[name] = configFieldData(field)
                }
            })
            console.log(data)
            reset(data)
        }
    },[values])
    return (
        <Form onSubmit={handleSubmit(submit)} {...more} >
            <Row>
            {title ? <h3 {...title.options} className={`mb-3 ${titleClass}`}>{title.value}</h3> : null}
            {
                renderFields
            }
            </Row>
            <Button type="submit" {...submitBtn.options} className={`${styles.submitBtn}`}>{submitBtn.value}</Button>
        </Form>
    );
}


export default CustomForm
