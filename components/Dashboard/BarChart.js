/* Style / images */

/* methods / packages */
import React, { useState, useEffect, useContext } from 'react'
import {Bar} from 'react-chartjs-2'
/* components */
import {Container, Row, Col, Card, CardBody, CardTitle} from 'reactstrap'

const BarChart = (props)=>{
    
    let data = {
        labels:['test1','test2','test3'],
        datasets:[{
            label:'first chart',
            data:[15,20,10]
        }]
    }
    let options = {
        maintainAspectRatio:false
    }
    return (
        <div className="bar-chart">
            <Bar data={data} width={props.width} height={props.height} options={options} />
        </div>
    )

}

export default BarChart