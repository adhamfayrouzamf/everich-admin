import styles from "../../../styles/DefaultLayout/Breadcrumb.module.scss"
import {useContext} from 'react';
import { Breadcrumb, BreadcrumbItem, Container } from 'reactstrap';
import { PageContext } from '../../contexts/PageContext';
import Link from "next/link"
import {useRouter} from "next/router"

const dash = {
    key:{en:"Dashboard",ar:"لوحة التحكم"},path:"/dashboard"
}
const Page = (props) => {
    const {page} = useContext(PageContext)
    const route = useRouter()
    return (
        <header className={styles.breadcrumb}>
            <Container fluid>
                <div className="content">
                    {route.pathname === dash.path ?
                        <span className={`${styles.breadItem} ${styles.active}`}>{dash.key[route.locale]}</span>
                    :   
                        <>
                        <Link href={dash.path} >
                            <a tag="a" className={styles.breadItem} >
                                {dash.key[route.locale]}
                            </a>
                        </Link>
                        <span className={styles.breadDivider}>/</span>
                        </>
                    }
                    {page.map((p,index)=>{
                        return (
                            <>
                            {route.pathname === p.path ? 
                                <span className={`${styles.breadItem} ${styles.active}`}>{p.key[route.locale]}</span>
                            :   
                            <>
                                <Link href={p.path} >
                                    <a tag="a" className={styles.breadItem} >
                                        {p.key[route.locale]}
                                    </a>
                                </Link>
                                <span className={styles.breadDivider}>/</span>
                            </>
                            }
                            </>
                        )
                    })}
                </div>
            </Container>
        </header>
    );
};

export default Page;