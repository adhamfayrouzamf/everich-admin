/* methods / packages */
import { useState, useEffect, useContext } from 'react'
import axios from 'axios'
import { UserContext } from '../contexts/AuthContext'
import {io} from 'socket.io-client'

/* Components */
import Header from './Header/Header'
import Footer from './Footer/Footer'
import SideMenu from './SideMenu/SideMenu'
import Page     from './Pagination/Pagination'
const DefaultLayout = (props) => {
    

    return (
        <>
            <Header />
            <Page />
            <div className="mt-5" style={{display:'flex'}}>
                <SideMenu/>
                <main className="view" style={{maxWidth:'96vw',minWidth:'70vw',width:'100%',display:'flex',flexDirection:'column',justifyContent:'space-between'}}>
                    {props.children}
                    <Footer />
                </main>
            </div>
        </>
    )
}


export default DefaultLayout