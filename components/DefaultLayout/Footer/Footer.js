/* style / images */
import styles from '../../../styles/DefaultLayout/Footer.module.scss'
import logo from '../../../public/images/logo.png'

/* methods / packages */
import React, { Component } from 'react'

/* components */
import {Container, Row, Col, List} from 'reactstrap'
import {FaWhatsapp, FaTwitter, FaInstagram, FaGooglePlusG, FaFacebookF, FaMobileAlt, FaEnvelope, FaMapMarkerAlt, FaApple, FaGooglePlay} from 'react-icons/fa'

const Footer =(props)=>{

        return (
            <footer className={styles.mainFooter}>
                <section className={styles.guide}>
                    <Container fluid>
                        <div className="content">
                            <Row>
                                Developed by OTS
                            </Row>
                        </div>
                    </Container>
                </section>
            </footer>
        )
}


export default Footer