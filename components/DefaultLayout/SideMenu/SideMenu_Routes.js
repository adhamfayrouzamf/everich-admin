import {FaUsers, FaCog, FaBars, FaUserCog, FaThLarge, FaTags, FaBoxOpen, FaTicketAlt, FaHome, FaTag, FaBell, FaAd, FaGlobeAfrica, FaMapSigns, FaAddressBook, FaShippingFast, FaAngleDown, FaAngleUp} from 'react-icons/fa'
const SideRoutes = [
    {
        name:{ar:"لوحة التحكم",en:"Dashboard"},
        url:"/dashboard",
        icon:<FaHome />,
        id:"dashboard",
        roleNumber:-1
    },
    {
        name:{ar:"test",en:"test"},
        url:"/test",
        icon:<FaHome />,
        id:"test",
        roleNumber:-1
    },
    {
        name:{ar:"المسئول الفرعي",en:"Sub Admins"},
        url:"/sub-admins",
        icon:<FaUserCog />,
        id:"subAdmins",
        roleNumber:6
    },
    {
        name:{ar:"إعدادات التطبيق",en:"App Settings"}, 
        icon:<FaCog/>,
        id:"AppSetting",
        roleNumber:-1,
        childrens:[
            {name:{ar:"عن التطبيق",en:"About App"}, url:"/AppSetting/AboutApp", roleNumber:-1},
        ]
    },
    {
        name:{ar:"العملاء",en:"Users"},
        url:"/users",
        icon:<FaUsers />,
        id:"users",
        roleNumber:7
    },
    {
        name:{ar:"الأقسام",en:"Categories"},
        icon:<FaThLarge />,
        id:"categories",
        roleNumber:1,
        childrens:[
            {name:{ar:"الأقسام الرئيسية",en:"Main Categories"}, url:"/categories", roleNumber:1},
            {name:{ar:"الأقسام الفرعية",en:"Sub Categories"}, url:"/sub-categories", roleNumber:-1}
        ]
    },
    {
        name:{ar:"العلامات التجارية",en:"Trademarks"},
        url:"/trademarks",
        icon:<FaTags />,
        id:"trademarks",
        roleNumber:5
    },
    {
        name:{ar:"المنتجات",en:"Products"},
        url:"/products",
        icon:<FaTag />,
        id:"products",
        roleNumber:2
    },
    {
        name:{ar:"الطلبات",en:"Orders"},
        url:"/orders",
        icon:<FaBoxOpen />,
        id:"orders",
        roleNumber:-1
    },
    {
        name:{ar:"الاشعارات",en:"Notifications"},
        url:"/notifications",
        icon:<FaBell />,
        id:"notifications",
        roleNumber:3
    },
    {
        name:{ar:"الخصومات",en:"Promocodes"},
        url:"/promocodes",
        icon:<FaTicketAlt />,
        id:"promocodes",
        roleNumber:-1
    },
    // {
    //     name:{ar:"الدول",en:"Countries"},
    //     url:"",
    //     icon:"",
    //     roleNumber:-1
    // },
    // {
    //     name:{ar:"المدن",en:"Cities"},
    //     url:"",
    //     icon:"",
    //     roleNumber:-1
    // },
    // {
    //     name:{ar:"الأحياء",en:"Regions"},
    //     url:"",
    //     icon:"",
    //     roleNumber:-1
    // },
    {
        name:{ar:"الاعلانات",en:"Ads"},
        url:"/ads",
        icon:<FaAd />,
        id:"ads",
        roleNumber:-1
    },
    {
        name:{ar:"شركة الشحن",en:"Shipping Companies"},
        url:"/shipping-companies",
        icon:<FaShippingFast />,
        id:"shippings",
        roleNumber:-1
    },
    // {
    //     name:{ar:"الحادثات",en:"Chat Support"},
    //     url:"",
    //     icon:"",
    //     roleNumber:-1
    // },
]
export default SideRoutes