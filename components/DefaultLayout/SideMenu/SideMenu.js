/* style / images */
import styles from '../../../styles/DefaultLayout/SideMenu.module.scss'
import logo from '../../../public/images/logo.png'

/* methods / packages */
import {useState, useEffect, useContext} from 'react'
import {UserContext} from '../../contexts/AuthContext'
import { useRouter } from "next/router"
/* components */
import { ListGroup, ListGroupItem, Tooltip, Button, Collapse } from 'reactstrap'
import Link from "next/link"
import Image from "next/image"
import { FaAngleDown, FaAngleUp, FaBars } from 'react-icons/fa'
// import {IoMdChatbubbles} from 'react-icons/io'
import SideRoutes from "./SideMenu_Routes"



const SideMenu =(props)=> {

    const [isWide, setIsWide] = useState(false)
    const [activeCollapse, setActiveCollapse] = useState("")
    const [activeTooltip, setActiveTooltip] = useState("")
    const route = useRouter()
    const user = useContext(UserContext)

    const toggleAccordion = (ind)=>{
        const isOpen = ind === activeCollapse ? false : true
        setActiveCollapse(prev=> prev === ind ? "" : ind)
        if(isOpen && !isWide)
            setIsWide(true)
    }
    const toggle = () => {
        setIsWide(!isWide)
        if(isWide)
            setActiveCollapse("")
    }
    const toggleTooltip = (ind)=>{
        
        setActiveTooltip(ind === activeTooltip ? "" : ind)
    }

    const ruleCheck = (path)=>{
        
        if(!user.isLoggedIn) return false
        if(user.type === "ADMIN") return true
        if(path.roleNumber === -1) return true
        if (!user.rules) return false

        const rule = user.rules.find(r=> r.rule.number === path.roleNumber)
        return rule.properties.includes("SHOW")
    }
    
    return (
        <aside className={`${styles.sideMenu} ${isWide && styles.wideMenu || ""}`}/*  onMouseEnter={toggle} onMouseLeave={toggle} */>
            <div className={styles.sideContent}>
                <Button className={styles.menuToggler} onClick={toggle}>
                    <FaBars/>
                </Button>
                <header className={styles.sideHeader}>
                    <Image src={logo} width={100} height={100} alt="everich-logo"/>
                </header>
                <ListGroup className={`${styles.sideList} p-0`}>
                    {SideRoutes.map((r,index)=>{
                        if(ruleCheck(r) || true){
                            const hasAccess = ruleCheck(r)
                            if(r.url)
                                return (
                                    <ListGroupItem id={r.id} className={styles.sideItem}>
                                        <Link href={r.url} passHref prefetch>
                                        <a className={!hasAccess && "disabled"}>
                                            <span className={styles.icon}>{r.icon}</span>
                                            <span className={styles.title}>{r.name[route.locale]}{!hasAccess && " (No Access)"}</span>
                                            {!isWide && <Tooltip className={styles.toolTip} isOpen={activeTooltip === index + 1} toggle={()=>toggleTooltip(index + 1)} placement={route.locale === "ar" ? "left" : "right"} target={r.id} >
                                                {r.name[route.locale]}{!hasAccess && " (No Access)"}
                                            </Tooltip>}
                                        </a>
                                        </Link>
                                    </ListGroupItem>
                                )
                            else if(r.childrens && r.childrens.length)
                                return (
                                    <ListGroupItem id={r.id} className={`${styles.sideItem} ${styles.accordion}`}>
                                        <div className={`${styles.collapse_toggler} ${isWide ? "justify-content-between" : "justify-content-center"} ${!hasAccess ? "disabled" : ""}`} onClick={()=>toggleAccordion(index+1)}>
                                            <div>
                                                <span className={styles.icon}>{r.icon}</span>
                                                <span className={styles.title}>{r.name[route.locale]}{!hasAccess && " (No Access)"}</span>
                                            </div>
                                            {isWide && <span className={`${styles.icon}`}>{activeCollapse===index+1 ? <FaAngleUp /> : <FaAngleDown /> }</span> || null}
                                        </div>
                                        <Collapse tag={ListGroup} isOpen={activeCollapse===index +1} className={styles.collapser}>
                                            {r.childrens.map((c,ind)=>{
                                                return (
                                                    <ListGroupItem>
                                                        <Link href={c.url} passHref prefetch>
                                                        <a>
                                                            <span className={styles.title}>{c.name[route.locale]}</span>
                                                        </a>
                                                        </Link>
                                                    </ListGroupItem>
                                                )
                                            })}
                                            
                                        </Collapse>
                                        {!isWide && <Tooltip className={styles.toolTip} isOpen={activeTooltip === index + 1} toggle={()=>toggleTooltip(index + 1)} placement={route.locale === "ar" ? "left" : "right"} target={r.id} >
                                            {r.name[route.locale]}{!hasAccess && " (No Access)"}
                                        </Tooltip>}
                                    </ListGroupItem>
                                )
                            else
                                return null
                        }else
                            return null
                    })}
                    
                </ListGroup>
            </div>
        </aside>
    )
}

export default SideMenu