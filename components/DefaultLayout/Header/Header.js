/* style / images */
import styles from '../../../styles/DefaultLayout/Header.module.scss'
import logo from '../../../public/images/logo.png'

/* methods / packages */
import React, {useState, useEffect, useContext} from 'react'
import axios from 'axios'
import {UserContext} from '../../contexts/AuthContext'
import { useLang } from "../../hooks/lang-hook"
import {useRouter} from "next/router"
// import strings from "../../../locales/strings"
/* components */
import { Navbar, Container, Button, UncontrolledDropdown, DropdownToggle, DropdownMenu, Badge, DropdownItem } from 'reactstrap'
import {FaUserCircle, FaCaretDown} from 'react-icons/fa'
import Link from "next/link"
const Header =(props)=> {
    const route = useRouter()
    const [isOpen, setIsOpen] = useState(false)
    const user = useContext(UserContext)
    const {t, setLang} = useLang()

    const toggle = () => {
        setIsOpen(!isOpen)
    }
    const switchLang = ()=>{
        setLang(t("lang.opposite.val"))
    }

    return (
        <header className={styles.mainHeader}>
            <Navbar className={styles.upperNav}>
                <Container fluid>
                    {
                        user ?
                        <div className="d-flex w-100 justify-content-end">
                        <Button onClick={switchLang} className={`${styles.langBtn} mx-2`}>
                            {t("lang.opposite.name")}
                        </Button>
                        <UncontrolledDropdown id={styles.userMenu} className={`${styles.accountMenu} `}>
                            <DropdownToggle nav className={styles.accountBtn}>
                                <FaUserCircle/>
                            </DropdownToggle>
                            <DropdownMenu tag="div" className={`${styles.dropdownMenu} text-end`} right>
                                <div className={styles.userInfo}>
                                    <FaUserCircle size="3em" />
                                    <span className={styles.username}>{user.username}</span>
                                    <span className={styles.userEmail}>{user.email}</span>
                                </div>
                                <div className={styles.userOptions}>
                                    <DropdownItem tag ={Button} className={styles.btn} id={styles.logoutBtn} onClick={user.logout}>
                                        {t("logout")}
                                    </DropdownItem>
                                    <DropdownItem id={styles.profileBtn} className={`${styles.btn} btn`} >
                                        {t("profile")}
                                    </DropdownItem>
                                </div>
                            </DropdownMenu>
                        </UncontrolledDropdown>
                        
                        </div>
                        : null
                    }
                </Container>
            </Navbar>
        </header>
    )
}

export default Header