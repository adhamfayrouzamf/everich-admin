import { useEffect } from 'react';
import { useRouter } from 'next/router';
import NProgress from "nprogress"


function RouteGuard({ children }) {
    const route = useRouter();
    useEffect(() => {
        route.events.on("routeChangeStart", NProgress.start);
        route.events.on("routeChangeComplete", NProgress.done);
        route.events.on("routeChangeError", NProgress.done);
        return () => {
            route.events.off("routeChangeStart", NProgress.start);
            route.events.off("routeChangeComplete", NProgress.done);
            route.events.off("routeChangeError", NProgress.done);
        }
    }, [])

    return (children);
}
export default RouteGuard