import {useState, useEffect} from "react"
import Lottie from 'react-lottie'
import * as animationData from '../public/fastLoader.json'



const Loader = (props) => {
    
    const [show,setShow] = useState(false)
    
    const options = {
        loop:true,
        autoplay: true, 
        animationData: animationData,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    }
    // const lottie = props.lottie || {width:400,heigth:400}
    useEffect(() => {
        setShow(false)
        if(props.Show)
            setTimeout(()=>{
                setShow(props.Show)
            },props.time || 500)
    }, [props.Show]);
    // const {lottie} = props
    return (
        <>
            {(!show && props.Render) &&
            <div className={"spinner-container " + props.className } style={{display:'flex',justifyContent:'center',alignItems:'center'}}>
                <Lottie options={options} width={props.width || 400} height={props.height || 400} isClickToPauseDisabled={true} />
            </div>
            }
            {(show && props.Render) && props.children}
        </>
    )
}

export default Loader