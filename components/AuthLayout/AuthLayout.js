import LOGO from '../../public/images/logo.png'
import styles from "../../styles/AuthLayout/AuthLayout.module.scss"

/* methods / packages */
import React, {Children, useState, useEffect} from 'react'
import {Row} from 'reactstrap'
import Link from 'next/link'
import axios from 'axios'
import Image from 'next/image'

import {useRouter} from 'next/router'

/* Components */


const AuthLayout = (props) =>{
    const {login, children} = props
    const child = Children.only(children)
    return (
        <div className={styles.auth}>
            <Row className={styles.authContainer}>
                <div className={styles.authForm}>
                    <header className={styles.authHead}>
                        <div className={styles.logo}>
                            <Image src={LOGO} width={100} height={100} alt="logo" />
                        </div>
                    </header>
                    {React.cloneElement(child,{
                        login
                    })}
                    
                </div>
            </Row>
        </div>
    )
}


export default AuthLayout
