import LocalizedStrings from "react-localization"

const errors = new LocalizedStrings({
    ar:{
        test:"testArabic"
    },
    en:{
        test:"testEnglish"
    }
})

export default errors