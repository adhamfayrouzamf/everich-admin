exports.authPaths = ["/auth/login"]


exports.privatePaths = [
    {url:"/dashboard",roleNumber:-1,redirect:true,property:"SHOW"},
    // Sub Admin Pages
    {url:"/sub-admins",roleNumber:6,redirect:true,property:"SHOW"},
    {url:"/sub-admins/add",roleNumber:6,redirect:true,property:"ADD"},
    {url:"/sub-admins/[subAdminId]",roleNumber:6,redirect:true,property:"DETAILS"},
    {url:"/sub-admins/[subAdminId]/edit",roleNumber:6,redirect:true,property:"UPDATE"},
    // User Pages
    {url:"/users",roleNumber:7,redirect:true,property:"SHOW"},
    {url:"/users/add",roleNumber:7,redirect:true,property:"ADD"},
    {url:"/users/[userId]",roleNumber:7,redirect:true,property:"DETAILS"},
    {url:"/users/[userId]/edit",roleNumber:7,redirect:true,property:"UPDATE"},
    // Category Pages
    {url:"/categories",roleNumber:1,redirect:true,property:"SHOW"},
    {url:"/categories/add",roleNumber:1,redirect:true,property:"ADD"},
    {url:"/categories/[categoryId]",roleNumber:1,redirect:true,property:"DETAILS"},
    {url:"/categories/[categoryId]/edit",roleNumber:1,redirect:true,property:"UPDATE"},
    // Sub Category Pages
    {url:"/sub-categories",roleNumber:-1,redirect:true,property:"SHOW"},
    // Trademark Pages
    {url:"/trademarks",roleNumber:5,redirect:true,property:"SHOW"},
    {url:"/trademarks/add",roleNumber:5,redirect:true,property:"ADD"},
    {url:"/trademarks/[trademarkId]",roleNumber:5,redirect:true,property:"DETAILS"},
    {url:"/trademarks/[trademarkId]/edit",roleNumber:5,redirect:true,property:"UPDATE"},
    // Product Pages
    {url:"/products",roleNumber:2,redirect:true,property:"SHOW"},
    {url:"/products/add",roleNumber:2,redirect:true,property:"ADD"},
    {url:"/products/[productId]",roleNumber:2,redirect:true,property:"DETAILS"},
    {url:"/products/[productId]/edit",roleNumber:2,redirect:true,property:"UPDATE"},
    // 
    {url:"/orders",roleNumber:-1,redirect:true,property:"SHOW"},
    // Notification Pages
    {url:"/notifications",roleNumber:3,redirect:true,property:"SHOW"},
    {url:"/notifications/add",roleNumber:3,redirect:true,property:"ADD"},
    {url:"/notifications/[notifId]",roleNumber:3,redirect:true,property:"DETAILS"},
    {url:"/notifications/[notifId]/edit",roleNumber:3,redirect:true,property:"UPDATE"},
    // 
    {url:"/promocodes",roleNumber:4,redirect:true,property:"SHOW"},
    {url:"/ads",roleNumber:-1,redirect:true,property:"SHOW"},
    {url:"/shipping-companies",roleNumber:-1,redirect:true,property:"SHOW"},
    

]