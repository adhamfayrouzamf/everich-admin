/* style / images */


/* methods / packages */
import { useContext, useState, useEffect } from 'react'
import axios from 'axios'
import { useRouter } from "next/router"
import { UserContext } from '../../../components/contexts/AuthContext'
import { PageContext } from '../../../components/contexts/PageContext';
import { toast } from 'react-toastify'
import { useFetchOne } from "../../../components/hooks/fetch-hook"
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../../components/CustomForm/CustomForm"
import Loader from "../../../components/Loader"
const validateImgType = (values)=>{
    if(values.length>0){
        console.log('type')
        for(let i = 0 ; i<values.length; i++){
            console.log(values[i].type.match(/image\/(png|jpg|jpeg|gif|svg\+xml|webp)/))
            if(!values[i].type.match(/image\/(png|jpg|jpeg|gif|svg\+xml|webp)/))
                return false
        }
        return true
    }
    return true
}
const validateImgSize = (values)=> {
    if(values.length>0){
        for(let i = 0 ; i<values.length; i++){
            console.log('size',values[i].size)
            if(values[i].size > 10000000)
                return false
        }
        return true
    }
    return true
}
const adsSchema = yup.object().shape({
    numberOfSlots   : yup.number().typeError("number of slots requires a number").positive('number of slots must be positive').integer('number of slots must be integer').min(1).max(4),
    imageFile       : yup.mixed().required('image is required')
                        .test("fileSize", "image maximum size must be 10MB",validateImgSize)
                        .test("fileType","must choose an image",validateImgType),
    type            : yup.string().nullable().required("type is required"),
    product         : yup.string().when("type",(type,field)=>{
        return type === "PRODUCT_PAGE" ? field.required("must choose product") : field
    })
    
})

const page=[
    {key:{en:"Advertisements",ar:"الاعلانات"},path:"/ads"},
    {key:{en:"edit Advertisement",ar:"تعديل اعلان"},path:"/ads/[adId]/edit"}
]
const options = {
    form:'Ad-addform',id:"editForm", title:{value:"Edit Advertisement"},
    submitBtn:{value:"Edit",options:{id:"submitBtn"}},
}
const fields= [
    {type:'file',name:"image",label:"Add Image",id:"adImage",dir:"ads",col:{md:"6"}},
    {type:'text',name:"numberOfSlots",placeholder:"Number Of Slots",id:"numOfSlots",col:{md:"6"}},
    {type:'conditional',onValue:'PRODUCT_PAGE',
        condition:{
            type:'radio',name:"type",choices:[{label:"Home",value:"HOME_PAGE"},{label:"Product",value:"PRODUCT_PAGE"}]
        },
        inputs:[
            {type:'select',name:"product",placeholder:"Select Product",fetch_url:'/api/product?name=',option:{value:'id',label:[{key:'name',localized:true}]}},
        ]
    }
]

const AdEditForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    const [data,error,isLoading] = useFetchOne(`/api/advertisement/${route.query.adId}`)
    setPage(page)
    const [ad,setAd] = useState()
    const editAd = (v,e) => {
        const {adId} = route.query
        console.log(v)
        axios.put(`/api/advertisement/${adId}`,v,{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>{
            console.log(res.data)
            toast.success("updated successfully")
        }).catch(err=>{
            console.log(err.message)
        })
    }

    
    
    useEffect(()=>{
        if(data)
            setAd({
                numberOfSlots:data.numberOfSlots,
                type:data.type,
                product:data.product || null,
                image:data.image || null
            })
    },[data])
    return (
        <div className="ad-page">
            <Container fluid>
                <div className="content">
                    <Loader Show={!isLoading} Render={true}>
                        <CustomForm 
                            config={options}
                            validation={adsSchema}
                            submit={editAd}
                            fields={fields}
                            values={ad}
                        />
                    </Loader>
                </div>
            </Container>
        </div>
    )
}


export default AdEditForm