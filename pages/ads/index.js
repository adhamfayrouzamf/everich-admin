/* style / images */


/* methods / packages */
import { useContext } from 'react'
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext';

/* components */
// import { FaStar, FaShoppingCart, FaPlus, FaMinus } from 'react-icons/fa'
import { Container } from 'reactstrap'
import CustomTable from '../../components/CustomTable/CustomTable';

const page=[
    {key:{en:"Advertisements",ar:"الاعلانات"},path:"/ads"}
]
const Category = props => {
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const options={
        title:{value:{en:"Advertisements",ar:"الاعلانات"}},
        addBtn:true,
        url:"/ads",api_url:"/api/advertisement",fetch_url:"/api/advertisement",
        cols:[
            {key:"id",label:{ar:"#",en:"#"},check:true},
            {key:"image",label:{ar:"الصورة",en:"Image"},isImage:true},
            {key:"numberOfSlots",label:{ar:"عدد الخانات",en:"Number of Slots"}},
            {key:"type",label:{ar:"النوع",en:"Type"}},
            {key:"product",label:{ar:"المنتج",en:"Product"},childs:["name"],notFound:"No Product", localized:true},
            {key:"createdAt",label:{ar:"تاريخ الإنشاء",en:"Date"},isDate:true}
        ],
        controls:[
            "DETAILS", "UPDATE", "DELETE"
        ]
    }

    return (
        <div className="ads-page">
            <Container fluid>
                <div className="content">
                    <CustomTable options={options} />
                </div>
            </Container>
        </div>
    )
}


export default Category