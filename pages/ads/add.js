/* style / images */


/* methods / packages */
import { useContext } from 'react'
import axios from 'axios'
import { useRouter } from "next/router"
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext';
import { toast } from 'react-toastify'
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../components/CustomForm/CustomForm"

const validateImgType = (values)=>{
    if(values.length>0){
        console.log('type')
        for(let i = 0 ; i<values.length; i++){
            console.log(values[i].type.match(/image\/(png|jpg|jpeg|gif|svg\+xml|webp)/))
            if(!values[i].type.match(/image\/(png|jpg|jpeg|gif|svg\+xml|webp)/))
                return false
        }
        return true
    }
    return false
}
const validateImgSize = (values)=> {
    if(values.length>0){
        for(let i = 0 ; i<values.length; i++){
            console.log('size',values[i].size)
            if(values[i].size > 10000000)
                return false
        }
        return true
    }
    return false
}
const adsSchema = yup.object().shape({
    numberOfSlots   : yup.number().typeError("number of slots requires a number").positive('number of slots must be positive').integer('number of slots must be integer').min(1).max(4),
    imageFile       : yup.mixed().required('image is required')
                        .test("fileSize", "image maximum size must be 10MB",validateImgSize)
                        .test("fileType","must choose an image",validateImgType),
    type            : yup.string().nullable().required("type is required"),
    product         : yup.string().when("type",(type,field)=>{
        return type === "PRODUCT_PAGE" ? field.required("must choose product") : field
    })
    
})

const page=[
    {key:{en:"Advertisements",ar:"الاعلانات"},path:"/ads"},
    {key:{en:"Add Advertisement",ar:"اضافة اعلان"},path:"/ads/add"}
]
const AdAddForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)

    const addAds = (v,e) => {
        axios.post('/api/advertisement',v).then(res=>{
            console.log(res.data)
            toast.success("added successfully")
        }).catch(err=>{
            console.log(err.message)
        })
    }

    const options = {
        form:'Ad-addform',id:"addForm", submit:addAds,validation:adsSchema,title:{value:"Add Advertisement"},
        submitBtn:{value:"Add",options:{id:"submitBtn"}},
        fields:[
            {type:'file',name:"image",label:"Add Image",id:"adImage",dir:"ads",col:{md:"6"}},
            {type:'text',name:"numberOfSlots",placeholder:"Number Of Slots",id:"numOfSlots",col:{md:"6"}},
            {type:'conditional',onValue:'PRODUCT_PAGE',
                condition:{
                    type:'radio',name:"type",choices:[{label:"Home",value:"HOME_PAGE"},{label:"Product",value:"PRODUCT_PAGE"}]
                },
                inputs:[
                    {type:'select',name:"product",placeholder:"Select Product",fetch_url:'/api/product?name=',option:{value:'id',label:[{key:'name',localized:true}]}},
                ]
            }
        ]
    }
    
    return (
        <div className="category-page">
            <Container fluid>
                <div className="content">
                    <CustomForm options={options} />
                </div>
            </Container>
        </div>
    )
}


export default AdAddForm