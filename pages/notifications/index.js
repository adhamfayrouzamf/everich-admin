/* style / images */

/* methods / packages */
import { useContext } from 'react'
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext';

/* components */
import { Container } from 'reactstrap'
import CustomTable from '../../components/CustomTable/CustomTable';

const page=[
    {key:{en:"Notifications",ar:"الإشعارات"},path:"/notifications"}
]
const NotificationTable = props => {
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const options={
        title:{value:{en:"Notifications",ar:"الإشعارات"}},
        addBtn:true,roleNumber:3,
        url:"/notifications",api_url:"/api/notification",fetch_url:"/api/notification/findAll",
        cols:[
            {key:"id",label:{ar:"#",en:"#"},check:true},
            {key:"image",label:{ar:"الصورة",en:"Image"},isImage:true, notFound:"No Image"},
            {key:"title",label:{ar:"العنوان",en:"Title"},localized:true},
            {key:"text",label:{ar:"الوصف",en:"Description"},localized:true},
            {key:"type",label:{ar:"أرسلت إلى",en:"Sent To"}},
            {key:"createdAt",label:{ar:"تاريخ الإنشاء",en:"Date"},isDate:true}
        ],
        controls:[
            "DETAILS", "DELETE", "SHOW"
        ]
    }
    return (
        <div className="notification-page">
            <Container fluid>
                <div className="content">
                    <CustomTable options={options} />
                </div>
            </Container>
        </div>
    )
}


export default NotificationTable