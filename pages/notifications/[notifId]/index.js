/* style / images */

/* methods / packages */
import { useContext, useEffect, useState } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"

import { UserContext } from '../../../components/contexts/AuthContext'
import { PageContext } from "../../../components/contexts/PageContext"
import { useFetchOne } from "../../../components/hooks/fetch-hook"
/* components */
import { Container, Table } from 'reactstrap';
import Loader from '../../../components/Loader';
import Moment from "react-moment"
import "moment/locale/ar"
import Image from "next/image"


const page=[
    {key:{en:"Notifications",ar:"الإشعارات"},path:"/notifications"},
    {key:{en:"Notification Details",ar:"تفاصيل الإشعار"},path:"/notifications/[notifId]"}
]
const ViewNotificationDetails = () => {
    const route = useRouter()
    const admin = useContext(UserContext)
    const [notif, error, isLoading] = useFetchOne(`/api/notification/${route.query.notifId}`)
    const {setPage} = useContext(PageContext)
    setPage(page)


    return (
        <div className="notification-page">
            <Container fluid>
                <div className="content">
                    <Loader Show={!isLoading} Render={true}>
                            <div className="notification_info">
                                {notif.image && <Image src={notif.image.url} width={250} height={250} />}
                                <h4>{notif.title && notif.title[route.locale]}</h4>
                                <p>{notif.text && notif.text[route.locale]}</p>
                            </div>
                        
                        
                        {notif.type === "SPECIFIC" ?
                            <Table responsive>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Login Method</th>
                                        <th>Controls</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {notif.users.map((user,index)=>{
                                        return (
                                            <tr key={user.id}>
                                                <td>{index + 1}</td>
                                                <td>{user.username}</td>
                                                <td>{user.email}</td>
                                                <td>{user.phone}</td>
                                                <td>{user.socialMediaType}</td>
                                                <td></td>
                                            </tr>
                                        )
                                    })
                                    }
                                </tbody>
                            </Table>
                            : null
                        }
                    </Loader>
                </div>
            </Container>
        </div>
    )
}


export default ViewNotificationDetails