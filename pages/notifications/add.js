/* style / images */

/* methods / packages */
import { useContext } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext';
import { toast } from 'react-toastify'
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../components/CustomForm/CustomForm"

const notifSchema = yup.object().shape({
    title       : yup.object().shape({
        en:yup.string().required('title is required').min(4),
        ar:yup.string().required('title is required').min(4) 
    }),
    text : yup.object().shape({
        en:yup.string().required('description is required').min(4),
        ar:yup.string().required('description is required').min(4) 
    }),
    type: yup.string().nullable().required("User Type is required"),
    users: yup.array().nullable().when("type",(type,field)=>{
        return type === "SPECIFIC" ? field.required("must choose at least one user") : field
    }),
    
})


const page=[
    {key:{en:"Notifications",ar:"الإشعارات"},path:"/notifications"},
    {key:{en:"Add Notification",ar:"اضافة إشعار"},path:"/notifications/add"}
]
const AddNotifForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const addNotif = (v,e) => {
        console.log(v.image)
        if(!v.image.url || !v.image.id)
            delete v.image

        if(v.type === "ALL")
            axios.post('/api/notification/sendToAll',{...v},{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>{
                toast.success("created successfully")
            }).catch(err=>{
                console.log(err.message)
                toast.error(err.message)
            })
        else if (v.type==="SPECIFIC")
            axios.post('/api/notification/sendToSpecific',{...v},{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>{
                console.log("done")
            }).catch(err=>{
                console.log(err.message)
            })
    }
    const options = {
        form:'notif-form',id:"notifForm", title:{value:"Add Notification"},
        submitBtn:{value:"Add",options:{id:"submit-btn"}},
    }
    const fields = [
        {type:"file",name:"image",label:"Notification Image",id:"image", dir:"notif", col:{md:"6"}},
        {type:'text',name:"title",locale:"en",placeholder:"Title in English",id:"title-en", col:{md:"6"}},
        {type:'text',name:"title",locale:"ar",placeholder:"Title in Arabic" ,id:"title-ar", col:{md:"6"}},
        {type:'text',name:"text",locale:"en",placeholder:"description in English",id:"description-en", col:{md:"6"}},
        {type:'text',name:"text",locale:"ar",placeholder:"description in Arabic" ,id:"description-ar", col:{md:"6"}},
        {type:"conditional",onValue:"SPECIFIC",col:{md:"6"},
            condition:{
                type:"radio",choices:[{value:"ALL",label:"Send To All Users"},{value:"SPECIFIC",label:"Send To Specific Users"}],
                name:"type",fieldset:{value:"Send To..."}
            },
            inputs:[
                {type:"select",isMulti:true,name:"users", placeholder:"Select Users...",fetch_url:"/api/user/allUsers?search="
                ,option:{label:[{key:"username"},{key:"email"},{key:"socialMediaType"},{key:"phone"}],value:"id"}}
            ]
        }
    ]

    return (
        <div className="notif-page">
            <Container fluid>
                <div className="content">
                    <CustomForm 
                        config={options}
                        fields={fields}
                        validation={notifSchema}
                        submit={addNotif}
                    />
                </div>
            </Container>
        </div>
    )
}


export default AddNotifForm