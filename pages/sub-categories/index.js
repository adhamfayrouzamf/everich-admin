/* style / images */


/* methods / packages */
import { useContext } from 'react'
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext'

/* components */
import { Container } from 'reactstrap'
import CustomTable from '../../components/CustomTable/CustomTable';


const page=[
    {key:{en:"Sub Categories",ar:"الأقسام الفرعية"},path:"/sub-categories"}
]
const Category = props => {
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const options={
        title:{value:{ar:"الأقسام الفرعية",en:"Sub Categories"}},
        addBtn:true,
        url:"/sub-categories",api_url:"/api/subcategory",fetch_url:"/api/subcategory",
        cols:[
            {key:"id",label:{ar:"#",en:"#"},check:true},
            {key:"image",label:{ar:"الصورة",en:"Image"},isImage:true},
            {key:"name",label:{ar:"الاسم",en:"Name"},localized:true},
            {key:"parent",label:{ar:"القسم الرئيسي",en:"Parent"},localized:true,childs:["name"]},
            {key:"createdAt",label:{ar:"تاريخ الانشاء",en:"Date"},isDate:true}
        ],
        controls:[
            "DETAILS", "UPDATE", "DELETE"
        ]
    }

    
    return (
        <div className="subCat-page">
            <Container fluid>
                <div className="content">
                    <CustomTable options={options} />
                </div>
            </Container>
        </div>
    )
}


export default Category