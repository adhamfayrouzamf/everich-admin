/* style / images */


/* methods / packages */
import { useContext } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext'
import { toast } from 'react-toastify'
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../components/CustomForm/CustomForm"

const validateImgType = (values)=>{
    if(values.length>0){
        for(let i = 0 ; i<values.length; i++){
            console.log(values[i].type.match(/image\/(png|jpg|jpeg|gif|svg\+xml|webp)/))
            if(!values[i].type.match(/image\/(png|jpg|jpeg|gif|svg\+xml|webp)/))
                return false
        }
        return true
    }
    return false
}
const validateImgSize = (values)=> {
    if(values.length>0){
        for(let i = 0 ; i<values.length; i++){
            console.log('size',values[i].size)
            if(values[i].size > 10000000)
                return false
        }
        return true
    }
    return false
}
const categorySchema = yup.object().shape({
    name       : yup.object().shape({
        en:yup.string().required('name is required').min(4),
        ar:yup.string().required('name is required').min(4) 
    }),
    parent     : yup.string().required("parent is required"),
    imageFile  : yup.mixed().required('image is required')
                .test("fileSize", "image maximum size must be 10MB",validateImgSize)
                .test("fileType","must choose an image",validateImgType)
    
})
const page=[
    {key:{en:"Sub Categories",ar:"الأقسام الفرعية"},path:"/sub-categories"},
    {key:{en:"Add Sub Category",ar:"اضافة قسم فرعي"},path:"/sub-categories/add"}

]
const SubCategoryAddForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const addSubCategory = (v,e) => {
        
        axios.post('/api/subcategory',v,{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>{
            console.log(res.data)
            toast.success("added successfully")
        }).catch(err=>{
            console.log(err.message)
        })
    }
    const options = {
        form:'subcategory-addform',id:"subcategoryForm", title:{value:"Add Sub Category"},
        submitBtn:{value:"Add",options:{id:"submitBtn"}},
    }
    const fields = [
        {type:'file',name:"image",label:"Sub Category Image",id:"subcategoryImage",dir:"category"},
        {type:'text',name:"name",locale:"en",placeholder:"Name in English",id:"name-en", col:{md:"6"}},
        {type:'text',name:"name",locale:"ar",placeholder:"Name in Arabic",id:"name-ar", col:{md:"6"}},
        {type:'select',name:"parent",placeholder:"Parent Category",id:"parent",col:{md:"6"},fetch_url:"/api/category?name=",option:{value:"id",label:[{key:'name',localized:true}]}}
    ]
    
    return (
        <div className="subcategory-page">
            <Container fluid>
                <div className="content">
                    <CustomForm 
                        config={options}
                        fields={fields}
                        validation={categorySchema}
                        submit={addSubCategory}
                        />
                </div>
            </Container>
        </div>
    )
}


export default SubCategoryAddForm