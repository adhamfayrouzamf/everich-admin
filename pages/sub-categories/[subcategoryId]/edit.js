/* style / images */


/* methods / packages */
import { useContext, useState, useEffect } from 'react'
import axios from 'axios'
import { useRouter } from "next/router"
import { UserContext } from '../../../components/contexts/AuthContext'
import { PageContext } from '../../../components/contexts/PageContext';
import { toast } from 'react-toastify'
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../../components/CustomForm/CustomForm"

const validateImgType = (values)=>{
    if(values.length>0){
        for(let i = 0 ; i<values.length; i++){
            console.log(values[i].type.match(/image\/(png|jpg|jpeg|gif|svg\+xml|webp)/))
            if(!values[i].type.match(/image\/(png|jpg|jpeg|gif|svg\+xml|webp)/))
                return false
        }
        return true
    }
    return true
}
const validateImgSize = (values)=> {
    if(values.length>0){
        for(let i = 0 ; i<values.length; i++){
            console.log('size',values[i].size)
            if(values[i].size > 10000000)
                return false
        }
        return true
    }
    return true
}
const subCategorySchema = yup.object().shape({
    name       : yup.object().shape({
        en:yup.string().required('name is required').min(4),
        ar:yup.string().required('name is required').min(4) 
    }),
    parent     : yup.string().required("parent is required"),
    imageFile  : yup.mixed().required('image is required')
                .test("fileSize", "image maximum size must be 10MB",validateImgSize)
                .test("fileType","must choose an image",validateImgType)
    
})

const page=[
    {key:{en:"Sub Categories",ar:"الأقسام الفرعية"},path:"/sub-categories"},
    {key:{en:"Edit Sub Category",ar:"تعديل قسم فرعي"},path:"/sub-categories/[subcategoryId]/edit"}

]
const SubCategoryEditForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const [subCategory,setSubCategory] = useState()
    const editSubCategory = (v,e) => {
        console.log(v)
        const {subcategoryId} = route.query
        axios.put(`/api/subcategory/${subcategoryId}`,v,{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>{
            console.log(res.data)
            toast.success("updated successfully")
        }).catch(err=>{
            console.log(err.message)
        })
    }

    const options = {
        form:'subCategory-editform',id:"subCategoryForm", title:{value:"Edit Sub Category"},
        submitBtn:{value:"Edit",options:{id:"submitBtn"}}
    }
    const fields=[
        {type:'file',name:"image",label:"Sub Category Image",id:"subCategoryImage",dir:"category"},
        {type:'text',name:"name",locale:"en",placeholder:"Name in English",id:"name-en", col:{md:"6"}},
        {type:'text',name:"name",locale:"ar",placeholder:"Name in Arabic",id:"name-ar", col:{md:"6"}},
        {type:'select',name:"parent",placeholder:"Parent Category",id:"parent",col:{md:"6"},fetch_url:"/api/category?name=",option:{value:"id",label:[{key:'name',localized:true}]}}
    ]
    useEffect(()=>{
        const {subcategoryId} = route.query
        axios.get(`/api/subcategory/${subcategoryId}`).then(res=>{
            const {data} = res
            console.log(data)
            setSubCategory({
                name:data.name,
                image:data.image,
                parent:data.parent
            })
        }).catch(err=>console.error(err))
    },[route.query.subcategoryId])
    return (
        <div className="subCategory-page">
            <Container fluid>
                <div className="content">
                    <CustomForm 
                        config={options} 
                        fields={fields} 
                        validation={subCategorySchema}
                        values={subCategory}
                        submit={editSubCategory} />
                </div>
            </Container>
        </div>
    )
}


export default SubCategoryEditForm