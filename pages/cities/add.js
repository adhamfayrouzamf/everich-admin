/* style / images */


/* methods / packages */
import { useContext } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext';
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../components/CustomForm/CustomForm"

const citySchema = yup.object().shape({
    name       : yup.object().shape({
        en:yup.string().required('name is required').min(4),
        ar:yup.string().required('name is required').min(4) 
    }),
    country : yup.string().required('must choose country'),
    
})


const page=[
    {key:{en:"Cities",ar:"المدن"},path:"/cities"},
    {key:{en:"Add City",ar:"اضافة مدينة"},path:"/cities/add"}
]
const AddCityForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const addCity = (v,e) => {
        axios.post('/api/city',{...v}).then(res=>{
            console.log("done")
        }).catch(err=>{
            console.log(err.message)
        })
    }
    const options = {
        form:'cityAddForm',id:"cityForm", title:{value:"Add City"},
        submitBtn:{value:"Add",options:{id:"submit-btn"}},
    }
    const fields = [
        {type:'text',name:"name",locale:"en",placeholder:"Name in English",id:"name-en",col:{md:"6"}},
        {type:'text',name:"name",locale:"ar",placeholder:"Name in Arabic",id:"name-ar",col:{md:"6"}},
        {type:"select",name:"country",placeholder:"Choose Country",id:"country",fetch_url:"/api/country?name=",option:{value:"id",label:[{key:"name",localized:true}]},col:{md:"6"}},
    ]

    return (
        <div className="city-page">
            <Container fluid>
                <div className="content">
                    <CustomForm 
                        config={options}
                        fields={fields}
                        validation={citySchema}
                        submit={addCity}
                    />
                </div>
            </Container>
        </div>
    )
}


export default AddCityForm