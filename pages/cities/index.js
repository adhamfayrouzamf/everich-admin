/* style / images */
// import styles from './CityTable.scss';

/* methods / packages */
import { useContext } from 'react'
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext';

/* components */
import { Container } from 'reactstrap'
import CustomTable from '../../components/CustomTable/CustomTable';

const page=[
    {key:{en:"Cities",ar:"المدن"},path:"/cities"}
]
const CityTable = props => {
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const options={
        title:{value:"Cities"},
        addBtn:{value:"Add City"},
        url:"/cities",api_url:"/api/city",fetch_url:"/api/city",
        cols:[
            {key:"id",label:"#",check:true},
            {key:"name",label:"Name",localized:true},
            {key:"country",label:"Country",childs:["name"],localized:true},
            {key:"createdAt",label:"Date",isDate:true}
        ],
        controls:[
            "DETAILS", "UPDATE", "DELETE"
        ]
    }


    return (
        <div className="city-page">
            <Container fluid>
                <div className="content">
                    <CustomTable options={options} />
                </div>
            </Container>
        </div>
    )
}


export default CityTable