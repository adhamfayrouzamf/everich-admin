/* style / images */


/* methods / packages */
import { useContext, useState, useEffect } from 'react'
import axios from 'axios'
import { useRouter } from "next/router"
import { UserContext } from '../../../components/contexts/AuthContext'
import { PageContext } from '../../../components/contexts/PageContext';
import { toast } from 'react-toastify'
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../../components/CustomForm/CustomForm"
import Loader from "../../../components/Loader"


const citySchema = yup.object().shape({
    name       : yup.object().shape({
        en:yup.string().required('name is required').min(4),
        ar:yup.string().required('name is required').min(4) 
    }),
    country : yup.string().required('must choose country'),
    
})

const page=[
    {key:{en:"Cities",ar:"المدن"},path:"/cities"},
    {key:{en:"Edit City",ar:"تعديل المدينة"},path:"/cities/[cityId]/edit"}
]

const CityEditForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const [city,setCity] = useState()
    const editCity = (v,e) => {
        console.log(v)
        const {cityId} = route.query
        axios.put(`/api/city/${cityId}`,v,{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>{
            console.log(res.data)
            toast.success("updated successfully")
        }).catch(err=>{
            console.log(err.message)
        })
    }

    const options = {
        form:'city-editform',id:"cityForm", title:{value:"Edit city"},
        submitBtn:{value:"Edit",options:{id:"submitBtn"}}
    }
    const fields=[
        {type:'text',name:"name",locale:"en",placeholder:"Name in English",id:"name-en", col:{md:"6"}},
        {type:'text',name:"name",locale:"ar",placeholder:"Name in Arabic",id:"name-ar", col:{md:"6"}},
        {type:"select",name:"country",placeholder:"Choose Country",id:"country",fetch_url:"/api/country?name=",option:{value:"id",label:[{key:"name",localized:true}]},col:{md:"6"}},
    ]
    useEffect(()=>{
        const {cityId} = route.query
        axios.get(`/api/city/${cityId}`).then(res=>{
            const {data} = res
            setCity({
                name:data.name,
                country:data.country,
            })
        }).catch(err=>console.error(err))
    },[route.query.cityId])
    return (
        <div className="city-page">
            <Container fluid>
                <div className="content">
                    <Loader Render={true} Show={city}>
                        <CustomForm 
                            config={options} 
                            fields={fields} 
                            validation={citySchema} 
                            values={city}
                            submit={editCity} />
                    </Loader>
                </div>
            </Container>
        </div>
    )
}


export default CityEditForm