import 'bootstrap/dist/css/bootstrap.min.css'
import "react-toastify/dist/ReactToastify.css";
import "nprogress/nprogress.css"
import '../styles/globals.scss'

import {useRouter} from 'next/router'
import { ToastContainer } from "react-toastify"


import UserContextProvider from '../components/contexts/AuthContext'
import NotifContextProvider from '../components/contexts/NotificationContext';
import PageContextProvider from '../components/contexts/PageContext';
import RouteGuard from '../components/RouteGuard';
import RouteProgress from '../components/RouteProgress';
import AuthLayout from "../components/AuthLayout/AuthLayout"
import DefaultLayout from "../components/DefaultLayout/DefaultLayout"

function MyApp({ Component, pageProps }) {
  const route = useRouter()
  
  return (
    <UserContextProvider >
    <RouteProgress>
      <RouteGuard  >
        <div>
        {
          route.pathname === "/auth/login" ? 
          <AuthLayout>
              <Component {...pageProps} />
          </AuthLayout>
          :
          <NotifContextProvider>
          <PageContextProvider>
            <DefaultLayout>
              <Component {...pageProps} />
            </DefaultLayout>
          </PageContextProvider>
          </NotifContextProvider>
          
        }
        <ToastContainer autoClose={3000} hideProgressBar closeButton={null} position="bottom-center" />
        </div>
      </RouteGuard>
    </RouteProgress>
    </UserContextProvider>
  )
}

export default MyApp
