/* style / images */

/* methods / packages */
import { useContext, useEffect, useState } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"

import { UserContext } from '../../../components/contexts/AuthContext'
import { PageContext } from "../../../components/contexts/PageContext"
import { useFetchOne } from "../../../components/hooks/fetch-hook"
/* components */
import { Container, Table } from 'reactstrap';
import Loader from '../../../components/Loader';
import Moment from "react-moment"
import "moment/locale/ar"



const page=[
    {key:{en:"Promocodes",ar:"كوبونات الخصم"},path:"/promocodes"},
    {key:{en:"Promocode Details",ar:"تفاصيل كوبون الخصم"},path:"/promocodes/[promocodeId]"}
]
const ViewPromocodeDetails = () => {
    const route = useRouter()
    const admin = useContext(UserContext)
    const [promocode, error, isLoading] = useFetchOne(`/api/promocode/${route.query.promocodeId}`)
    const {setPage} = useContext(PageContext)
    setPage(page)


    return (
        <div className="promocode-page">
            <Container fluid>
                <div className="content">
                    <Loader Show={!isLoading} Render={true}>
                        <Table className="promocode_table">
                            <tbody>
                                <tr>
                                    <th>Code</th>
                                    <td>{promocode.code}</td>
                                </tr>
                                <tr>
                                    <th>Discount</th>
                                    <td>{promocode.discount} %</td>
                                </tr>
                                <tr>
                                    <th>Number Of Use</th>
                                    <td>{promocode.numberOfUse}</td>
                                </tr>
                                <tr>
                                    <th>Start Date</th>
                                    <td><Moment locale={route.locale} date={promocode.startDate} format="DD-MM-YYYY"/></td>
                                </tr>
                                <tr>
                                    <th>End Date</th>
                                    <td><Moment locale={route.locale} date={promocode.endDate} format="DD-MM-YYYY"/></td>
                                </tr>
                            </tbody>
                        </Table>
                        {promocode.usersType === "SPECIFIC" ?
                            <Table responsive>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Login Method</th>
                                        <th>Controls</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {promocode.users.map((user,index)=>{
                                        return (
                                            <tr key={user.id}>
                                                <td>{index + 1}</td>
                                                <td>{user.username}</td>
                                                <td>{user.email}</td>
                                                <td>{user.phone}</td>
                                                <td>{user.socialMediaType}</td>
                                                <td></td>
                                            </tr>
                                        )
                                    })
                                    }
                                </tbody>
                            </Table>
                            : null
                        }
                    </Loader>
                </div>
            </Container>
        </div>
    )
}


export default ViewPromocodeDetails