/* style / images */


/* methods / packages */
import { useContext } from 'react'
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext';

/* components */
import { Container } from 'reactstrap'
import CustomTable from '../../components/CustomTable/CustomTable';


const page=[
    {key:{en:"Promocodes",ar:"كوبونات الخصم"},path:"/promocodes"}
]
const PromocodeTable = props => {
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const options={
        title:{value:{en:"Promocodes",ar:"كوبونات الخصم"}},
        addBtn:true,
        url:"/promocodes",api_url:"/api/promocode",fetch_url:"/api/promocode",
        cols:[
            {key:"id",label:{ar:"#",en:"#"},check:true},
            {key:"code",label:{ar:"كود الخصم",en:"Code"}},
            {key:"discount",label:{ar:"الخصم",en:"Discount"}},
            {key:"numberOfUse",label:{ar:"عدد الاستعمال",en:"Number of Use"}},
            {key:"startDate",label:{ar:"تاريخ الإنشاء",en:"Start Date"},isDate:true},
            {key:"endDate",label:{ar:"تاريخ الإنتهاء",en:"End Date"}, isDate:true},
            {key:"usersType",label:{ar:"أرسل إلى",en:"Sent To"}},
        ],
        controls:[
            "view", "delete"
        ]
    }
    return (
        <div className="promocode-page">
            <Container fluid>
                <div className="content">
                    <CustomTable options={options} />
                </div>
            </Container>
        </div>
    )
}


export default PromocodeTable