/* style / images */
// import './Category.scss';

/* methods / packages */
import React, { useContext } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext';
import { toast } from 'react-toastify'
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../components/CustomForm/CustomForm"

const promoSchema = yup.object().shape({
    
    code : yup.string().required('code is required'),
    discount : yup.number().typeError('discount is required').min(1).max(100),
    numberOfUse : yup.number().typeError('discount is required').min(1),
    startDate: yup.date().typeError("Start Date is required")
    .when("endDate",(endDate,field)=>{
        return endDate instanceof Date && !isNaN(endDate) ? field.max(endDate,"Start Date must be less than End Date") : field
    }),
    endDate:yup.date().typeError("End Date is required")
    .when("startDate",(startDate,field)=>{
        return startDate instanceof Date && !isNaN(startDate) ? field.min(startDate,"End Date must be later than Start Date") : field
    }),
    usersType: yup.string().nullable().required('User Type is required'),
    users: yup.array().nullable().when("usersType",(type,field)=>{
        return type === "SPECIFIC" ? field.required("must choose at least one user") : field
    }),
},["startDate","endDate"])
const page=[
    {key:{en:"Promocodes",ar:"كوبونات الخصم"},path:"/promocodes"},
    {key:{en:"Add Promocode",ar:"اضافة كوبون"},path:"/promocodes/add"}
]

const AddPromocodeForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const addPromocode = (v,e) => {
        axios.post('/api/promocode',{...v},{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>{
            toast.success("created successfully")
        }).catch(err=>{
            console.log(err.message)
            toast.error(err.message)
        })
    }
    const options = {
        form:'promo-form',id:"promoForm", title:{value:"Add promocode"},
        submitBtn:{value:"Add",options:{id:"submit-btn"}},
    }
    const fields = [
        {type:'text',name:"code", placeholder:"Type Code",id:"code",col:{md:"6"}},
        {type:'text',name:"discount",placeholder:"Type Discount" ,id:"discount",col:{md:"6"}},
        {type:'text',name:"numberOfUse",placeholder:"Number Of Use",id:"numberofuse",col:{md:"6"}},
        {type:'date',name:"startDate",placeholder:"Start Date",id:"start_date",col:{md:"6"},min:new Date().toISOString().slice(0,10),},
        {type:'date',name:"endDate",placeholder:"End Date",id:"end_date",col:{md:"6"}},
        {type:"conditional",onValue:"SPECIFIC",col:{md:"12"},
            condition:{
                type:"radio",choices:[{value:"ALL",label:"Send To All Users"},{value:"SPECIFIC",label:"Send To Specific Users"}],
                name:"usersType",fieldset:{value:"Send To..."},col:{md:"6"},
            },
            inputs:[
                {type:"select",isMulti:true,name:"users", placeholder:"Select Users...",fetch_url:"/api/user/allUsers?search="
                ,option:{label:[{key:"username"},{key:"email"},{key:"socialMediaType"},{key:"phone"}],value:"id"},col:{md:"6"}}
            ]
        }
    ]
    return (
        <div className="promocode-page">
            <Container fluid>
                <div className="content">
                    <CustomForm 
                        config={options}
                        fields={fields}
                        validation={promoSchema}
                        submit={addPromocode}
                    />
                </div>
            </Container>
        </div>
    )
}


export default AddPromocodeForm