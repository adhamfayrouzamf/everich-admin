/* Style / images */
import styles from '../../styles/Dashboard/Dashboard.module.scss'

/* methods / packages */
import { useState, useEffect, useContext } from 'react'
import axios from 'axios'
import {UserContext} from '../../components/contexts/AuthContext'

/* components */
import {Container, Row, Col, Card, CardBody, CardTitle} from 'reactstrap'
import Link from 'next/link';
import BarChart from '../../components/Dashboard/BarChart'
import PieChart from '../../components/Dashboard/PieChart'
import LineChart from '../../components/Dashboard/LineChart'
import DoughnutChart from '../../components/Dashboard/DoughnutChart'
import Loader from '../../components/Loader'
const Dashboard = (props)=>{
    const user = useContext(UserContext)

    return (
        <main className={styles.dashboardPage}>
            <Container fluid>
                <div className="content">
                    <Row>
                        <Col md="6">
                            <Loader Show={true} Render={true} time={1000} height={300} lottie={{heigth:200,width:200}}>
                                <BarChart height={300} className="test-bar" />
                            </Loader>
                        </Col>
                        <Col md="6">
                            <Loader Show={true} Render={true} time={2000} height={300} lottie={{heigth:200,width:200}}>
                                <PieChart height={300} className="test-pie" />
                            </Loader>
                        </Col>
                        <Col md="6">
                            <Loader Show={true} Render={true} time={3000} height={300} lottie={{heigth:200,width:200}}>
                                <LineChart height={300} className="test-line" />
                            </Loader>
                        </Col>
                        <Col md="6">
                            <Loader Show={true} Render={true} time={4000} height={300} lottie={{heigth:200,width:200}}>
                                <DoughnutChart height={300} className="test-doughnut" />
                            </Loader>
                        </Col>
                    </Row>
                    
                </div>
            </Container>
        </main>
    )

}

export default Dashboard