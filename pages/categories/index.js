/* style / images */


/* methods / packages */
import { useContext } from 'react'
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext'

/* components */
import { Container } from 'reactstrap'
import CustomTable from '../../components/CustomTable/CustomTable';


const page=[
    {key:{en:"Categories",ar:"الأقسام"},path:"/categories"}
]
const Category = props => {
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const options={
        title:{value:{ar:"الأقسام الرئيسية",en:"Categories"}},
        addBtn:true,
        url:"/categories",api_url:"/api/category",fetch_url:"/api/category",
        cols:[
            {key:"id",label:{ar:"#",en:"#"},check:true},
            {key:"image",label:{ar:"الصورة",en:"Image"},isImage:true},
            {key:"name",label:{en:"Name",ar:"الاسم"},localized:true},
            {key:"createdAt",label:{ar:"تاريخ الانشاء",en:"Date"},isDate:true}
        ],
        controls:[
            "DETAILS", "UPDATE", "DELETE"
        ]
    }

    
    return (
        <div className="category-page">
            <Container fluid>
                <div className="content">
                    <CustomTable options={options} />
                </div>
            </Container>
        </div>
    )
}


export default Category