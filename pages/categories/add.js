/* style / images */


/* methods / packages */
import { useContext } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext'
import { toast } from 'react-toastify'
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../components/CustomForm/CustomForm"

const validateImgType = (values)=>{
    if(values.length>0){
        for(let i = 0 ; i<values.length; i++){
            console.log(values[i].type.match(/image\/(png|jpg|jpeg|gif|svg\+xml|webp)/))
            if(!values[i].type.match(/image\/(png|jpg|jpeg|gif|svg\+xml|webp)/))
                return false
        }
        return true
    }
    return false
}
const validateImgSize = (values)=> {
    if(values.length>0){
        for(let i = 0 ; i<values.length; i++){
            console.log('size',values[i].size)
            if(values[i].size > 10000000)
                return false
        }
        return true
    }
    return false
}
const categorySchema = yup.object().shape({
    name       : yup.object().shape({
        en:yup.string().required('name is required').min(4),
        ar:yup.string().required('name is required').min(4) 
    }),
    imageFile  : yup.mixed().required('image is required')
                .test("fileSize", "image maximum size must be 10MB",validateImgSize)
                .test("fileType","must choose an image",validateImgType)
    
})
const page=[
    {key:{en:"Categories",ar:"الأقسام"},path:"/categories"},
    {key:{en:"Add Category",ar:"اضافة قسم"},path:"/categories/add"}

]
const CategoryAddForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const addCategory = (v,e) => {
        axios.post('/api/category',v,{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>{
            console.log(res.data)
            toast.success("added successfully")
        }).catch(err=>{
            console.log(err.message)
        })
    }
    const options = {
        form:'category-addform',id:"categoryForm", title:{value:"Add Category"},
        submitBtn:{value:"Add",options:{id:"submitBtn"}},
    }
    const fields = [
        {type:'file',name:"image",label:"Category Image",id:"categoryImage",dir:"category"},
        {type:'text',name:"name",locale:"en",placeholder:"Name in English",id:"name-en", col:{md:"6"}},
        {type:'text',name:"name",locale:"ar",placeholder:"Name in Arabic",id:"name-ar", col:{md:"6"}},
    ]
    
    return (
        <div className="category-page">
            <Container fluid>
                <div className="content">
                    <CustomForm 
                        config={options}
                        fields={fields}
                        validation={categorySchema}
                        submit={addCategory}
                        />
                </div>
            </Container>
        </div>
    )
}


export default CategoryAddForm