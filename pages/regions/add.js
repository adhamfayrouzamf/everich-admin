/* style / images */


/* methods / packages */
import { useContext } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext';
import * as yup from 'yup'

/* components */
// import { FaStar, FaShoppingCart, FaPlus, FaMinus } from 'react-icons/fa'
import { Container } from 'reactstrap'
import CustomForm from "../../components/CustomForm/CustomForm"

const regionSchema = yup.object().shape({
    name       : yup.object().shape({
        en:yup.string().required('name is required').min(4),
        ar:yup.string().required('name is required').min(4) 
    }),

    city : yup.string().required('must choose city'),
})

const page=[
    {key:{en:"Regions",ar:"الأحياء"},path:"/regions"},
    {key:{en:"Add Region",ar:"اضافة حي"},path:"/regions/add"}
]
const AddRegionForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const addRegion = (v,e) => {
        axios.post('/api/region',{...v}).then(res=>{
            console.log("done")
        }).catch(err=>{
            console.log(err.message)
        })
    }
    const options = {
        form:'regionAddFrom',id:"regionForm", title:{value:"Add Region"},
        submitBtn:{value:"Add",options:{id:"submit-btn"}},
    }
    const fields = [
        {type:'text',name:"name",locale:"en",placeholder:"Name in English",id:"name-en",col:{md:"6"}},
        {type:'text',name:"name",locale:"ar",placeholder:"Name in Arabic",id:"name-ar",col:{md:"6"}},
        {type:"select",name:"city",placeholder:"Choose City",id:"city",fetch_url:"/api/city?name=",option:{value:"id",label:[{key:"name",localized:true},{key:"country",childs:["name"],localized:true}]},col:{md:"6"}},
        // {type:"select",name:"country",placeholder:"Choose Country",id:"country",choices:choices},
    ]
    return (
        <div className="region-page">
            <Container fluid>
                <div className="content">
                    <CustomForm 
                        config={options}
                        fields={fields}
                        validation={regionSchema}
                        submit={addRegion}
                    />
                </div>
            </Container>
        </div>
    )
}


export default AddRegionForm