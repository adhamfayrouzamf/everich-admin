/* style / images */


/* methods / packages */
import { useContext } from 'react'
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext';

/* components */
import { Container } from 'reactstrap'
import CustomTable from '../../components/CustomTable/CustomTable';

const page=[
    {key:{en:"Regions",ar:"الأحياء"},path:"/regions"}
]
const RegionTable = props => {
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const options={
        title:{value:"Regions"},
        addBtn:{value:"Add Region"},
        url:"/regions",api_url:"/api/region",fetch_url:"/api/region",
        cols:[
            {key:"id",label:"#",check:true},
            {key:"name",label:"Name",localized:true},
            {key:"city",label:"City",childs:["name"],localized:true},
            {key:"city",label:"Country",childs:["country","name"],localized:true},
            {key:"createdAt",label:"Date",isDate:true}
        ],
        controls:[
            "DETAILS", "UPDATE", "DELETE"
        ]
    }


    return (
        <div className="region-page">
            <Container fluid>
                <div className="content">
                    <CustomTable options={options} />
                </div>
            </Container>
        </div>
    )
}


export default RegionTable