/* style / images */


/* methods / packages */
import { useContext, useState, useEffect } from 'react'
import axios from 'axios'
import { useRouter } from "next/router"
import { UserContext } from '../../../components/contexts/AuthContext'
import { PageContext } from '../../../components/contexts/PageContext';
import { toast } from 'react-toastify'
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../../components/CustomForm/CustomForm"
import Loader from "../../../components/Loader"


const regionSchema = yup.object().shape({
    name       : yup.object().shape({
        en:yup.string().required('name is required'),
        ar:yup.string().required('name is required') 
    }),
    city : yup.string().required('must choose city'),
    
})

const page=[
    {key:{en:"Regions",ar:"الأحياء"},path:"/regions"},
    {key:{en:"Edit Region",ar:"تعديل الحي"},path:"/regions/[regionId]/edit"}
]

const RegionEditForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const [region,setRegion] = useState()
    const editRegion = (v,e) => {
        console.log(v)
        const {regionId} = route.query
        axios.put(`/api/region/${regionId}`,v,{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>{
            console.log(res.data)
            toast.success("updated successfully")
        }).catch(err=>{
            console.log(err.message)
        })
    }

    const options = {
        form:'region-editform',id:"regionForm", title:{value:"Edit region"},
        submitBtn:{value:"Edit",options:{id:"submitBtn"}}
    }
    const fields=[
        {type:'text',name:"name",locale:"en",placeholder:"Name in English",id:"name-en", col:{md:"6"}},
        {type:'text',name:"name",locale:"ar",placeholder:"Name in Arabic",id:"name-ar", col:{md:"6"}},
        {type:"select",name:"city",placeholder:"Choose City",id:"city",fetch_url:"/api/city?name=",option:{value:"id",label:[{key:"name",localized:true}]},col:{md:"6"}},
    ]
    useEffect(()=>{
        const {regionId} = route.query
        axios.get(`/api/region/${regionId}`).then(res=>{
            const {data} = res
            setRegion({
                name:data.name,
                city:data.city,
            })
        }).catch(err=>console.error(err))
    },[route.query.regionId])
    return (
        <div className="region-page">
            <Container fluid>
                <div className="content">
                    <Loader Render={true} Show={region}>
                        <CustomForm 
                            config={options} 
                            fields={fields} 
                            validation={regionSchema} 
                            values={region}
                            submit={editRegion} />
                    </Loader>
                </div>
            </Container>
        </div>
    )
}


export default RegionEditForm