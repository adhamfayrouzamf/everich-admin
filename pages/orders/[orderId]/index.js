/* style / images */

/* methods / packages */
import { useContext, useEffect, useState } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"

import { UserContext } from '../../../components/contexts/AuthContext'
import { PageContext } from "../../../components/contexts/PageContext"

/* components */
import { Container, TabContent, TabPane, Nav, NavItem, NavLink, Table, Row, Col, Card, CardHeader, CardBody, Button } from 'reactstrap';
import Loader from '../../../components/Loader';
import Moment from "react-moment"
import "moment/locale/ar"
import {FaEye} from "react-icons/fa"
import Image from "next/image"
import Link from "next/link"

const page=[
    {key:{en:"Orders",ar:"الطلبات"},path:"/orders"},
    {key:{en:"Order Details",ar:"تفاصيل الطلب"},path:"/orders/[orderId]"}
]
const ViewOrderDetails = () => {
    const route = useRouter()
    const admin = useContext(UserContext)
    const [order, setOrder] = useState({})
    const [products,setProducts]= useState([])
    const [activeTab, setActiveTab] = useState("1")
    const [isLoading, setIsLoading] = useState(true)
    const {setPage} = useContext(PageContext)
    setPage(page)

    useEffect(()=>{
        const {orderId} = route.query
        if(orderId){
            axios.get(`/api/order/${orderId}`).then(res=>{
                console.log(res.data)
                setOrder(res.data)
                setProducts(res.data.products)
                setIsLoading(false)
            }).catch(err=>console.error(err))
            
        }

    },[route.query.orderId])

    return (
        <div className="order-page">
            <Container fluid>
                <div className="content">
                    <Nav tabs>
                        <NavItem>
                            <NavLink
                                onClick={() => { setActiveTab('1'); }}
                                className={activeTab === "1" ?"active":"clickable"}
                            >
                                Basic Info
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                onClick={() => { setActiveTab('2'); }}
                                className={activeTab === "2" ? "active" : "clickable"}
                            >
                                Reciept
                            </NavLink>
                        </NavItem>
                    </Nav>
                    <Loader Show={!isLoading} Render={true}>
                        <TabContent activeTab={activeTab}>
                            <TabPane tabId="1">
                                <h3>Order Number: {order.orderNumber}</h3>
                                <div className="order_data">
                                    <Row>
                                        <Col lg="6">
                                            <Card>
                                                <CardHeader className="d-flex justify-content-between align-items-center">
                                                    <span>Order State</span>
                                                    <Moment locale={route.locale} element="span" date={order.createdAt} format="DD-MM-YYYY" />
                                                </CardHeader>
                                                <CardBody>
                                                    {order.status}
                                                </CardBody>
                                            </Card>
                                        </Col>
                                        <Col lg="6">
                                            <Card>
                                                <CardBody>
                                                    <div>
                                                        <span>Items Count: </span><span>{products.length}</span>
                                                    </div>
                                                    <div>
                                                        <span>Payment Method: </span><span>{order.paymentMethod}</span>
                                                    </div>
                                                </CardBody>
                                            </Card>
                                        </Col>
                                        <Col lg="6">
                                            <Card>
                                                <CardBody>
                                                    <div>
                                                        <span>Price: </span><span>{order.totalPrice}</span>
                                                        <span>Price After Discount: </span><span>{order.totalPriceAfterDiscount}</span>
                                                        <span>Price After Taxes: </span><span>{order.totalPriceAfterTaxes}</span>
                                                        <span>Final Price: </span><span>{order.finalPrice}</span>
                                                    </div>
                                                    {/* <div>
                                                        <span>Payment Method: </span><span>{order.price}</span>
                                                    </div> */}
                                                </CardBody>
                                            </Card>
                                        </Col>
                                        <Col lg="6">
                                            <Card>
                                                <CardHeader>
                                                    <h6>Address</h6>
                                                </CardHeader>
                                                <CardBody>
                                                    Addresses
                                                </CardBody>
                                            </Card>
                                        </Col>
                                    </Row>
                                </div>
                                {products.length && <>
                                    <h4>Items ({products.length})</h4>
                                    <Table>
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Image</th>
                                                <th>Product Name</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th>Offer</th>
                                                <th>Trademark</th>
                                                <th>Category</th>
                                                <th>Sub Category</th>
                                                <th>Controls</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {products.map((p,index)=>{
                                                const {product,quantity} = p
                                                return(
                                                <tr key={product.id}>
                                                    <td>{index+1}</td>
                                                    <td><Image src={product.image.url} alt="product_image" width={100} height={100} /></td>
                                                    <td>{product.name[route.locale]}</td>
                                                    <td>{product.priceAfterOffer}</td>
                                                    <td>{quantity}</td>
                                                    <td>{product.offer} %</td>
                                                    <td>{product.trademark.name[route.locale]}</td>
                                                    <td>{product.category.name[route.locale]}</td>
                                                    <td>{product.subCategory.name[route.locale]}</td>
                                                    <td>
                                                        <Link href={`/products/${product.id}`} passHref>
                                                            <Button className="mx-1" tag="a"><FaEye /></Button>
                                                        </Link>
                                                    </td>
                                                </tr>
                                            )})}
                                        </tbody>
                                    </Table>
                                </>
                                }
                            </TabPane>
                            <TabPane tabId="2">
                                Reciept
                            </TabPane>
                            
                        </TabContent>
                    </Loader>
                </div>
            </Container>
        </div>
    )
}


export default ViewOrderDetails