/* style / images */
// import styles from './OrderTable.scss';

/* methods / packages */
import React, { useState, useContext } from 'react'
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from "../../components/contexts/PageContext"

/* components */
import { Container } from 'reactstrap'
import CustomTable from '../../components/CustomTable/CustomTable';
import axios from 'axios';

const page=[
    {key:{en:"Orders",ar:"الطلبات"},path:"/orders"}
]
const OrderTable = props => {
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    

    const AcceptOrRejectOrder = (value,id)=>{
        return axios.put(`/api/order/${id}/acceptOrReject`,{status:value},{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>res)
    }
    const PrepareOrder = (value,id)=>{
        return axios.put(`/api/order/${id}/prepared`,{status:value},{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>res)
    }
    const ShipOrder = (value,id)=>{
        return axios.put(`/api/order/${id}/shipped`,{status:value},{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>res)
    }
    const OnTheWayOrder = (value,id)=>{
        return axios.put(`/api/order/${id}/onTheWay`,{status:value},{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>res)
    }
    const DeliverOrder = (value,id)=>{
        return axios.put(`/api/order/${id}/delivered`,{status:value},{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>res)
    }

    const options={
        title:{value:{en:"Orders",ar:"الطلبات"}},
        url:"/orders", api_url:"/api/order",fetch_url:"/api/order",
        cols:[
            {key:"id",label:{ar:"#",en:"#"},check:true},
            {key:"orderNumber",label:{ar:"رقم الطلب",en:"Order Number"}},
            {key:"user",label:{ar:"العميل",en:"User"},childs:["username"]},
            {key:"paymentMethod",label:{ar:"طريقة الدفع",en:"Payment Method"}},
            {key:"finalPrice",label:{ar:"السعر الاجمالي",en:"Total Price"}},
            // {key:"shipping",label:{ar:"سعر التوصيل",en:"Transport Price"},childs:["shippingPrice"]},
            {key:"createdAt",label:{ar:"تاريخ الإنشاء",en:"Date"},isDate:true},
            {key:"status",label:{ar:"الحالة",en:"Status"},isToggler:true,
                choices:[
                    {value:"WAITING",onClick:AcceptOrRejectOrder,onReject:AcceptOrRejectOrder,label:{ar:"قبول / رفض",en:"Accept / Reject"},toState:1,tooltip:"WAITING",
                    modal:{message:"Set Order to accepted or rejected",title:"Order Acceptance",confirmBtn:"Accept",rejectBtn:"Reject"}
                    },
                    {value:"ACCEPTED",onClick:PrepareOrder,label:{ar:"تحضير",en:"Prepare"},toState:2,tooltip:"ACCEPTED",
                    modal:{message:"Are You Sure You Want to Prepare This Order",title:"Prepare Order",confirmBtn:"Prepare"}
                    },
                    {value:"PREPARED",onClick:ShipOrder,label:{ar:"شحن",en:"Ship"},toState:3,tooltip:"PREPARED",
                    modal:{message:"Are You Sure You Want to Ship This Order",title:"Ship Order",confirmBtn:"Ship"}
                    },
                    {value:"SHIPPED",onClick:OnTheWayOrder,label:{ar:"في الطريق",en:"On The Way"},toState:4,tooltip:"SHIPPED",
                    modal:{message:"Are You Sure You Want to set This Order to On The Way",title:"On The Way",confirmBtn:"Set"}
                    },
                    {value:"ONTHEWAY",onClick:DeliverOrder,label:{ar:"توصيل",en:"Deliver"},toState:5,tooltip:"ONTHEWAY",
                    modal:{message:"Are You Sure You Want to Deliver This Order",title:"Deliver Order",confirmBtn:"Deliver"}
                    },
                    {value:"DELIVERED",toState:null},
                ]
            }
        ],
        controls:["view","delete"]
    }
    



    return (
        <div className="order-page">
            <Container fluid>
                <div className="content">
                    <CustomTable options={options} />
                </div>
            </Container>
        </div>
    )
}


export default OrderTable