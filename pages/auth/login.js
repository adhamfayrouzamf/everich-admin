/* style / images */
import styles from '../../styles/AuthLayout/Login.module.scss'

/* files / packages */
import {useContext} from 'react'
import { UserContext, LoginContext } from '../../components/contexts/AuthContext'
import * as yup from "yup"
import {useRouter} from 'next/router'


/* components */
import CustomForm from '../../components/CustomForm/CustomForm'

const loginSchema = yup.object().shape({
    email:yup.string().required('Email is required').email('Email is invalid'),
    password:yup.string().required('password is required'),
    type:yup.string().nullable().required('type is required')
})

const LoginPage = ()=>{
    const route = useRouter()
    const user = useContext(UserContext)
    const login = useContext(LoginContext)
    
    const options = {
        form:'login-form',className:styles.loginForm,
        submitBtn:{value:"Login",options:{id:styles.submitBtn}},
    }
    const fields = [
        {type:'text',name:"email",placeholder:"Email",id:"email",formGroup:{className:styles.field},col:{xs:"12"}},
        {type:'password',name:"password",placeholder:"Password",id:"password",formGroup:{className:styles.field},col:{xs:"12"}},
        {type:'radio',name:"type",choices:[{label:"Admin",value:"ADMIN",id:"admin",},{label:"Sub-Admin",value:"SUB_ADMIN",id:"subadmin",}],
        fieldset:{value:"Admin Type", options:{className:styles.adminLegend}},formGroup:{id:styles.adminType,className:styles.field}
        ,col:{xs:"12"}
        },
    ]
    return (
        <CustomForm 
            config={options}
            submit={login}
            validation={loginSchema}
            fields={fields}
        />
    );
}


export default LoginPage;
