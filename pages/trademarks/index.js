/* style / images */

/* methods / packages */
import { useContext } from 'react'
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from "../../components/contexts/PageContext"

/* components */
import { Container } from 'reactstrap'
import CustomTable from '../../components/CustomTable/CustomTable';


const page=[
    {key:{en:"Trademarks",ar:"العلامات التجارية"},path:"/trademarks"}
]
const TrademarkTable = props => {
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const options={
        title:{value:{ar:"العلامات التجارية",en:"Trademarks"}},
        addBtn:true,
        url:"/trademarks",api_url:"/api/trademark",fetch_url:"/api/trademark",
        cols:[
            {key:"id",label:{ar:"#",en:"#"},check:true},
            {key:"name",label:{ar:"الاسم",en:"Name"},localized:true},
            {key:"category",label:{ar:"القسم",en:"Category"},childs:["name"],localized:true},
            {key:"createdAt",label:{ar:"تاريخ الإنشاء",en:"Date"},isDate:true}
        ],
        controls:[
            "DETAILS", "UPDATE", "DELETE"
        ]
    }

    return (
        <div className="trademark-page">
            <Container fluid>
                <div className="content">
                    <CustomTable options={options} />
                </div>
            </Container>
        </div>
    )
}


export default TrademarkTable