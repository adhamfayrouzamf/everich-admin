/* style / images */

/* methods / packages */
import { useContext, useEffect, useState } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"

import { UserContext } from '../../../components/contexts/AuthContext'
import { PageContext } from "../../../components/contexts/PageContext"

/* components */
import { Container, TabContent, TabPane, Nav, NavItem, NavLink, Table } from 'reactstrap';
import Loader from '../../../components/Loader';
import { useFetchOne } from "../../../components/hooks/fetch-hook"
import CustomTable from "../../../components/CustomTable/CustomTable"
import Image from "next/image"

const page=[
    {key:{en:"Trademarks",ar:"العلامات التجارية"},path:"/trademarks"},
    {key:{en:"Trademark Details",ar:"تفاصيل العلامة التجارية"},path:"/trademarks/[trademarkId]"}
]
const ViewTrademarkDetails = () => {
    const route = useRouter()
    const admin = useContext(UserContext)
    const [trademark, error, isLoading] = useFetchOne(`/api/subcategory/${route.query.trademarkId}`)
    const [productsTable,setProductsTable]= useState()
    const [activeTab, setActiveTab] = useState("1")
    const {setPage} = useContext(PageContext)
    setPage(page)

    useEffect(()=>{
        const {trademarkId} = route.query
        if(trademarkId){
            setProductsTable({
                url:"/products",api_url:`/api/product?trademark=${trademarkId}`,fetch_url:`/api/product?trademark=${trademarkId}`,
                cols:[
                    {key:"id",label:"#"},{key:"serialNumber",label:"Serial Number"},
                    {key:"name",label:"Name",localized:true},
                    {key:"category",label:"Category",localized:true,childs:["name"]},
                    {key:"subCategory",label:"Sub Category",localized:true,childs:["name"],notFound:"No Sub Category"},
                    {key:"useStatus",label:"Status"},
                    {key:"totalRate",label:"Rate",notFound:"0"},
                    {key:"image",label:"Image",isImage:true},
                    {key:"createdAt",label:"Date",isDate:true}
                ],
                controls:[
                    "view"
                ]
            })
        }

    },[route.query.trademarkId])

    return (
        <div className="trademark-page">
            <Container fluid>
                <div className="content">
                    <Nav tabs>
                        <NavItem>
                            <NavLink
                                onClick={() => { setActiveTab('1'); }}
                                className={activeTab === "1" ?"active":"clickable"}
                            >
                                Basic Info
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                onClick={() => { setActiveTab('2'); }}
                                className={activeTab === "2" ? "active" : "clickable"}
                            >
                                Products
                            </NavLink>
                        </NavItem>
                    </Nav>
                    <Loader Show={!isLoading} Render={true}>
                        <TabContent activeTab={activeTab}>
                            <TabPane tabId="1">
                                {trademark.name && <h3>{trademark.name[route.locale]}</h3>}
                            </TabPane>
                            <TabPane tabId="2">
                                {productsTable && <CustomTable options={productsTable} />}
                            </TabPane>
                            
                        </TabContent>
                    </Loader>
                </div>
            </Container>
        </div>
    )
}


export default ViewTrademarkDetails