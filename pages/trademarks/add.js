/* style / images */

/* methods / packages */
import { useContext } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from "../../components/contexts/PageContext"
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../components/CustomForm/CustomForm"

const trademarkSchema = yup.object().shape({
    name       : yup.object().shape({
        en:yup.string().required('name is required').min(4),
        ar:yup.string().required('name is required').min(4) 
    }),
    category : yup.string().required('must choose category'),
    
})

const page=[
    {key:{en:"Trademarks",ar:"العلامات التجارية"},path:"/trademarks"},
    {key:{en:"Add Trademark",ar:"اضافة علامة تجارية"},path:"/trademarks/add"}
]
const AddTrademarkForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)

    const addTrademark = (v,e) => {
        axios.post('/api/trademark',{...v}).then(res=>{
            console.log("done")
        }).catch(err=>{
            console.log(err.message)
        })
    }
    const options = {
        form:'trademark-form',id:"trademark-form", title:{value:"Add Trademark"},
        submitBtn:{value:"Add",options:{id:"submit-btn"}},
    }
    const fields = [
        {type:'text',name:"name",locale:"en",placeholder:"Name in English",id:"name-en",col:{md:"6"}},
        {type:'text',name:"name",locale:"ar",placeholder:"Name in Arabic",id:"name-ar",col:{md:"6"}},
        {type:"select",name:"category",placeholder:"Choose Category",id:"category",fetch_url:"/api/category?name=",option:{value:"id",label:[{key:"name",localized:true}]},col:{md:"6"}}
    ]

    return (
        <div className="trademark-page">
            <Container fluid>
                <div className="content">
                    <CustomForm 
                        config={options}
                        fields={fields}
                        validation={trademarkSchema}
                        submit={addTrademark}
                    />
                </div>
            </Container>
        </div>
    )
}


export default AddTrademarkForm