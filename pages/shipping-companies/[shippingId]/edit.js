/* style / images */

/* methods / packages */
import { useContext, useState, useEffect, useRef } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"
import { UserContext } from '../../../components/contexts/AuthContext'
import { PageContext } from "../../../components/contexts/PageContext"
import {setValue} from "react-select/async"
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../../components/CustomForm/CustomForm"

const shippingSchema = yup.object().shape({
    name       : yup.object().shape({
        en:yup.string().required('name is required'),
        ar:yup.string().required('name is required')
    }),
    shippingPrice: yup.number().typeError('shipping price requires a number').positive('shipping price must be positive')

})

const page=[
    {key:{en:"Shipping Companies",ar:"شركات الشحن"},path:"/shipping-companies"},
    {key:{en:"Edit Shipping Company",ar:"تعديل شركة شحن"},path:"/shipping-companies/[shippingId]/edit"}
]
const EditShippingForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const [shipping,setShipping] = useState()
    const editShipping = (v,e) => {
        const {shippingId} = route.query
        axios.put(`/api/shipping/${shippingId}`,{...v},{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>{
            console.log("done")
        }).catch(err=>{
            console.log(err.message)
        })
    }
    useEffect(()=>{
        const {shippingId} = route.query
        axios.get(`/api/shipping/${shippingId}`).then(res=>{
            const {data} = res
            setShipping({
                name:data.name,
                shippingPrice:data.shippingPrice
            })
        }).catch(err=>console.error(err))
    },[route.query.shippingId])
    const options = {
        form:'shipping-editform',id:"shippingForm", title:{value:"Edit Shipping Company"},
        submitBtn:{value:"Edit",options:{id:"submitBtn"}}
    }
    const fields = [
        {type:'text',name:"name",locale:"en",placeholder:"Name in English",id:"name-en",col:{md:"6"}},
        {type:'text',name:"name",locale:"ar",placeholder:"Name in Arabic",id:"name-ar",col:{md:"6"}},
        {type:'text',name:"shippingPrice",placeholder:"Transport Fees",id:"transport-price",col:{md:"6"}},
    ]

    return (
        <div className="shipping-page">
            <Container fluid>
                <div className="content">
                    <CustomForm 
                        config={options}
                        fields={fields}
                        validation={shippingSchema}
                        submit={editShipping}
                        values={shipping}
                    />
                </div>
            </Container>
        </div>
    )
}


export default EditShippingForm