/* style / images */

/* methods / packages */
import { useContext, useEffect, useState } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"

import { UserContext } from '../../../components/contexts/AuthContext'
import { PageContext } from "../../../components/contexts/PageContext"
import { useFetchOne } from "../../../components/hooks/fetch-hook"
/* components */
import { Container, Nav, NavItem, NavLink } from 'reactstrap';
import Loader from '../../../components/Loader';
// import Moment from "react-moment"
// import "moment/locale/ar"
import CustomTable from "../../../components/CustomTable/CustomTable"


const page=[
    {key:{en:"Shipping Companies",ar:"شركات الشحن"},path:"/shipping-companies"},
    {key:{en:"Shipping Company Details",ar:"تفاصيل شركة الشحن"},path:"/shipping-companies/[shippingId]"}
]
const ViewShippingCompanyDetails = () => {
    const route = useRouter()
    const admin = useContext(UserContext)
    const [shipping, error, isLoading] = useFetchOne(`/api/shipping/${route.query.shippingId}`)
    const {setPage} = useContext(PageContext)
    setPage(page)


    return (
        <div className="shipping-page">
            <Container fluid>
                <div className="content">
                    <Loader Show={!isLoading} Render={true}>
                        {shipping.name && <h3>{shipping.name[route.locale]}</h3>}
                        {shipping.shippingPrice}
                    </Loader>
                </div>
            </Container>
        </div>
    )
}


export default ViewShippingCompanyDetails