/* style / images */


/* methods / packages */
import { useContext } from 'react'
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext';


/* components */
import { Container } from 'reactstrap'
import CustomTable from '../../components/CustomTable/CustomTable';

const page=[
    {key:{en:"Shipping Companies",ar:"شركات الشحن"},path:"/shipping-companies"}
]
const ShippingTable = props => {
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const options={
        title:{value:{en:"Shipping Companies",ar:"شركات الشحن"}},
        addBtn:true,
        url:"/shipping-companies",api_url:"/api/shipping",fetch_url:"/api/shipping",
        cols:[
            {key:"id",label:{ar:"#",en:"#"},check:true},
            {key:"name",label:{ar:"الاسم",en:"Name"},localized:true},
            {key:"shippingPrice",label:{ar:"مصاريف الشحن",en:"Shpping Fees"}},
        ],
        controls:[
            "DETAILS", "UPDATE", "DELETE"
        ]
    }

    return (
        <div className="shipping-page">
            <Container fluid>
                <div className="content">
                    <CustomTable options={options} />
                </div>
            </Container>
        </div>
    )
}


export default ShippingTable