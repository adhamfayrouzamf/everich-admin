/* style / images */


/* methods / packages */
import { useContext } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from "../../components/contexts/PageContext"
import { toast } from 'react-toastify'
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../components/CustomForm/CustomForm"

const shippingSchema = yup.object().shape({
    name       : yup.object().shape({
        en:yup.string().required('name is required'),
        ar:yup.string().required('name is required')
    }),
    shippingPrice: yup.number().typeError('shipping price requires a number').positive('shipping price must be positive')

})

const page=[
    {key:{en:"Shipping Companies",ar:"شركات الشحن"},path:"/shipping-companies"},
    {key:{en:"Add Shipping Company",ar:"اضافة شركة شحن"},path:"/shipping-companies/add"}
]

const AddShippingForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)

    const addShipping = (v,e) => {
        
        axios.post('/api/shipping',v).then(res=>{
            console.log(res.data)
            toast.success("added successfully")
        }).catch(err=>{
            console.log(err.message)
        })
    }
    const options = {
        form:'shipping-addform',id:"shippingForm", title:{value:"Add Shipping Company"},
        submitBtn:{value:"Add",options:{id:"submitBtn"}},
    }
    const fields = [
        {type:'text',name:"name",locale:"en",placeholder:"Name in English",id:"name-en",col:{md:"6"}},
        {type:'text',name:"name",locale:"ar",placeholder:"Name in Arabic",id:"name-ar",col:{md:"6"}},
        {type:'text',name:"shippingPrice",placeholder:"Transport Fees",id:"transport-price",col:{md:"6"}},
    ]

    return (
        <div className="shipping-page">
            <Container fluid>
                <div className="content">
                    <CustomForm 
                        config={options}
                        fields={fields}
                        validation={shippingSchema}
                        submit={addShipping}
                    />
                </div>
            </Container>
        </div>
    )
}


export default AddShippingForm