import handler from "../../../handler/handler"
import countryController from "../../../controllers/country.controller/country.controller"

import {requireAdmin} from '../../../services/passport';
const router = handler()

router
    .get(countryController.findById)
    .put(requireAdmin,countryController.validateBody(true),countryController.update)
    .delete(requireAdmin,countryController.delete)



export default router;