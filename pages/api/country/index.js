import handler from "../../../handler/handler"
import countryController from "../../../controllers/country.controller/country.controller"

import {requireAdmin} from '../../../services/passport';
const router = handler()

router
    .get(countryController.findAll)
    .post(requireAdmin,countryController.validateBody(),countryController.create)



export default router;