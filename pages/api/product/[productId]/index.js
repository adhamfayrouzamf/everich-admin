// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import handler from "../../../../handler/handler"
import productController from "../../../../controllers/product.controller/product.controller"
import ApiError from '../../../../helpers/ApiError'
import {checkExistThenGet} from '../../../../helpers/CheckMethods'
import {requireAdmin} from "../../../../services/passport"
const router = handler()

router
    .get(productController.findById)
    .put(requireAdmin, productController.validateBody(true), productController.update)
    .delete(requireAdmin, productController.delete)


export default router
