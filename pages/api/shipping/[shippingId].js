import handler from "../../../handler/handler"
import shippingController from "../../../controllers/shipping.controller/shipping.controller"

import {requireAdmin} from '../../../services/passport';
const router = handler()

router
    .get(shippingController.findById)
    .put(requireAdmin,shippingController.validateBody(true),shippingController.update)
    .delete(requireAdmin,shippingController.delete)



export default router;