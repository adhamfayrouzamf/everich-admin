import handler from "../../../handler/handler"
import shippingController from "../../../controllers/shipping.controller/shipping.controller"

import {requireAdmin} from '../../../services/passport';
const router = handler()

router
    .get(shippingController.findAll)
    .post(requireAdmin,shippingController.validateBody(),shippingController.create)



export default router;