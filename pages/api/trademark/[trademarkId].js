import handler from "../../../handler/handler"
import tradeMarkController from "../../../controllers/tradeMark.controller/tradeMark.controller"

import {requireAdmin} from '../../../services/passport';
const router = handler()

router
    .get(tradeMarkController.findById)
    .put(requireAdmin,tradeMarkController.validateBody(true),tradeMarkController.update)
    .delete(requireAdmin,tradeMarkController.delete)



export default router;