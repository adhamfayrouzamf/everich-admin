import handler from "../../../handler/handler"
import tradeMarkController from "../../../controllers/tradeMark.controller/tradeMark.controller"

import {requireAdmin} from '../../../services/passport';
const router = handler()

router
    .get(tradeMarkController.findAll)
    .post(requireAdmin,tradeMarkController.validateBody(),tradeMarkController.create)



export default router;