
import handler from "../../../handler/handler"
import advertismentController from "../../../controllers/advertisments.controller/advertisments.controller"

import {requireAdmin} from '../../../services/passport';


const router = handler()

router    
    .post(requireAdmin,advertismentController.validateBody(),advertismentController.create)
    .get(advertismentController.findAll);



export default router;