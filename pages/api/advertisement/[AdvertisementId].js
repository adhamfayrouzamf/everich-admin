
import handler from "../../../handler/handler"
import advertismentController from "../../../controllers/advertisments.controller/advertisments.controller"

import {requireAdmin} from '../../../services/passport';


const router = handler()

router
    .put(requireAdmin,advertismentController.validateBody(true),advertismentController.update)
    .delete(requireAdmin,advertismentController.delete)
    .get(advertismentController.findById)



export default router;