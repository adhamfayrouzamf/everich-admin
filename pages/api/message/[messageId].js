import handler from "../../../handler/handler"
import messageController from "../../../controllers/message.controller/message.controller"

import {requireAuth} from '../../../services/passport';
const router = handler()

router
    .get(/* requireAuth, */messageController.getById)
    .delete(requireAuth, messageController.deleteForEveryOne)



export default router;