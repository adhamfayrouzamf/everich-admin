import handler from "../../../handler/handler"
import messageController from "../../../controllers/message.controller/message.controller"

import {requireAuth} from '../../../services/passport';
const router = handler()

router
    .post(requireAuth, /* multerSaveTo('chat').single('file'), */  messageController.validate(), messageController.create )
    .get(requireAuth,messageController.getMyChat)



export default router;