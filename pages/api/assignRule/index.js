import handler from "../../../handler/handler"
import assignRuleController from "../../../controllers/assignRule.controller/assignRule.controller";
import {requireAdmin} from '../../../services/passport';

const router = handler()

router
    .get(requireAdmin,assignRuleController.findAll)
    .post(requireAdmin,assignRuleController.validateBody(),assignRuleController.create)



export default router;