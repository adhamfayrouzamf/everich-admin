import handler from "../../../handler/handler"
import assignRuleController from "../../../controllers/assignRule.controller/assignRule.controller";
import {requireAdmin} from '../../../services/passport';

const router = handler()

router
    .get(requireAdmin,assignRuleController.findById)
    .put(requireAdmin,assignRuleController.validateBody(true),assignRuleController.update)
    .delete(requireAdmin,assignRuleController.delete)

export default router;