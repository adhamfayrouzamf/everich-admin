import handler from "../../../../handler/handler"
import NotifController from '../../../../controllers/notification.controller/notification.controller';
import {requireAuth} from '../../../../services/passport';

const router = handler()

router
    .put(requireAuth, NotifController.read)


export default router;