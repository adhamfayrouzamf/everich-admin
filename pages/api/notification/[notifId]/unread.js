import handler from "../../../../handler/handler"
import NotifController from '../../../../controllers/notification.controller/notification.controller';
import {requireAuth} from '../../../../services/passport';

const router = handler()

router
    .put(requireAuth, NotifController.unread);


export default router;