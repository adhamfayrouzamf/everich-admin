import handler from "../../../../handler/handler"
import NotifController from '../../../../controllers/notification.controller/notification.controller';
import {requireAuth} from '../../../../services/passport';

const router = handler()

router
    .get(NotifController.findById)
    .delete(requireAuth, NotifController.delete);




export default router;