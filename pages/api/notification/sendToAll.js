import handler from "../../../handler/handler"
import NotifController from '../../../controllers/notification.controller/notification.controller';
import {requireAdmin} from '../../../services/passport';

const router = handler()

router.post(
    requireAdmin, 
    NotifController.validateAdminSendToAll(), 
    NotifController.adminSendToAllUsers)


export default router;