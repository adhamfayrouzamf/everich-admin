import handler from "../../../handler/handler"
import NotifController from '../../../controllers/notification.controller/notification.controller';
import {requireAuth} from '../../../services/passport';

const router = handler()

router
.get(requireAuth, NotifController.findMyNotification);

// router.route('/user-delete/:notifId').delete(requireAuth,NotifController.userDelete)

export default router;