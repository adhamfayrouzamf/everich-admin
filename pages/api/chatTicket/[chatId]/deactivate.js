import handler from "../../../../handler/handler"
import chatTicketController from "../../../../controllers/chatTicket.controller/chatTicket.controller"

import {requireAdmin} from '../../../../services/passport';
const router = handler()

router
    .put(requireAdmin, chatTicketController.deactivate)



export default router;