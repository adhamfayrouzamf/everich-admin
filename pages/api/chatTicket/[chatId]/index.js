import handler from "../../../../handler/handler"
import chatTicketController from "../../../../controllers/chatTicket.controller/chatTicket.controller"

import {requireAuth} from '../../../../services/passport';
const router = handler()

router
    .get(/* requireAuth, */chatTicketController.findById)
    .put(chatTicketController.validateBody(),chatTicketController.update)
    .delete(/* requireAuth, */ chatTicketController.delete)



export default router;