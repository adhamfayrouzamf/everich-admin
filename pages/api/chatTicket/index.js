import handler from "../../../handler/handler"
import chatTicketController from "../../../controllers/chatTicket.controller/chatTicket.controller"

import {requireAuth} from '../../../services/passport';
const router = handler()

router
    .post(requireAuth,  chatTicketController.validateBody(), chatTicketController.create )
    .get(chatTicketController.findAll)



export default router;