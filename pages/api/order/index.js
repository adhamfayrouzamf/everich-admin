import handler from "../../../handler/handler"
import orderController from "../../../controllers/order.controller/order.controller"

import {requireAuth} from '../../../services/passport';
const router = handler()

router
    .get(orderController.findAll)
    .post(requireAuth,
        orderController.validateBody(),
        orderController.create)



export default router;