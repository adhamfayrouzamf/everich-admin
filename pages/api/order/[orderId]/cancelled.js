import handler from "../../../../handler/handler"
import orderController from "../../../../controllers/order.controller/order.controller"

import {requireAuth} from '../../../../services/passport';

const router = handler()

router
.put(requireAuth, orderController.canceled)




export default router;