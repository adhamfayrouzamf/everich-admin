import handler from "../../../../handler/handler"
import orderController from "../../../../controllers/order.controller/order.controller"

import {requireAuth} from '../../../../services/passport';

const router = handler()

router
    .get(/* requireAuth, */ orderController.findById)
    .delete(requireAuth, orderController.delete)




export default router;