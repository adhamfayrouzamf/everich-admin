import handler from "../../../../handler/handler"
import orderController from "../../../../controllers/order.controller/order.controller"

import {requireAdmin} from '../../../../services/passport';

const router = handler()

router
.put(requireAdmin, orderController.shipped)




export default router;