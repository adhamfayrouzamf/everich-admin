
import handler from "../../../handler/handler"
import commentController from "../../../controllers/comment.controller/comment.controller"

import {requireAuth} from '../../../services/passport';

const router = handler()

router 
    .get(commentController.findById)
    .put(requireAuth,commentController.validateBody(true),commentController.update)
    .delete(requireAuth,commentController.delete)



export default router;