
import handler from "../../../handler/handler"
import commentController from "../../../controllers/comment.controller/comment.controller"

import {requireAuth} from '../../../services/passport';


const router = handler()

router  
    .get(commentController.findAll)
    .post(requireAuth,commentController.validateBody(),commentController.create)



export default router;