import handler from "../../../handler/handler"
import favoritesController from "../../../controllers/favourites.controller/favourites.controller"

import {requireAuth} from '../../../services/passport';
const router = handler()

router
    .get(/* requireAuth, */favoritesController.findById)
    .put(/* requireAuth, */favoritesController.validateBody(true),favoritesController.update)



export default router;