import handler from "../../../handler/handler"
import favoritesController from "../../../controllers/favourites.controller/favourites.controller"

import {requireAuth} from '../../../services/passport';
const router = handler()

router
    .get(favoritesController.findAll)
    .post(requireAuth,favoritesController.validateBody(),favoritesController.create)
    .delete(requireAuth,favoritesController.delete)



export default router;