
import handler from "../../../handler/handler"
import addressController from "../../../controllers/address.controller/address.controller"

import {requireAuth} from '../../../services/passport';

const router = handler()

router    
    .get(addressController.findById)
    .put(requireAuth,addressController.validateBody(true),addressController.update)
    .delete(requireAuth,addressController.delete)



export default router;