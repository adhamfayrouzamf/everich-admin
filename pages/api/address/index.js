import handler from "../../../handler/handler"
import addressController from "../../../controllers/address.controller/address.controller"

import {requireAuth} from '../../../services/passport';
const router = handler()


router
    .get(addressController.findAll)
    .post(requireAuth,addressController.validateBody(),addressController.create)


export default router;