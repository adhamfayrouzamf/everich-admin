import handler from "../../../handler/handler"
import regionController from "../../../controllers/region.controller/region.controller"

import {requireAdmin} from '../../../services/passport';
const router = handler()

router
    .get(regionController.findAll)
    .post(requireAdmin,regionController.validateBody(),regionController.create)



export default router;