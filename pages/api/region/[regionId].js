import handler from "../../../handler/handler"
import regionController from "../../../controllers/region.controller/region.controller"

import {requireAdmin} from '../../../services/passport';
const router = handler()

router
    .get(regionController.findById)
    .put(requireAdmin,regionController.validateBody(true),regionController.update)
    .delete(requireAdmin,regionController.delete)



export default router;