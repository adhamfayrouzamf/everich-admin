import handler from "../../../handler/handler"
import fileController from "../../../controllers/file.controller/file.controller"

import {requireAuth} from '../../../services/passport';
import { fileUploader } from '../../../services/formidable'

const router = handler()

router
    .post(fileUploader,
        fileController.validateBody(), fileController.upload)

export const config = {
    api:{
        bodyParser:false
    }
}

export default router;