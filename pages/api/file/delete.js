import handler from "../../../handler/handler"
import fileController from "../../../controllers/file.controller/file.controller"

import {requireAuth} from '../../../services/passport';
import { multerSaveTo } from '../../../services/multer-service'
import { fileUploader } from '../../../services/formidable'

const router = handler()

router
    .delete(fileController.delete)


export default router;