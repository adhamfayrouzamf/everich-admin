
import handler from "../../../handler/handler"
import cityController from "../../../controllers/city.controller/city.controller"

import {requireAdmin} from '../../../services/passport';


const router = handler()

router   
    .get(cityController.findAll)
    .post(requireAdmin,cityController.validateBody(),cityController.create)



export default router;