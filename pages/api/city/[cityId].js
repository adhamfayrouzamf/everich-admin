
import handler from "../../../handler/handler"
import cityController from "../../../controllers/city.controller/city.controller"

import {requireAdmin} from '../../../services/passport';

const router = handler()

router
    .get(cityController.findById)
    .put(requireAdmin,cityController.validateBody(true),cityController.update)
    .delete(requireAdmin,cityController.delete)



export default router;