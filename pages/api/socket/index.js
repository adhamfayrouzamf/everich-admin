import { Server as ServerIO } from "socket.io";
import nc from "next-connect"
import cors from "cors"

export const config = {
    api: {
        bodyParser: false,
    },
};

const IoReq = nc().use(cors({origin:"*"}))


export default IoReq.get((req, res) => {
    // console.log("socket request",res.socket.server.io)
    // res.socket.server.io=null
    if (!res.socket.server.io) {
        console.log("New Socket.io server...");
        // adapt Next's net Server to http Server
        const httpServer = res.socket.server ;
        const io = new ServerIO(httpServer, {
            cors:{origin:"*"},
            path: "/api/socket",
        });
        io.on("connection",(socket)=>{
            console.log("connected from server")
        })
        // append SocketIO server to Next.js socket server response
        res.socket.server.io = io;
    }
    res.end();
})
