import handler from "../../../handler/handler"
import promocodeController from "../../../controllers/promocode.controller/promocode.controller"

import {requireAdmin} from '../../../services/passport';
const router = handler()

router
    .post(requireAdmin,promocodeController.validateConfirm(),promocodeController.confirmCode)



export default router;