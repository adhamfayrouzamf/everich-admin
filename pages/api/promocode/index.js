import handler from "../../../handler/handler"
import promocodeController from "../../../controllers/promocode.controller/promocode.controller"

import {requireAdmin} from '../../../services/passport';
const router = handler()

router
    .post(requireAdmin, promocodeController.validateBody(),promocodeController.create)
    .get(promocodeController.findAll)



export default router;