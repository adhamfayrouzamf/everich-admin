import handler from "../../../handler/handler"
import promocodeController from "../../../controllers/promocode.controller/promocode.controller"

import {requireAdmin} from '../../../services/passport';
const router = handler()

router
    .get(promocodeController.findById)
    .delete(requireAdmin,promocodeController.delete)
    .put(requireAdmin,promocodeController.validateUpdateBody(),promocodeController.update)



export default router;