import handler from "../../../handler/handler"
import cartController from "../../../controllers/cart.controller/cart.controller"

import {requireAuth} from '../../../services/passport';
const router = handler()

router
    .post(requireAuth,cartController.validatePromoCode(),cartController.addPromoCode)
    .delete(requireAuth,cartController.deletePromoCode)



export default router;