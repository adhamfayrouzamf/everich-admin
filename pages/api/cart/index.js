import handler from "../../../handler/handler"
import cartController from "../../../controllers/cart.controller/cart.controller"

import {requireAuth} from '../../../services/passport';
const router = handler()

router
    .get(requireAuth,cartController.getMyCart)
    // .post(requireAuth,cartController.validateBody(),cartController.create)



export default router;