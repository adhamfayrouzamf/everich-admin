
import handler from "../../../handler/handler"
import CompanyController from "../../../controllers/company.controller/company.controller"

import {requireAdmin} from '../../../services/passport';
const router = handler()

router    
    .put(requireAdmin,
        CompanyController.validateBody(true),
        CompanyController.update
    )
    .get(CompanyController.findById)
    .delete(requireAdmin,CompanyController.delete);



export default router;