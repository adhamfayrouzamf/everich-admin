import handler from "../../../handler/handler"
import CompanyController from "../../../controllers/company.controller/company.controller"

import {requireAdmin} from '../../../services/passport';
const router = handler()

router
    .post(requireAdmin,
        CompanyController.validateBody(),
        CompanyController.create
    )
    .get(CompanyController.getCompany);



export default router;