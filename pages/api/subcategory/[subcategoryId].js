
import handler from "../../../handler/handler"
import subcategoryController from "../../../controllers/sub-category.controller/sub-category.controller"

import {requireAdmin} from '../../../services/passport';


const router = handler()

router
    .get(subcategoryController.findById)
    .put(requireAdmin,
        subcategoryController.validateBody(true),
        subcategoryController.update)
    .delete(requireAdmin, subcategoryController.delete)



export default router;