
import handler from "../../../handler/handler"
import subcategoryController from "../../../controllers/sub-category.controller/sub-category.controller"
import {requireAdmin} from '../../../services/passport';

const router = handler()

router
    .get(subcategoryController.findAll)
    .post(requireAdmin,subcategoryController.validateBody(), subcategoryController.create)



export default router;