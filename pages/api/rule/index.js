import handler from "../../../handler/handler"
import ruleController from "../../../controllers/rule.controller/rule.controller";
import {requireAdmin} from '../../../services/passport';

const router = handler()

router
    .get(requireAdmin,ruleController.findAll)
    .post(requireAdmin,ruleController.validate(),ruleController.create)



export default router;