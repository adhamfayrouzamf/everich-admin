import handler from "../../../handler/handler"
import ruleController from "../../../controllers/rule.controller/rule.controller";
import {requireAdmin} from '../../../services/passport';

const router = handler()

router
    .get(requireAdmin,ruleController.findById)
    .put(requireAdmin,ruleController.validate(true),ruleController.update)
    .delete(requireAdmin,ruleController.delete)

export default router;