import handler from "../../../handler/handler"
import adminController from "../../../controllers/admin.controller/admin.controller"
import {requireAuth} from '../../../services/passport';
const router = handler()

router.post( adminController.validateAdminSignin(), adminController.adminSignIn);


export default router;