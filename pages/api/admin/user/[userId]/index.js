import handler from "../../../../../handler/handler"
import adminController from "../../../../../controllers/admin.controller/admin.controller"
import {requireAdmin} from '../../../../../services/passport';
const router = handler()


router.get(requireAdmin, adminController.userInformation)

router.put( requireAdmin, adminController.validateAdminChangeUser(), adminController.adminUpdateUser);

router.delete(requireAdmin, adminController.deleteUserAccount)

export default router;