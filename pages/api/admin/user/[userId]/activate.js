import handler from "../../../../../handler/handler"
import adminController from "../../../../../controllers/admin.controller/admin.controller"
import {requireAdmin} from '../../../../../services/passport';
const router = handler()

router.put( requireAdmin, adminController.validateAdminActivateUser(), adminController.adminActivateUser);


export default router;