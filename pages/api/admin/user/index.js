import handler from "../../../../handler/handler"
import adminController from "../../../../controllers/admin.controller/admin.controller"
import {requireAdmin} from '../../../../services/passport';
const router = handler()

router.post( requireAdmin, adminController.validateAddUser(), adminController.addUser);


export default router;