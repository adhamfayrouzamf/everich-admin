
import handler from "../../../handler/handler"
import categoryController from "../../../controllers/category.controller/category.controller"

import {requireAdmin} from '../../../services/passport';


const router = handler()

router
    .get(categoryController.findById)
    .put(requireAdmin,
        categoryController.validateBody(true),
        categoryController.update)
    .delete(requireAdmin, categoryController.delete)



export default router;