
import handler from "../../../handler/handler"
import categoryController from "../../../controllers/category.controller/category.controller"
import {requireAdmin} from '../../../services/passport';

const router = handler()

router
    .get(categoryController.findAll)
    .post(requireAdmin, categoryController.validateBody(), categoryController.create)



export default router;