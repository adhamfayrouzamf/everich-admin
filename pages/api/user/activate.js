import handler from "../../../handler/handler"
import userController from "../../../controllers/user.controller/user.controller"
import {requireAdmin} from '../../../services/passport';
const router = handler()

router
    .put(requireAdmin,userController.validateActivateBody(),userController.userActivate)



export default router;