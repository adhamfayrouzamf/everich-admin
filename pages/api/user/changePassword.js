import handler from "../../../handler/handler"
import userController from "../../../controllers/user.controller/user.controller"
import {requireAuth} from '../../../services/passport';
const router = handler()


router.put(
    /* requireAuth, */
    userController.validateUpdatedPassword(),
    userController.updatePasswordFromProfile);




export default router;