import handler from "../../../handler/handler"
import userController from "../../../controllers/user.controller/user.controller"
import {requireAuth} from '../../../services/passport';
const router = handler()


// forgetpassword by mail 
router.post( userController.validateForgetPassword(), userController.forgetPassword);





export default router;