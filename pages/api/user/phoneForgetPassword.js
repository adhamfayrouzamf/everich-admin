import handler from "../../../handler/handler"
import userController from "../../../controllers/user.controller/user.controller"
import {requireAuth} from '../../../services/passport';
const router = handler()


// forgetpassword by phone number  
router.post( userController.validateForgetPasswordByPhone(), userController.forgetPasswordByPhone);





export default router;