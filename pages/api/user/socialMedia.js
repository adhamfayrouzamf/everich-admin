import handler from "../../../handler/handler"
import userController from "../../../controllers/user.controller/user.controller"
import {requireAuth} from '../../../services/passport';
const router = handler()


router
    .post(userController.validateSocialMediaLogin(), userController.socialMedialLogin);


export default router;