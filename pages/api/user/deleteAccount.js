import handler from "../../../handler/handler"
import userController from "../../../controllers/user.controller/user.controller"
import {requireAuth} from '../../../services/passport';
const router = handler()


router.delete(/* requireAuth, */ userController.validateDeleteUserAccount(), userController.deleteUserAccount);


export default router;