import handler from "../../../handler/handler"
import userController from "../../../controllers/user.controller/user.controller"
import {multerSaveTo} from '../../../services/multer-service';
import {requireAuth} from '../../../services/passport';
const router = handler()

router.put(
    /* requireAuth, */
    multerSaveTo('users').single('image'),
    userController.validateUserUpdate(true),
    userController.updateInfo);



export default router;