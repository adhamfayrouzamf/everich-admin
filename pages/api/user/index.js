import handler from "../../../handler/handler"
import userController from "../../../controllers/user.controller/user.controller"
import {multerSaveTo} from '../../../services/multer-service';
import {requireAuth} from '../../../services/passport';
const router = handler()

router.put('/openActiveChatHead',/* requireAuth, */userController.openActiveChatHead)
router.put('/closeActiveChatHead',/* requireAuth, */ userController.closeActiveChatHead)

router
    .post('/signup',userController.validateUserCreateBody(), userController.userSignUp);

router
    .put('/activate',userController.validateActivateBody(),userController.userActivate)

router
    .post('/socialMedia',userController.validateSocialMediaLogin(), userController.socialMedialLogin);

router.delete('/deleteAccount',/* requireAuth, */ userController.validateDeleteUserAccount(), userController.deleteUserAccount);

router.post('/signin', userController.validateUserSignin(), userController.signIn);

router.get('/allUsers', /* requireAuth, */ userController.findAll);

router.get('/userInfo', /* requireAuth, */ userController.userInformation)

router.post('/addToken',/* requireAuth, */ userController.validateAddToken(), userController.addToken);

router.post('/logout',/* requireAuth, */ userController.validateLogout(), userController.logout);


router.post('/checkExistPhone', userController.validateCheckPhone(), userController.checkExistPhone);
router.post('/checkExistEmail', userController.validateCheckEmail(), userController.checkExistEmail);


router.put('/updateInfo',
    /* requireAuth, */
    multerSaveTo('users').single('image'),
    userController.validateUserUpdate(true),
    userController.updateInfo);

router.put('/changePassword',
    /* requireAuth, */
    userController.validateUpdatedPassword(),
    userController.updatePasswordFromProfile);


router.post('/reset-password',
    userController.validateResetPassword(),
    userController.resetPassword);

// forgetpassword by mail 
router.post('/forgetPassword', userController.validateForgetPassword(), userController.forgetPassword);
router.put('/confirmationCode', userController.validateConfirmCode(), userController.verifyForgetPasswordCode);
router.put('/confirmationchange', userController.validateResetPassword(), userController.updatePassword);

// forgetpassword by phone number  
router.post('/phoneForgetPassword', userController.validateForgetPasswordByPhone(), userController.forgetPasswordByPhone);
router.put('/phoneConfirmationCode', userController.validateVerifyForgetPasswordByPhone(), userController.verifyForgetPasswordByPhone);
router.put('/phonePasswordChange', userController.validateUpdatePasswordByPhone(), userController.updatePasswordByPhone);

router.delete('/account',/* requireAuth, */ userController.deleteAccount);




export default router;