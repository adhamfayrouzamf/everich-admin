/* style / images */
// import styles from './Category.scss';

/* methods / packages */
import { useContext } from 'react'
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext';

/* components */
import { Container } from 'reactstrap'
import CustomTable from '../../components/CustomTable/CustomTable';

const page=[
    {key:{en:"Products",ar:"المنتجات"},path:"/products"}
]

const ProductsTable = props => {
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const options={
        title:{value:{en:"Products",ar:"المنتجات"}},
        addBtn:true,
        url:"/products",api_url:"/api/product",fetch_url:"/api/product",
        cols:[
            {key:"id",label:{ar:"#",en:"#"},check:true},
            {key:"image",label:{ar:"الصورة",en:"Image"},isImage:true},
            {key:"name",label:{ar:"الاسم",en:"Name"},localized:true},
            {key:"price",label:{ar:"السعر",en:"Price"}},
            {key:"offer",label:{ar:"الخصم",en:"Discount"}},
            {key:"category",label:{ar:"القسم",en:"Category"},localized:true,childs:["name"]},
            {key:"subCategory",label:{ar:"القسم",en:"Category"},localized:true,childs:["name"],notFound:"No Sub Category"},
            {key:"trademark",label:{ar:"العلامة التجارية",en:"Trademark"},localized:true,childs:["name"]},
        ],
        controls:[
            "DETAILS", "UPDATE", "DELETE"
        ]
    }

    return (
        <div className="product-page">
            <Container fluid>
                <div className="content">
                    <CustomTable options={options} />
                </div>
            </Container>
        </div>
    )
}


export default ProductsTable