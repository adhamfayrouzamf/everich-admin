/* style / images */
// import './Category.scss';

/* methods / packages */
import { useContext, useState, useRef } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext';
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../components/CustomForm/CustomForm"

const validateImgType = (values)=>{
    if(values && values[0]){
        for(let i = 0 ; i<values.length; i++){
            if(!values[i].type.match(/image\/(png|jpg|jpeg|svg\+xml|webp)/))
                return false
        }
        return true
    }
    return false
}
const validateImgSize = (values)=> {
    if(values && values[0]){
        for(let i = 0 ; i<values.length; i++){
            if(values[i].size > 1000000)
                return false
        }
        return true
    }
    return false
}

const productSchema = yup.object().shape({
    name       : yup.object().shape({
        en:yup.string().required('name is required').min(4),
        ar:yup.string().required('name is required').min(4) 
    }),
    desc       : yup.object().shape({
        en:yup.string().required('description is required'),
        ar:yup.string().required('description is required') 
    }),
    useStatus  : yup.string().nullable().required('must choose a status'),
    category : yup.string().required('must choose a category'),
    trademark  : yup.string().required('must choose a trademark'),
    price      : yup.number().typeError('price must be a number').positive('price must be positive'),
    offer      : yup.number().typeError('offer must be a number').positive('offer must be positive'),
    quantity   : yup.number().typeError('quantity must be a number').positive('quantity must be positive').integer('quantity must be integer'),
    imageFile  : yup.mixed('imageFile').required('image is required')
                .test("fileType","this file is not an image",validateImgType)
                .test("fileSize", "image maximum size must be 10MB",validateImgSize),
    imagesFile  : yup.mixed()
                .test("filesType","must choose images only",validateImgType)
                .test("filesSize", "image maximum size must be 10MB per each",validateImgSize),
})


const page=[
    {key:{en:"Products",ar:"المنتجات"},path:"/products"},
    {key:{en:"Add Product",ar:"اضافة منتج"},path:"/products/add"}
]
const AddProductForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const subCat = useRef()
    const [category, setCategory] = useState(null)
    const [subCats,setSubCats] = useState([])
    const addProduct = (v,e) => {
        console.log(v)
        axios.post('/api/product',{...v},{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>{
            console.log("done")
        }).catch(err=>{
            console.log(err.message)
        })
    }
    const handleCategory = (v) =>{
        console.log(subCat.current)
        const cat = v ? v.value : null
        setCategory(cat)
        const url = `/api/subcategory?${cat ? `parent=${cat}` : ""}`
        axios.get(url).then(res=>setSubCats(res.data.data))
    }
    
    const options = {
        form:'productAddForm',id:"product", title:{value:"Add Product"},
        submitBtn:{value:"Add",options:{id:"submit-btn"}},
    }
    const fields = [
        {type:'file',name:"image",label:"Add Image",id:"productImage",dir:"product"},
        {type:'text',name:"name",locale:"en",placeholder:"Name in English",id:"name-en",col:{md:"6"}},
        {type:'text',name:"name",locale:"ar",placeholder:"Name in Arabic",id:"name-ar",col:{md:"6"}},
        {type:'text',name:"desc",locale:"en",placeholder:"Description in English",id:"desc-en",col:{md:"6"}},
        {type:'text',name:"desc",locale:"ar",placeholder:"Description in Arabic",id:"desc-ar",col:{md:"6"}},
        {type:'file',name:"images",label:"Add Slider Images",id:"productImages",dir:"product", multiple:true},
        {type:"select",name:"category",placeholder:"Choose Category",id:"category",fetch_url:"/api/category?name=",option:{value:"id",label:[{key:"name",localized:true}]},onChange:handleCategory,col:{md:"6"}},
        {type:"select",name:"subCategory",placeholder:"Choose Sub Category",fetch_url:"/api/subcategory?name=",id:"subCat",option:{value:"id",label:[{key:"name",localized:true}]},col:{md:"6"}},
        {type:"select",name:"trademark",placeholder:"Choose Trademark",id:"trademark",fetch_url:"/api/trademark?name=",option:{value:"id",label:[{key:"name",localized:true}]},col:{md:"6"}},
        {type:"radio",name:"useStatus",fieldset:{value:"Use Status"},choices:[{label:"New", value:"NEW"}, {label:"used",value:"USED"}],col:{xs:"12"}},
        {type:"text",name:"price",placeholder:"enter product price", id:"price", label:"product price",col:{md:"6"}},
        {type:"text",name:"offer",placeholder:"enter product offer", id:"offer", label:"product offer",col:{md:"6"}},
        {type:"text",name:"quantity",placeholder:"enter product quantity", id:"quantity", label:"product quantity",col:{md:"6"}},
    ]
    return (
        <div className="product-page">
            <Container fluid>
                <div className="content">
                    <CustomForm 
                        config={options}
                        fields={fields}
                        submit={addProduct}
                        validation={productSchema}
                        />
                </div>
            </Container>
        </div>
    )
}


export default AddProductForm