/* style / images */

/* methods / packages */
import { useContext, useEffect, useState } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"

import { UserContext } from '../../../components/contexts/AuthContext'
import { PageContext } from "../../../components/contexts/PageContext"

/* components */
import { Container, Nav, NavItem, NavLink } from 'reactstrap';
import Loader from '../../../components/Loader';
// import Moment from "react-moment"
// import "moment/locale/ar"
import CustomTable from "../../../components/CustomTable/CustomTable"
import Image from "next/image"
const page=[
    {key:{en:"Products",ar:"المنتجات"},path:"/products"},
    {key:{en:"product Details",ar:"تفاصيل المنتج"},path:"/products/[productId]"}
]
const ViewProducDetails = () => {
    const route = useRouter()
    const admin = useContext(UserContext)
    const [product, setProduct] = useState({})
    const [activeTab, setActiveTab] = useState("1")
    const [isLoading, setIsLoading] = useState(true)
    const {setPage} = useContext(PageContext)
    setPage(page)

    useEffect(()=>{
        const {productId} = route.query
        if(productId){
            axios.get(`/api/product/${productId}`).then(res=>{
                console.log(res.data)
                setProduct(res.data)
                setIsLoading(false)
            }).catch(err=>console.error(err))
        }

    },[route.query.productId])

    return (
        <div className="product-page">
            <Container fluid>
                <div className="content">
                    <Loader Show={!isLoading} Render={true}>
                        {product.name && <h3>{product.name[route.locale]}</h3>}
                        {product.image && <Image src={product.image.url} alt="product_image" width={200} height={200} />}
                    </Loader>
                </div>
            </Container>t
        </div>
    )
}


export default ViewProducDetails