/* style / images */

/* methods / packages */
import { useContext } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from "../../components/contexts/PageContext"
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../components/CustomForm/CustomForm"

const userSchema = yup.object().shape({
    username:yup.string().required('username is required'),
    email : yup.string().required('Email is required'),
    password : yup.string().required('Password is required').min(4),
    confirmPass: yup.string().oneOf([yup.ref("password")],"Password must match"),
    phone : yup.string().required('phone is required'),
})

const options = {
    form:'user-form',id:"user_form", title:{value:"Add User"},
    submitBtn:{value:"Add",options:{id:"submit-btn"}},
}
const fields = [
    {type:'text',name:"username",placeholder:"Username",id:"username",col:{md:"6"}},
    {type:'text',name:"email",placeholder:"Email",id:"email",col:{md:"6"}},
    {type:'password',name:"password",placeholder:"Password",id:"password",col:{md:"6"}},
    {type:'password',name:"confirmPass",placeholder:"confirmPass",id:"confirmPass",col:{md:"6"}},
    {type:'text',name:"phone",placeholder:"Phone",id:"phone",col:{md:"6"}},
    {type:'hidden',name:"type",id:"user_type"},
]
const page=[
    {key:{en:"Users",ar:"العملاء"},path:"/users"},
    {key:{en:"Add User",ar:"اضافة عميل"},path:"/users/add"}
]
const AddUserForm = () => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)

    const addUser = (v,e) => {
        axios.post('/api/admin/user',{...v},{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>{
            console.log("done")
        }).catch(err=>{
            console.log(err.message)
        })
    }
    

    return (
        <div className="user-page">
            <Container fluid>
                <div className="content">
                    <CustomForm 
                        config={options}
                        fields={fields}
                        validation={userSchema}
                        submit={addUser}
                        values={{type:"CLIENT"}}
                    />
                </div>
            </Container>
        </div>
    )
}


export default AddUserForm