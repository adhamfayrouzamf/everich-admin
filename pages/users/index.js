/* style / images */


/* methods / packages */
import React, { useContext } from 'react'
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext';

/* components */
import { Container } from 'reactstrap'
import CustomTable from '../../components/CustomTable/CustomTable';
import axios from "axios"

const page=[
    {key:{en:"Users",ar:"العملاء"},path:"/users"}
]
const UserTable = props => {
    const {setPage} = useContext(PageContext)
    setPage(page)
    const user= useContext(UserContext)
    const activate = (value,id) => {
        console.log(value,id)
        return axios.put(`/api/admin/user/${id}/activate`,{activated:value},{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>res)
    }

    const options={
        title:{value:{ar:"العملاء",en:"Users"}},
        addBtn:true,
        url:"/users",api_url:"/api/user",fetch_url:"/api/user/allUsers?type=CLIENT",
        cols:[
            {key:"id",label:{ar:"#",en:"#"},check:true},
            {key:"username",label:{ar:"الاسم",en:"Name"}},
            {key:"email",label:{ar:"الايميل",en:"Email"}},
            {key:"phone",label:{ar:"الهاتف",en:"Phone"},notFound:"###########"},
            {key:"socialMediaType",label:{ar:"التسجيل بواسطة",en:"Login Method"}},
            {key:"activated",label:{ar:"الحالة",en:"Status"}, 
                isToggler:true, 
                choices:[
                    {value:true,onClick:activate,label:{ar:"تعطيل",en:"Deactivate"},toState:1,
                    modal:{message:"Are You Sure You Want to Deactivate This User",title:"Deactivate User",confirmBtn:"Deactivate"}
                    },
                    {value:false,onClick:activate,label:{ar:"تفعيل",en:"Activate"},toState:0,
                    modal:{message:"Are You Sure You Want to Activate This User",title:"Activate User",confirmBtn:"Activate"}
                    }
                ]
            },
            {key:"createdAt",label:{ar:"تاريخ الانشاء",en:"Date"},isDate:true}
        ],
        controls:[
            "DETAILS", "UPDATE", "DELETE"
        ],
    }

    return (
        <div className="user-page">
            <Container fluid>
                <div className="content">
                    <CustomTable options={options} />
                </div>
            </Container>
        </div>
    )
}


export default UserTable