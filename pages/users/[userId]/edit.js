/* style / images */

/* methods / packages */
import { useContext, useState, useEffect } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"
import { UserContext } from '../../../components/contexts/AuthContext'
import { PageContext } from "../../../components/contexts/PageContext"
import { useFetchOne } from "../../../components/hooks/fetch-hook"
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../../components/CustomForm/CustomForm"

const userSchema = yup.object().shape({
    username:yup.string().required('username is required'),
    email : yup.string().required('Email is required'),
    phone : yup.string().required('phone is required'),
})

const options = {
    form:'user-form',id:"user_form", title:{value:"Edit User"},
    submitBtn:{value:"Edit",options:{id:"submit-btn"}},
}
const fields = [
    {type:'text',name:"username",placeholder:"Username",id:"username",col:{md:"6"}},
    {type:'text',name:"email",placeholder:"Email",id:"email",col:{md:"6"}},
    {type:'text',name:"phone",placeholder:"Phone",id:"phone",col:{md:"6"}},
    {type:'hidden',name:"type",id:"user_type"},
]

const page=[
    {key:{en:"Users",ar:"العملاء"},path:"/users"},
    {key:{en:"Edit User",ar:"تعديل عميل"},path:"/users/[userId]/edit"}
]
const EditUserForm = () => {
    const route = useRouter()
    const admin = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const [user, error, isLoading] = useFetchOne(`/api/admin/user/${route.query.userId}`,{headers:{Authorization:`Bearer ${admin.token}`}})
    const [values,setValues] = useState()


    const editUser = (v,e) => {
        axios.put(`/api/admin/user/${route.query.userId}`,{...v},{headers:{Authorization:`Bearer ${admin.token}`}}).then(res=>{
            console.log("done")
        }).catch(err=>{
            console.log({...err})
        })
    }

    useEffect(()=>{
        if(!isLoading){
            setValues({
                username:user.username,
                email:user.email,
                phone:user.phone,
                type:"CLIENT",
                socialMediaType: user.socialMediaType
            })
        }
    },[user])
    
    return (
        <div className="user-page">
            <Container fluid>
                <div className="content">
                    <CustomForm 
                        config={options}
                        fields={fields}
                        validation={userSchema}
                        submit={editUser}
                        values={values}
                    />
                </div>
            </Container>
        </div>
    )
}


export default EditUserForm