/* style / images */

/* methods / packages */
import { useContext, useEffect, useState } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"

import { UserContext } from '../../../components/contexts/AuthContext'
import { PageContext } from "../../../components/contexts/PageContext"

/* components */
import { Container, TabContent, TabPane, Nav, NavItem, NavLink, Table } from 'reactstrap';
import Loader from '../../../components/Loader';
import Moment from "react-moment"
import "moment/locale/ar"
import CustomTable from "../../../components/CustomTable/CustomTable"
import { useFetchOne } from "../../../components/hooks/fetch-hook"
const page=[
    {key:{en:"Users",ar:"العملاء"},path:"/users"},
    {key:{en:"User Details",ar:"تفاصيل العميل"},path:"/users/[userId]"}
]
const ViewUserDetails = () => {
    const route = useRouter()
    const admin = useContext(UserContext)
    const [user, error, isLoading] = useFetchOne(`/api/admin/user/${route.query.userId}`,{headers:{Authorization:`Bearer ${admin.token}`}})
    const [addresses,setAddresses] = useState([])
    const [activeTab, setActiveTab] = useState("1")
    const [ordersTable, setOrdersTable] = useState()
    const [favoritesTable,setFavoritesTable]= useState()
    const {setPage} = useContext(PageContext)
    setPage(page)

    useEffect(()=>{
        const {userId} = route.query
        
        if(userId){
            axios.get(`/api/address?user=${userId}`,{headers:{Authorization:`Bearer ${admin.token}`}}).then(res=>{
                setAddresses(res.data.data)
            }).catch(err=>console.error(err))
            setOrdersTable({
                url:"/orders", api_url:`/api/order?user=${userId}`,fetch_url:`/api/order?user=${userId}`,
                cols:[
                    {key:"id",label:"#"},
                    {key:"orderNumber",label:"Order Number"},
                    {key:"user",label:"User",childs:["username"]},
                    {key:"paymentMethod",label:"Pay Methods"},
                    {key:"finalPrice",label:"Price"},
                    {key:"shipping",label:"Transport Price",childs:["shippingPrice"]},
                    {key:"status",label:"Status"},
                    {key:"createdAt",label:"Date",isDate:true},
                ],
                controls:["view"]
            })
            setFavoritesTable({
                url:"/products",api_url:`/api/product?favorites=${userId}`,fetch_url:`/api/product?favorites=${userId}`,
                cols:[
                    {key:"id",label:"#"},{key:"serialNumber",label:"Serial Number"},
                    {key:"name",label:"Name",localized:true},
                    {key:"category",label:"Category",localized:true,childs:["name"]},
                    {key:"subCategory",label:"Sub Category",localized:true,childs:["name"],notFound:"No Sub Category"},
                    {key:"useStatus",label:"Status"},
                    {key:"totalRate",label:"Rate",notFound:"0"},
                    {key:"image",label:"Image",isImage:true},
                    {key:"createdAt",label:"Date",isDate:true}
                ],
                controls:[
                    "view"
                ]
            })
        }

    },[route.query.userId])

    return (
        <div className="user-page">
            <Container fluid>
                <div className="content">
                    <Nav tabs>
                        <NavItem>
                            <NavLink
                                onClick={() => { setActiveTab('1'); }}
                                className={activeTab === "1" ?"active":"clickable"}
                            >
                                Basic Info
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                onClick={() => { setActiveTab('2'); }}
                                className={activeTab === "2" ? "active" : "clickable"}
                            >
                                Orders
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                onClick={() => { setActiveTab('3'); }}
                                className={activeTab === "3" ? "active" : "clickable"}
                            >
                                Favorites
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                onClick={() => { setActiveTab('4'); }}
                                className={activeTab === "4" ? "active" : "clickable"}
                            >
                                Addresses
                            </NavLink>
                        </NavItem>
                    </Nav>
                    <Loader Show={!isLoading} Render={true}>
                        <TabContent activeTab={activeTab}>
                            <TabPane tabId="1">
                                <Table className="Basic_info">
                                    <tbody>
                                        <tr>
                                            <th>Name</th>
                                            <td>{user.username}</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>{user.email}</td>
                                        </tr>
                                        <tr>
                                            <th>Phone</th>
                                            <td>{user.phone}</td>
                                        </tr>
                                        {/* <tr>
                                            <th>LoginMethod</th>
                                            <td></td>
                                        </tr> */}
                                        <tr>
                                            <th>Status</th>
                                            <td>{user.activated ? "Active" : "InActive"}</td>
                                        </tr>
                                        <tr>
                                            <th>Registered Date</th>
                                            <td><Moment locale={route.locale} date={user.createdAt} format="DD-MM-YYYY"/></td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </TabPane>
                            <TabPane tabId="2">
                                {ordersTable && <CustomTable options={ordersTable} />}
                            </TabPane>
                            <TabPane tabId="3">
                                {favoritesTable && <CustomTable options={favoritesTable} />}
                            </TabPane>
                            <TabPane tabId="4">
                                Addresses
                            </TabPane>
                        </TabContent>
                    </Loader>
                </div>
            </Container>
        </div>
    )
}


export default ViewUserDetails