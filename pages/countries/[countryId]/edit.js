/* style / images */


/* methods / packages */
import { useContext, useState, useEffect } from 'react'
import axios from 'axios'
import { useRouter } from "next/router"
import { UserContext } from '../../../components/contexts/AuthContext'
import { PageContext } from '../../../components/contexts/PageContext';
import { toast } from 'react-toastify'
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../../components/CustomForm/CustomForm"
import Loader from "../../../components/Loader"


const countrySchema = yup.object().shape({
    name       : yup.object().shape({
        en:yup.string().required('name is required').min(4),
        ar:yup.string().required('name is required').min(4) 
    }),
    
})

const page=[
    {key:{en:"Countries",ar:"الدول"},path:"/countries"},
    {key:{en:"Edit Country",ar:"تعديل الدولة"},path:"/countries/[countryId]/edit"}
]

const CountryEditForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const [country,setCountry] = useState()
    const editCountry = (v,e) => {
        console.log(v)
        const {countryId} = route.query
        axios.put(`/api/country/${countryId}`,v,{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>{
            console.log(res.data)
            toast.success("updated successfully")
        }).catch(err=>{
            console.log(err.message)
        })
    }

    const options = {
        form:'country-editform',id:"countryForm", title:{value:"Edit Country"},
        submitBtn:{value:"Edit",options:{id:"submitBtn"}}
    }
    const fields=[
        {type:'text',name:"name",locale:"en",placeholder:"Name in English",id:"name-en", col:{md:"6"}},
        {type:'text',name:"name",locale:"ar",placeholder:"Name in Arabic",id:"name-ar", col:{md:"6"}},
    ]
    useEffect(()=>{
        const {countryId} = route.query
        axios.get(`/api/country/${countryId}`).then(res=>{
            const {data} = res
            setCountry({
                name:data.name,
            })
        }).catch(err=>console.error(err))
    },[route.query.countryId])
    return (
        <div className="country-page">
            <Container fluid>
                <div className="content">
                    <Loader Render={true} Show={country}>
                        <CustomForm 
                            config={options} 
                            fields={fields} 
                            validation={countrySchema} 
                            values={country}
                            submit={editCountry} />
                    </Loader>
                </div>
            </Container>
        </div>
    )
}


export default CountryEditForm