/* style / images */

/* methods / packages */
import React, { useContext } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from "../../components/contexts/PageContext"
import { toast } from 'react-toastify'
import * as yup from 'yup'

/* components */
import { Container } from 'reactstrap'
import CustomForm from "../../components/CustomForm/CustomForm"

const countrySchema = yup.object().shape({
    name       : yup.object().shape({
        en:yup.string().required('name is required'),
        ar:yup.string().required('name is required')
    }),
    countrykey:yup.string().required('must add country key'),
    countryCode: yup.number().typeError("country code must be a number").integer('must be integer').positive('must be positive')

})

const page=[
    {key:{en:"Countries",ar:"الدول"},path:"/countries"},
    {key:{en:"Add Countrie",ar:"اضافة دولة"},path:"/countries/add"}
]
const AddCountryForm = props => {
    const route = useRouter()
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const addCountry = (v,e) => {
        
        axios.post('/api/country',v,{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>{
            console.log(res.data)
            toast.success("added successfully")
        }).catch(err=>{
            console.log(err.message)
        })
    }
    const options = {
        form:'country-addform',id:"countryForm", title:{value:"Add Country"},
        submitBtn:{value:"Add",options:{id:"submit-btn"}},
    }
    const fields = [
        {type:'text',name:"name",locale:"en",placeholder:"Name in English",id:"name-en",col:{md:"6"}},
        {type:'text',name:"name",locale:"ar",placeholder:"Name in Arabic",id:"name-ar",col:{md:"6"}},
        {type:'text',name:"countryKey",placeholder:"Country Key",id:"country-key",col:{md:"6"}},
        {type:'text',name:"countryCode",placeholder:"Country Code",id:"country-cide",col:{md:"6"}},
    ]

    return (
        <div className="country-page">
            <Container fluid>
                <div className="content">
                    <CustomForm 
                        config={options}
                        fields={fields}
                        validation={countrySchema}
                        submit={addCountry}
                    />
                </div>
            </Container>
        </div>
    )
}


export default AddCountryForm