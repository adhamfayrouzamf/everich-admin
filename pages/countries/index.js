/* style / images */


/* methods / packages */
import { useContext } from 'react'
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from "../../components/contexts/PageContext"

/* components */
import { Container } from 'reactstrap'
import CustomTable from '../../components/CustomTable/CustomTable';

const page=[
    {key:{en:"Countries",ar:"الدول"},path:"/countries"}
]

const CountryTable = props => {
    const user = useContext(UserContext)
    const {setPage} = useContext(PageContext)
    setPage(page)
    const options={
        title:{value:"Countries"},
        addBtn:{value:"Add Country"},
        url:"/countries",api_url:"/api/country",fetch_url:"/api/country",
        cols:[
            {key:"id",label:"#",check:true},
            {key:"name",label:"Name",localized:true},
            {key:"createdAt",label:"Date",isDate:true}
        ],
        controls:[
            "DETAILS", "UPDATE", "DELETE"
        ]
    }


    return (
        <div className="country-page">
            <Container fluid>
                <div className="content">
                    <CustomTable options={options} />
                </div>
            </Container>
        </div>
    )
}


export default CountryTable