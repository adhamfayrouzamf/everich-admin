import { io, Socket } from 'socket.io-client'
import { useState, useEffect } from 'react'
import axios from 'axios'
import CustomForm from '../components/CustomForm/CustomForm'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Table } from 'reactstrap'
import Link from "next/link"
import Select from "react-select"
import {useForm} from "react-hook-form"
import {sortableContainer, sortableElement} from 'react-sortable-hoc';
import {arrayMoveImmutable as arrayMove} from "array-move"
import {useFetch, useFetchOne, useCache} from "../components/hooks/fetch-hook"
import * as yup from "yup"
import {yupResolver} from "@hookform/resolvers/yup"
import DatePicker from "react-datepicker"

import {GoogleMap, withScriptjs, withGoogleMap} from "react-google-maps"


const MyMap = withScriptjs(withGoogleMap((props)=>{
  const {defaultCenter} = props
  return <GoogleMap 
            defaultZoom={10}
            defaultCenter={defaultCenter}
        />
}))


const Map = (props)=>{
  const {defaultCenter} = props
  return (
    <div>
      <MyMap 
        isMarkerShown
        defaultCenter={defaultCenter}
        googleMapURL={"https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDrRuJv86ApLY1xwKVk-M1Kx4xFgQ1DhEU&libraries=geometry,drawing,places"}
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `400px` }} />}
        mapElement={<div style={{ height: `100%` }} />}
      />
    </div>
  )
}

const Home = ({products}) => {
  // const [connection, setConnection] = useState('')
  const [message, setMessage] = useState('')
  const [socket, setSocket] = useState()
  const [data,hasNext,getNext,isLoadingMore,error,isLoading] = useCache("/api/product",{},{fallbackData:products || null})

  useEffect(()=>{
    const socket = io("/",{path:"/api/socket"})
    setSocket(socket)
    socket.on("connect",()=>{
      console.log("connected successfully")
      setMessage("connected successfully")
    })
    socket.on("message",(message)=>{
      console.log(message)
      setMessage(message)
    })
    return ()=>{socket.off("connect");socket.off("message")}
  },[])
  useEffect(()=>{
    console.log("socket===>",socket)
  },[socket])

  useEffect(()=>{
    console.log("data===>",data)
  },[isLoading])
  useEffect(()=>{
    console.log("products===>",products)
  },[products])
  return (
    <div>
      {/* <Map defaultCenter={{lat: 30.037445733005914,lng: 31.21138572692871}} /> */}
      {message}
      <div>
        {data.length && data.map((d)=>{
          return (
            <div>
              {d.name.en}
            </div>
          )
        })}
      </div>
    </div>
  )
}
Home.getInitialProps = async ({req}) =>{
  if(!req){
    return {products:null}
  }
  const res = await axios.get("/api/product")
  
  return {
      products:res.data
  }
}

export default Home