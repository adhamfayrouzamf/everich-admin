/* style / images */

/* methods / packages */
import { useContext, useEffect, useState } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"

import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from "../../components/contexts/PageContext"

/* components */
import { Container, Nav, NavItem, NavLink } from 'reactstrap';
import Loader from '../../components/Loader';
import { useFetchOne } from "../../components/hooks/fetch-hook"
import CustomForm from "../../components/CustomForm/CustomForm"
import * as yup from "yup"

const aboutAppSchema = yup.object().shape({
    email: yup.string().required("email is required").email(),
    phone: yup.string().required("phone is required"),
    landlinePhone: yup.string().required("landlinePhone is required"),
    whatsappNumber: yup.string().required("whatsappNumber is required"),
    taxes: yup.number().typeError("taxes must be a number").required("taxes is required").min(0),
    taxNumber: yup.number().typeError("taxNumber must be a number").required("taxNumber is required"),
    androidUrl: yup.string().required("androidUrl is required"),
    iosUrl: yup.string().required("iosUrl is required"),
})

const page=[
    {key:{en:"About App",ar:"عن التطبيق"},path:"/AppSetting/AboutApp"},
]

const options = {
    form:'AboutAppForm',id:"AboutAppForm",
    submitBtn:{value:"Edit",options:{id:"submit-btn",className:"mx-auto"}},
}
const fields = [
    {type:'text',name:"email",label:"Email",id:"email",col:{md:"6"}},
    {type:'text',name:"phone",label:"Phone",id:"phone",col:{md:"6"}},
    {type:'text',name:"landlinePhone",label:"Landline Phone",id:"landlinePhone",col:{md:"6"}},
    {type:'text',name:"whatsappNumber",label:"Whatsapp Number",id:"whatsappNumber",col:{md:"6"}},
    {type:'text',name:"taxes",label:"taxes",id:"taxes",col:{md:"6"}},
    {type:'text',name:"taxNumber",label:"Tax Number",id:"taxNumber",col:{md:"6"}},
    {type:'text',name:"androidUrl",label:"Android Url",id:"androidUrl",col:{md:"6"}},
    {type:'text',name:"iosUrl",label:"IOS Url",id:"iosUrl",col:{md:"6"}},
    
]

const AboutApp = () => {
    const route = useRouter()
    const admin = useContext(UserContext)
    const [company, error, isLoading] = useFetchOne(`/api/company`)
    const [values,setValues] = useState()
    const [activeTab, setActiveTab] = useState("1")
    const {setPage} = useContext(PageContext)
    setPage(page)
    const updateInfo = (values)=>{
        axios.put(`/api/company/aboutApp`,values,{headers:{Authorization:`Bearer ${admin.token}`}})
        .then(res=>{
            console.log("company updated===>",res.data)
        }).catch(err=>console.error(err))
    }
    useEffect(()=>{
        console.log(company)
        if(company){
            const values = {
                email:company.email,
                phone:company.phone,
                landlinePhone:company.landlinePhone,
                whatsappNumber:company.whatsappNumber,
                taxes:company.taxes,
                taxNumber:company.taxNumber,
                androidUrl:company.androidUrl,
                iosUrl:company.iosUrl
            }

            setValues(values)
        }
    },[company])
    return (
        <div className="about_app">
            <Container fluid>
                <div className="content">
                    <Loader Show={!isLoading && values} Render={true}>
                        <CustomForm
                            config={options}
                            fields={fields}
                            submit={updateInfo}
                            validation={aboutAppSchema}
                            values={values}
                        />
                    </Loader>
                </div>
            </Container>
        </div>
    )
}


export default AboutApp