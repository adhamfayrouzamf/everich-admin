/* style / images */

/* methods / packages */
import { useContext, useEffect, useState } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"

import { UserContext } from '../../../components/contexts/AuthContext'
import { PageContext } from "../../../components/contexts/PageContext"

/* components */
import { Container, TabContent, TabPane, Nav, NavItem, NavLink, Table } from 'reactstrap';
import Loader from '../../../components/Loader';
import Moment from "react-moment"
import "moment/locale/ar"
import CustomTable from "../../../components/CustomTable/CustomTable"
import { useFetchOne } from "../../../components/hooks/fetch-hook"
const page=[
    {key:{en:"Sub Admins",ar:"المسئولين"},path:"/sub-admins"},
    {key:{en:"Sub Admin Details",ar:"تفاصيل المسئول الفرعي"},path:"/sub-admins/[subAdminId]"}
]
const ViewUserDetails = () => {
    const route = useRouter()
    const admin = useContext(UserContext)
    const [user, error, isLoading] = useFetchOne(`/api/admin/user/${route.query.subAdminId}`,{headers:{Authorization:`Bearer ${admin.token}`}})
    const [activeTab, setActiveTab] = useState("1")
    const {setPage} = useContext(PageContext)
    setPage(page)

    return (
        <div className="user-page">
            <Container fluid>
                <div className="content">
                    <Nav tabs>
                        <NavItem>
                            <NavLink
                                onClick={() => { setActiveTab('1'); }}
                                className={activeTab === "1" ?"active":"clickable"}
                            >
                                Basic Info
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink
                                onClick={() => { setActiveTab('2'); }}
                                className={activeTab === "2" ? "active" : "clickable"}
                            >
                                Permissions
                            </NavLink>
                        </NavItem>
                    </Nav>
                    <Loader Show={!isLoading} Render={true}>
                        <TabContent activeTab={activeTab}>
                            <TabPane tabId="1">
                                <Table className="Basic_info">
                                    <tbody>
                                        <tr>
                                            <th>Name</th>
                                            <td>{user.username}</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>{user.email}</td>
                                        </tr>
                                        <tr>
                                            <th>Phone</th>
                                            <td>{user.phone}</td>
                                        </tr>
                                        {/* <tr>
                                            <th>LoginMethod</th>
                                            <td></td>
                                        </tr> */}
                                        <tr>
                                            <th>Status</th>
                                            <td>{user.activated ? "Active" : "InActive"}</td>
                                        </tr>
                                        <tr>
                                            <th>Registered Date</th>
                                            <td><Moment locale={route.locale} date={user.createdAt} format="DD-MM-YYYY"/></td>
                                        </tr>
                                    </tbody>
                                </Table>
                            </TabPane>
                            <TabPane tabId="2">
                                Roles
                            </TabPane>
                        </TabContent>
                    </Loader>
                </div>
            </Container>
        </div>
    )
}


export default ViewUserDetails