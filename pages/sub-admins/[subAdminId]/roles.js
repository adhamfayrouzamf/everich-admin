/* style / images */

/* methods / packages */
import { useContext, useEffect, useState } from 'react'
import axios from 'axios'
import {useRouter} from "next/router"

import { UserContext } from '../../../components/contexts/AuthContext'
import { PageContext } from "../../../components/contexts/PageContext"

/* components */
import { Container, Row, Col, Card, CardBody, CardTitle, Form, Button, Input } from 'reactstrap';
import Loader from '../../../components/Loader';
import Moment from "react-moment"
import "moment/locale/ar"
import { useForm } from "react-hook-form"
import { useFetch } from "../../../components/hooks/fetch-hook"
// import { CheckField } from '../../../components/CustomForm/FormFields'
import { useLang } from "../../../components/hooks/lang-hook"

const createPromise = (query) => {
    let newPromise = new Promise(async (resolve, reject) => {
        try {
            const result = await query
            console.log('query',query)
            resolve(result)
        } catch (error) {
            reject(error)
        }
    })
    return newPromise
}
const page=[
    {key:{en:"Sub Admins",ar:"المسئولين"},path:"/sub-admins"},
    {key:{en:"Sub Admin Roles",ar:"صلاحيات المسئول الفرعي"},path:"/sub-admins/[subAdminId]/roles"}
]
const SubAdminRoles = ()=>{
    const route = useRouter()
    const admin = useContext(UserContext)
    const {t} = useLang()
    const [rules, rulesNext, getNextRules, rulesIsLoadingMore, rulesError, rulesIsLoading] = useFetch(`/api/rule`,{headers:{Authorization:`Bearer ${admin.token}`}})
    const [assignRules, hasNext, getNext, isLoadingMore, error, isLoading] = useFetch(`/api/assignRule?user=${route.query.subAdminId}`,{headers:{Authorization:`Bearer ${admin.token}`}})
    const {setPage} = useContext(PageContext)
    setPage(page)
    const {register, unregister, reset, setValue, handleSubmit} = useForm({
        form :"rule-form"
    })
    const filterRoles = (values)=>{
        let vals = values.rule

        for(let i=0; i<vals.length; i++){
            const rule = vals[i]
            const props = []
            for(let j=0; j<rule.properties.length; j++){
                if(rule.properties[j] && rule.properties[j] !== false){
                    props.push(rule.properties[j])
                }
            }
            rule.properties = props
            vals[i] = rule
        }
        values.rule = vals
    }
    const submitRoles = (values)=>{
        filterRoles(values)
        console.log(values)
        const promises = []
        const {user,rule} = values
        for(let i=0; i<rule.length; i++){
            const {properties, number, id, assignRule} = rule[i]
            if(assignRule){
                if(properties.length)
                    promises.push(createPromise(axios.put(`/api/assignRule/${assignRule}`,{properties},{headers:{Authorization:`Bearer ${admin.token}`}})))
                else
                    promises.push(createPromise(axios.delete(`/api/assignRule/${assignRule}`,{headers:{Authorization:`Bearer ${admin.token}`}})))
            }else{
                if(properties.length)
                    promises.push(createPromise(axios.post(`/api/assignRule`,{properties,rule:+id,user},{headers:{Authorization:`Bearer ${admin.token}`}})))
            }
        }
        Promise.all(promises).then(res=>{
            console.log("roles is updated",res)
            route.push(`/sub-admins`)
        }).catch(err=>console.error("error===>",error))
    }
    useEffect(()=>{
        if(rules.length && assignRules.length){
            let vals = []
            for(let i = 0; i< rules.length;i++){
                const rule = assignRules.find(r=>r.rule.id === rules[i].id)
                const props = rule ? rules[i].properties.map(p=> rule.properties.find(prop=> prop === p) || false) : []
                vals[i] = {properties: props || [], number:rules[i].number, id:rules[i].id,assignRule:rule && rule.id || ""}
            }
            console.log("resetting")
            reset({
                rule:vals,
                user: route.query.subAdminId
            })
        }
    },[assignRules, rules])


    const ruleCard = (rule,ruleIndex)=>{
        let assignRule = assignRules.find((r)=> r.rule.id === rule.id)
        return(
            <Card className="mb-5" style={{width:"255px"}}>
                <CardBody>
                    <CardTitle tag="h6">{rule.name[route.locale]}</CardTitle>
                    <input type="hidden" {...register(`rule.${ruleIndex}.number`)} />
                    <input type="hidden" {...register(`rule.${ruleIndex}.id`)} />
                    <input type="hidden" {...register(`rule.${ruleIndex}.assignRule`)} />
                    <input type="hidden" {...register(`user`)} />
                    {rule.properties.map((p,index)=>{
                        const {ref, ...field} = register(`rule.${ruleIndex}.properties.${index}`)
                        return (
                            <label key={index} className="d-block" htmlFor={`${rule.id}_${p}`}>
                                <Input type="checkbox" id={`${rule.id}_${p}`} value={p} {...field} innerRef={ref}  />
                                &nbsp;&nbsp; {t(p)}
                            </label>
                        )
                    })}
                </CardBody>
            </Card>
        )
    }

    return (
        <div className="user-page">
            <Container fluid>
                <div className="content">
                    <Loader Show={!isLoading && !rulesIsLoading} Render={true}>
                        <Form id="roles_form" onSubmit={handleSubmit(submitRoles)}>
                            <Row>
                                {rules.map((r,index)=>{
                                    return (
                                        <Col md="3" key={index}>
                                            {ruleCard(r,index)}
                                        </Col>
                                    )
                                })}
                            </Row>
                            <Button type="submit">Save</Button>
                        </Form>
                    </Loader>
                </div>
            </Container>
        </div>
    )
}


export default SubAdminRoles