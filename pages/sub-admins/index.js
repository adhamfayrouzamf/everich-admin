/* style / images */


/* methods / packages */
import React, { useContext } from 'react'
import { UserContext } from '../../components/contexts/AuthContext'
import { PageContext } from '../../components/contexts/PageContext';
/* components */
import { FaUserCheck } from "react-icons/fa"
import { Container } from 'reactstrap'
import CustomTable from '../../components/CustomTable/CustomTable';
import axios from "axios"

const page=[
    {key:{en:"Sub Admins",ar:"المسئولين"},path:"/sub-admins"}
]
const UserTable = props => {
    const {setPage} = useContext(PageContext)
    setPage(page)
    const user= useContext(UserContext)
    const activate = (value,id) => {
        console.log(value,id)
        return axios.put(`/api/admin/user/${id}/activate`,{activated:value},{headers:{Authorization:`Bearer ${user.token}`}}).then(res=>res)
    }

    const options={
        title:{value:{en:"Sub Admins",ar:"المسئولين الفرعيين"}},
        addBtn:true,roleNumber:6,
        url:"/sub-admins",api_url:"/api/user",fetch_url:"/api/user/allUsers?type=SUB_ADMIN",
        cols:[
            {key:"id",label:{en:"#",ar:"#"},check:true},
            {key:"username",label:{en:"Name",ar:"الاسم"}},
            {key:"email",label:{ar:"الايميل",en:"Email"}},
            {key:"phone",label:{ar:"الهاتف",en:"Phone"},notFound:"###########"},
            {key:"socialMediaType",label:{ar:"التسجيل بواسطة",en:"Login Method"}},
            {key:"id",label:{en:"Roles",ar:"الصلاحيات"},icon:<FaUserCheck />,isLink:true,url:"roles"},
            {key:"activated",label:{ar:"الحالة",en:"Status"}, 
                isToggler:true, 
                choices:[
                    {value:true,onClick:activate,label:{ar:"تعطيل",en:"Deactivate"},toState:1,
                    modal:{message:"Are You Sure You Want to Deactivate This User",title:"Deactivate User",confirmBtn:"Deactivate"}
                    },
                    {value:false,onClick:activate,label:{ar:"تفعيل",en:"Activate"},toState:0,
                    modal:{message:"Are You Sure You Want to Activate This User",title:"Activate User",confirmBtn:"Activate"}
                    }
                ]
            },
            {key:"createdAt",label:{ar:"تاريخ الانشاء",en:"Date"},isDate:true}
        ],
        controls:[
            "DETAILS", "UPDATE", "DELETE", "SHOW"
        ],
    }

    return (
        <div className="user-page">
            <Container fluid>
                <div className="content">
                    <CustomTable options={options} />
                </div>
            </Container>
        </div>
    )
}

export default UserTable