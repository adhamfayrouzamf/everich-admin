const { createServer } = require('http')
const { parse } = require('url')
const socketIO = require('socket.io')
const next = require('next')
const mongoose = require('mongoose')
// const i18n = require('i18n')
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()


// i18n.configure({
//         locales: ["ar", "en"],
//     directory: __dirname + '/locales',
//     register: global,
//     defaultLocale: 'ar'
// })
app.prepare().then(() => {
    var server = createServer((req, res) => {
        // Be sure to pass `true` as the second argument to `url.parse`.
        // This tells it to parse the query portion of the URL.
        const parsedUrl = parse(req.url, true)
        const { pathname, query } = parsedUrl
        if (pathname === '/a') {
            app.render(req, res, '/a', query)
        } else if (pathname === '/b') {
            app.render(req, res, '/b', query)
        } else {
            handle(req, res, parsedUrl)
        }
    })
    
    const io = socketIO(server,{
        cors:{
            origin:"*"
        }
    })

    global.chatNSP = io.of('/chat')
    global.adminNSP = io.of('/admin')

    chatNSP.on('connection', socket => {
        console.log('connected to socket')
        socket.on('joinChat', (data) => {
            console.log(data)
            socket.join('room-' + data.id)
            socket.emit('joined',{message:'joined chat successfully'})
        })

    })

    adminNSP.on('connection', socket => {
        socket.on('adminPermissions', () => {
            socket.join('adminChatTickets')
        })
        
    })
    
    const port = process.env.PORT || 5000
    server.listen(process.env.PORT || 5000, (err) => {
        if (err) throw err
        console.log('> Ready on http://localhost:'+port)
        mongoose.connect("mongodb+srv://madloldb:madloldb@madlol.fmhuj.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", { useNewUrlParser: true, useUnifiedTopology: true }, (error) => {
            if (error)
                console.log(error)
            console.log('connected to db')
        })
    })
})