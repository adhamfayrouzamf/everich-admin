import bcrypt from 'bcrypt';
import { body } from "express-validator";
import { checkValidations } from '../shared.controller/shared.controller';
import generateToken from '../../utils/token';
import User from '../../models/user.model/user.model';
import AssignRule from "../../models/assignRule.model/assignRule.model"
import Rule from "../../models/rule.model/rule.model"
import { checkExistThenGet, checkExist } from '../../helpers/CheckMethods';
import ApiError from '../../helpers/ApiError';

// import i18n from 'i18n'
// import reportController from '../report.controller/report.controller'
// import socketEvents from '../../socketEvents';
// import ConfirmationCode from '../../models/confirmationsCodes.model/confirmationscodes.model'
// import notificationController from '../notif.controller/notif.controller';
let populateQuery = [
    { path: 'rules', model: 'assignRule', populate: [{ path: 'rule', model: 'rule' }] },
    // { path: 'country', model: 'country' },
    // { path: 'city', model: 'city', populate: [{ path: 'country', model: 'country' }] },
    // { path: 'region', model: 'region', populate: [{ path: 'city', model: 'city', populate: [{ path: 'country', model: 'country' }] }] },
];
export default {


    validateAdminSignUp(isUpdate = false) {
        let validations;
        if (!isUpdate) {
            validations = [
                body('name').not().isEmpty().withMessage(() => { return ('nameRequired') }),
                body('email').trim().not().isEmpty().withMessage(() => { return ('emailRequired') })
                    .isEmail().withMessage(() => { return ('EmailNotValid') })
                    .custom(async (value, { req }) => {
                        value = (value.trim()).toLowerCase();
                        let userQuery = { email: value, deleted: false };
                        if (await User.findOne(userQuery))
                            throw new Error(('emailDuplicated'));
                        else
                            return true;
                    }),
                body('password').not().isEmpty().withMessage(() => { return ('passwordRequired') }),
                body('type').not().isEmpty().withMessage(() => { return ('typeIsRequired') }).isIn(['ADMIN', 'SUB_ADMIN']).withMessage(() => { return ('userTypeWrong') }),
                body('phone').not().isEmpty().withMessage(() => { return ('PhoneIsRequired') })
                    .custom(async (value, { req }) => {
                        value = (value.trim()).toLowerCase();
                        let userQuery = { phone: value, deleted: false };
                        if (await User.findOne(userQuery))
                            throw new Error(('phoneIsDuplicated'));
                        else
                            return true;
                    }),
                body('language').optional().not().isEmpty().withMessage(() => { return ('languageRequired') }),
                body('countryCode').not().isEmpty().withMessage(() => { return ('countryCodeRequired') }),
                body('countryKey').not().isEmpty().withMessage(() => { return ('countryKeyRequired') })
            ];
        } else {
            validations = [
                body('name').optional().not().isEmpty().withMessage(() => { return ('nameRequired') }),
                body('email').optional().trim().not().isEmpty().withMessage(() => { return ('emailRequired') })
                    .isEmail().withMessage(() => { return ('EmailNotValid') })
                    .custom(async (value, { req }) => {
                        value = (value.trim()).toLowerCase();
                        let userQuery = { email: value, deleted: false, _id: { $ne: req.user._id } };
                        if (await User.findOne(userQuery))
                            throw new Error(('emailDuplicated'));
                        else
                            return true;
                    }),
                body('password').optional().not().isEmpty().withMessage(() => { return ('passwordRequired') }),
                body('phone').optional().not().isEmpty().withMessage(() => { return ('PhoneIsRequired') })
                    .custom(async (value, { req }) => {
                        value = (value.trim()).toLowerCase();
                        let userQuery = { phone: value, deleted: false, _id: { $ne: req.user._id } };

                        if (await User.findOne(userQuery))
                            throw new Error(('phoneIsDuplicated'));
                        else
                            return true;
                    }),
                body('language').optional().not().isEmpty().withMessage(() => { return ('languageRequired') }),
                body('newPassword').optional().not().isEmpty().withMessage(() => { return ('newPasswordRequired') }),
                body('currentPassword').optional().not().isEmpty().withMessage(() => { return ('CurrentPasswordRequired') }),
                body('countryCode').optional().not().isEmpty().withMessage(() => { return ('countryCodeRequired') }),
                body('countryKey').optional().not().isEmpty().withMessage(() => { return ('countryKeyRequired') })
            ];
        }
        return validations;
    },

    async signUp(req, res, next) {
        try {
            const validated = checkValidations(req);
            validated.email = (validated.email.trim()).toLowerCase();
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'image', isUpdate: false });
                validated.image = image;
            }
            let createdUser = await User.create(validated);
            res.status(200).send({ user: createdUser, token: generateToken(createdUser.id) })
        } catch (err) {
            next(err);
        }
    },

    async updateProfile(req, res, next) {
        try {
            let user = req.user;
            const validated = checkValidations(req);
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'image', isUpdate: false });
                validated.image = image;
            }
            if (validated.password) {
                const salt = bcrypt.genSaltSync();
                validated.password = await bcrypt.hash(validated.password, salt);
            }
            if (validated.newPassword) {

                if (validated.currentPassword) {
                    if (bcrypt.compareSync(validated.currentPassword, user.password)) {
                        const salt = bcrypt.genSaltSync();
                        var hash = await bcrypt.hash(validated.newPassword, salt);
                        validated.password = hash;
                        delete validated.newPassword;
                        delete validated.currentPassword;
                        user = await User.findOneAndUpdate({ deleted: false, _id: user.id }, validated, { new: true })
                        user = await User.schema.methods.toJSONLocalizedOnly(user, i18n.getLocale());
                        res.status(200).send(user);
                    } else {
                        return next(new ApiError(403, ('currentPasswordInvalid')))
                    }
                } else {
                    return res.status(400).send({
                        error: [{
                            location: 'body',
                            param: 'currentPassword', msg: ('CurrentPasswordRequired')
                        }]
                    });
                }
            } else {
                user = await User.findOneAndUpdate({ deleted: false, _id: user.id }, validated, { new: true })
                res.status(200).send(user);
            }
        } catch (err) {
            next(err);
        }
    },

    validateAdminSignin() {
        let validations = [
            body('email').not().isEmpty().withMessage(() => { return ('emailRequired') }),
            body('password').not().isEmpty().withMessage(() => { return ('passwordRequired') }),
            body('type').not().isEmpty().withMessage(() => { return ('typeIsRequired') })
                .isIn(['ADMIN', 'SUB_ADMIN']).withMessage(() => { return ('userTypeWrong') }),
        ];
        return validations;
    },
    async adminSignIn(req, res, next) {
        try {
            const validated = checkValidations(req);
            var query = { deleted: false, type: validated.type };
            query.email = validated.email.toLowerCase();
            let user = await User.findOne(query).populate(populateQuery)
            if (user) {
                await user.isValidPassword(validated.password, async function (err, isMatch) {
                    if (err) {
                        next(err)
                    }
                    if (isMatch) {
                        if (!user.activated) {
                            return next(new ApiError(403, ('accountStop')));
                        }
                        res.status(200).send({ ...user._doc, token: generateToken(user.id) });
                    } else {
                        return next(new ApiError(400, ('passwordInvalid')));
                    }
                })
            } else {
                return next(new ApiError(403, ('userNotFound')));
            }
        } catch (err) {
            next(err);
        }
    },

    validateAddUser() {
        let validations = [
            body('username').not().isEmpty().withMessage(() => { return ('nameRequired') }),
            body('email').optional().trim().not().isEmpty().withMessage(() => { return ('emailRequired') })
                .isEmail().withMessage(() => { return 'EmailNotValid' })
                .custom(async (value, { req }) => {
                    value = (value.trim()).toLowerCase();
                    
                    let userQuery = { email: value, deleted: false };
                    if (await User.findOne(userQuery))
                        throw new Error(('emailDuplicated'));
                    else
                        return true;
                }),
            body('password').not().isEmpty().withMessage(() => { return ('passwordRequired') }),
            body('phone').not().isEmpty().withMessage(() => { return ('PhoneIsRequired') })
                .custom(async (value, { req }) => {
                    value = (value.trim()).toLowerCase();
                    let userQuery = { phone: value, deleted: false };
                    if (await User.findOne(userQuery))
                        throw new Error(('phoneIsDuplicated'));
                    else
                        return true;
                }),
            body('type').not().isEmpty().withMessage(() => { return ('typeIsRequired') })
                .isIn(['ADMIN', 'SUB_ADMIN', 'CLIENT']).withMessage(() => { return ('userTypeWrong') }),
            body('countryCode').not().isEmpty().withMessage(() => { return ('countryCodeRequired') }),
            body('countryKey').not().isEmpty().withMessage(() => { return ('countryKeyRequired') })
        ];
        return validations;
    },

    validateAdminChangeUser() {
        let validations = [
            body('username').optional().not().isEmpty().withMessage(() => { return ('nameRequired') }),
            body('email').optional().trim().not().isEmpty().withMessage(() => { return ('emailRequired') })
                .isEmail().withMessage(() => { return ('EmailNotValid') })
                .custom(async (value, { req }) => {
                    const {userId} = req.query
                    const user = await checkExistThenGet(userId, User)
                    value = (value.trim()).toLowerCase();
                    let userQuery = { email: value, deleted: false, _id: { $ne: userId }, socialMediaType:user.socialMediaType };
                    if (await User.findOne(userQuery))
                        throw new Error('emailDuplicated');
                    else
                        return true;
                }),
            body('password').optional().not().isEmpty().withMessage(() => { return ('passwordRequired') }),
            body('phone').optional().not().isEmpty().withMessage(() => { return ('PhoneIsRequired') })
                .custom(async (value, { req }) => {
                    value = (value.trim()).toLowerCase();
                    let userQuery = { phone: value, deleted: false, _id: { $ne: req.query.userId } };

                    if (await User.findOne(userQuery))
                        throw new Error(('phoneIsDuplicated'));
                    else
                        return true;
                }),
            body('language').optional().not().isEmpty().withMessage(() => { return ('languageRequired') }),
            body('notification').optional().not().isEmpty().withMessage(() => { return ('notificationRequired') }),
            body('countryCode').optional().not().isEmpty().withMessage(() => { return ('countryCodeRequired') }),
            body('countryKey').optional().not().isEmpty().withMessage(() => { return ('countryKeyRequired') }),
        ];

        return validations;
    },


    async addUser(req, res, next) {
        try {
            const validated = checkValidations(req);
            if (validated.email) {
                validated.email = (validated.email.trim()).toLowerCase();
            }
            let createdUser = await User.create(validated);
            res.status(200).send(createdUser);
        }catch(error) {
            next(error)
        }
    },

    async userInformation(req, res, next) {
        try {
            let userId = req.query.userId;
            if (!userId) userId = req.user.id;
            const user = await checkExistThenGet(userId, User, { deleted: false });
            // user = await User.schema.methods.toJSONLocalizedOnly(user, i18n.getLocale());
            res.status(200).send(user);
        } catch (error) {
            next(error);
        }
    },

    async adminUpdateUser(req, res, next) {
        try {
            const {userId} = req.query
            let validated = checkValidations(req);
            if (validated.email) validated.email = (validated.email.trim()).toLowerCase();
            let user = await checkExistThenGet(userId, User, { deleted: false });
            if (validated.password) {
                const salt = bcrypt.genSaltSync();
                validated.password = await bcrypt.hash(validated.password, salt);
            }
            user = await User.findOneAndUpdate({ deleted: false, _id: userId }, validated, { new: true })
            // await reportController.create({ ar: 'تعديل بيانات ', en: "Update Information" }, 'UPDATE', userId);
            res.status(200).send(user);
        } catch (error) {
            next(error);
        }
    },


    async deleteUserAccount(req, res, next) {
        try {
            const {userId} = req.query
            await checkExist(userId, User);

            // notificationNSP.to('room-' + userId).emit(socketEvents.LogOut, { user })
            res.status(200).send('Deleted Successfully');
        } catch (error) {
            next(error);
        }
    },

    validateAdminActivateUser() {
        let validations = [
            body('activated').not().isEmpty().withMessage(() => { return ('activatedRequired') }).isBoolean().withMessage(() => { return ('activatedValueError') }),
        ];
        return validations;
    },

    async adminActivateUser(req, res, next) {
        try {
            const {userId} = req.query
            let validated = checkValidations(req);
            console.log("test")

            await checkExist(+userId, User, { deleted: false });
            var newUser = await User.findByIdAndUpdate(userId, { activated: validated.activated }, { new: true });
            // if (newUser.activated == false) {
            //     notificationNSP.to('room-' + userId).emit(socketEvents.LogOut, { newUser })
            // }
            // if(newUser.activated == false && newUser.type == 'SUB_ADMIN'){
            //     adminNSP.to('room-' + userId).emit(socketEvents.LogOut, { newUser })
            // }
            res.status(200).send(newUser);
        } catch (error) {
            next(error);
        }
    }

};