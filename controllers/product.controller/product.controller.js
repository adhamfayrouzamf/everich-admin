import ApiResponse                       from "../../helpers/ApiResponse"
import ApiError                          from "../../helpers/ApiError"
import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods"
import { checkValidations, createDate }  from "../shared.controller/shared.controller"
import { body }                          from "express-validator"
import TradeMark                         from "../../models/tradeMarks.model/tradeMarks.model"
import Product                           from "../../models/product.model/product.model"
import Category                          from "../../models/category.model/category.model"
import Comment                           from '../../models/comment.model/comment.model'
import Favourites                        from '../../models/favourites.model/favourites.model'
import Advertisement                     from '../../models/advertisements.model/advertisements.model'
import Rate                              from "../../models/rate.model/rate.model"
import i18n                              from 'i18n'
import {deleteFile}                      from '../../services/cloudinary'
// const moment = require('moment')
// const dotObject = require('dot-object')

// const { sendEmail } = require('../../services/emailMessage.service')

let populateQuery = [
    { path: 'trademark', model: 'trademark' },
    { path: 'category', model: 'category'},
    { path: 'subCategory', model: 'category', populate: [{ path: 'parent', model: 'category' }] },
    { path: 'advertisement', model:'advertisement'}
    // { path: 'countrys', model: 'country' },
    // { path: 'cities', model: 'city' },
    // { path: 'regions', model: 'region' },
    // { path: 'colors.color', model: 'color' },
    // { path: 'colors.sizes.size', model: 'size' },
    // {path:'product', model:'rate'}
]
let lookupQuery = [
    {$lookup:{from:TradeMark.collection.name,localField:"trademark",foreignField:"_id",as:"trademark"}},
    {$lookup:{from:Category.collection.name,localField:"category",foreignField:"_id",as:"category"}},
    {$lookup:{from:Category.collection.name,localField:"subCategory",foreignField:"_id",as:"subCategory"}},
    {$lookup:{from:Advertisement.collection.name,localField:"advertisement",foreignField:"_id",as:"advertisement"}},
    {$unwind:"$trademark"},{$unwind:"$category"},{$unwind:"$subCategory"},{$unwind:{path:"$advertisement","preserveNullAndEmptyArrays": true}}
]


let createPromise = (query) => {
    let newPromise = new Promise(async (resolve, reject) => {
        try {
            const result = await query
            console.log('query',query)
            resolve(result)
        } catch (error) {
            reject(error)
        }
    })
    return newPromise
}

let checkinFavourites = async (list, userId) => {
    try {
        let promises = [];
        let query = { deleted: false, user: userId }
        for (let index = 0; index < list.length; index++) {
            query.product = list[index].id;
            let promis = Favourites.findOne(query);
            if (promis)
                promises.push(createPromise(promis));
        }
        let finalResult = await Promise.all(promises);
        for (let index = 0; index < finalResult.length; index++) {
            if (finalResult[index]) {
                list[index].favourite = true;
            } else {
                list[index].favourite = false;
            }
        }
        return list;
    } catch (error) {
        throw error
    }
}

let checkInUserRate = async (productId,userId)=>{
    try{
        let rate = await Rate.findOne({user:userId,product:productId})
        if(rate)
            return rate.rate
        return 0.0

    }catch(error){
        throw error
    }
}

let getProducts = async (list) => {
    try {
        let promises = []
        let result = []
        for (let index = 0; index < list.length; index++) {
            let aggregateQuery = [{ $match: { deleted: false, 'products.product': +list[index], status: 'DELIVERED' } },
            { $group: { _id: { product: '$products' } } },
            { $unwind: '$_id.product' },
            { $match: { '_id.product.product': +list[index] } },
            { $group: { _id: "$_id.product.product", totalQuantity: { $sum: "$_id.product.quantity" }, totalPrice: { $sum: { $multiply: ["$_id.product.quantity", "$_id.product.priceAfterOffer"] } } } },
            { $sort: { "totalQuantity": -1 } },
            {
                $lookup: {
                    from:require( Product.collection.name),
                    localField: "_id",
                    foreignField: "_id",
                    as: "product"
                }
            },
            { $unwind: '$product' },
            ]

            promises.push(createPromise(Order.aggregate(aggregateQuery)))
        }
        let finalResult = await Promise.all(promises)
        for (let index = 0 ;index < finalResult.length; index++) {
            if (finalResult[index].length > 0) {
                delete finalResult[index][0]._id
                result.push(finalResult[index][0].product._id)
            }
        }

        return result
    } catch (error) {
        throw error
    }

}
export default
{
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            const {category, subCategory, user, favorites, localized,similar,categoryProducts,
                name, description, price, offer, hasOffer,
                quantity, rate, trademark,sortByPrice,
                fromPrice, toPrice, topSelling, lastProducts, lastOffers, useStatus, serialNumber, all} = req.query

            let query = {deleted:false}
            let sortQuery = {updatedAt:-1}
            let date = createDate(req)

            if (date) query.createdAt = {$gte: date.startOfDate, $lte: date.endOfDate}
            
            if (serialNumber) query.serialNumber = serialNumber;

            if (fromPrice && toPrice) {
                query.price = { $gte: fromPrice, $lte: toPrice }
            } else if (fromPrice) {
                query.price = { $gte: fromPrice }
            } else if (toPrice) {
                query.price = { $lte: toPrice }
            }
            if (hasOffer) query.offer = { $gt: 0 };
            if (quantity) query.quantity = quantity;
            if (rate) query.rate = rate;
            if (offer) query.offer = offer;
            if (price) query.price = price;
            if (useStatus) query.useStatus = useStatus;

            if (categoryProducts) {
                let subIds = await Category.find({ deleted: false, parent: categoryProducts }).distinct('_id');
                query.$or = [{ category: categoryProducts }, { category: { $in: subIds } }];
            }


            if (trademark) {
                if (Array.isArray(trademark)) {
                    query.trademark = { $in: trademark };
                } else if (!isNaN(trademark)) {
                    query.trademark = +trademark;
                }
            }
            if (category) {
                if (Array.isArray(category)) {
                    query.category = { $in: category };
                } else if (!isNaN(category)) {
                    query.category = +category;
                }
            }
            if(subCategory){
                if (Array.isArray(subCategory)) {
                    query.subCategory = { $in: subCategory };
                } else if (!isNaN(subCategory)) {
                    query.subCategory = +subCategory;
                }
            }

            if (name) {
                query.$or = [{ 'name.en': { '$regex': name, '$options': 'i' } }, { 'name.ar': { '$regex': name, '$options': 'i' } }]
            }
            
            if (description) {
                query.$or = [{ 'description.en': { '$regex': description, '$options': 'i' } }, { 'description.ar': { '$regex': description, '$options': 'i' } }]
            }

            if (similar) {
                let product = await checkExistThenGet(similar, Product, { deleted: false });
                query.$or = [{ category: product.category, subCategory:product.subCategory, trademark: product.trademark }];
                query._id = { $ne: similar }
            }
            if (topSelling) {
                let topSellingProducts = await getProducts(await Product.find({ deleted: false }).distinct('_id'));
                if (topSellingProducts.length > 0) {
                    query._id = { $in: topSellingProducts }
                } else {
                    sortQuery = { updatedAt: 1 }
                }
            }
            if (lastProducts) {
                sortQuery = { updatedAt: -1 }
            }
            if (lastOffers) {
                query.offer = { $gt: 0 }
            }
            if (sortByPrice) {
                sortQuery = { priceAfterOffer: sortByPrice }
            }
            if(favorites){
                query.favorites = +favorites
            }
            let p = await Product.aggregate([
                {$match:query},
                {$addFields:{id:"$_id",favorite:{$cond:{if:{$in:[+favorites || +user ,"$favorites"]},then:true,else:false}}}},
                {$project:{favorites:0,_id:0}},
                {$sort:sortQuery},
                {$limit:limit},
                {$skip:(page-1)*limit},
                ...lookupQuery
            ])
            
            // p = await Product.populate(p,populateQuery)
            const productCount = await Product.count(query);
            let pageCount = Math.ceil(productCount / limit);
            console.log(req.locale,p)
            if(localized)
                p = Product.schema.methods.toJSONLocalizedOnly(p,req.locale)

            res.status(200).send(new ApiResponse(p, page, pageCount, limit, productCount, req))
        } catch (err) {
            next(err)
        }
    },

    validateBody(isUpdate = false) {
        let validations = []
        if (!isUpdate) {
            validations = [
                

                body('price').not().isEmpty().withMessage(),
                body('offer').optional().not().isEmpty().withMessage('offer is empty')
                    .isInt({ max: 100, min: 0 }).withMessage('offer must be integer'),
                body('quantity').optional().not().isEmpty().withMessage('quantity is empty')
                    .isInt().withMessage('quantity must be integer'),

                body('category').not().isEmpty().withMessage('category required')
                    .custom(async (value) => {
                        await checkExist(value, Category, { deleted: false })
                        return true
                    }),
                body('subCategory').optional().not().isEmpty().withMessage('')
                .custom(async (value,{req}) => {
                    await checkExist(value, Category, { deleted: false, parent: req.body.category })
                    return true
                }),
                body('trademark').not().isEmpty().withMessage('trademark required')
                    .custom(async (value) => {
                        await checkExist(value, TradeMark, { deleted: false })
                        return true
                    }),
                body('advertisement').optional().not().isEmpty().withMessage('must add advertisement').custom(async(val,{req})=>{
                    await checkExist(val,Advertisement,{deleted:false})
                    return true
                }),
                body('name.en').not().isEmpty().withMessage(),
                body('name.ar').not().isEmpty().withMessage(),
                body('desc.en').not().isEmpty().withMessage(),
                body('desc.ar').not().isEmpty().withMessage(),
                body('image.id').not().isEmpty().withMessage('image id is required'),
                body('image.url').not().isEmpty().withMessage('image id is required'),
                body('images.*.id').not().isEmpty().withMessage('all images must contain id'),
                body('images.*.url').not().isEmpty().withMessage('all images must contain url'),
                body('useStatus').not().isEmpty().withMessage( ).isIn(['USED', 'NEW']).withMessage('error type'),
                body('serialNumber').optional().not().isEmpty().withMessage('serial number required'),

            ]
        }
        else {
            validations = [
                body('price').optional().not().isEmpty().withMessage(),

                body('offer').optional().not().isEmpty().withMessage()
                    .isInt({ max: 100, min: 0 }).withMessage(),
                body('quantity').optional().not().isEmpty().withMessage(''),

                body('category').optional().not().isEmpty().withMessage('')
                    .custom(async (value) => {
                        await checkExist(value, Category, { deleted: false })
                        return true
                    }),
                body('subCategory').optional().not().isEmpty().withMessage('')
                .custom(async (value,req) => {
                    await checkExist(value, Category, { deleted: false, parent: req.body.category })
                    return true
                }),
                body('trademark').optional().not().isEmpty().withMessage('')
                .custom(async (value) => {
                    await checkExist(value, TradeMark, { deleted: false })
                    return true
                }),
                body('advertisement').optional().not().isEmpty().withMessage('must add advertisement').custom(async(val,{req})=>{
                    await checkExist(val,Advertisement,{deleted:false})
                    return true
                }),
                body('name.en').optional().not().isEmpty().withMessage(),
                body('name.ar').optional().not().isEmpty().withMessage(),
                body('desc.en').optional().not().isEmpty().withMessage(),
                body('desc.ar').optional().not().isEmpty().withMessage(),
                body('image.id').optional().not().isEmpty().withMessage(),
                body('image.url').optional().not().isEmpty().withMessage(),
                body('images.*.id').optional().not().isEmpty().withMessage(),
                body('images.*.url').optional().not().isEmpty().withMessage(),
                body('useStatus').optional().not().isEmpty().withMessage( ).isIn(['USED', 'NEW']).withMessage('error type'),
                body('serialNumber').optional().not().isEmpty().withMessage(),


            ]
        }
        return validations
    },

    async create(req, res, next) {
        try {
            
            let validated = checkValidations(req)
            validated.priceAfterOffer=validated.offer >0 ? validated.price - (validated.price * (validated.offer/100.0)).toFixed(1) : validated.price

            let createdproduct = await Product.create(validated)
            createdproduct = await Product.populate(createdproduct,populateQuery)
            res.status(200).send(createdproduct)

        } catch (err) {
            console.log(err.message)
            next(err)
        }
    },


    async findById(req, res, next) {
        try {
            let { productId } = req.query
            let { user, localized } = req.query
            

            let product = (await Product.aggregate([
                {$match:{_id:+productId}},
                {$addFields:{id:"$_id",favorite:{$cond:{if:{$in:[+user,"$favorites"]},then:true,else:false}}}},
                {$project:{_id:0,favorites:0}},
                {$limit:1},
                ...lookupQuery
            ]))[0]
            console.log(product)
            if (user) {
                let rate = await checkInUserRate(productId,user)
                product.userRate = rate
            }
            if(localized)
                product = Product.schema.methods.toJSONLocalizedOnly(product,req.locale)
            
            res.status(200).send(product)
        } catch (err) {
            next(err)
        }
    },

    async update(req, res, next) {
        try {
            let { productId } = req.query
            let validated = checkValidations(req)

            if(validated.image){
                let product = await checkExistThenGet(productId, Product, { deleted: false })
                if(validated.image.id !== product.image.id)
                    deleteFile(product.image.id)
            }
            let updatedproduct = await Product.findByIdAndUpdate(productId, {$set:validated}, { new: true })
            updatedproduct = await Product.populate(updatedproduct, populateQuery)
            res.status(200).send(updatedproduct)
        }
        catch (err) {
            next(err)
        }
    },

    async delete(req, res, next) {
        try {
            let { productId } = req.query
            await checkExist(productId, Product, 'product not found')
            await Product.deleteOne({_id:productId})
            await Rate.deleteMany({ product: productId })
            await Favourites.deleteMany({ product: productId })
            await Comment.deleteMany({ product:productId })
            res.status(200).send('Deleted Successfully')
        }
        catch (err) {
            next(err)
        }
    },

    validateRate() {
        return [
            body('rate').not().isEmpty().withMessage('must add rate')
                .isInt({ min: 1, max: 5 }).withMessage('rate must be between 1 & 5'),
        ]
    },

    async rate(req, res, next) {
        try {
            let userId = req.user._id
            let { productId } = req.query
            let product = await checkExistThenGet(productId, Product, { deleted: false, populate:populateQuery })
            let validatedcheck = checkValidations(req)
            let checkIfRated = await Rate.findOne({ product: productId, user: userId })
            console.log(validatedcheck.rate)
            if (checkIfRated) {
                if (product.rateValues < checkIfRated.rate) product.rateValues = 0
                else { product.rateValues = Number(product.rateValues) - Number(checkIfRated.rate) }

                product.rateValues = Number(product.rateValues) + Number(validatedcheck.rate)
                checkIfRated.rate = validatedcheck.rate
                console.log(product.rateValues)
                await checkIfRated.save()
            }
            else {
                await Rate.create({ product: productId, user: userId, rate: validatedcheck.rate })
                product.rateValues = Number(product.rateValues) + Number(validatedcheck.rate)
                product.rateCount = Number(product.rateCount) + 1
                console.log('created')
            }

            product.totalRate = (product.rateValues / product.rateCount).toFixed(1)
            console.log(product.totalRate)
            await product.save()
            res.status(200).send(product)
        }
        catch (err) {
            next(err)
        }
    },

    async uploadImage(req, res, next) {
        try {
            let productImage = await handleImg(req, { attributeName: 'image', isUpdate: false })
            res.status(200).send({ link: productImage })
        } catch (error) {
            next(error)
        }
    },
}