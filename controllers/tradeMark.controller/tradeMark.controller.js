import ApiResponse                       from "../../helpers/ApiResponse"
import ApiError                          from "../../helpers/ApiError"
import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods"
import { checkValidations, createDate }  from "../shared.controller/shared.controller"
import { body }                          from "express-validator"
import TradeMark                         from "../../models/tradeMarks.model/tradeMarks.model"
import Comment                           from "../../models/comment.model/comment.model"
import Category                          from "../../models/category.model/category.model"
import moment                            from 'moment'
import i18n                              from 'i18n'

let populateQuery = [{path:'category',model:'category', populate:[{path:'parent', model:'category'}]}];


export default {

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let {name, category,isParent,subCats ,all, localized} = req.query
            let query = {deleted: false};
            let date = createDate(req)
            if(date) query.createdAt = {$gte: date.startOfDate, $lte: date.endOfDate}
            // if (category && isParent){
            //     subIds = await Category.find({parent:categoryId}).distinct('_id')
            //     subIds.push(categoryId)
            //     query.categoryId = {$in:subIds};
            // }else if(categoryId){
            //     query.categoryId = categoryId
            // }
            if(category) query.category = category
            if (name)
                query.$or = [{ 'name.en': { '$regex': name, '$options': 'i' } }, { 'name.ar': { '$regex': name, '$options': 'i' } }]

            let tradeMark = await TradeMark.find(query).sort({updatedAt:-1}).limit(limit).skip((page-1)*limit).populate(populateQuery)
            let trademarkCount = await TradeMark.count(query)
            let pageCount = Math.ceil(trademarkCount / limit)
            if(localized)
                tradeMark = TradeMark.schema.methods.toJSONLocalizedOnly(tradeMark,req.locale)
            res.send(new ApiResponse(tradeMark, page, pageCount, limit, trademarkCount, req ));
        } catch (err) {
            throw err
        }
    },

    validateBody(isUpdate = false) {
        let validations = [];
        if (!isUpdate) {
            validations = [
                body('category').not().isEmpty().withMessage('must add category id')
                    .custom(async (value) => {
                        await checkExist(value, Category, { deleted: false });
                        return true;
                    }),

                body('name.ar').not().isEmpty().withMessage('must add name').custom(async(val,{req})=>{
                    let tradeMark = await TradeMark.findOne({'name.ar':val}).lean()
                    if(tradeMark)
                        throw new Error('name already exists')
                    return true
                }),
                body('name.en').not().isEmpty().withMessage('must add name').custom(async(val,{req})=>{
                    let tradeMark = await TradeMark.findOne({'name.en':val}).lean()
                    if(tradeMark)
                        throw new Error('name already exists')
                    return true
                }),
            ];
        }
        else{
            validations = [
                body('category').optional().not().isEmpty().withMessage('must add category id')
                    .custom(async (value) => {
                        await checkExist(value, Category, { deleted: false });
                        return true;
                    }),

                body('name.ar').optional().not().isEmpty().withMessage('must add name').custom(async(val,{req})=>{
                    let {trademarkId} = req.query
                    let tradeMark = await TradeMark.findOne({_id:{$ne:trademarkId},'name.ar':val, deleted:false}).lean()
                    if(tradeMark)
                        throw new Error('name already exists')
                    return true
                }),
                body('name.en').optional().not().isEmpty().withMessage('must add name').custom(async(val,{req})=>{
                    let {trademarkId} = req.query
                    let tradeMark = await TradeMark.findOne({_id:{$ne:trademarkId},'name.en':val, deleted:false}).lean()
                    if(tradeMark)
                        throw new Error('name already exists')
                    return true
                }),
            ];
        }
        return validations;
    },

    async create(req, res, next) {
        try {
            const validated = checkValidations(req);
            let createdtradeMark = await TradeMark.create(validated);
            createdtradeMark = TradeMark.schema.methods.toJSONLocalizedOnly(createdtradeMark, req.locale);
            res.status(200).send(createdtradeMark);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            console.log('test')
            let { trademarkId } = req.query;
            let {localized} = req.query
            let query = {populate:populateQuery}

            let tradeMark = await checkExistThenGet(trademarkId, TradeMark, query);
            if(localized)
                tradeMark = TradeMark.schema.methods.toJSONLocalizedOnly(tradeMark, req.locale);
                
            res.status(200).send(tradeMark);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {
        try {
            
            let { trademarkId } = req.query;
            await checkExist(trademarkId, TradeMark, { deleted: false });
            var validated = checkValidations(req);
            let updatedtradeMark = await TradeMark.findByIdAndUpdate(trademarkId, validated , { new: true })
            updatedtradeMark = await TradeMark.populate(updatedtradeMark,populateQuery)
            res.status(200).send(updatedtradeMark);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            
            let { trademarkId } = req.query;
            await TradeMark.findByIdAndDelete(trademarkId)
            let products = await Product.find({ trademark: trademarkId }).distinct('_id')
            await Product.deleteMany({ trademark: trademarkId })
            await Favourites.deleteMany({ product: { $in: products } })
            await Rate.deleteMany({ product: { $in: products } })
            await Comment.deleteMany({ product:{ $in: products } })
            res.status(200).send('Deleted Successfully');
        }
        catch (err) {
            next(err);
        }
    }
};