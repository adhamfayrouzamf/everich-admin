import ApiResponse                       from "../../helpers/ApiResponse"
import ApiError                          from "../../helpers/ApiError"
import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods"
import { checkValidations, createDate }  from "../shared.controller/shared.controller"
import { check }                         from "express-validator"
import ChatTicket                        from "../../models/chatTicket.model/chatTicket.model"
import Message                           from "../../models/message.model/message.model"
import User                              from "../../models/user.model/user.model"
import moment                            from 'moment'


let populateQuery = [
    { path: 'client', model: 'user' },
    { path: 'admin', model: 'user' },
]
const createMessage = async (text,sender,reciever,chatTicket)=>{
    try{
        let message = { sender: sender, message: {text:text},chatTicket:chatTicket, reciever:{user:reciever} };

        message.lastMessage = true;
        let createdMessage = await Message.create(message);
        createdMessage = await Message.populate(createdMessage, [{ path: 'sender', model: 'user' }, { path: 'reciever.user', model: 'user' }]);

        await Message.updateMany({deleted:false , _id:{$ne:createdMessage.id} ,chatTicket:chatTicket},{$set:{lastMessage:false}});
        
        chatNSP.to('room-'+chatTicket).emit('sendMessage', createdMessage);
    }catch(err){
        console.log(err)
    }
    
}

export default {

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1 , limit = +req.query.limit || 20
            const {status} = req.query
            let date = createDate(req)
            const query = {deleted:false}
            if(date) query.createdAt = {$gte: date.startOfDate, $lte: date.endOfDate}
            if(status) query.status = status
            
            let chats= await ChatTicket.find(query).sort({updatedAt:-1}).limit(limit).skip((page-1)*limit).populate(populateQuery)
            let chatCount = await ChatTicket.count(query)
            let pageCount = Math.ceil(chatCount / limit)
            res.status(200).send(new ApiResponse(chats, page, pageCount, limit, chatCount, req))
        } catch (err) {
            next(err)
        }
    },

    validateBody(isUpdate = false) {
        let validations = []
        if (!isUpdate) {
            validations = [
                check('client').not().isEmpty().withMessage('must add client id').custom(async (val,{req})=>{
                    await checkExist(val,User,{deleted:false})
                    return true
                }),
                check('admin').optional().not().isEmpty().withMessage('must add admin id').custom(async (val,{req})=>{
                    checkExist(val,User,{deleted:false,$or:[{type:'ADMIN'},{type:'SUB_ADMIN'}]})
                    return true
                }),
                check('status').optional().not().isEmpty().withMessage('must add status'),
                check('notes').not().isEmpty().withMessage('must add notes'),
                check('reason').optional().not().isEmpty().withMessage('must add reason')
            ]
        }
        else {
            validations = [
                check('client').optional().not().isEmpty().withMessage('must add client id').custom(async (val,{req})=>{
                    await checkExist(val,User,{deleted:false})
                    return true
                }),
                check('admin').optional().not().isEmpty().withMessage('must add admin id').custom(async (val,{req})=>{
                    checkExist(val,User,{deleted:false,$or:[{type:'ADMIN'},{type:'SUB_ADMIN'}]})
                    return true
                }),
                check('status').optional().not().isEmpty().withMessage('must add status'),
                check('notes').optional().not().isEmpty().withMessage('must add notes'),
                check('reason').optional().not().isEmpty().withMessage('must add reason')
            ]
        }
        return validations
    },

    async create(req, res, next) {
        try {
            let validated = checkValidations(req)
            
            let createdChat = await ChatTicket.create(validated)
            createdChat = await ChatTicket.populate(createdChat,populateQuery)
            res.status(200).send(createdChat)

            adminNSP.to('adminChatTickets').emit('requestTicket',createdChat)
            createMessage('Someone Will be with you in minutes',null,req.user.id,createdChat.id)
        } catch (err) {
            next(err)
        }
    },


    async findById(req, res, next) {
        try {
            const {chatId} = req.query
            let query = {deleted:false, populate:populateQuery}
            let c = await checkExistThenGet(id, ChatTicket, query)

            res.status(200).send(c)
        } catch (err) {
            next(err)
        }
    },

    async update(req, res, next) {
        try {
            let validated = checkValidations(req)
            const {chatId} = req.query
            let chat = await checkExistThenGet(id, ChatTicket, { deleted: false })
            
            let updatedCategory = await ChatTicket.findByIdAndUpdate(id, {$set:validated},{new:true})
            updatedCategory = await ChatTicket.populate(updatedCategory,populateQuery)
            res.status(200).send(updatedCategory)
        }
        catch (err) {
            next(err)
        }
    },

    async activate(req, res, next) {
        try {

            const {chatId} = req.query
            if(!id)
                throw new ApiError(404,'must add chatId')

            if(!req.user || req.user.type !== "ADMIN" && req.user.type !=="SUB_ADMIN"){
                throw new ApiError(401,'unauthorized user')
            }
            
            let chat = await checkExistThenGet(id, ChatTicket, { deleted: false })

            chat.status = "ACTIVE"
            chat.admin = req.user.id
            chat.save()
            chat = await ChatTicket.populate(chat,populateQuery)
            res.status(200).send(chat)
            createMessage(`This Chat is now active`,req.user.id,chat.client,chat.id)
            chatNSP.to('room-'+chat.id).emit('activate-chat','ACTIVE',chat.admin)
        }
        catch (err) {
            next(err)
        }
    },
    async deactivate(req, res, next) {
        try {

            const {chatId} = req.query
            if(!id)
                throw new ApiError(404,'must add chatId')
                
            if(!req.user || req.user.type !== "ADMIN" && req.user.type !=="SUB_ADMIN"){
                throw new ApiError(404,'unauthorized user')
            }

            let chat = await checkExistThenGet(id, ChatTicket, { deleted: false })

            chat.status = "CLOSED"
            chat.save()

            chat = await ChatTicket.populate(chat,populateQuery)
            res.status(200).send(chat)
            
            createMessage(`This Chat session got closed by ${req.user.username}`,req.user.id,chat.client,chat.id)
            chatNSP.to('room-'+chat.id).emit('activate-chat','CLOSED',chat.admin)
        }
        catch (err) {
            next(err)
        }
    },
    async delete(req, res, next) {
        try {
            
            const {chatId} = req.query
            
            await checkExist(id, ChatTicket,'chat ticket not found')
            await ChatTicket.deleteOne({_id:id})

            await Message.deleteMany({chatTicket:id})
            
            res.status(200).send('Deleted Successfully')
        }
        catch (err) {
            next(err)
        }
    },
}