import ApiResponse                            from "../../helpers/ApiResponse"
import ApiError                               from "../../helpers/ApiError"
import { body }                               from 'express-validator'
import { checkValidations }                   from "../shared.controller/shared.controller"
import {uploadFile,uploadManyImgs,deleteFile} from '../../services/cloudinary'


let createPromise = (query) => {
    let newPromise = new Promise(async (resolve, reject) => {
        try {
            const result = await query
            console.log('query',query)
            resolve(result)
        } catch (error) {
            reject(error)
        }
    })
    return newPromise
}


export default {
    validateBody(){
        return [
            body('dir').not().isEmpty().withMessage('directory name is required')
        ]
    },

    async upload(req,res,next){
        try{
            let validated = checkValidations(req)
            let promises = []
            if(req.files && req.files['files'] && req.files['files'].length>0){
                for(let i = 0; i<req.files['files'].length;i++){
                    let promise = uploadFile(req.files['files'][i],validated.dir)
                    promises.push(createPromise(promise))
                }
                let files = await Promise.all(promises)
                res.status(200).send(files)
            }else{
                throw new ApiError(404,'files required')
            }
        }catch(err){
            next(err)
        }
    },
    async uploadMany(req,res,next){
        try{
            let validated = checkValidations(req)
            let images
            console.log(req.files)
            if(req.files && req.files['images'] && req.files['images'].length>0){
                images = await uploadManyImgs(req.files['images'],validated.dir)
            }else{
                throw new ApiError(404,'images required')
            }
            res.json(images)
        }catch(err){
            console.log(err)
        }
    },
    async delete(req,res,next){
        try{
            const {ids} = req.body
            console.log(ids)
            if(!ids || !ids.length)
                throw new ApiError(404,'image required')

            let promises = ids.map(id=>{
                return deleteFile(id)
            })
            await Promise.all(promises)
            res.send("images deleted successfully")
        }catch(err){
            console.log(err)
            next(err)
        }
    }
}