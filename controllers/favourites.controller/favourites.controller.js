import User                              from "../../models/user.model/user.model"
import Product                           from "../../models/product.model/product.model"
import Favourites                        from "../../models/favourites.model/favourites.model"
import Category                          from "../../models/category.model/category.model"
import Trademark                         from "../../models/tradeMarks.model/tradeMarks.model"
import ApiResponse                       from "../../helpers/ApiResponse"
import ApiError                          from "../../helpers/ApiError"
import { checkExistThenGet, checkExist } from "../../helpers/CheckMethods"
import { check }                         from 'express-validator'
import { checkValidations, createDate }  from "../shared.controller/shared.controller"
import i18n                              from 'i18n'

let populateQuery = [
    { path: 'product', model: 'product', 
        populate: [{ path: 'category', model: 'category', populate:[{path:'parent', model:'category'}] },{ path: 'trademark', model: 'trademark' }] 
    }
    // { path: 'userId', model: 'user' }
];


export default {

    validateBody(isUpdate = false) {
        let validations = [
            check('product').not().isEmpty().withMessage('product id is required')
                .custom(async (value) => {
                    await checkExistThenGet(+value, Product, { deleted: false})
                })
        ];
        return validations;
    },

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            const {product, localized} = req.query;
            let date = createDate(req)
            let query = { deleted: false };
            
            if (date) query.createdAt = {$gte: date.startOfDate, $lte: date.endOfDate}
            if (req.user) query.user = req.user._id;
            if (product) query.product = product;
            
            let favs = await Favourites.find(query).sort({updatedAt:-1}).limit(limit).skip((page-1)*limit).populate(populateQuery)
            let favCount = await Favourites.count(query)
            let pageCount = Math.ceil(favCount / limit)
            if(localized)
                favs = Favourites.schema.methods.toJSONLocalizedOnly(favs,req.locale)
            res.send(new ApiResponse(favs, page, pageCount, limit, favCount, req));
        } catch (err) {
            next(err);
        }
    },

    async create(req, res, next) {
        try {
            let validated = checkValidations(req);
            validated.user = req.user._id;
            let oldFav = await Favourites.findOne({ ...validated, deleted: false });
            if (oldFav) {
                return next(new ApiError(400,'duplicated'));
            }
            let favourite = await Favourites.create(validated);
            console.log('favourite')
            favourite = await Favourites.populate(favourite, populateQuery);
            res.status(200).send({ favourite: favourite });
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {
        try {
            let { favouriteId } = req.query;
            await checkExist(favouriteId, Favourites, { deleted: false });
            let validated = checkValidations(req);
            let updatedfavourite = await Favourites.findByIdAndUpdate(favouriteId, validated, { new: true });
            updatedfavourite = await Favourites.populate(updatedfavourite, populateQuery);
            res.status(200).send({ favourite: updatedfavourite });
        } catch (err) {
            next(err);
        }
    },

    async findById(req, res, next) {
        try {
            let { favouriteId } = req.query;
            let favourite = await checkExistThenGet(favouriteId, Favourites, { deleted: false, populate: populateQuery });
            res.status(200).send(favourite);

        } catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            const {product} = req.body
            let query = { user: req.user._id, product:product }
            let favourite = await Favourites.deleteOne(query);
            res.status(200).send("Deleted Successfully");
        }
        catch (err) {
            next(err);
        }
    }
}