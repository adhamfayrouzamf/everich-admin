import Region                            from "../../models/region.model/region.model"
import City                              from "../../models/city.model/city.model"
import Country                           from "../../models/country.model/country.model"
import ApiResponse                       from "../../helpers/ApiResponse"
import ApiError                          from "../../helpers/ApiError"
import { checkExistThenGet, checkExist } from "../../helpers/CheckMethods"
import { body }                          from 'express-validator'
import { checkValidations, createDate }  from "../shared.controller/shared.controller"
import i18n                              from 'i18n'
import moment                            from 'moment'

let populateQuery = [{ path: 'city', model: 'city',populate: [{ path: 'country', model: 'country' }] }];


export default {

    validateBody(isUpdate = false) {
        let validations
        if (!isUpdate) {
            validations = [
                body('name').not().isEmpty().withMessage('must add region')
                    .custom(async (val, { req }) => {

                        let query = { 'name': val, deleted: false, city: req.body.city };
                        let region = await Region.findOne(query).lean();
                        if (region)
                            throw new Error('region already exists');
                        return true;
                    }),

                body('city').not().isEmpty().withMessage('must add city id').custom(async (val, { req }) => {
                    await checkExist(val, City, { deleted: false });
                    return true;
                }),
                // body('transportPrice').not().isEmpty().withMessage('must add transportPrice')
            ];
        }
        else {
            validations = [
                body('name').optional().not().isEmpty().withMessage('must add region')
                    .custom(async (val, { req }) => {

                        let query = { 'name': val, deleted: false, city: req.body.city };
                        let region = await Region.findOne(query).lean();
                        if (region)
                            throw new Error('region already exists');
                        return true;
                    }),

                body('city').optional().not().isEmpty().withMessage('must add city id').custom(async (val, { req }) => {
                    await checkExist(val, City, { deleted: false });
                    return true;
                }),
                // body('transportPrice').optional().not().isEmpty().withMessage('must add transportPrice')
            ];

        }
        return validations;
    },

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            var { name, city, country, localized} = req.query;
            let query = { deleted: false };
            let date = createDate(req)
            if (date) query.createdAt = {$gte: date.startOfDate, $lte: date.endOfDate}
            if (name) query.$or = [{'name.ar': { '$regex': name, '$options': 'i' }},{'name.en': { '$regex': name, '$options': 'i' }}];
            if (city) query.city = city;
            if (country){
                let cities = await City.find({deleted: false,country:country}).distinct('_id');
                query.city = {$in: cities}
            }
            
            let regions = await Region.find(query).sort({updatedAt:-1}).limit(limit).skip((page-1)*limit).populate(populateQuery)
            let regionCount = await Region.count(query)
            let pageCount = Math.ceil(regionCount / limit)

            if (localized) {
                regions = Region.schema.methods.toJSONLocalizedOnly(regions, req.locale);
            }

            res.send(new ApiResponse(regions, page, pageCount, limit, pageCount, req));


        } catch (err) {
            next(err);
        }
    },

    async create(req, res, next) {
        try {
            
            let validated = checkValidations(req);
            let region = await Region.create(validated);
            region = await Region.populate(region, populateQuery);
            res.status(200).send(region);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {
        try {
            let { regionId } = req.query;         
            await checkExist(regionId, Region, { deleted: false });
            let validated = checkValidations(req);
            let updatedRegion = await Region.findByIdAndUpdate(regionId, validated, { new: true });
            res.status(200).send(updatedRegion);
        } catch (err) {
            next(err);
        }
    },

    async findById(req, res, next) {
        try {
            let { regionId } = req.query;
            let {localized} = req.query
            let region = await checkExistThenGet(regionId, Region, { populate: populateQuery });
            if (localized) {
                region = Region.schema.methods.toJSONLocalizedOnly(region, req.locale);
            }
            res.status(200).send(region);

        } catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let { regionId } = req.query;
            let region = await checkExistThenGet(regionId, Region, { deleted: false });
            region.deleted = true;
            await region.save();
            res.status(200).send("Deleted Successfully");
        }
        catch (err) {
            next(err);
        }
    }
}