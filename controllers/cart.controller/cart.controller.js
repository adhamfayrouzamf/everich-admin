import  ApiResponse                       from "../../helpers/ApiResponse"
import  ApiError                          from "../../helpers/ApiError"
import  { checkExist, checkExistThenGet } from "../../helpers/CheckMethods"
import  { checkValidations }              from "../shared.controller/shared.controller"
import  { body }                          from "express-validator"
import  Product                           from "../../models/product.model/product.model"
import  Promocode                         from "../../models/promocode.model/promocode.model"
import  Company                           from "../../models/company.model/company.model"
import  User                              from "../../models/user.model/user.model"
import  Rate                              from "../../models/rate.model/rate.model"
import  Order                             from "../../models/order.model/order.model"
import  fs                                from 'fs'
import  i18n                              from 'i18n'
import  {deleteFile}                      from '../../services/cloudinary'
import  moment                            from 'moment'
// const dotObject = require('dot-object')

// const { sendEmail } = require('../../services/emailMessage.service')

let populateQuery = [
    { path: 'cart.products.product', model: 'product' },
    { path: 'cart.shippingCompany', model: 'shipping'},
    { path: 'cart.promocode', model:'promocode'}
]

let handleProducts = async (list)=>{
    
    let promises = [];
    let query = { deleted: false }
    let ids= []
    for (let index = 0; index < list.length; index++) {
        // query["_id"] = list[index].product
        // let promis = Product.findOne(query);
        // if (promis)
        //     promises.push(createPromise(promis));
        // ids.push(list[index].product)
    }
    // let finalResult = await Promise.all(promises);
    // finalResult = Product.schema.methods.toJSONLocalizedOnly(finalResult,req.locale)
    // console.log('final',finalResult)
    // for (let index = 0; index < finalResult.length; index++) {
    //     if (finalResult[index]) {
    //         list[index].product = finalResult[index];
    //     }
    // }
    return list;
}

let createPromise = (query) => {
    let newPromise = new Promise(async (resolve, reject) => {
        try {
            const result = await query
            resolve(result)
        } catch (error) {
            reject(error)
        }
    })
    return newPromise
}

export default
{
    async getMyCart(req, res, next) {
        try {
            let user = await User.populate(req.user,populateQuery)
            console.log('test')
            // user = User.schema.methods.toJSONLocalizedOnly(user,req.locale)

            

            // user.cart.products = user.cart.products.map(p=>{
            //     let product = p.product
            //     let lang = req.locale
            //     product.name = product.name[lang]
            //     p.product = product
            //     return p
            // })

            res.send(user.cart || {totalPrice:0,products:[]})
        } catch (err) {
            next(err)
        }
    },

    validateProduct() {
        
        return [body('product').not().isEmpty().withMessage('product is Required'),
                body('quantity').not().isEmpty().withMessage("quantity is required")]
    },

    validatePromoCode(){
            
        return [body('code').not().isEmpty().withMessage('promocode is Required')]
    },
    validateShippingCompany(){
        return [body('shippingCompany').not().isEmpty().withMessage('shippingCompany is Required'),]
    },

    async addProduct(req, res, next) {
        try {
            let validated = checkValidations(req)
            let user = await User.findById(req.user._id).populate(populateQuery)
            let product = await checkExistThenGet(+validated.product,Product,{deleted:false})
            let cart = user.cart || {totalPrice:0,products:[]}
            let company = await Company.findOne({deleted:false})
            let exist = await User.findOne({_id:user._id,"cart.products.product":+product.id})
            console.log(exist)
            if(exist){
                cart.products = cart.products.map((p)=>{
                    let pro = p
                    if(p.product.id == product.id){
                        console.log('changed')
                        cart.totalPrice = cart.totalPrice - (p.product.priceAfterOffer * p.quantity)
                        pro.quantity = +validated.quantity
                    }
                    return pro
                })
            }
            cart.totalPrice = cart.totalPrice + (product.priceAfterOffer * validated.quantity)
            cart.totalPriceAfterDiscount = cart.totalPrice
            if(cart.promocode){
                cart.totalPriceAfterDiscount -= ((cart.totalPrice * cart.promocode.discount/100).toFixed(2))
            }
            let taxes = (cart.totalPriceAfterDiscount * (company.taxes/100))
            console.log(taxes,cart.totalPrice,cart.totalPriceAfterDiscount)
            cart.totalPriceAfterTaxes = (cart.totalPriceAfterDiscount + taxes).toFixed(2)

            let newUser
            if(exist){
                newUser = await User.findOneAndUpdate({_id:user._id},{cart:cart},{new:true})
            }else{
                cart.products.push({product:+product.id,quantity:+validated.quantity})
                newUser = await User.findOneAndUpdate({_id:user._id},{cart:cart},{new:true})
            }
            newUser = await User.populate(newUser,populateQuery)
            res.status(200).send(newUser.cart)

        } catch (err) {
            console.log(err.message)
            next(err)
        }
    },
    async addPromoCode(req, res, next) {
        try {
            
            let validated = checkValidations(req)
            let user = await User.findById(req.user._id).populate(populateQuery)
            let promocode = await Promocode.findOne({code:validated.code})
            let cart = user.cart || {totalPrice:0,products:[]}
            let company = await Company.findOne({deleted:false})

            if(!promocode){
                throw new Error('no such promocode')
            }
            let orderCount = Order.count({user:user._id,promocode:promocode.id,deleted:false})
            if(orderCount>= promocode.numberOfUse){
                throw new Error('you used all chances for this promocode')
            }
            cart.promocode = promocode.id
            cart.totalPriceAfterDiscount = (cart.totalPrice - cart.totalPrice * promocode.discount/100).toFixed(2)
            let taxes = (cart.totalPriceAfterDiscount * (company.taxes/100))
            cart.totalPriceAfterTaxes = (cart.totalPriceAfterDiscount + taxes).toFixed(2)
            
            let newUser = await User.findOneAndUpdate({_id:user._id},{cart:cart},{new:true})
            newUser = await User.populate(newUser,populateQuery)
            res.status(200).send(newUser.cart)


        } catch (err) {
            console.log(err.message)
            next(err)
        }
    },
    async deletePromoCode(req, res, next) {
        try {
            
            let validated = checkValidations(req)
            let user = await User.findById(req.user._id).populate(populateQuery)
            let cart = user.cart || {totalPrice:0,products:[]}
            let company = await Company.findOne({deleted:false})

            cart.promocode = null
            cart.totalPriceAfterDiscount = cart.totalPrice
            let taxes = (cart.totalPriceAfterDiscount * (company.taxes/100))
            cart.totalPriceAfterTaxes = (cart.totalPriceAfterDiscount + taxes).toFixed(2)
            
            let newUser = await User.findOneAndUpdate({_id:user._id},{cart:cart},{new:true})
            newUser = await User.populate(newUser,populateQuery)
            res.status(200).send(newUser.cart)


        } catch (err) {
            console.log(err.message)
            next(err)
        }
    },
    async addShippingCompany(req, res, next) {
        try {
            
            let validated = checkValidations(req)
            

            res.status(200).send(createdproduct)

        } catch (err) {
            console.log(err.message)
            next(err)
        }
    },


    async findById(req, res, next) {
        try {
            let { productId } = req.query
            let { userId, related } = req.query
            let query = {deleted: false, populate:populateQuery}
            let product = await checkExistThenGet(productId, Product, query)
            if (userId) {
                console.log('testing')
                product = await checkinFavourites([product], userId)
                product = product[0]
                let rate = await checkInUserRate(productId,userId)
                product.userRate = rate
            }
            

            if(related){
                let related = await Product.find({_id:{$ne:productId},categoryId:product.categoryId}).populate(populateQuery)
                
                if (userId && related.length>0) related = await checkinFavourites(related, userId)
                product.related = await related
            }
            res.status(200).json(product)
        } catch (err) {
            next(err)
        }
    },

    async update(req, res, next) {
        try {
            let { productId } = req.query
            let validated = checkValidations(req)

            if(validated.image){
                let product = await checkExistThenGet(productId, Product, { deleted: false })
                if(validated.image.id !== product.image.id)
                    deleteFile(product.image.id)
            }
            let updatedproduct = await Product.findByIdAndUpdate(productId, {$set:validated}, { new: true })
            res.status(200).send(updatedproduct)
        }
        catch (err) {
            next(err)
        }
    },

    async delete(req, res, next) {
        try {
            let { productId } = req.query
            let product = await checkExistThenGet(productId, Product, { deleted: false })
            product.deleted = true
            await product.save()
            res.status(200).send('Deleted Successfully')
            /////////////////////////////////////////////////////////////////
            await Rate.updateMany({ product: productId, deleted: false }, { deleted: true })
            await Favourites.updateMany({ product: productId, deleted: false }, { deleted: true })
        }
        catch (err) {
            next(err)
        }
    },

}