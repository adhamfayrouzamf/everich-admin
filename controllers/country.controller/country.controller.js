import Country                           from "../../models/country.model/country.model"
import City                              from "../../models/city.model/city.model"
import Region                            from "../../models/region.model/region.model"
import ApiResponse                       from "../../helpers/ApiResponse"
import ApiError                          from "../../helpers/ApiError"
import { checkExistThenGet, checkExist } from "../../helpers/CheckMethods"
import { body }                          from 'express-validator'
import { checkValidations, createDate }  from "../shared.controller/shared.controller"
import i18n                              from 'i18n'
import moment                            from 'moment'

export default {

    validateBody(isUpdate = false) {
        let validations
        if (!isUpdate) {
            validations = [
                body('name.en').not().isEmpty().withMessage(() => { return i18n.__('englishName') })
                .custom(async (val, { req }) => {
                    let query = { 'name.en': val, deleted: false };
                        let country = await Country.findOne(query).lean();
                        if (country)
                            throw new Error('country already exists');
                        return true;
                }),
                body('name.ar').not().isEmpty().withMessage(() => { return i18n.__('arabicName') })
                    .custom(async (val, { req }) => {
                        let query = { 'name.ar': val, deleted: false };
                        let country = await Country.findOne(query).lean();
                        if (country)
                            throw new Error('country already exists');
                        return true;
                    }),
                body('countryKey').optional().not().isEmpty().withMessage('must add country key'),
                body('countryCode').optional().not().isEmpty().withMessage('must add country code'),
                body('language').optional().not().isEmpty().withMessage('must add language'),
            ];
        }
        else {
            validations = [
                body('name.en').optional().not().isEmpty().withMessage(() => { return i18n.__('englishName') })
                .custom(async (val, { req }) => {
                    let query = { 'name.en': val, deleted: false,_id:{$ne:req.query.countryId} };
                    let country = await Country.findOne(query).lean();
                    if (country)
                        throw new Error('country already exists');
                    return true;
                }),
                body('name.ar').optional().not().isEmpty().withMessage(() => { return i18n.__('arabicName') })
                    .custom(async (val, { req }) => {
                        let query = { 'name.ar': val, deleted: false,_id:{$ne:req.query.countryId} };
                        let country = await Country.findOne(query).lean();
                        if (country)
                            throw new Error('country already exists');
                        return true;
                    }),
                body('countryKey').optional().not().isEmpty().withMessage('must add country key'),
                body('countryCode').optional().not().isEmpty().withMessage('must add country code'),
                body('language').optional().not().isEmpty().withMessage('must add language'),
            ];

        }
        return validations;
    },

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            const { name, localized } = req.query;
            let query = { deleted: false };
            let date = createDate(req)
            if (date) query.createdAt = {$gte: date.startOfDate, $lte: date.endOfDate}
            if (name)
                query.$or = [{ 'name.en': { '$regex': name, '$options': 'i' } }, { 'name.ar': { '$regex': name, '$options': 'i' } }]

            let countries = await Country.find(query);
            let countryCount = await Country.count(query)
            let pageCount = Math.ceil(countryCount / limit)

            if (localized) {
                countries = Country.schema.methods.toJSONLocalizedOnly(countries, req.locale);
            }
            res.send(new ApiResponse(countries, page, pageCount, limit, countryCount, req));


        } catch (err) {
            next(err);
        }
    },

    async create(req, res, next) {
        try {
            
            let validatedBody = checkValidations(req);
            let country = await Country.create(validatedBody);
            res.status(200).send(country);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {
        try {
            let { countryId } = req.query;
            await checkExist(countryId, Country, { deleted: false });
            let validatedBody = checkValidations(req);
            let updatedCountry = await Country.findByIdAndUpdate(countryId, {
                ...validatedBody,
            }, { new: true });
            
            res.status(200).send(updatedCountry);
        } catch (err) {
            next(err);
        }
    },

    async findById(req, res, next) {
        try {
            let { countryId } = req.query;
            let { localized } = req.query
            let country = await checkExistThenGet(countryId, Country, { deleted: false });

            if (localized) {
                country = Country.schema.methods.toJSONLocalizedOnly(country, req.locale);
            }
            res.status(200).send(country);

        } catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            
            let { countryId } = req.query;
            await checkExist(countryId, Country,'country not found');
            await Country.deleteOne({_id: +countryId})
            let allCities = await City.find({country:countryId}).distinct('_id');
            await City.deleteMany({country:countryId})
            await Region.deleteMany({city:{$in:allCities}})
            res.status(200).send("Deleted Successfully");
        }
        catch (err) {
            next(err);
        }
    }
}