import Order                             from "../../models/order.model/order.model"
import Product                           from '../../models/product.model/product.model'
import Address                           from '../../models/address.model/address.model'
import PromoCode                         from '../../models/promocode.model/promocode.model'
import Shipping                          from '../../models/shipping.model/shipping.model'
import User                              from '../../models/user.model/user.model'
import Category                          from '../../models/category.model/category.model'
import Trademark                         from '../../models/tradeMarks.model/tradeMarks.model'
import ApiResponse                       from "../../helpers/ApiResponse"
import ApiError                          from "../../helpers/ApiError"
import { checkExistThenGet, checkExist } from "../../helpers/CheckMethods"
import { body }                          from 'express-validator'
import { checkValidations, createDate }  from "../shared.controller/shared.controller"
import generateVerifyCode                from '../../services/generator-code-service'
import i18n                              from 'i18n'
import moment                            from 'moment'
import { sendMail }                      from '../../services/twilo'
const  config                            = process.env
// const Company = require('../../models/company.model/company.model');
// const Notification = require('../../models/notif.model/notif.model');
// const CreditCard = require('../../models/credit.model/credit.model');
// const notifyController = require('../notif.controller/notif.controller');
// const socketEvents = require('../../socketEvents');
// const https = require('https');
// const querystring = require('querystring');

let populateQuery = [
    { path: 'user', model: 'user' },
    { path: 'products.product', model: 'product', select:"-favorites", populate: [{ path: 'trademark', model: 'trademark' }, { path: 'category', model: 'category' },{ path: 'subCategory', model: 'category' }]},
    // { path: 'products.color', model: 'color' },
    // { path: 'products.size', model: 'size' },
    { path: 'address', model: 'address'/* , populate: [{ path: 'city', model: 'city', populate: [{ path: 'country', model: 'country' }] }] */ },
    { path: 'promocode', model: 'promocode' },
    { path:'shipping',model:'shipping' },
    { path: 'user', model: 'user' , select:"-password"},
];

let checkAvailability = async (list) => {
    let products = [];
    let colorFound = false;
    let sizeFound = false;
    for (let index = 0; index < list.length; index++) {
        colorFound = false;
        sizeFound = false;
        let product = await Product.findById(list[index].product);
        if (product.colors.length > 0) {
            if (!list[index].colorId || !list[index].sizeId) {
                throw new ApiError(400, i18n.__('mustChoiseColorSize'));
            }
            for (let i = 0; i < product.colors.length; i++) {
                if (product.colors[i]._id == list[index].colorId) {
                    colorFound = true;
                    list[index].color = product.colors[i].color;
                    for (let k = 0; k < product.colors[i].sizes.length; k++) {
                        if (product.colors[i].sizes[k]._id == list[index].sizeId) {
                            sizeFound = true;
                            list[index].size = product.colors[i].sizes[k].size;
                            if (product.colors[i].sizes[k].quantity < list[index].quantity) {
                                throw new ApiError(400, i18n.__('invalidQuantity'));
                            }
                        }
                    }
                }
            }
            if (!(colorFound && sizeFound)) {
                throw new ApiError(404, i18n.__('invalidColorOrSize'));
            }
        } else {
            if (list[index].colorId || list[index].sizeId) {
                throw new ApiError(400, i18n.__('donotSendColorSize'));
            }
            if (product.quantity < list[index].quantity) {
                throw new ApiError(400, i18n.__('invalidQuantity'));
            }
        }
        let singleProduct = list[index];
        singleProduct.price = product.price;
        if (product.offer && product.offer > 0) {
            singleProduct.offer = product.offer;
            singleProduct.priceAfterOffer = +((singleProduct.price - (singleProduct.price * (singleProduct.offer / 100.0))).toFixed(3))
        } else {
            singleProduct.priceAfterOffer = singleProduct.price;
        }
        singleProduct.priceBeforeTaxes = product.priceBeforeTaxes;
        products.push(singleProduct);
    }
    return products;

}

let updateProductAvailability = async (list) => {
    for (let index = 0; index < list.length; index++) {
        let singleProduct = await Product.findOne({ _id: list[index].product.id });
        let colors = singleProduct.colors;

        singleProduct.quantity = singleProduct.quantity - list[index].quantity;
        if (colors.length > 0) {
            for (let i = 0; i < colors.length; i++) {

                if (colors[i].color == list[index].color.id) {
                    for (let k = 0; k < colors[i].sizes.length; k++) {
                        if (colors[i].sizes[k].size == list[index].size.id) {
                            colors[i].sizes[k].quantity = colors[i].sizes[k].quantity - list[index].quantity;
                        }
                    }
                }


            }
            singleProduct.colors = colors;
            await Product.findByIdAndUpdate(singleProduct.id, { $set: { colors: colors }, quantity: singleProduct.quantity })
        } else {
            if (singleProduct.quantity < 0) {

                let admins = await User.find({ type: 'ADMIN', deleted: false }).distinct('_id')
                await Notification.create({
                    resource: 1, type: "USERS", subjectType: "PRODUCT",
                    subject: singleProduct.id, users: admins, description: { ar: " يجب إعادة شحن الكمية لتكلمة الطلب ", en: "The quantity in this product not enough ypu need to fill the store " }
                })
            }
            else if (singleProduct.quantity == 0) {

                let admins = await User.find({ type: 'ADMIN', deleted: false }).distinct('_id')
                await Notification.create({
                    resource: 1, type: "USERS", subjectType: "PRODUCT",
                    subject: singleProduct.id, users: admins, description: { ar: " الكمية المتاحة في هذا المنتج انتهت", en: "The quantity in the product not enough" }
                })
            }
        }
        console.log(singleProduct.quantity)
        if (singleProduct.quantity >= 0) {
            await singleProduct.save();
        }

    }
}


let reversProductQuantity = async (list) => {

    for (let index = 0; index < list.length; index++) {
        let singleProduct = await Product.findOne({ _id: list[index].product.id });
        let colors = singleProduct.colors;

        singleProduct.quantity = singleProduct.quantity + list[index].quantity;
        if (colors.length > 0) {
            for (let i = 0; i < colors.length; i++) {

                if (colors[i].color == list[index].color.id) {
                    for (let k = 0; k < colors[i].sizes.length; k++) {
                        if (colors[i].sizes[k].size == list[index].size.id) {
                            colors[i].sizes[k].quantity = colors[i].sizes[k].quantity + list[index].quantity;
                        }
                    }
                }
            }
            singleProduct.colors = colors;
            await Product.findByIdAndUpdate(singleProduct.id, { $set: { colors: colors }, quantity: singleProduct.quantity })
        }

        if (singleProduct.quantity >= 0) {
            await singleProduct.save();
        }

    }
}

let calculatePrice = async (list) => {
    let price = 0;
    for (let index = 0; index < list.length; index++) {
        if (list[index].offer && list[index].offer > 0) {
            price = price + ((list[index].price - (+(list[index].price * (list[index].offer / 100.0)).toFixed(1))) * list[index].quantity);
        }
        else {
            price = price + (list[index].price * list[index].quantity)
        }
    }
    return price;

}

let calculatePriceBeforeProductTaxes = async (list) => {
    let price = 0;
    for (let index = 0; index < list.length; index++) {
        if (list[index].offer && list[index].offer > 0) {
            price = price + ((list[index].priceBeforeTaxes - (+(list[index].priceBeforeTaxes * (list[index].offer / 100.0)).toFixed(3))) * list[index].quantity);
            // console.log('in ',list[index].priceBeforeTaxes)
        }
        else {
            // console.log('in else')
            price = price + (list[index].priceBeforeTaxes * list[index].quantity)
        }
    }
    return price;
}

let getFinalPrice = async (validateBody) => {
    if (validateBody.promoCode) {
        console.log('promo',validateBody.promoCode)
        let promoCode = await PromoCode.findById(validateBody.promoCode);
        if (promoCode.promoCodeType == 'RATIO') {
            let discount = validateBody.price - (validateBody.price * (promoCode.discount / 100))
            validateBody.discountValue = (validateBody.price * (promoCode.discount / 100))
            validateBody.totalPrice = +((discount).toFixed(3));
        } else {
            validateBody.totalPrice = (((validateBody.price - promoCode.discount) > 0) ? (validateBody.price - promoCode.discount) : validateBody.price);
            validateBody.discountValue = (((validateBody.price - promoCode.discount) > 0) ? promoCode.discount : 0);
        }
    } else {
        validateBody.totalPrice = validateBody.price;
    }
    return validateBody;
}

const getCheckoutId = async (request, response, next, order, paymentBrand) => {
    try {
        let cardEntityId;
        var checkoutIdPath = '/v1/checkouts';
        let amount = order.totalPrice;
        let name = request.user.name.split(" ");
        if (name.length == 1) {
            name.push(name[0]);
        }
        if (paymentBrand == 'MADA') {
            cardEntityId = config.payment.Entity_ID_Mada;
        } else {
            cardEntityId = config.payment.Entity_ID_Card;
        }
        let body = {
            'merchantTransactionId': order.orderNumber,
            'entityId': cardEntityId,
            'amount': Number(amount).toFixed(2),
            'currency': config.payment.Currency,
            'paymentType': config.payment.PaymentType,
            'notificationUrl': config.payment.notificationUrl,
            // 'testMode': config.payment.testMode,
            'customer.email': request.user.email || '',
            'billing.street1': request.address.address || '',
            'billing.city': 'Riyadh',
            'billing.state': 'Layla',
            'billing.country': 'SA',
            'billing.postcode': '11461',
            'customer.givenName': name[0],
            'customer.surname': name[1]
        }
        // if(paymentBrand != 'MADA') body.testMode = config.payment.testMode;
        console.log(body)
        var data = querystring.stringify(body);
        var options = {
            port: 443,
            host: config.payment.host,
            path: checkoutIdPath,
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': data.length,
                'Authorization': config.payment.access_token
            }
        };
        var postRequest = https.request(options, function (res) {
            res.setEncoding('utf8');
            res.on('data', async function (chunk) {
                let result = JSON.parse(chunk)
                console.log(result)

                result = { checkoutId: result.id, amount: amount }
                console.log(result)
                result.order = await Order.findByIdAndUpdate(order.id, { $set: { checkoutId: result.checkoutId, paymentStatus: 'PENDING' } }, { new: true });
                response.status(200).send(result);
            });
        });
        postRequest.write(data);
        postRequest.end();
    } catch (error) {
        throw error
    }
}

export default {

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let { status, paymentMethod, month, year, fromDate, toDate, type, formDate,
                userName, _id, price, orderDate, totalPrice, promocode,
                orderNumber, numberOfProducts, visitorsOrder, populated, user, localized
            } = req.query;
            let query = { deleted: false/* , $or: [{ paymentMethod: 'CREDIT', paymentStatus: 'SUCCESSED' }, { paymentMethod: 'CASH' }] */ };
            let date = createDate(req)
            if (date) query.createdAt = {$gte: date.startOfDate, $lte: date.endOfDate}
            if(user) query.user = user
            if(promocode) query.promocode = promocode

            let orders = await Order.find(query).sort({ updatedAt: -1 }).limit(limit).skip((page - 1) * limit).populate(populateQuery)
            let ordersCount = await Order.count(query);
            const pageCount = Math.ceil(ordersCount / limit);
            if(localized)
                orders = Order.schema.methods.toJSONLocalizedOnly(orders, req.locale);

            res.status(200).send(new ApiResponse(orders, page, pageCount, limit, ordersCount, req));
            // if ((req.user.type == 'ADMIN') || (req.user.type == 'SUB_ADMIN')) {
            //     await Order.updateMany({ deleted: false, type: type, adminInformed: false }, { $set: { adminInformed: true } });
            //     let onlineOrder = await Order.count({ deleted: false, type: 'ONLINE', adminInformed: false });
            //     let manualOrder = await Order.count({ deleted: false, type: 'MANULAY', adminInformed: false });
            //     adminNSP.emit(socketEvents.UpdateOrderCount, { onlineOrder, manualOrder });
            // }


        } catch (err) {
            next(err);
        }
    },

    validateBody() {
        return [
            
            // body('products.*.product').not().isEmpty().withMessage(() => { return i18n.__('productRequired') })
            //     .custom(async (val, { req }) => {
            //         req.product = await checkExistThenGet(val, Product, { deleted: false }, i18n.__('productNotFound'));
            //         return true;
            //     }),
            // body('products.*.colorId').optional().not().isEmpty().withMessage(() => { return i18n.__('colorIdRequired') }),
            /*.custom(async (val, { req }) => {
                console.log("req.product.id == ",req.product.id)
                let color = await Product.find({ _id: req.product.id, 'colors._id': val }, { _id: 0, 'colors.$': 1 })
                if (color.length == 0) {
                    throw new Error(i18n.__('invalidColorId'));
                }
                req.sizes = color[0].colors[0].sizes;
                val = color[0].colors[0].color;

                return true;
            }),*/
            // body('products.*.sizeId').optional().not().isEmpty().withMessage(() => { return i18n.__('sizeIdRequired') }),
            /* .custom(async (val, { req }) => {
                 if (req.sizes) {
                     if (req.sizes.some(obj => obj['_id'] == val)) {
                         for (let index = 0; index < req.sizes.length; index++) {
                             if (req.sizes[index]._id == val) {
                                 val = req.sizes[index].size;
                                }
                         }
                         return true
                     }
                 }
                 else {
                     throw new Error(i18n.__('invalidSizeId'));
                 }
             }),*/
             // body('products.*.quantity').not().isEmpty().withMessage(() => { return i18n.__('quantityRequired') }),
            body('moneyReminder').optional().not().isEmpty().withMessage(() => { return i18n.__('moneyReminderRequired') }),
            body('address').not().isEmpty().withMessage('must add address')
                .custom(async (val, { req }) => {
                    req.address = await checkExistThenGet(val, Address, { deleted: false, user: req.user._id },'address not found');
                    return true;
                }),
            body('paymentMethod').not().isEmpty().withMessage('must add pay method').isIn(['CASH', 'CREDIT']).withMessage('Wrong payment type'),
            body('notes').optional().not().isEmpty().withMessage('must add notes'),
            body('shippingCompany').not().isEmpty().withMessage('must add ship company').custom(async(val)=>{
                await checkExist(+val,Shipping,{deleted:false})
                return true
            }),
            body('status').optional().not().isEmpty().withMessage('must add status').isIn(['WAITING','ACCEPTED','REJECTED','SHIPPED','DELIVERED','CANCELLED'])
            // body('discount').optional().not().isEmpty().withMessage('must add discount'),
            // body('transportPrice').not().isEmpty().withMessage('must add transportPrice'),
        ];
    },

    async create(req, res, next) {
        try {
            let user = req.user
            let validated = checkValidations(req);
            let shippingCompany = await checkExistThenGet(validated.shippingCompany,Shipping,{deleted:false})
            validated.user = user._id
            validated.shippingCompany = shippingCompany.id

            if(user.cart.promocode)
                validated.promocode = user.cart.promocode
            validated.products=user.cart.products
            validated.totalPrice = user.cart.totalPrice
            validated.totalPriceAfterDiscount = user.cart.totalPriceAfterDiscount
            validated.totalPriceAfterTaxes = user.cart.totalPriceAfterTaxes
            validated.finalPrice = (user.cart.totalPriceAfterTaxes + shippingCompany.shippingPrice).toFixed(2)
            validated.orderNumber = generateVerifyCode(/^\d+$/);
            console.log('validated')

            let order = await Order.create(validated);
            order.orderNumber = order.orderNumber + order.id;
            await order.save();
            // if (validated.paymentMethod == 'CREDIT') {
            //     getCheckoutId(req, res, next, order, 'VISA');
            // } else {
            await User.updateOne({_id:user._id},{cart:{totalUser:0,products:[]}})
                res.status(200).send(order);
                // order = await Order.populate(order, populateQuery)
                // await updateProductAvailability(order.products)
                // let onlineOrder = await Order.count({ deleted: false, type: 'ONLINE', adminInformed: false });
                // adminNSP.emit(socketEvents.UpdateOrderCount, { onlineOrder });
            // }

        } catch (err) {
            next(err);
        }
    },

    async createManualOrder(req, res, next) {
        try {
            let user = req.user;
            let validated = { replied: false, type: 'MANULAY' };
            validated.user = user.id;
            validated.orderNumber = generateVerifyCode(/^\d+$/);
            if (req.file) {
                validated.image = handleImg(req, { attributeName: 'image' });
            }
            let order = await Order.create(validated);
            order.orderNumber = order.orderNumber + order.id;
            await order.save()
            res.status(200).send(order);
            let manualOrder = await Order.count({ deleted: false, type: 'MANULAY', adminInformed: false });
            adminNSP.emit(socketEvents.UpdateOrderCount, { manualOrder });
        } catch (err) {
            next(err);
        }
    },

    async changeManulaOrderToReplied(req, res, next) {
        try {
            const {orderId} = req.query;
            let updatedOrder = await checkExistThenGet(id, Order, { deleted: false });
            updatedOrder.replied = true;
            await updatedOrder.save();
            updatedOrder = Order.schema.methods.toJSONLocalizedOnly(updatedOrder, req.locale);
            res.status(200).send(updatedOrder);
        } catch (error) {
            next(error);
        }
    },

    async update(req, res, next) {
        try {
            let { orderId } = req.query;
            await checkExist(orderId, Order, { deleted: false });
            let validated = checkValidations(req);

            let updatedOrder = await Order.findByIdAndUpdate(orderId, {
                ...validated,
            }, { new: true }).populate(populateQuery);
            updatedOrder = Order.schema.methods.toJSONLocalizedOnly(updatedOrder, req.locale);

            res.status(200).send(updatedOrder);
        } catch (err) {
            next(err);
        }
    },

    async findById(req, res, next) {
        try {
            let { orderId } = req.query;

            let query = {deleted:false, populate:populateQuery}
            let order = await checkExistThenGet(orderId, Order, query);
            res.status(200).send(order);

        } catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let { orderId } = req.query;
            let order = await checkExistThenGet(orderId, Order, { deleted: false });
            order.deleted = true;
            await order.save();
            res.status(200).send("Deleted Successfully");
        }
        catch (err) {
            next(err);
        }
    },

    validateAcceptOrReject() {
        let validations = [
            body('status').optional().not().isEmpty().withMessage(() => { return i18n.__('statusRequired') })
                .isIn(['ACCEPTED', 'REJECTED']).withMessage(() => { return i18n.__('statusRequired') }).custom((val, { req }) => {
                    if (val == 'REJECTED' && !req.body.rejectReason) {
                        throw new Error(i18n.__('rejectReasonRequired'))
                    }
                    return true;
                }),
            body('rejectReason').optional().not().isEmpty().withMessage(() => { return i18n.__('rejectReasonRequired') })
        ];
        return validations;
    },

    async acceptOrReject(req, res, next) {
        try {
            if (req.user.type != 'ADMIN' && req.user.type != 'SUB_ADMIN')
                return next(new ApiError(403, i18n.__('admin.auth')));
            let { orderId } = req.query;
            await checkExist(orderId, Order, { deleted: false, status: 'WAITING' });
            let validated = checkValidations(req);

            let updatedOrder = await Order.findByIdAndUpdate(orderId, validated, { new: true }).populate(populateQuery);
            // updatedOrder = Order.schema.methods.toJSONLocalizedOnly(updatedOrder, req.locale);
            if(validated.status === "ACCEPTED")
                sendMail(updatedOrder.user.email,"Order Accepted","Your Order has been Accepted","Your Order has been Accepted")
            else if(validated.status === "REJECTED")
                sendMail(updatedOrder.user.email,"Order Rejected","Your Order has been Rejected","Your Order has been Rejected")

            res.status(200).send(updatedOrder);
            // let description;
            // if (validated.status == 'ACCEPTED') {
            //     // await updateProductAvailability(updatedOrder.products)
            //     description = { en: updatedOrder.orderNumber + ' : ' + 'Your Order Has Been Approved', ar: updatedOrder.orderNumber + ' : ' + ' تم تجهيز طلبك' };
            // } else {
            //     await reversProductQuantity(updatedOrder.products);
            //     description = { en: updatedOrder.orderNumber + ' : ' + 'Your Order Has Been Rejected ' + validated.rejectReason, ar: updatedOrder.orderNumber + ' : ' + ' تم رفض طلبك ' + validated.rejectReason };
            // }
            // await notifyController.create(req.user.id, updatedOrder.user.id, description, updatedOrder.id, 'CHANGE_ORDER_STATUS', updatedOrder.id);
            // notificationNSP.to('room-' + updatedOrder.user.id).emit(socketEvents.ChangeOrderStatus, { order: updatedOrder });
            // if (updatedOrder.user.language == "ar") {
            //     notifyController.pushNotification(updatedOrder.user.id, 'CHANGE_ORDER_STATUS', updatedOrder.id, description.ar, config.notificationTitle.ar);
            //     await sendEmail(updatedOrder.user.email, description.ar)
            // }
            // else {
            //     notifyController.pushNotification(updatedOrder.user.id, 'CHANGE_ORDER_STATUS', updatedOrder.id, description.en, config.notificationTitle.en);
            //     await sendEmail(updatedOrder.user.email, description.en)
            // }

        } catch (err) {
            // console.log(err);
            next(err);
        }
    },
    async prepared(req, res, next) {
        try {
            if (req.user.type != 'ADMIN' && req.user.type != 'SUB_ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { orderId } = req.query;
            await checkExist(orderId, Order, { deleted: false, status: "ACCEPTED" });
            let updatedOrder = await Order.findByIdAndUpdate(orderId, { status: 'PREPARED' }, { new: true }).populate(populateQuery);
            // updatedOrder = Order.schema.methods.toJSONLocalizedOnly(updatedOrder, req.locale);
            
            res.status(200).send(updatedOrder);
            sendMail(updatedOrder.user.email,"Order Prepared","Your Order has been Prepared","Your Order has been Prepared")
            // let description = { ar: updatedOrder.orderNumber + ' : ' + ' تم تغير حالة الطلب الي تم الشحن ', en: updatedOrder.orderNumber + ' : ' + 'Order Status Changed To Order Shipped' };
            // await notifyController.create(req.user.id, updatedOrder.user.id, description, updatedOrder.id, 'CHANGE_ORDER_STATUS', updatedOrder.id);
            // notificationNSP.to('room-' + updatedOrder.user.id).emit(socketEvents.ChangeOrderStatus, { order: updatedOrder });
            // if (updatedOrder.user.language == "ar") {
            //     notifyController.pushNotification(updatedOrder.user.id, 'CHANGE_ORDER_STATUS', updatedOrder.id, description.ar, config.notificationTitle.ar);
            //     await sendEmail(updatedOrder.user.email, description.ar)
            // }
            // else {
            //     notifyController.pushNotification(updatedOrder.user.id, 'CHANGE_ORDER_STATUS', updatedOrder.id, description.en, config.notificationTitle.en);
            //     await sendEmail(updatedOrder.user.email, description.ar)
            // }
        } catch (err) {
            next(err);
        }
    },
    async shipped(req, res, next) {
        try {
            if (req.user.type != 'ADMIN' && req.user.type != 'SUB_ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { orderId } = req.query;
            await checkExist(orderId, Order, { deleted: false, status: "PREPARED" });
            let updatedOrder = await Order.findByIdAndUpdate(orderId, { status: 'SHIPPED' }, { new: true }).populate(populateQuery);
            // updatedOrder = Order.schema.methods.toJSONLocalizedOnly(updatedOrder, req.locale);
            res.status(200).send(updatedOrder);
            sendMail(updatedOrder.user.email,"Order Shipped","Your Order has been Shipped","Your Order has been Shipped")
            // let description = { ar: updatedOrder.orderNumber + ' : ' + ' تم تغير حالة الطلب الي تم الشحن ', en: updatedOrder.orderNumber + ' : ' + 'Order Status Changed To Order Shipped' };
            // await notifyController.create(req.user.id, updatedOrder.user.id, description, updatedOrder.id, 'CHANGE_ORDER_STATUS', updatedOrder.id);
            // notificationNSP.to('room-' + updatedOrder.user.id).emit(socketEvents.ChangeOrderStatus, { order: updatedOrder });
            // if (updatedOrder.user.language == "ar") {
            //     notifyController.pushNotification(updatedOrder.user.id, 'CHANGE_ORDER_STATUS', updatedOrder.id, description.ar, config.notificationTitle.ar);
            //     await sendEmail(updatedOrder.user.email, description.ar)
            // }
            // else {
            //     notifyController.pushNotification(updatedOrder.user.id, 'CHANGE_ORDER_STATUS', updatedOrder.id, description.en, config.notificationTitle.en);
            //     await sendEmail(updatedOrder.user.email, description.ar)
            // }
        } catch (err) {
            next(err);
        }
    },
    async onTheWay(req, res, next) {
        try {
            if (req.user.type != 'ADMIN' && req.user.type != 'SUB_ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { orderId } = req.query;
            await checkExist(orderId, Order, { deleted: false, status: "SHIPPED" });
            let updatedOrder = await Order.findByIdAndUpdate(orderId, { status: 'ONTHEWAY' }, { new: true }).populate(populateQuery);
            
            // updatedOrder = Order.schema.methods.toJSONLocalizedOnly(updatedOrder, req.locale);
            res.status(200).send(updatedOrder);
            sendMail(updatedOrder.user.email,"Order On The Way","Your Order is on the way to you","Your Order is on the way to you")
            // let description = { ar: updatedOrder.orderNumber + ' : ' + ' تم تغير حالة الطلب الي تم الشحن ', en: updatedOrder.orderNumber + ' : ' + 'Order Status Changed To Order Shipped' };
            // await notifyController.create(req.user.id, updatedOrder.user.id, description, updatedOrder.id, 'CHANGE_ORDER_STATUS', updatedOrder.id);
            // notificationNSP.to('room-' + updatedOrder.user.id).emit(socketEvents.ChangeOrderStatus, { order: updatedOrder });
            // if (updatedOrder.user.language == "ar") {
            //     notifyController.pushNotification(updatedOrder.user.id, 'CHANGE_ORDER_STATUS', updatedOrder.id, description.ar, config.notificationTitle.ar);
            //     await sendEmail(updatedOrder.user.email, description.ar)
            // }
            // else {
            //     notifyController.pushNotification(updatedOrder.user.id, 'CHANGE_ORDER_STATUS', updatedOrder.id, description.en, config.notificationTitle.en);
            //     await sendEmail(updatedOrder.user.email, description.ar)
            // }
        } catch (err) {
            next(err);
        }
    },
    async delivered(req, res, next) {
        try {
            if (req.user.type != 'ADMIN' && req.user.type != 'SUB_ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { orderId } = req.query;
            await checkExist(orderId, Order, { deleted: false, status: "ONTHEWAY" });
            let updatedOrder = await Order.findByIdAndUpdate(orderId, { status: 'DELIVERED', deliveredDate: new Date() }, { new: true }).populate(populateQuery);
            
            // updatedOrder = Order.schema.methods.toJSONLocalizedOnly(updatedOrder, req.locale);
            res.status(200).send(updatedOrder);
            sendMail(updatedOrder.user.email,"Order Delivered","Your Order has been delivered successfully","Your Order has been delivered successfully")

            // let description = { ar: updatedOrder.orderNumber + ' : ' + ' تم تغير حالة الطلب الي تم التسليم ', en: updatedOrder.orderNumber + ' : ' + 'Order Status Changed To Delivered' };
            // await notifyController.create(req.user.id, updatedOrder.user.id, description, updatedOrder.id, 'CHANGE_ORDER_STATUS', updatedOrder.id);
            // notificationNSP.to('room-' + updatedOrder.user.id).emit(socketEvents.ChangeOrderStatus, { order: updatedOrder });
            // if (updatedOrder.user.language == "ar") {
            //     notifyController.pushNotification(updatedOrder.user.id, 'CHANGE_ORDER_STATUS', updatedOrder.id, description.ar, config.notificationTitle.ar);
            //     await sendEmail(updatedOrder.user.email, description.ar)
            // }
            // else {
            //     notifyController.pushNotification(updatedOrder.user.id, 'CHANGE_ORDER_STATUS', updatedOrder.id, description.en, config.notificationTitle.en);
            //     await sendEmail(updatedOrder.user.email, description.en)
            // }

            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        } catch (err) {
            next(err);
        }
    },

    async canceled(req, res, next) {
        try {
            let { orderId } = req.query;
            let order = await checkExistThenGet(orderId, Order, { deleted: false, user: req.user._id, populate: populateQuery },'not allow to cancel');
            if (order.status != 'WAITING') {
                next(new ApiError(400,'not allow to cancel'))
            }
            console.log('test')
            order.status = 'CANCELLED'
            await order.save();
            // await reversProductQuantity(order.products);
            res.status(200).send(order);
        } catch (err) {
            next(err);
        }
    },
    updateProductAvailability
}