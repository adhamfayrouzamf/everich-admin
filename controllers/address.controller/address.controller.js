import Address                           from "../../models/address.model/address.model"
import Country                           from "../../models/country.model/country.model"
import City                              from "../../models/city.model/city.model"
import Region                            from "../../models/region.model/region.model"
import User                              from "../../models/user.model/user.model"
import ApiResponse                       from "../../helpers/ApiResponse"
import { checkExistThenGet, checkExist } from "../../helpers/CheckMethods"
import { body }                          from 'express-validator'
import { checkValidations, createDate }  from "../shared.controller/shared.controller"
import moment                            from 'moment'
import ApiError                          from "../../helpers/ApiError"

let populateQuery = [{path:'region', model:'region'},{path:'city',model:'city'},{path:'country',model:'country'},{ path: 'user', model: 'user' }];

export default {

    validateBody(isUpdate = false) {
        let validations
        if (!isUpdate) {
            validations = [
                body('detailedAddress').not().isEmpty().withMessage('detailedAddres is required'),
                body('street').not().isEmpty().withMessage('street is required'),
                body('long').optional().not().isEmpty().withMessage('long is required'),
                body('lat').optional().not().isEmpty().withMessage('lat is required'),
                body('address').optional().not().isEmpty().withMessage('address is required'),
                body('details').optional().not().isEmpty().withMessage('details is required'),
                body('region').optional().not().isEmpty().withMessage('region is required'),
                body('phone').optional().not().isEmpty().withMessage('phone is required'),
                body('additional').optional().not().isEmpty().withMessage('additional is required'),
                body('country').not().isEmpty().withMessage('country is required').custom(async(val,{req})=> {
                    await checkExist(val, Country, { deleted: false });
                    return true;
                }),
                body('city').not().isEmpty().withMessage('').custom(async(val,{req})=> {
                    await checkExist(val, City, { deleted: false });
                    return true;
                }),
                body('region').not().isEmpty().withMessage('').custom(async(val,{req})=> {
                    await checkExist(val, Region, { deleted: false });
                    return true;
                }),
            ];
        }
        else {
            validations = [
                body('detailedAddress').optional().not().isEmpty().withMessage(''),
                body('street').optional().not().isEmpty().withMessage(''),
                body('long').optional().not().isEmpty().withMessage(''),
                body('lat').optional().not().isEmpty().withMessage(''),
                body('address').optional().not().isEmpty().withMessage(''),
                body('details').optional().not().isEmpty().withMessage(''),
                body('country').optional().not().isEmpty().withMessage('country is required').custom(async(val,{req})=> {
                    await checkExist(val, Country, { deleted: false });
                    return true;
                }),
                body('city').optional().not().isEmpty().withMessage('').custom(async(val,{req})=> {
                    await checkExist(val, City, { deleted: false });
                    return true;
                }),
                body('region').optional().not().isEmpty().withMessage('').custom(async(val,{req})=> {
                    await checkExist(val, Region, { deleted: false });
                    return true;
                }),
                body('phone').optional().not().isEmpty().withMessage(''),
                body('additional').optional().not().isEmpty().withMessage('additional is required'),
            ];

        }
        return validations;
    },

    async findAll(req, res, next) {
        try {
            let { city,region,user, country, month, year ,address} = req.query;
            let page = +req.query.page || 1; let limit = +req.query.limit || 20
            let query = { deleted: false };
            const date = createDate(req) 
            console.log(date)
            if (date) query.createdAt = {$gte: date.startOfDate, $lte: date.endOfDate}
            if (address) query.address={ '$regex': address, '$options': 'i' }
            if (region) query.region={ '$regex': region, '$options': 'i' }
            if (city) query.city = city;
            if (user) query.user = user;
            
            let addresses = await Address.find(query).sort({updatedAt:-1}).limit(limit).skip((page-1)*limit).populate(populateQuery)
            let addressCount = await Address.count(query)
            let pageCount = Math.ceil(addressCount / limit)
            res.send(new ApiResponse(advertisment, page, pageCount, limit, advertismentCount, req ));

        } catch (err) {
            next(err);
        }
    },

    async create(req, res, next) {
        try {
            let validated = checkValidations(req);
            validated.user = req.user._id
            // if (validated.long || validated.lat ) {
            //     if (!(validated.lat && validated.long && validated.address )) {
            //         return next(new ApiError(404,i18n.__('locationValueError')));
            //     }
            // }
            let address = await Address.create(validated);
            address = await Address.populate(address, populateQuery);
            res.status(200).send(address);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {
        try {
            let { addressId } = req.query;
            await checkExist(addressId, Address, { deleted: false });
            let validated = checkValidations(req);
            // if (validated.long || validated.lat) {
            //     if (!(validated.lat && validated.long)) {
            //         return next(new ApiError(404,i18n.__('locationValueError')));
            //     }
            // }
            // if(!validated.detailedAddress){
            //     validated.$unset = {detailedAddress:""}
            // }
            // if(!validated.details){
            //     validated.$unset = {details:""}
            // }
            let updatedaddress = await Address.findByIdAndUpdate(addressId, validated, { new: true }).populate(populateQuery) 
            res.status(200).send(updatedaddress);
        } catch (err) {
            next(err);
        }
    },

    async findById(req, res, next) {
        try {
            let { addressId } = req.query;
            let query = {deleted:false, populate:populateQuery}

            let address = await checkExistThenGet(addressId, Address, query);

            res.status(200).send(address);

        } catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let { addressId } = req.query;
            await Address.findByIdAndDelete(addressId)
            // let address = await checkExistThenGet(addressId, Address, { deleted: false });
            // address.deleted = true;
            // await address.save();
            res.status(200).send("Deleted Successfully");
        }
        catch (err) {
            next(err);
        }
    }
}