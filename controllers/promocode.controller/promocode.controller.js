import Promocode                         from '../../models/promocode.model/promocode.model'
import { body }                          from 'express-validator'
import { checkValidations, createDate }  from '../shared.controller/shared.controller'
import { checkExistThenGet, checkExist } from '../../helpers/CheckMethods'
import ApiResponse                       from '../../helpers/ApiResponse'
import generateVerifyCode                from '../../services/generator-code-service'
import ApiError                          from '../../helpers/ApiError'
import User                              from '../../models/user.model/user.model'
import Order                             from '../../models/order.model/order.model'
import Notif                             from '../../models/notification.model/notification.model'
import i18n                              from 'i18n'
import { sendPushNotification }          from '../../services/push-notification-service'
import moment                            from "moment"
import notifController                   from '../notification.controller/notification.controller'
// const socketEvents = require('../../socketEvents')

const populateQuery = [
    { path: 'users', model: 'user' }

];
const notif = {
    text:{
        ar:"مبروك لقد حصلت على كود خصم جديد",
        en:"Contratulations You Got A Promocode"
    },
    title : {
        ar:"بروموكود جديد",
        en:"New Promocode"
    }
}

export default {
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let { user, code, from, to, month, year, archive, usersType, numberOfUse, discount, startDate, endDate } = req.query;
            let date = createDate(req)
            let query = { deleted: false };

            if (date) query.createdAt = {$gte: date.startOfDate, $lte: date.endOfDate}
            if (user) query.users = { $in: user };
            if (numberOfUse) query.numberOfUse = numberOfUse;
            if (discount) query.discount = discount;
            // if (startDate) {
            //     query.startDate = { $gte: new Date(moment(startDate).startOf('day')), $lte: new Date(moment(startDate).endOf('day')) };
            // }
            // if (endDate) {
            //     query.endDate = { $gte: new Date(moment(endDate).startOf('day')), $lte: new Date(moment(endDate).endOf('day')) };
            // }
            if (code) {
                query.code = code
            }
            // if (from) {
            //     query.startDate = { $gte: from }
            // }
            // if (to) {
            //     let date = new Date(to);
            //     to = moment(date).endOf('day')
            //     date = new Date(to);
            //     query.endDate = { $lte: to }
            // }
            // let date = new Date();
            // if (year && !month) {
            //     date.setFullYear(year);
            //     let startOfDate = moment(date).startOf('year');
            //     let endOfDate = moment(date).endOf('year');
            //     query.createdAt = { $gte: new Date(startOfDate), $lte: new Date(endOfDate) }
            // }
            // if (month && year) {
            //     month = month - 1;
            //     date.setMonth(month);
            //     date.setFullYear(year);
            //     let startOfDate = moment(date).startOf('month');
            //     let endOfDate = moment(date).endOf('month');

            //     query.createdAt = { $gte: new Date(startOfDate), $lte: new Date(endOfDate) }
            // }
            let promocodes = await Promocode.find(query).sort({updatedAt:-1}).limit(limit).skip((page-1)*limit).populate(populateQuery)
            let promocodeCount = await Promocode.count(query)
            let pageCount = Math.ceil(promocodeCount / limit)
            res.status(200).send(new ApiResponse(promocodes, page, pageCount, limit, promocodeCount, req));

        } catch (error) {
            next(error);
        }
    },

    validateBody() {
        let validations = [
            body('code').optional().not().isEmpty().withMessage('must add code')
                .custom(async (value, { req }) => {
                    // let startOfStartDate = moment(req.body.startDate).startOf('day');
                    // let endOfEndDate = moment(req.body.endDate).endOf('day');
                    let query = { deleted: false, code: value }
                    let currentCode = await Promocode.findOne(query);
                    if (currentCode) {
                        return false;
                    }
                }).withMessage('code already exists'),
            body('discount').not().isEmpty().withMessage(() => { return i18n.__('discountRequired') }),
            body('startDate').not().isEmpty().withMessage(() => { return i18n.__('startDateRequired') }),
            body('endDate').not().isEmpty().withMessage(() => { return i18n.__('endDateRequired') }),
            body('numberOfUse').not().isEmpty().withMessage(() => { return i18n.__('numberOfUseRequired') }),
            body('usersType').optional().not().isEmpty().withMessage(() => { return i18n.__('usersRequired') }),
            body('users').optional().not().isEmpty().withMessage(() => { return i18n.__('usersRequired') }),
            body('users.*').optional().not().isEmpty().withMessage(() => { return i18n.__('usersRequired') }).custom(async (val) => {
                await checkExist(val, User, { deleted: false });
                return true;
            }),
            // body('promoCodeType').not().isEmpty().withMessage('promoCodeTypeRequired').isIn(['RATIO', 'VALUE']).withMessage(() => { return i18n.__('invalidType') })
        ]
        return validations;
    },

    async create(req, res, next) {
        try {
            let data = checkValidations(req);
            if (!data.code) {
                data.code = generateVerifyCode();
            }
            // data.startDate = moment(data.startDate).startOf('day');
            // data.endDate = moment(data.endDate).endOf('day');

            let promocode = await Promocode.create(data);
            let users 
            let notifiObj = { resource: req.user.id, subjectType: "PROMOCODE",subject:promocode.id, text: notif.text, title:notif.title };
            if(data.usersType === "SPECIFIC"){
                users = await User.find({_id:{$in:data.users},notification:true})
                notifiObj.type="SPECIFIC"
            }else if(data.usersType === "ALL"){
                users = await User.find({notification:true})
                notifiObj.type="ALL"
            }
            await Notif.create(notifiObj)
            users.forEach(async (user) => {
                sendPushNotification(
                    {
                        targetUser: user,
                        subjectType: notifiObj.subjectType,
                        subjectId: notifiObj.subject,
                        text: notifiObj.text[user.language],
                        title: notifiObj.title[user.language],
                        image: "https://res.cloudinary.com/adhamfayrouz/image/upload/v1632651920/categories/upload_d425dbeeada6c596d54aec0abf165af2_qqwrhs.png"
                    });
                
                // notificationNSP.to('room-'+user.id).emit(socketEvents.NotificationsCount, { count:await Notif.count({$or:[{target: user.id},{users:user.id}],informed: { $ne: user.id },deleted: false,usersDeleted: { $ne: user.id }}) });

            })
            // let desc = {
            //     ar: ' لديك كود خصم جديد ' + promocode.code,
            //     en: ' You have a new discount code ' + promocode.code
            // }
//
            // if (promocode.usersType === 'ALL') {
            //     let users = await User.find({ type: 'CLIENT', deleted: false })
            //     for (let index = 0; index < users.length; index++) {
            //         if (users[index].language == 'ar') {
            //             notifiController.pushNotification(users[index]._id, 'PROMOCODE', promocode._id, desc.ar,config.notificationTitle.ar);
            //         } else {
            //             notifiController.pushNotification(users[index]._id, 'PROMOCODE', promocode._id, desc.en,config.notificationTitle.ar);
            //         }
            //         notifiController.create(req.user.id, users[index]._id, desc, promocode._id, "PROMOCODE")
            //         notificationNSP.to('room-' + users[index].id).emit(socketEvents.NewUser, { user: users[index] });
            //     }
            // } else {
            //     let users = await User.find({ _id: { $in: promocode.users } }) 
            //     for (let index = 0; index < users.length; index++) {
            //         let user = users[index]
            //         if (user && user.language == 'ar') {
            //             notifiController.pushNotification(user.id, 'PROMOCODE', promocode._id, desc.ar, config.notificationTitle.ar);
            //         } else {
            //             notifiController.pushNotification(user.id, 'PROMOCODE', promocode._id, desc.en, config.notificationTitle.ar);
            //         }
            //         notifiController.create(req.user.id, user.id, desc, promocode._id, "PROMOCODE")
            //         notificationNSP.to('room-' + users[index].id).emit(socketEvents.NewUser, { user: users[index] });

            //     }
            // }
            res.status(200).send(promocode);
        } catch (error) {
            next(error);
        }
    },
    async findById(req, res, next) {
        try {
            let {promocodeId} = req.query;
            const {localized} = req.query
            let promocode = await checkExistThenGet(promocodeId, Promocode, { deleted: false, populate: populateQuery });
            if(localized)
                promocode = await Promocode.schema.methods.toJSONLocalizedOnly(promocode, req.locale);

            res.status(200).send(promocode);
        } catch (error) {
            next(error);
        }
    },

    async delete(req, res, next) {
        try {
            let id = req.query.id;
            await checkExist(id, Promocode );
            await Promocode.deleteOne({_id:id})
            res.status(200).send('Deleted Successfully');
        } catch (error) {
            next(error);
        }
    },
    validateUpdateBody() {
        let validations = [
            body('code').optional().not().isEmpty().withMessage(() => { return i18n.__('codeRequired') }),
            body('discount').optional().not().isEmpty().withMessage(() => { return i18n.__('discountRequired') }),
            body('startDate').optional().not().isEmpty().withMessage(() => { return i18n.__('startDateRequired') }),
            body('endDate').optional().not().isEmpty().withMessage(() => { return i18n.__('endDateRequired') }),
            body('numberOfUse').optional().not().isEmpty().withMessage(() => { return i18n.__('numberOfUseRequired') }),
            body('usersType').optional().not().isEmpty().withMessage(() => { return i18n.__('usersTypeRequired') }).isIn(['ALL', 'SPECIFIC']).withMessage(() => { return i18n.__('invalidType') }).custom(async (value, { req }) => {
                if (value === 'SPECIFIC') {
                    if (!req.body.users) {
                        throw new Error('Enter the users');
                    }
                    if (req.body.users.length == 0) {
                        throw new Error('Enter the users');
                    }
                }
                return true;
            }),
            body('users').optional().not().isEmpty().withMessage(() => { return i18n.__('usersRequired') }),
            body('users.*').optional().not().isEmpty().withMessage(() => { return i18n.__('usersRequired') }).custom(async (val) => {
                await checkExist(val, User, { deleted: false });
                return true;
            }),
        ]
        return validations;
    },
    async update(req, res, next) {
        try {
            let id = req.query.id;
            let promocode = await checkExistThenGet(id, Promocode, { deleted: false });
            let data = checkValidations(req);
            promocode = await Promocode.findOneAndUpdate({ _id: id, deleted: false }, data, { new: true });
            res.status(200).send(promocode);
        } catch (error) {
            next(error);
        }
    },
    validateConfirm() {
        return [
            body('code').not().isEmpty().withMessage(() => { return i18n.__('codeRequired') })
        ]
    },
    async confirmCode(req, res, next) {
        try {
            let user = req.user;
            const code = checkValidations(req).code;
            let currentDate = new Date();
            let query = {
                deleted: false,
                code: code,
                startDate: { $lte: currentDate },
                endDate: { $gte: currentDate },
                $or: [{ usersType: 'ALL' }, { usersType: 'SPECIFIC', users: { $elemMatch: { $eq: user.id } } }]
            };
            //console.log(query)
            let promoCode = await Promocode.findOne(query);
            if (!promoCode) {
                return next(new ApiError(400, i18n.__('promoCodeInvalid')));
            }

            let count = await Order.count({deleted:false, user: user.id, promoCode: promoCode.id });
            if (count >= promoCode.numberOfUse) {
                return next(new ApiError(400, i18n.__('ExccedPromoCodeNumberOuse')));
            }
            notificationNSP.to('room-' + user.id).emit(socketEvents.NewUser, { user: user });

            res.status(200).send(promoCode);
        } catch (error) {
            next(error);
        }
    }
}