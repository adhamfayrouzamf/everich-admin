import bcrypt                            from 'bcrypt'
import { check }                         from 'express-validator'
import { checkValidations }              from '../shared.controller/shared.controller'
import generateToken                     from '../../utils/token'
import User                              from '../../models/user.model/user.model'
import { checkExistThenGet, checkExist } from '../../helpers/CheckMethods'
import ApiError                          from '../../helpers/ApiError'
import ApiResponse                       from '../../helpers/ApiResponse'
import moment                            from 'moment'
import { twilioMail, twilioVerify }      from '../../services/twilo'
import Country                           from '../../models/country.model/country.model'
import City                              from '../../models/city.model/city.model'
import Region                            from '../../models/region.model/region.model'
import Address                           from '../../models/address.model/address.model'
// const reportController = require('../report.controller/report.controller')
// const ConfirmationCode = require('../../models/confirmationsCodes.model/confirmationscodes.model')
// const { sendEmail } = require('../../services/emailMessage.service')
// const Hash = require("../../models/hash.model/hash.model")
// const socketEvents = require('../../socketEvents)'
// const checkValidCoordinates = require('is-valid-coordinates');
// const NotificationController = require('../notif.controller/notif.controller');


const checkUserExistByEmail = async (email) => {
    let user = await User.findOne({ email, deleted: false });
    if (!user)
        throw new ApiError.BadRequest('email Not Found');
    return user;
}
let populateQuery = [
    { path: 'country', model: 'country' },
    { path: 'city', model: 'city', populate: [{ path: 'country', model: 'country' }] },
    { path: 'region', model: 'region', populate: [{ path: 'city', model: 'city', populate: [{ path: 'country', model: 'country' }] }] },
];

export default {

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;

            var { all, name, type, fromDate, toDate, phone, email, activated, countryKey, countryCode,
                month, year, day, country,search} = req.query;

            var query = { deleted: false };
            if (name) query.username = { '$regex': name, '$options': 'i' };
            if (phone) query.phone = { '$regex': phone, '$options': 'i' };
            if (email) query.email = { '$regex': email, '$options': 'i' };
            if (search) query.$or = [{ username:{'$regex': search, '$options': 'i'} },{ email:{'$regex': search, '$options': 'i'} },{ phone:{'$regex': search, '$options': 'i'} }]
            if (type) query.type = type;
            if (activated) query.activated = activated;
            if (countryKey) query.countryKey = countryKey;
            if (countryCode) query.countryCode = countryCode;
            if (country) {
                let userCountries = await Address.find({deleted:false , country :  country }).distinct('user');
                query._id = {$in : userCountries}
            }

            let users = await User.find(query).sort({ updatedAt: -1 }).limit(limit).skip((page-1)*limit).populate(populateQuery);
            let userCount= await User.count(query)
            let pageCount = Math.ceil(userCount / limit)

            res.send(new ApiResponse(users, page, pageCount, limit, userCount, req));
        } catch (error) {
            next(error)
        }
    },

    validateUserSignin() {
        let validations = [
            check('email').not().isEmpty().withMessage(''),
            check('password').not().isEmpty().withMessage(''),
            check('type').optional().not().isEmpty().withMessage('')
        ];
        return validations;
    },

    async signIn(req, res, next) {
        try {
            const validated = checkValidations(req);
            var query = { deleted: false,type:'CLIENT',activated:true,socialMediaType:'NORMAL'};
            // query.$or = [{phone: validated.phone.trim()},{name: validated.phone.trim()}]
            query.email = validated.email
            console.log(validated.password)
            if(validated.type)  query.type = validated.type
            let user = await User.findOne(query);
            if (user) {
                await user.isValidPassword(validated.password, async function (err, isMatch) {
                    if (err) {
                        next(err)
                    } else if (isMatch) {
                        console.log('test')
                        console.log('ismatch',isMatch)
                        if (!user.activated) {
                            return next(new ApiError(403,'accountStop'));
                        }
                        console.log('user',user)
                        return res.status(200).send({ ...user._doc, token: generateToken(user.id) });
                    } else {
                        return next(new ApiError(400, 'passwordInvalid'));
                    }
                })
            } else {
                return next(new ApiError(403,'userNotFound'));
            }
        } catch (err) {
            next(err);
        }
    },

    validateUserCreateBody() {
        let validations = [
            check('username').not().isEmpty().withMessage('must add username')
            .custom(async (value, { req }) => {
                value = (value.trim());
                let userQuery = { username: value, deleted: false,activated:true,socialMediaType:'NORMAL' };
                if (await User.findOne(userQuery))
                    throw new Error('user name already exists');
                return true;
            }),
            check('email').not().isEmpty().withMessage('must add email')
                .trim().isEmail().withMessage('email must be correct')
                .custom(async(value,{req})=>{
                    value = (value.trim()).toLowerCase();
                    let userQuery = { email: value, deleted: false, activated:true,socialMediaType:'NORMAL' };
                    if (await User.findOne(userQuery))
                        throw new Error('email already exists');
                    return true;
                }),

            check('password').not().isEmpty().withMessage('must add password'),
            check('phone').not().isEmpty().withMessage('must add phone').custom(async(value,{req})=>{
                value = (value.trim()).toLowerCase();
                let userQuery = { phone: value, deleted: false, activated:true,socialMediaType:'NORMAL' };
                if (await User.findOne(userQuery))
                    throw new Error('phone already exists');
                return true;
            }),
            check('countryCode').optional().not().isEmpty().withMessage('must add country code'),
            check('countryKey').optional().not().isEmpty().withMessage('must add  country key'),
            check('country').optional().not().isEmpty().withMessage('must add country').custom(async(val,{req})=>{
                await checkExist(val, Country, { deleted: false });
                return true;
            }),
            check('city').optional().not().isEmpty().withMessage('must add city').custom(async(val,{req})=>{
                await checkExist(val, City, { deleted: false });
                return true;
            }),
            check('region').optional().not().isEmpty().withMessage('must add region').custom(async(val,{req})=>{
                await checkExist(val, Region, { deleted: false });
                return true;
            })

        ];
        return validations;
    },

    async userSignUp(req, res, next) {
        try {
            const validated = checkValidations(req);
            validated.email = (validated.email.trim()).toLowerCase();
            let user = await User.findOne({email: validated.email, deleted: false,socialMediaType:'NORMAL' })
            let createdUser;
            createdUser = await User.create({...validated});
            // if(user){
            //     const salt = bcrypt.genSaltSync();
            //     var hash = await bcrypt.hash(validated.password, salt);
            //     validated.password = hash
            //     createdUser = await User.findOneAndUpdate(user._doc,validated,{new:true})
            //     console.log('test')
            // }
            // else
            
            // twilioMail(createdUser._doc.email)
            res.status(200).send({...createdUser._doc,token:generateToken(createdUser._doc._id)})
            // adminNSP.to('room-admin').emit(socketEvents.NewSignup, { user: createdUser });

        } catch (err) {
            next(err);
        }
    },
    validateActivateBody(){
        return [
        check('code').not().isEmpty().withMessage('code is required').isLength(6).withMessage('code must be 6 digits').matches(/^[0-9]{6}$/,"i").withMessage('code must contain digits only'),
        check('userId').not().isEmpty().withMessage('user id is required').custom(async(val,{req})=>{
            let user = User.findOne({_id:val,deleted:false}).lean()
            if(!user)
                throw new Error('user does not exist')
            return true
        })]
    },
    async userActivate(req,res,next){
        try{
            const validated = checkValidations(req)
            let user = await User.findOne({_id:validated.userId,deleted:false})
            const verified = await twilioVerify(user._doc.email,validated.code)
            console.log('verify',verified)
            if(verified){
                user = await User.findOneAndUpdate({_id:validated.userId,deleted:false},{activated:true},{new:true})
                res.status(200).send({...user._doc,token:generateToken(user._id)})
            }
            else{
                throw new Error('code is expired or not valid')
            }

        }catch(err){
            next(err)
        }
    },
    validateCheckPhone() {
        return [
            check('phone').not().isEmpty().withMessage('')
        ]
    },
    async checkExistPhone(req, res, next) {
        try {
            let phone = checkValidations(req).phone;
            let exist = await User.findOne({ phone: phone, deleted: false });
            return res.status(200).send({ duplicated: exist ? true : false });
        } catch (error) {
            next(error);
        }
    },

    validateCheckEmail() {
        return [
            check('email').trim().not().isEmpty().withMessage('')
        ]
    },
    async checkExistEmail(req, res, next) {
        try {
            let email = checkValidations(req).email.toLowerCase();
            let exist = await User.findOne({ email: email, deleted: false });
            return res.status(200).send({ duplicated: exist ? true : false });
        } catch (error) {
            next(error);
        }
    },

    validateUserUpdate() {
        let validations = [
            check('name').optional().not().isEmpty().withMessage(''),
            check('email').optional().trim().not().isEmpty().withMessage('')
                .isEmail().withMessage('')
                .custom(async (value, { req }) => {
                    value = (value.trim()).toLowerCase();
                    let userQuery = { _id: { $ne: req.user.id },socialMediaType:req.user.socialMediaType, email: value, deleted: false };
                    if (await User.findOne(userQuery))
                        throw new Error(i18n.__('emailDuplicated'));
                    else
                        return true;
                }),
            check('phone').optional().not().isEmpty().withMessage('')
                .custom(async (value, { req }) => {
                    value = (value.trim()).toLowerCase();
                    let userQuery = { _id: { $ne: req.user.id }, phone: value, deleted: false };
                    if (await User.findOne(userQuery))
                        throw new Error(i18n.__('phoneDuplicated'));
                    else
                        return true;
                }),
            check('language').optional().not().isEmpty().withMessage(),
            check('notification').optional().not().isEmpty().withMessage(''),
            check('newPassword').optional().not().isEmpty().withMessage(''),
            check('currentPassword').optional().not().isEmpty().withMessage(''),
            check('countryCode').optional().not().isEmpty().withMessage(''),
            check('countryKey').optional().not().isEmpty().withMessage(''),
            check('country').optional().not().isEmpty().withMessage('').custom(async(val,{req})=> {
                await checkExist(val, Country, { deleted: false });
                return true;
            }),
            check('city').optional().not().isEmpty().withMessage('').custom(async(val,{req})=> {
                await checkExist(val, City, { deleted: false });
                return true;
            }),
            check('region').optional().not().isEmpty().withMessage('').custom(async(val,{req})=> {
                await checkExist(val, Region, { deleted: false });
                return true;
            })
        ];

        return validations;
    },
    async updateInfo(req, res, next) {
        try {
            let userId = req.user._id;
            let validated = checkValidations(req);
            let user = await checkExistThenGet(userId, User, { deleted: false });
            if (validated.email) {
                validated.email = (validated.email.trim()).toLowerCase();
            }
            if (req.file) {
                let image = await handleImg(req, { attributeName: 'image', isUpdate: false });
                validated.image = image;
            }
            if (validated.city && !validated.country && !validated.region) {
                let city = await City.findById(validated.city)
                validated.country = city.country;
            }
            else if (validated.region && !validated.country && !validated.city) {
                let region = await Region.findById(validated.region).populate('city')
                validated.city = region.city.id;
                validated.country = region.city.country
            }
            if (validated.newPassword) {

                if (validated.currentPassword) {
                    if (bcrypt.compareSync(validated.currentPassword, user.password)) {
                        const salt = bcrypt.genSaltSync();
                        var hash = await bcrypt.hash(validated.newPassword, salt);
                        validated.password = hash;
                        delete validated.newPassword;
                        delete validated.currentPassword;
                        user = await User.findOneAndUpdate({ deleted: false, _id: userId }, validated, { new: true }).populate(populateQuery);
                        res.status(200).send(user);
                    } else {
                        return next(new ApiError(403, i18n.__('currentPasswordInvalid')))
                    }
                } else {
                    return res.status(400).send({
                        error: [{
                            location: 'body',
                            param: 'currentPassword',
                            msg: i18n.__('CurrentPasswordRequired')
                        }]
                    });
                }
            } else {
                user = await User.findOneAndUpdate({ deleted: false, _id: userId }, validated, { new: true }).populate(populateQuery);
                res.status(200).send(user);
            }

        } catch (error) {
            next(error);
        }
    },

    validateUpdatedPassword() {
        let validation = [
            check('newPassword').not().isEmpty().withMessage(''),
            check('currentPassword').not().isEmpty().withMessage(''),
        ];
        return validation;
    },
    async updatePasswordFromProfile(req, res, next) {
        try {
            let validated = checkValidations(req);
            let user = req.user;
            if (bcrypt.compareSync(validated.currentPassword, user.password)) {
                const salt = bcrypt.genSaltSync();
                var hash = await bcrypt.hash(validated.newPassword, salt);
                validated.password = hash;
                delete validated.newPassword;
                delete validated.currentPassword;
                user = await User.findOneAndUpdate({ deleted: false, _id: user.id }, validated, { new: true }).populate(populateQuery);
                res.status(200).send({ user: user });
            } else {
                return next(new ApiError(403, i18n.__('currentPasswordInvalid')))
            }
        } catch (error) {
            next(error);
        }
    },

    /////////////////////////////////////////////////////////////////////////////////////////// forget password by email
    validateForgetPassword() {
        return [
            check('email').not().isEmpty().withMessage(''),
            check('type').not().isEmpty().withMessage('')
                .isIn(['ADMIN', 'SUB_ADMIN', 'CLIENT']).withMessage(''),
        ];
    },
    async forgetPassword(req, res, next) {
        try {
            let validated = checkValidations(req);
            var email = validated.email;
            email = (email.trim()).toLowerCase();
            console.log(email)
            var user = await User.findOne({ email: email, deleted: false, type: validated.type });
            if (!user)
                return next(new ApiError(403, i18n.__('EmailNotFound')));
            var randomCode = '' + (Math.floor(1000 + Math.random() * 9000));
            var code = new ConfirmationCode({ email: email, code: randomCode });
            await code.save();
            var text = 'Enter This Code To Change Your Password ' + randomCode + ' .';
            await sendEmail(email, text);

            res.status(200).send(i18n.__('checkYourMail'));
        } catch (err) {
            next(err);
        }
    },

    validateConfirmCode() {
        return [
            check('email').not().isEmpty().withMessage(''),
            check('code').not().isEmpty().withMessage(''),
            check('type').not().isEmpty().withMessage('')
                .isIn(['ADMIN', 'SUB_ADMIN', 'CLIENT']).withMessage(''),
        ];
    },
    async verifyForgetPasswordCode(req, res, next) {
        try {
            let validated = checkValidations(req);
            var email = validated.email;
            var code = validated.code;
            email = (email.trim()).toLowerCase();
            var user = await ConfirmationCode.findOne({ code, email });

            if (user) {
                await ConfirmationCode.remove({ code, email });
                res.status(200).send(i18n.__('CodeSuccess'));
            } else
                res.status(400).send(i18n.__('CodeFail'));
        } catch (err) {
            next(err);
        }
    },

    async updatePassword(req, res, next) {
        try {
            let validated = checkValidations(req)
            validated.email = (validated.email.trim()).toLowerCase();
            const salt = bcrypt.genSaltSync();
            var hash = await bcrypt.hash(validated.newPassword, salt);
            var password = hash;
            var user = await User.findOneAndUpdate({ email: validated.email, deleted: false, type: validated.type }, { password: password }, { new: true }).populate(populateQuery);
            if (user) {
                res.status(200).send({
                    user,
                    token: generateToken(user.id)
                });
            } else
                res.status(404).send(i18n.__('EmailNotFound'));
        } catch (err) {
            next(err);
        }
    },
    ////////////////////////////////////////////////////////////////////////// forget password by phone
    validateForgetPasswordByPhone() {
        return [
            check('phone').not().isEmpty().withMessage(''),
        ];
    },
    async forgetPasswordByPhone(req, res, next) {
        try {
            let validated = checkValidations(req);
            var phone = validated.phone;
            phone = phone.trim()
            var user = await User.findOne({ phone: phone, deleted: false });
            if (!user)
                return next(new ApiError(403, i18n.__('userNotFound')));
            twilioSend('+2' + phone, user.language || 'ar');
            res.status(200).send(i18n.__('checkYourPhone'));
        } catch (err) {
            next(err);
        }
    },

    validateVerifyForgetPasswordByPhone() {
        return [
            check('code').not().isEmpty().withMessage(''),
            check('phone').not().isEmpty().withMessage(''),
        ];
    },
    async verifyForgetPasswordByPhone(req, res, next) {
        try {
            let validated = checkValidations(req);
            var phone = validated.phone;
            phone = phone.trim()
            var user = await User.findOne({ phone: phone, deleted: false });
            if (!user)
                return next(new ApiError(403, i18n.__('userNotFound')));
            twilioVerify('+' + user.countryCode + phone, validated.code, user, res, next);
        } catch (err) {
            next(err);
        }
    },

    validateUpdatePasswordByPhone() {
        return [
            check('password').not().isEmpty().withMessage(''),
            check('phone').not().isEmpty().withMessage(''),
        ];
    },
    async updatePasswordByPhone(req, res, next) {
        try {
            let validated = checkValidations(req);
            validated.phone = validated.phone.trim();
            let user = await User.findOne({ deleted: false, phone: validated.phone });
            if (!user) {
                return next(new ApiError(403, i18n.__('userNotFound')));
            }
            user.password = validated.password;
            await user.save();
            res.status(200).send({
                user,
                token: generateToken(user.id)
            });
        } catch (err) {
            next(err);
        }
    },
    ////////////////////////////////////////////////////////////////////////////////////////// reset password

    validateResetPassword() {
        return [
            check('email').not().isEmpty().withMessage(''),
            check('newPassword').not().isEmpty().withMessage(''),
            check('type').not().isEmpty().withMessage(''),
        ];
    },

    async resetPassword(req, res, next) {
        try {

            let validated = checkValidations(req);
            let user = await checkUserExistByEmail(validated.email);

            user.password = validated.newPassword;

            await user.save();

            await reportController.create({ "ar": 'تغير الرقم السري لمستخدم ', "en": "Change Password" }, 'UPDATE', req.user.id);

            res.status(200).send();

        } catch (err) {
            next(err);
        }
    },

    ////////////////////////////////////////////////////////////////////////////////////////// 
    validateAddToken() {
        let validations = [
            check('token').not().isEmpty().withMessage(''),
            check('type').not().isEmpty().withMessage('')
                .isIn(['ios', 'android', 'web']).withMessage('')
        ];
        return validations;
    },
    async addToken(req, res, next) {
        try {
            let validated = checkValidations(req);
            let user = await checkExistThenGet(req.user._id, User, { deleted: false });
            if(!user.tokens) user.tokens=[]
            let tokens = user.tokens;
            let found = false;
            for (let index = 0; index < tokens.length; index++) {
                if (tokens[index].token == validated.token) {
                    found = true
                }
            }
            if (!found) {
                
                user.tokens.push(validated);
                var q = {
                    token: validated.token,
                    deleted: false
                }
                // var doc = await Hash.findOne(q);
                // if (doc) {
                //     if (req.user.id != doc.user) {
                //         var newdoc = await Hash.findOneAndUpdate(q, { user: req.user.id });
                //         var newUser = await User.findByIdAndUpdate(doc.user, { $pull: { tokens: { token: validated.token } } }, { new: true });
                //     }
                // } else {
                //     await Hash.create({ token: validated.token, user: req.user.id });
                // }
                await user.save();
                console.log(user.tokens)
            }
            res.status(200).send(user);
        } catch (err) {
            next(err);
        }
    },

    validateLogout() {
        return [
            check('token').not().isEmpty().withMessage('tokenRequired')
        ];
    },
    async logout(req, res, next) {
        try {
            const {token} = checkValidations(req)
            let user = await checkExistThenGet(req.user._id, User, { deleted: false });

            let tokens = [];
            for (let i = 0; i < user.tokens.length; i++) {
                if (user.tokens[i].token != token) {
                    tokens.push(user.tokens[i]);
                }
            }
            user.tokens = tokens;
            user.workStatus = 'OFFLINE';
            user.logedIn = false;
            await user.save();
            res.status(200).send(await checkExistThenGet(req.user._id, User, { deleted: false }));
        } catch (err) {
            next(err)
        }
    },

    async userInformation(req, res, next) {
        try {
            var userId = req.query.userId;
            var user = await checkExistThenGet(userId, User, { deleted: false, populate: populateQuery });
            res.status(200).send({ user: user });
        } catch (error) {
            next(error);
        }
    },
    async deleteAccount(req, res, next) {
        try {
            var user = await checkExistThenGet(req.user.id, User, { deleted: false });
            user.deleted = true;
            await user.save();
            await ConfirmationCode.deleteMany({ email: user.email });
            res.status(200).send('Deleted Successfully');
        } catch (error) {
            next(error);
        }
    },

    async openActiveChatHead(req, res, next) {
        try {
            let user = req.user;
            let newUser = await User.findByIdAndUpdate(user.id, { activeChatHead: true }, { new: true });
            res.status(200).send({ user: newUser });
        } catch (error) {
            next(error);
        }
    },

    async closeActiveChatHead(req, res, next) {
        try {
            let user = req.user;
            let newUser = await User.findByIdAndUpdate(user.id, { activeChatHead: false }, { new: true });
            res.status(200).send({ user: newUser });

        } catch (error) {
            next(error);
        }
    },

    validateDeleteUserAccount() {
        let validations = [
            check('userId').not().isEmpty().withMessage(''),
        ];
        return validations;
    },
    async deleteUserAccount(req, res, next) {
        try {
            let userId = checkValidations(req).userId;
            var user = await checkExistThenGet(userId, User, { deleted: false });
            await ConfirmationCode.deleteMany({ email: user.email });
            user.deleted = true;
            await user.save();
            if (user.type == 'FAMILY') {
                notificationNSP.to('room-' + userId).emit(socketEvents.LogOut, { user })
            }
            res.status(200).send('Deleted Successfully');
        } catch (error) {
            next(error);
        }
    },

    validateSocialMediaLogin() {
        let validations = [
            check('email').not().isEmpty().withMessage(''),
            check('phone').optional().not().isEmpty().withMessage(''),
            check('username').not().isEmpty().withMessage(''),
            check('image').optional().not().isEmpty().withMessage(''),
            check('socialId').not().isEmpty().withMessage(''),
            check('socialMediaType').not().isEmpty().withMessage('')
        ];
        return validations;
    },
    async socialMedialLogin(req, res, next) {
        try {
            let validated = checkValidations(req);
            let query = { deleted: false, type: 'CLIENT', socialId: validated.socialId };

        if (validated.email) {
            validated.email = (validated.email.trim()).toLowerCase();
            query.email = validated.email;
        }
            let user = await User.findOne(query);
            if (user) {
                res.status(200).send({ ...user._doc, token: generateToken(user.id) });
            }
            else {
                let createdUser = await User.create(validated);
                res.status(200).send({...createdUser._doc, token: generateToken(createdUser.id) });
            }
        } catch (err) {
            next(err);
        }
    },

};