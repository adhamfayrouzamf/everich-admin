import Contactus                        from '../../models/contactUs.model/contactUs.model'
import ApiResponse                      from "../../helpers/ApiResponse"
import { checkExistThenGet }            from "../../helpers/CheckMethods"
import { body }                         from 'express-validator'
import { checkValidations, createDate } from "../shared.controller/shared.controller"
import i18n                             from 'i18n'
// const notifyController = require('../notif.controller/notif.controller')
// const socketEvents = require('../../socketEvents')
// const config = require('../../config)'

const populateQuery = [
    { path: 'user', model: 'user' }
];

let countNotReplied = async () => {
    try {
        let count = await Contactus.count({ deleted: false, "reply.0": { "$exists": false } });
        adminNSP.emit(socketEvents.ContactUsCount, { count: count });
    } catch (error) {
        throw error;
    }
}

export default {

    async find(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;

            let { name, notes, phone, user, firebaseTokenType } = req.query
            let date = createDate(req)
            let query = { deleted: false };

            if (date) query.createdAt = {$gte: date.startOfDate, $lte: date.endOfDate}

            if (name) {
                query.name = { '$regex': name, '$options': 'i' }
            }
            if (notes) {
                query.notes = { '$regex': notes, '$options': 'i' }
            }
            if (phone) {
                query.phone = { '$regex': phone, '$options': 'i' }
            }
            if (user) {
                query.user = user
            }
            if (firebaseTokenType) {
                query.firebaseTokenType = firebaseTokenType
            }

            let contactus = await Contactus.find(query)
                .sort({ updatedAt: -1 }).populate(populateQuery)
                .limit(limit)
                .skip((page - 1) * limit);

            const contactusCount = await Contactus.count(query);
            const pageCount = Math.ceil(contactusCount / limit);
            
            res.status(200).send(new ApiResponse(contactus, page, pageCount, limit, contactusCount, req));

        } catch (err) {
            next(err);
        }
    },

    validateBody() {
        let validations = [
            body('name').optional().not().isEmpty().withMessage(() => { return i18n.__('nameRequired') }),
            body('email').optional().not().isEmpty().withMessage(() => { return i18n.__('emailRequired') }),
            body('notes').not().isEmpty().withMessage(() => { return i18n.__('notesRequired') }),
            body('phone').not().isEmpty().withMessage(() => { return i18n.__('phoneRequired') })

        ];

        return validations;
    },

    async create(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            if (req.user)
                validatedBody.user = req.user.id;
            let contactUs = await Contactus.create(validatedBody);
            
            res.status(200).send(contactUs);
            // await countNotReplied()
        } catch (error) {
            next(error)
        }
    },

    async findById(req, res, next) {
        try {
            let { contactUsId } = req.query;
            let contactUs = await checkExistThenGet(contactUsId, Contactus, { deleted: false });
            res.status(200).send(contactUs);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let { contactUsId } = req.query;
            await checkExist(contactUsId, Contactus, 'contact not found');
            await Contactus.deleteOne({_id:contactUsId})
            res.status(200).send("Deleted Successfully");
        }
        catch (err) {
            next(err);
        }
    },

    validateReply() {
        let validations = [
            body('reply').not().isEmpty().withMessage('replyRequired')
        ];
        return validations;
    },

    async reply(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let { contactUsId } = req.query;
            let contactUs = await checkExistThenGet(contactUsId, Contactus, { deleted: false });
            let newContactUs = await Contactus.findOneAndUpdate({ deleted: false, _id: contactUsId }, { $push: { reply: validatedBody.reply } }, { new: true })
            res.status(200).send({ newContactUs });
            // await notifyController.create(req.user.id, newContactUs.user, { en: validatedBody.reply, ar: validatedBody.reply }, newContactUs.id, 'CONTACTUS');
            // if (req.user.language == 'ar'){
            //     await notifyController.pushNotification(newContactUs.user, 'CONTACTUS', newContactUs.id, validatedBody.reply,config.notificationTitle.ar );

            // }else{
            // await notifyController.pushNotification(newContactUs.user, 'CONTACTUS', newContactUs.id, validatedBody.reply,config.notificationTitle.ar );

            // }
            // await sendEmail(contactUs.email, validatedBody.reply);
            // await countNotReplied();
        }
        catch (err) {
            next(err);
        }
    },

    // countNotReplied
}

