import ApiResponse                       from "../../helpers/ApiResponse"
import ApiError                          from '../../helpers/ApiError'
import Company                           from "../../models/company.model/company.model"
import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods"
import { checkValidations }              from "../shared.controller/shared.controller"
import { body }                          from "express-validator"
import i18n                              from 'i18n'
// const socketEvents = require( '../../socketEvents')

export default {

    async getCompany(req, res, next) {
        try {
            let { localized } = req.query

            let query = { deleted: false };
            let companies = await Company.findOne(query)

            if (localized) {
                companies = Company.schema.methods.toJSONLocalizedOnly(companies, req.locale);
            }

            res.send(companies);

        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [];
        if (!isUpdate) {
            validations = [
                body('footerText').not().isEmpty().withMessage(() => { return i18n.__('footerTextRequired') }),
                body('footerText.ar').not().isEmpty().withMessage(() => { return i18n.__('arfooterTextRequired') }),
                body('footerText.en').not().isEmpty().withMessage(() => { return i18n.__('enfooterTextRequired') }),

                body('instructionsForUse').not().isEmpty().withMessage(() => { return i18n.__('instructionsForUseRequired') }).isArray().withMessage('Must Be Array'),
                body('instructionsForUse.*.ar').not().isEmpty().withMessage(() => { return i18n.__('arinstructionsForUseRequired') }),
                body('instructionsForUse.*.en').not().isEmpty().withMessage(() => { return i18n.__('eninstructionsForUseRequired') }),

                body('privacy').not().isEmpty().withMessage(() => { return i18n.__('privacyRequired') }).isArray().withMessage('Must Be Array'),
                body('privacy.*.ar').not().isEmpty().withMessage(() => { return i18n.__('arPrivacyRequired') }),
                body('privacy.*.en').not().isEmpty().withMessage(() => { return i18n.__('enPrivacyRequired') }),

                body('aboutUs').not().isEmpty().withMessage(() => { return i18n.__('arAboutUsRequired') }).isArray().withMessage('Must Be Array'),
                body('aboutUs.*.ar').not().isEmpty().withMessage(() => { return i18n.__('arAboutUsRequired') }),
                body('aboutUs.*.en').not().isEmpty().withMessage(() => { return i18n.__('enAboutUsRequired') }),

                body('returnPolicy').not().isEmpty().withMessage(() => { return i18n.__('arReturnPolicyRequired') }).isArray().withMessage('Must Be Array'),
                body('returnPolicy.*.ar').not().isEmpty().withMessage(() => { return i18n.__('arReturnPolicyRequired') }),
                body('returnPolicy.*.en').not().isEmpty().withMessage(() => { return i18n.__('enReturnPolicyRequired') }),
                
                body('email').not().isEmpty().withMessage(() => { return i18n.__('emailRequired') }),
                body('phone').not().isEmpty().withMessage(() => { return i18n.__('phoneRequired') }),
                body('landlinePhone').not().isEmpty().withMessage(() => { return i18n.__('landlinePhoneRequired') }),
                body('whatsappNumber').not().isEmpty().withMessage(() => { return i18n.__('whatsappNumberRequired') }),
                body('androidUrl').not().isEmpty().withMessage(() => { return i18n.__('androidUrlRequired') }),
                body('iosUrl').not().isEmpty().withMessage(() => { return i18n.__('iosUrlRequired') }),

                body('socialLinks').not().isEmpty().withMessage(() => { return i18n.__('socialLinksRequired') })
                    .isArray().withMessage(() => { return i18n.__('socialLinksValueError') }),
                body('socialLinks.*.key').not().isEmpty().withMessage(() => { return i18n.__('socialLinksRequired') }),
                body('socialLinks.*.value').not().isEmpty().withMessage(() => { return i18n.__('socialLinksRequired') }),

                // body('location.long').not().isEmpty().withMessage(() => { return i18n.__('longitudeRequired') }),
                // body('location.lat').not().isEmpty().withMessage(() => { return i18n.__('latitudeRequired') }),
                // body('location.address').not().isEmpty().withMessage(() => { return i18n.__('addressRequired') }),
                // body('minimumOrder').not().isEmpty().withMessage(() => { return i18n.__('minimumOrderRequired') }),
                // body('minimumOrderTime').not().isEmpty().withMessage(() => { return i18n.__('minimumOrderTimeRequired') }),

                body('transportPrice').optional().not().isEmpty().withMessage(() => { return i18n.__('transportPriceRequired') }),
                body('taxes').optional().not().isEmpty().withMessage(() => { return i18n.__('taxesRequired') }),
                body('taxNumber').not().isEmpty().withMessage(() => { return i18n.__('taxNumberRequired') }),

            ];
        }
        else {
            validations = [
                body('numberOfRowsForAdvertisments').optional().not().isEmpty().withMessage(() => { return i18n.__('numberOfRowsForAdvertismentsRequired') }),
                
                body('footerText').optional().not().isEmpty().withMessage(() => { return i18n.__('footerTextRequired') }),
                body('footerText.ar').optional().not().isEmpty().withMessage(() => { return i18n.__('arfooterTextRequired') }),
                body('footerText.en').optional().not().isEmpty().withMessage(() => { return i18n.__('enfooterTextRequired') }),

                body('instructionsForUse').optional().not().isEmpty().withMessage(() => { return i18n.__('instructionsForUseRequired') }).isArray().withMessage('Must Be Array'),
                body('instructionsForUse.*.ar').not().isEmpty().withMessage(() => { return i18n.__('arinstructionsForUseRequired') }),
                body('instructionsForUse.*.en').not().isEmpty().withMessage(() => { return i18n.__('eninstructionsForUseRequired') }),

                body('privacy').optional().not().isEmpty().withMessage(() => { return i18n.__('privacyRequired') }).isArray().withMessage('Must Be Array'),
                body('privacy.*.ar').not().isEmpty().withMessage(() => { return i18n.__('arPrivacyRequired') }),
                body('privacy.*.en').not().isEmpty().withMessage(() => { return i18n.__('enPrivacyRequired') }),

                body('aboutUs').optional().not().isEmpty().withMessage(() => { return i18n.__('arAboutUsRequired') }).isArray().withMessage('Must Be Array'),
                body('aboutUs.*.ar').not().isEmpty().withMessage(() => { return i18n.__('arAboutUsRequired') }),
                body('aboutUs.*.en').not().isEmpty().withMessage(() => { return i18n.__('enAboutUsRequired') }),

                body('returnPolicy').optional().not().isEmpty().withMessage(() => { return i18n.__('arReturnPolicyRequired') }).isArray().withMessage('Must Be Array'),
                body('returnPolicy.*.ar').not().isEmpty().withMessage(() => { return i18n.__('arReturnPolicyRequired') }),
                body('returnPolicy.*.en').not().isEmpty().withMessage(() => { return i18n.__('enReturnPolicyRequired') }),

                body('email').optional().not().isEmpty().withMessage(() => { return i18n.__('emailRequired') }),
                body('phone').optional().not().isEmpty().withMessage(() => { return i18n.__('phoneRequired') }),
                body('landlinePhone').optional().not().isEmpty().withMessage(() => { return i18n.__('landlinePhoneRequired') }),
                body('whatsappNumber').optional().not().isEmpty().withMessage(() => { return i18n.__('whatsappNumberRequired') }),
                body('androidUrl').optional().not().isEmpty().withMessage(() => { return i18n.__('androidUrlRequired') }),
                body('iosUrl').optional().not().isEmpty().withMessage(() => { return i18n.__('iosUrlRequired') }),

                body('socialLinks').optional().not().isEmpty().withMessage(() => { return i18n.__('socialLinksRequired') })
                    .isArray().withMessage(() => { return i18n.__('mustBeArray') }),
                body('socialLinks.*.key').not().isEmpty().withMessage(() => { return i18n.__('socialLinksRequired') }),
                body('socialLinks.*.value').not().isEmpty().withMessage(() => { return i18n.__('socialLinksRequired') }),

                body('location.long').optional().not().isEmpty().withMessage(() => { return i18n.__('longitudeRequired') }),
                body('location.lat').optional().not().isEmpty().withMessage(() => { return i18n.__('latitudeRequired') }),
                body('location.address').optional().not().isEmpty().withMessage(() => { return i18n.__('addressRequired') }),
                body('minimumOrder').optional().not().isEmpty().withMessage(() => { return i18n.__('minimumOrderRequired') }),
                body('minimumOrderTime').optional().not().isEmpty().withMessage(() => { return i18n.__('minimumOrderTimeRequired') }),

                // body('firstCategory').optional().not().isEmpty().withMessage(() => { return i18n.__('categoryRequired') }),
                // body('firstCategory.name').optional().not().isEmpty().withMessage(() => { return i18n.__('categoryNameRequired') }),
                // body('firstCategory.name.ar').optional().not().isEmpty().withMessage(() => { return i18n.__('categoryNameArRequired') }),
                // body('firstCategory.name.en').optional().not().isEmpty().withMessage(() => { return i18n.__('catrgoryNameEnRequired') }),
                
                // body('secondCategory').optional().not().isEmpty().withMessage(() => { return i18n.__('categoryRequired') }),
                // body('secondCategory.name').optional().not().isEmpty().withMessage(() => { return i18n.__('categoryNameRequired') }),
                // body('secondCategory.name.ar').optional().not().isEmpty().withMessage(() => { return i18n.__('categoryNameArRequired') }),
                // body('secondCategory.name.en').optional().not().isEmpty().withMessage(() => { return i18n.__('catrgoryNameEnRequired') }),

                // body('thirdCategory').optional().not().isEmpty().withMessage(() => { return i18n.__('categoryRequired') }),
                // body('thirdCategory.name').optional().not().isEmpty().withMessage(() => { return i18n.__('categoryNameRequired') }),
                // body('thirdCategory.name.ar').optional().not().isEmpty().withMessage(() => { return i18n.__('categoryNameArRequired') }),
                // body('thirdCategory.name.en').optional().not().isEmpty().withMessage(() => { return i18n.__('catrgoryNameEnRequired') }),

                body('transportPrice').optional().not().isEmpty().withMessage(() => { return i18n.__('transportPriceRequired') }),
                body('taxes').optional().not().isEmpty().withMessage(() => { return i18n.__('taxesRequired') }),
                body('taxNumber').optional().not().isEmpty().withMessage(() => { return i18n.__('taxNumberRequired') }),

            ];
        }
        return validations;
    },

    async create(req, res, next) {
        try {
            const validated = checkValidations(req);
            
            await Company.deleteMany();
            let createdCompany = await Company.create(validated);
            res.status(201).send(createdCompany);
        } catch (err) {
            next(err);
        }
    },

    async findById(req, res, next) {
        try {
            let { companyId } = req.query;
            let { localized } = req.query;
            let company = await checkExistThenGet(companyId, Company, { deleted: false });
            if (localized) {
                company = Company.schema.methods.toJSONLocalizedOnly(company, req.locale);
            }
            res.send(company);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {
        try {
            let validated = checkValidations(req);

            let updatedCompany = await Company.findOneAndUpdate({},validated, { new: true });
            res.status(200).send(updatedCompany);
            // notificationNSP.emit(socketEvents.Company, { company: updatedCompany });
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let { companyId } = req.query;
            await checkExist(companyId, Company, 'company not found');
            Company.deleteOne({_id:companyId})
            res.status(200).send('delete success');
        }
        catch (err) {
            next(err);
        }
    },

    async share(req, res, next) {
        try {
            let company = await Company.findOne({ deleted: false });
            if (!company) {
                res.status(200).send('Done');
            }
            company.appShareCount = company.appShareCount + 1
            await company.save()
            res.status(200).send('Done');
        } catch (error) {
            next(error)
        }
    }
};