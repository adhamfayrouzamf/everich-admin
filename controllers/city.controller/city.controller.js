import City                              from "../../models/city.model/city.model"
import Country                           from "../../models/country.model/country.model"
import Region                            from "../../models/region.model/region.model"
import ApiResponse                       from "../../helpers/ApiResponse"
import ApiError                          from "../../helpers/ApiError"
import { checkExistThenGet, checkExist } from "../../helpers/CheckMethods"
import { body }                          from 'express-validator'
import { checkValidations, createDate }  from "../shared.controller/shared.controller"
import i18n                              from 'i18n'
import moment                            from 'moment'

let populateQuery = [{ path: 'country', model: 'country' }];

export default {

    validateBody(isUpdate = false) {
        let validations
        if (!isUpdate) {
            validations = [
                body('name.en').not().isEmpty().withMessage('must add city name')
                    .custom(async (val, { req }) => {

                        let query = { 'name.en': val, deleted: false, country: req.body.country };
                        let city = await City.findOne(query).lean();
                        if (city)
                            throw new Error('city already exists');
                        return true;
                    }),
                body('name.ar').not().isEmpty().withMessage('must add city name')
                .custom(async (val, { req }) => {

                    let query = { 'name.ar': val, deleted: false, country: req.body.country };
                    let city = await City.findOne(query).lean();
                    if (city)
                        throw new Error('city already exists');
                    return true;
                }),
                body('country').not().isEmpty().withMessage('must add country id').custom(async (val, { req }) => {
                    await checkExist(val, Country, { deleted: false });
                    return true;
                })
            ];
        }
        else {
            validations = [
                body('name.en').optional().not().isEmpty().withMessage('must add city name')
                    .custom(async (val, { req }) => {

                        let query = { 'name.en': val, deleted: false, country: req.body.country,_id:{$ne:req.query.cityId} };
                        let city = await City.findOne(query).lean();
                        if (city)
                            throw new Error('city already exists');
                        return true;
                    }),
                body('name.ar').optional().not().isEmpty().withMessage('must add city name')
                .custom(async (val, { req }) => {

                    let query = { 'name.ar': val, deleted: false, country: req.body.country,_id:{$ne:req.query.cityId} };
                    let city = await City.findOne(query).lean();
                    if (city)
                        throw new Error('city already exists');
                    return true;
                }),
                body('country').optional().not().isEmpty().withMessage('must add country id').custom(async (val, { req }) => {
                    await checkExist(val, Country, { deleted: false });
                    return true;
                })
            ];

        }
        return validations;
    },

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1 , limit = +req.query.limit || 20
            var { name, country, localized} = req.query;
            let date = createDate(req)
            let query = { deleted: false };
            if(date) query.createdAt = {$gte: date.startOfDate, $lte: date.endOfDate}
            if (name)
                query.$or = [{ 'name.en': { '$regex': name, '$options': 'i' } }, { 'name.ar': { '$regex': name, '$options': 'i' } }]


            if (country) query.country = country;
            
            let cities = await City.find(query).sort({updatedAt:-1}).limit(limit).skip((page-1)*limit).populate(populateQuery)
            let cityCount = await City.count(query)
            let pageCount = Math.ceil(cityCount / limit)

            if (localized) {
                cities = City.schema.methods.toJSONLocalizedOnly(cities, req.locale);
            }

            res.status(200).send(new ApiResponse(cities,page,pageCount, limit,cityCount,req));


        } catch (err) {
            next(err);
        }
    },

    async create(req, res, next) {
        try {
            let validated = checkValidations(req);
            let city = await City.create(validated);
            city = await City.populate(city, populateQuery);
            res.status(200).send(city);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {
        try {
            let { cityId } = req.query;
            await checkExist(cityId, City, { deleted: false });
            let validated = checkValidations(req);
            let updatedCity = await City.findByIdAndUpdate(cityId, validated, { new: true })
            updatedCity = await City.populate(updatedCity,populateQuery);
            res.status(200).send(updatedCity);
        } catch (err) {
            next(err);
        }
    },

    async findById(req, res, next) {
        try {
            let { cityId } = req.query;
            let {localized} = req.query
            let query = {deleted:false, populate : populateQuery}
            let city = await checkExistThenGet(cityId, City, query);
            if (localized) {
                city = City.schema.methods.toJSONLocalizedOnly(city, req.locale);
            }
            res.status(200).send(city);

        } catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            
            let { cityId } = req.query;
            await checkExist(cityId, City,'city not found');
            await city.deleteOne({_id:cityId});
            await Region.deleteMany({deleted: false,city:cityId})
            res.status(200).send("Deleted Successfully");

        }
        catch (err) {
            next(err);
        }
    }
}