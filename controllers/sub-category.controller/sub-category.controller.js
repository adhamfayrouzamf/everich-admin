import ApiResponse                       from "../../helpers/ApiResponse"
import ApiError                          from "../../helpers/ApiError"
import { checkExist, checkExistThenGet } from "../../helpers/CheckMethods"
import { checkValidations, createDate }  from "../shared.controller/shared.controller"
import { body }                          from "express-validator"
import SubCategory                       from "../../models/category.model/category.model"
import Comment                           from "../../models/comment.model/comment.model"
import Product                           from "../../models/product.model/product.model"
import Rate                              from "../../models/rate.model/rate.model"
import Favourites                        from "../../models/favourites.model/favourites.model"
import {deleteFile}                      from '../../services/cloudinary'
import moment                            from 'moment'
// const dotObject = require('dot-object');

let populateQuery = [{ path: 'parent', model: 'category' }]


export default {

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let { name, parent, all, client, localized } = req.query
            let query = { deleted: false, parent: { $ne: null } }
            
            const date  = createDate(req)
            if (date) query.createdAt = {$gte: date.startOfDate, $lte: date.endOfDate}
            if (name)
                query.$or = [{ 'name.en': { '$regex': name, '$options': 'i' } }, { 'name.ar': { '$regex': name, '$options': 'i' } }]
            if (parent) {
                query.parent = parent
            }
            let categories = await SubCategory.find(query).sort({updatedAt:-1}).limit(limit).skip((page-1)*limit).populate(populateQuery)
            let catCount = await SubCategory.count(query)
            const pageCount = Math.ceil(catCount / limit)
            if(localized){
                categories =  SubCategory.schema.methods.toJSONLocalizedOnly(categories, req.locale);
            }
            res.status(200).send(new ApiResponse(categories, page, pageCount, limit, catCount, req ))
            
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = []
        if (!isUpdate) {
            validations = [
                
                body('name.en').not().isEmpty().withMessage(() => { return ('englishName') })
                    .custom(async (val, { req }) => {
                        let query = { 'name.en': val, deleted: false };
                        let category = await SubCategory.findOne(query).lean();
                        if (category)
                            throw new Error(('englishNameDublicated'));
                        return true;
                    }),
                body('name.ar').not().isEmpty().withMessage(() => { return ('arabicName') })
                    .custom(async (val, { req }) => {
                        let query = { 'name.ar': val, deleted: false };
                        let category = await SubCategory.findOne(query).lean();
                        if (category)
                            throw new Error(('arabicNameDublicated'));
                        return true;
                    }),
                body('parent').optional().not().isEmpty().withMessage('must add parent category').custom(async (val,{req})=>{
                    let category = await SubCategory.findOne({_id:val}).lean()
                    if(!category){
                        throw new Error('category doesn\'t exist')
                    }
                    return true
                }),
                body('image.url').not().isEmpty().withMessage('image url is required'),
                body('image.id').not().isEmpty().withMessage('image id is required'),
            ]
        }
        else {
            validations = [
                body('name.en').optional().not().isEmpty().withMessage(() => { return ('englishName') })
                    .custom(async (val, { req }) => {
                        const {subcategoryId} = req.query
                        let query = { 'name.en': val, deleted: false, _id:{$ne:+subcategoryId}};
                        let category = await SubCategory.findOne(query).lean();
                        if (category)
                            throw new Error(('englishNameDublicated'));
                        return true;
                    }),
                body('name.ar').optional().not().isEmpty().withMessage(() => { return ('arabicName') })
                    .custom(async (val, { req }) => {
                        const {subcategoryId} = req.query
                        let query = { 'name.ar': val, deleted: false, _id:{$ne:+subcategoryId} };

                        let category = await SubCategory.findOne(query).lean();
                        if (category)
                            throw new Error('arabicNameDublicated');
                        return true;
                    }),
                body('parent').optional().custom(async (val,{req})=>{
                    if(val){
                        let category = await SubCategory.findOne({_id:val,parent:null}).lean()
                        if(!category){
                            throw new Error('category doesn\'t exist')
                        }
                    }
                    return true
                }),
                body('image.url').optional().not().isEmpty().withMessage('image url is required'),
                body('image.id').optional().not().isEmpty().withMessage('image id is required'),
            ]
        }
        return validations
    },

    async create(req, res, next) {
        try {
            let validated = checkValidations(req);
            //////////////////////////////////////////////////
            // let products = await Product.find({category: validated.parent}).distinct('_id');
            // if(products.length > 0){
            //     let newSub = await SubCategory.create({image:'/otherImage.png',name:{ar:'اخري',en:'OTHER'},parent:validated.parent});
            //     await Product.updateMany({_id:{$in:products}},{category:newSub.id})
            // }
            /////////////////////////////////////////////////
            let createdsubCategory = await SubCategory.create(validated);
            res.status(200).send(createdsubCategory);
            await SubCategory.findByIdAndUpdate(validated.parent, { hasChild: true })
            
        } catch (err) {
            next(err);
        }
    },

    async findById(req, res, next) {
        try {
            let { subcategoryId, localized } = req.query;
            var subcategory = await checkExistThenGet(+subcategoryId, SubCategory, { populate: populateQuery });
            if (localized) {
                subcategory = SubCategory.schema.methods.toJSONLocalizedOnly(subcategory, req.locale);
            }
            res.status(200).send(subcategory);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {
        try {

            let { subcategoryId, localized } = req.query;
            let subcategory = await checkExistThenGet(subcategoryId, SubCategory, { deleted: false });
            let validated = checkValidations(req);
            console.log(validated)
            if(validated.image){
                if(validated.image.id !== subcategory.image.id)
                    await deleteFile(subcategory.image.id)
            }
            if (subcategory.parent != +validated.parent) {
                await SubCategory.findByIdAndUpdate(validated.parent, { hasChild: true })
                let prevParentSub = await SubCategory.count({ deleted: false, parent: subcategory.parent,_id:{$ne:subcategoryId} })
                if (prevParentSub <= 0)
                    await SubCategory.findByIdAndUpdate(subcategory.parent, { hasChild: false })
            }
            let updatedsubCategory = await SubCategory.findByIdAndUpdate(subcategoryId, validated, { new: true }).populate(populateQuery)
            if (localized) {
                updatedsubCategory = SubCategory.schema.methods.toJSONLocalizedOnly(updatedsubCategory, req.locale);
            }
            res.status(200).send(updatedsubCategory);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            
            let { subcategoryId } = req.query
            await SubCategory.deleteOne({_id:subcategoryId})
            
            let products = await Product.find({ subCategory: +subcategoryId }).distinct('_id')
            await Product.deleteMany({ subCategory: +subcategoryId })
            await Favourites.deleteMany({ product: { $in: products } })
            await Rate.deleteMany({ product: { $in: products } })
            await Comment.deleteMany({ product:{ $in: products } })
            res.status(200).send('Deleted Successfully')
        }
        catch (err) {
            next(err)
        }
    },
};