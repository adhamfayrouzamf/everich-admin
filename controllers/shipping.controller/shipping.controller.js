import Shipping                          from '../../models/shipping.model/shipping.model'
import ApiResponse                       from "../../helpers/ApiResponse"
import ApiError                          from "../../helpers/ApiError"
import { checkExistThenGet, checkExist } from "../../helpers/CheckMethods"
import { body }                          from 'express-validator'
import { checkValidations, createDate }  from "../shared.controller/shared.controller"
import i18n                              from 'i18n'
import moment                            from 'moment'

export default {

    validateBody(isUpdate = false) {
        let validations
        if (!isUpdate) {
            validations = [
                body('name.en').not().isEmpty().withMessage(() => { return i18n.__('englishName') })
                .custom(async (val, { req }) => {
                    let query = { 'name.en': val, deleted: false };
                        let shipping = await Shipping.findOne(query).lean();
                        if (shipping)
                            throw new Error('name already exists');
                        return true;
                }),
                body('name.ar').not().isEmpty().withMessage(() => { return i18n.__('englishName') })
                .custom(async (val, { req }) => {
                    let query = { 'name.ar': val, deleted: false };
                        let shipping = await Shipping.findOne(query).lean();
                        if (shipping)
                            throw new Error('name already exists');
                        return true;
                }),
                body('shippingPrice').not().isEmpty().withMessage('must add shipping price').isNumeric({min:1}).withMessage('must add valid price'),
            ];
        }
        else {
            validations = [
                body('name.en').optional().not().isEmpty().withMessage(() => { return i18n.__('englishName') })
                .custom(async (val, { req }) => {
                    let query = { 'name.en': val, deleted: false };
                        let shipping = await Shipping.findOne(query).lean();
                        if (shipping)
                            throw new Error('name already exists');
                        return true;
                }),
                body('name.ar').optional().not().isEmpty().withMessage(() => { return i18n.__('englishName') })
                .custom(async (val, { req }) => {
                    let query = { 'name.ar': val, deleted: false };
                        let shipping = await Shipping.findOne(query).lean();
                        if (shipping)
                            throw new Error('name already exists');
                        return true;
                }),
                body('shippingPrice').optional().not().isEmpty().withMessage('must add shipping price').isNumeric({min:1}).withMessage('must add valid price'),
            ];

        }
        return validations;
    },

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            var { name, localized } = req.query
            let query = { deleted: false }

            const date  = createDate(req)
            if (date) query.createdAt = {$gte: date.startOfDate, $lte: date.endOfDate}
            if (name)
                query.$or = [{ 'name.en': { '$regex': name, '$options': 'i' } }, { 'name.ar': { '$regex': name, '$options': 'i' } }]

            let shippings = await Shipping.find(query).sort({updatedAt:-1}).limit(limit).skip((page-1)*limit)
            let shippingCount = await Shipping.count(query)
            let pageCount = Math.ceil(shippingCount / limit)

            if (localized) {
                shippings = Shipping.schema.methods.toJSONLocalizedOnly(shippings, req.locale);
            }
            res.send(new ApiResponse(shippings, page, pageCount, limit, shippingCount, req));


        } catch (err) {
            next(err);
        }
    },

    async create(req, res, next) {
        try {
            
            let validatedBody = checkValidations(req);
            let shipping = await Shipping.create(validatedBody);
            res.status(200).send(shipping);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {
        try {
            let { shippingId } = req.query;
            await checkExist(shippingId, Shipping, { deleted: false });
            let validatedBody = checkValidations(req);
            let updatedShipping = await Shipping.findByIdAndUpdate(shippingId, {
                ...validatedBody,
            }, { new: true });
            
            res.status(200).send(updatedShipping);
        } catch (err) {
            next(err);
        }
    },

    async findById(req, res, next) {
        try {
            let { shippingId } = req.query;
            const {localized} = req.query
            let shipping = await checkExistThenGet(shippingId, Shipping, { deleted: false });
            if(localized)
                shipping = Shipping.schema.methods.toJSONLocalizedOnly(shipping, req.locale);
            res.status(200).send(shipping);

        } catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            
            let { shippingId } = req.query;
            await checkExist(shippingId, Shipping );
            await Shipping.deleteOne({_id:shippingId})
            res.status(200).send("Deleted Successfully");
        }
        catch (err) {
            next(err);
        }
    }
}