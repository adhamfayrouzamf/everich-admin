import ApiResponse                           from "../../helpers/ApiResponse"
import ApiError                              from "../../helpers/ApiError"
import { checkExist, checkExistThenGet }     from "../../helpers/CheckMethods"
import { checkValidations, createDate }      from "../shared.controller/shared.controller"
import { body }                              from "express-validator"
import Comment                               from "../../models/comment.model/comment.model"
import User                                  from "../../models/user.model/user.model"
import Product                               from "../../models/product.model/product.model"
import Rate                                  from "../../models/rate.model/rate.model"
import moment                                from 'moment'
import i18n                                  from 'i18n'

let populateQuery = [
    {path:'user',model:'user'},
    {path:'product',model:'product'
        /* ,populate:[
            {path:'category',model:'category', populate:[{path:'parent', model:'category'}]},
            {path:'trademark',model:'trademark'}
    ] */}
];

let createPromise = (query) => {
    let newPromise = new Promise(async (resolve, reject) => {
        try {
            const result = await query
            resolve(result)
        } catch (error) {
            reject(error)
        }
    })
    return newPromise
}

let checkinRates = async (list) => {
    try {
        let promises = [];
        let query = { deleted: false}
        for (let index = 0; index < list.length; index++) {
            query.product = list[index].product;
            query.user    = list[index].user;
            let promis = Rate.findOne(query);
            if (promis)
                promises.push(createPromise(promis));
        }
        let finalResult = await Promise.all(promises);
        console.log(finalResult.length)
        console.log('list')
        for (let index = 0; index < finalResult.length; index++) {
            if (finalResult[index]) {
                list[index].userRate = finalResult[index].rate;
            }
        }
        console.log(list)
        return list;
    } catch (error) {
        throw error
    }
}


export default {

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let {name,month,year,product,user,all, localized} = req.query
            let query = {deleted: false};
            let date = createDate(req)
            if(date) query.createdAt = {$gte: date.startOfDate, $lte: date.endOfDate}
            
            if(user) query.user = user
            if(product) query.product = product

            let comment = await Comment.find(query).sort({updatedAt:-1}).limit(limit).skip((page-1)*limit).populate(populateQuery)
            comment = await checkinRates(comment)
            let commentCount = await Comment.count(query)
            let pageCount = Math.ceil(commentCount / limit)

            if(localized)
                comment = Comment.schema.methods.toJSONLocalizedOnly(comment,req.locale)

            res.send(new ApiResponse(comment, page, pageCount, limit, commentCount, req ));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [];
        if (!isUpdate) {
            validations = [
                body('comment').not().isEmpty().withMessage('must add comment'),
                body('product').not().isEmpty().withMessage('must add product').custom(async(val,{req})=>{
                    await checkExist(val,Product,{deleted:false})
                    return true
                }),
            ];
        }
        else{
            validations = [
                body('comment').optional().not().isEmpty().withMessage('must add comment'),
                body('product').optional().not().isEmpty().withMessage('must add product').custom(async(val,{req})=>{
                    await checkExist(val,Product,{deleted:false})
                    return true
                }),
            ];
        }
        return validations;
    },

    async create(req, res, next) {
        try {
            let user = req.user
            const validated = checkValidations(req);
            validated.user = user._id
            let createdcomment = await Comment.create(validated);
            createdcomment = await Comment.populate(createdcomment,populateQuery)
            createdcomment = (await checkinRates([createdcomment]))[0]
            createdcomment = Comment.schema.methods.toJSONLocalizedOnly(createdcomment, req.locale);
            res.status(200).send(createdcomment);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { commentId } = req.query;
            let {localized} = req.query
            let query = {deleted:false, populate : populateQuery}
            let comment = await checkExistThenGet(commentId, Comment, query);
            comment = (await checkinRates([comment]))[0]

            if(localized)
                comment = Comment.schema.methods.toJSONLocalizedOnly(comment, req.locale);
                
            res.status(200).send(comment);
        } catch (err) {
            next(err);
        }
    },

    async update(req, res, next) {
        try {
            
            let { commentId } = req.query;
            let { localized } = req.query
            await checkExist(commentId, Comment, { deleted: false });
            var validated = checkValidations(req);

            let updatedcomment = await Comment.findByIdAndUpdate(commentId, validated , { new: true })
            updatedcomment = await Comment.populate(updatedcomment,populateQuery) 

            updatedcomment = (await checkinRates([updatedcomment]))[0]
            
            res.status(200).send(updatedcomment);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            
            let { commentId } = req.query;
            await checkExist(commentId, Comment,'comment not found');
            Comment.deleteOne({_id:commentId})
            res.status(200).send('Deleted Successfully');
        }
        catch (err) {
            next(err);
        }
    }
};