import  ApiResponse                                   from  "../../helpers/ApiResponse"
import  ApiError                                      from "../../helpers/ApiError"
import  { checkExist, checkExistThenGet }             from "../../helpers/CheckMethods"
import  { checkValidations, createDate }              from "../shared.controller/shared.controller"
import  { body }                                      from "express-validator"
import  Category                                      from "../../models/category.model/category.model"
import  moment                                        from 'moment'
import  Product                                       from "../../models/product.model/product.model"
import  TradeMark                                     from "../../models/tradeMarks.model/tradeMarks.model"
import  Favourites                                    from "../../models/favourites.model/favourites.model"
import  Comment                                       from "../../models/comment.model/comment.model"
import  Rate                                          from "../../models/rate.model/rate.model"
import  i18n                                          from 'i18n'
import  fs                                            from 'fs'
import {deleteFile}                                   from '../../services/cloudinary'


let populateQuery = [
    { path: 'parent', model: 'category' }
]

export default {

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            const { name, all, localized } = req.query
            let query = { parent: { $eq: null } };
            const date  = createDate(req)
            if (date) query.createdAt = {$gte: date.startOfDate, $lte: date.endOfDate}
            if (name)
                query.$or = [{ 'name.en': { '$regex': name, '$options': 'i' } }, { 'name.ar': { '$regex': name, '$options': 'i' } }]

            let cats= await Category.find(query).sort({updatedAt:-1}).limit(limit).skip((page-1)*limit).populate(populateQuery)
            let catCount = await Category.count(query)
            const pageCount = Math.ceil(catCount / limit)
            if(localized){
                cats =  Category.schema.methods.toJSONLocalizedOnly(cats, req.locale);
            }
            res.status(200).send(new ApiResponse(cats, page, pageCount, limit, catCount, req ))
        } catch (err) {
            next(err)
        }
    },

    validateBody(isUpdate = false) {
        let validations = []
        if (!isUpdate) {
            validations = [
                
                body('name.en').not().isEmpty().withMessage(() => { return i18n.__('englishName') })
                    .custom(async (val, { req }) => {
                        let query = { 'name.en': val, deleted: false };
                        let category = await Category.findOne(query).lean();
                        if (category)
                            throw new Error(i18n.__('englishNameDublicated'));
                        return true;
                    }),
                body('name.ar').not().isEmpty().withMessage(() => { return i18n.__('arabicName') })
                    .custom(async (val, { req }) => {
                        let query = { 'name.ar': val, deleted: false };
                        let category = await Category.findOne(query).lean();
                        if (category)
                            throw new Error(i18n.__('arabicNameDublicated'));
                        return true;
                    }),
                body('image.url').not().isEmpty().withMessage('image url is required'),
                body('image.id').not().isEmpty().withMessage('image id is required'),
            ]
        }
        else {
            validations = [
                body('name.en').optional().not().isEmpty().withMessage(() => { return i18n.__('englishName') })
                    .custom(async (val, { req }) => {
                        let query = { 'name.en': val, deleted: false };
                        if (isUpdate)
                            query._id = { $ne: req.query.categoryId };
                        let category = await Category.findOne(query).lean();
                        if (category)
                            throw new Error(i18n.__('englishNameDublicated'));
                        return true;
                    }),
                body('name.ar').optional().not().isEmpty().withMessage(() => { return i18n.__('arabicName') })
                    .custom(async (val, { req }) => {
                        let query = { 'name.ar': val, deleted: false };
                        if (isUpdate)
                            query._id = { $ne: req.query.categoryId };
                        let category = await Category.findOne(query).lean();
                        if (category)
                            throw new Error(i18n.__('arabicNameDublicated'));
                        return true;
                    }),
                body('image.url').optional().not().isEmpty().withMessage('image url is required'),
                body('image.id').optional().not().isEmpty().withMessage('image id is required'),
            ]
        }
        return validations
    },

    async create(req, res, next) {
        try {
            let validated = checkValidations(req)
            console.log(validated)
            // let image = req.file
            // image.url="https://madlol-back.herokuapp.com/uploads/category/"+image.filename
            // const {url} = await cloudinary.uploader.upload(req.file.path,{
            //     upload_preset:'ml_default'
            // })
            // let image = {url:url,name:req.file.filename}

            let createdCategory = await Category.create(validated)
            createdCategory = await Category.populate(createdCategory,populateQuery)
            res.status(200).send(createdCategory)
        } catch (err) {
            fs.unlink(req.file.path,()=>console.log('removed'))
            next(err)
        }
    },


    async findById(req, res, next) {
        try {
            let { categoryId } = req.query
            let { localized } = req.query
            let query = { populate:populateQuery }
            let c = await checkExistThenGet(categoryId, Category, query)
            if (localized) {
                c = Category.schema.methods.toJSONLocalizedOnly(c, req.locale);
            }
            res.status(200).send(c)
        } catch (err) {
            next(err)
        }
    },

    async update(req, res, next) {
        try {
            let validated = checkValidations(req)
            let { categoryId } = req.query
            if(validated.image){
                let category = await checkExistThenGet(categoryId, Category)
                if(validated.image.id !== category.image.id)
                    await deleteFile(category.image.id)
            }
            let updatedCategory = await Category.findByIdAndUpdate(categoryId, {$set:validated},{new:true})
            res.status(200).send(updatedCategory)
        }
        catch (err) {
            next(err)
        }
    },

    async delete(req, res, next) {
        try {
            
            let { categoryId } = req.query
            await Category.deleteOne({_id:categoryId})

            await Category.deleteMany({ parent: categoryId })

            let products = await Product.find({ category: categoryId }).distinct('_id')
            await Product.deleteMany({ category: categoryId })
            await Favourites.deleteMany({ product: { $in: products } })
            await Rate.deleteMany({ product: { $in: products } })
            await TradeMark.deleteMany({ category: categoryId })
            await Comment.deleteMany({ product:{ $in: products } })
            res.status(200).send('Deleted Successfully')
        }
        catch (err) {
            next(err)
        }
    },
}