import User from "../../models/user.model/user.model";
import { checkExistThenGet, checkExist } from "../../helpers/CheckMethods";
import Notif from "../../models/notification.model/notification.model";
import ApiResponse from "../../helpers/ApiResponse";
import { sendPushNotification } from '../../services/push-notification-service'
import { checkValidations, createDate } from '../shared.controller/shared.controller';
import {deleteFile} from "../../services/cloudinary"
import { body } from 'express-validator';
import ApiError from "../../helpers/ApiError";

// import socketEvents from '../../socketEvents';
const config = process.env

const populateQuery = [
    { path: 'resource', model: 'user' },
    { path: 'target', model: 'user' },
    { path: 'users', model: 'user' },
    { path: 'order', model: 'order' },
    { path: 'promocode', model: 'promocode' },

];

let create = async (resource, target, title, text, subject, subjectType, order,users) => {
    try {
        var query = { resource, target, text, title }
        if (subjectType == "PROMOCODE") query.promocode = subject;
        if (subjectType == "ORDER") query.order = subject;
        if (subjectType == "CHANGE_ORDER_STATUS") query.order = subject;

        if (subject && subjectType) {
            query.subjectType = subjectType;
            query.subject = subject;
        }
        if (order) {
            query.order = order;
        }

        var newNotification = await Notif.create(query);

        let counter = await Notif.count({ deleted: false, target: target, informed: { $ne: target } });
        await sendPushNotification(
            {
                targetUser: target,
                subjectType: subjectType,
                subjectId: subject,
                text: text,
                title: title,
                image: "https://res.cloudinary.com/adhamfayrouz/image/upload/v1632651920/categories/upload_d425dbeeada6c596d54aec0abf165af2_qqwrhs.png"
            });
        // notificationNSP.to('room-' + target).emit(socketEvents.NotificationsCount, { count: counter });
    } catch (error) {
        console.log(error.message)
    }
}

export default {

    async findMyNotification(req, res, next) {
        try {
            let user = req.user._id;
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = {
                deleted: false,
                $or: [{ target: user }, { users: { $elemMatch: { $eq: user } } }, { type: 'ALL' }],
                createdAt: { $gte: req.user.createdAt },
            };
            let { subjectType } = req.query
            if (subjectType) query = { subjectType: 'ADMIN', deleted: false, usersDeleted: { $ne: user } };

            var notifs = await Notif.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);

            notifs = Notif.schema.methods.toJSONLocalizedOnly(notifs, i18n.getLocale());
            const notifsCount = await Notif.count(query);
            const pageCount = Math.ceil(notifsCount / limit);
            if (!subjectType) {
                query = { $or: [{ target: user }, { users: user, type: 'USERS' }], informed: { $ne: user }, deleted: false, usersDeleted: { $ne: user } }

                await Notif.updateMany(query, { $addToSet: { informed: user } });
                var toRoom = 'room-' + user;
                notificationNSP.to(toRoom).emit(socketEvents.NotificationsCount, { count: 0 });
            }

            res.send(new ApiResponse(notifs, page, pageCount, limit, notifsCount, req));
        } catch (err) {
            next(err);
        }
    },

    async read(req, res, next) {
        try {
            let { notifId } = req.query;
            let notif = await checkExistThenGet(notifId, Notif);
            notif.read = true;
            await notif.save();
            res.send('notif read');
        } catch (error) {
            next(error);
        }
    },

    async unread(req, res, next) {
        try {
            let { notifId } = req.query;
            let notif = await checkExistThenGet(notifId, Notif);
            notif.read = false;
            await notif.save();
            res.send('notif unread');
        } catch (error) {
            next(error);
        }
    },

    async getCountNotification(id, admin = false) {
        try {
            var toRoom = 'room-' + id;
            var query = {
                $or:[{target: id},{users:id}],
                informed: { $ne: id },
                deleted: false,
                usersDeleted: { $ne: id }
            }
            var notifsCount = await Notif.count(query);
            if (!admin) {
                notificationNSP.to(toRoom).emit(socketEvents.NotificationsCount, { count: notifsCount });
            }
            else {
                adminNSP.to('room-admin').emit(socketEvents.NotificationsCount, { count: notifsCount });
            }
        } catch (err) {
            console.log(err.message);
        }
    },
    // NewNotification


    async pushNotification(targetUser, subjectType, subjectId, text, title) {
        try {
            var user = await checkExistThenGet(targetUser, User, { deleted: false });
            if (user.notification) {
                let data = { targetUser: user, subjectType: subjectType, subjectId: subjectId, text: text, title: title }

                sendPushNotification(data);
            } else {
                return true;
            }
        } catch (error) {
            console.log(error.message)
        }
    },

    validateAdminSendToAll() {
        let validations = [
            body('title.ar').not().isEmpty().withMessage('title is required'),
            body('title.en').not().isEmpty().withMessage('title is required'),
            body('text.ar').not().isEmpty().withMessage('text is required'),
            body('text.en').not().isEmpty().withMessage('text is required'),
            body('image.url').optional().not().isEmpty().withMessage("image url is required"),
            body('image.id').optional().not().isEmpty().withMessage("image id is required")

        ];
        return validations;
    },

    async adminSendToAllUsers(req, res, next) {
        try {
            let user = req.user;
            let validated = checkValidations(req);
            
            let notifiObj = { resource: req.user.id, type: "ALL", subjectType: "ADMIN", text: validated.text, title:validated.title };
            if(validated.image) notifiObj.image = validated.image
            await Notif.create(notifiObj)
            var allUsers = await User.find({ deleted: false,  notification:true });
            allUsers.forEach(async (user) => {
                await sendPushNotification(
                    {
                        targetUser: user,
                        subjectType: "ADMIN",
                        subjectId: 1,
                        text: validated.text[user.language],
                        title: validated.title[user.language],
                        image: (validated.image) ? validated.image.url : "https://res.cloudinary.com/adhamfayrouz/image/upload/v1632651920/categories/upload_d425dbeeada6c596d54aec0abf165af2_qqwrhs.png"
                    });
                // notificationNSP.to('room-'+user.id).emit(socketEvents.NotificationsCount, { count:await Notif.count({$or:[{target: user.id},{users:user.id}],informed: { $ne: user.id },deleted: false,usersDeleted: { $ne: user.id }}) });

            });
            res.status(200).send("Successfully send to all users");
        } catch (error) {
            next(error)
        }
    },

    validateAdminSendToSpecificUsers() {
        let validations = [
            body('title.ar').not().isEmpty().withMessage('title is required'),
            body('title.en').not().isEmpty().withMessage('title is required'),
            body('text.ar').not().isEmpty().withMessage('text is required'),
            body('text.en').not().isEmpty().withMessage('text is required'),
            body('users').not().isEmpty().withMessage('users is required').isArray().withMessage('must be array').custom(async (val, { req }) => {
                for (let index = 0; index < val.length; index++) {
                    await checkExist(val[index], User, { deleted: false });
                }
                return true;
            }),
            body('image.url').optional().not().isEmpty().withMessage("image url is required"),
            body('image.id').optional().not().isEmpty().withMessage("image id is required")
        ];
        return validations;
    },
    async adminSendToAllSpecificUsers(req, res, next) {
        try {
            let validated = checkValidations(req);
            let notifiObj = { resource: req.user.id, type: "SPECIFIC", subjectType: "ADMIN", text: validated.text, users: validated.users, title:validated.title };
            if(validated.image) notifiObj.image = validated.image
            await Notif.create(notifiObj)
            var allUsers = await User.find({ deleted: false, _id: { $in: validated.users }, notification:true });
            allUsers.forEach(async (user) => {
                sendPushNotification(
                    {
                        targetUser: user,
                        subjectType: "ADMIN",
                        subjectId: 1,
                        text: validated.text[req.locale],
                        title: validated.title[req.locale],
                        image: (validated.image) ? validated.image.url : "https://res.cloudinary.com/adhamfayrouz/image/upload/v1632651920/categories/upload_d425dbeeada6c596d54aec0abf165af2_qqwrhs.png"
                    });
                
                // notificationNSP.to('room-'+user.id).emit(socketEvents.NotificationsCount, { count:await Notif.count({$or:[{target: user.id},{users:user.id}],informed: { $ne: user.id },deleted: false,usersDeleted: { $ne: user.id }}) });

            });
            res.status(200).send("Successfully send to user");

        } catch (error) {
            next(error)

        }
    },
    create,
    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let { resource, admin, localized } = req.query;
            let query = { deleted: false, subjectType: "ADMIN", type: { $ne: null } };
            if (resource) query.resource = resource;
            var notifs = await Notif.find(query).populate(populateQuery)
                .sort({ _id: -1 })
                .limit(limit)
                .skip((page - 1) * limit);
            if (localized)
                notifs = Notif.schema.methods.toJSONLocalizedOnly(notifs, req.locale);
            const notifsCount = await Notif.count(query);
            const pageCount = Math.ceil(notifsCount / limit);
            res.send(new ApiResponse(notifs, page, pageCount, limit, notifsCount, req));
        } catch (err) {
            next(err);
        }
    },


    async delete(req, res, next) {
        try {
            let { notifId } = req.query;
            await Notif.deleteOne({_id:notifId})
            res.send('notif deleted');
        } catch (error) {
            next(error);
        }
    },
    async userDelete(req, res, next) {
        try {
            let { notifId } = req.query;
            let notif = await checkExistThenGet(notifId, Notif, { deleted: false });
            if(notif.image){
                await deleteFile(notif.image.id)
            }
            await Notif.findByIdAndUpdate(notifId, { $push: { usersDeleted: req.user.id } });
            res.send('notif deleted');
        } catch (error) {
            next(error);
        }
    },
    async findById(req, res, next) {
        try {
            console.log("findbyid")
            let { notifId, localized } = req.query;
            let notifi = await checkExistThenGet(+notifId, Notif, { deleted: false, populate: populateQuery });
            if (localized) {
                notifi = Notif.schema.methods.toJSONLocalizedOnly(notifi, req.locale);
            }
            res.status(200).send(notifi)

        } catch (err) {
            next(err)
        }
    }
}