import Advertisements                    from '../../models/advertisements.model/advertisements.model'
import Category                          from '../../models/category.model/category.model'
import Product                           from '../../models/product.model/product.model'
import ApiResponse                       from "../../helpers/ApiResponse"
import { checkExistThenGet, checkExist } from "../../helpers/CheckMethods"
import { body }                          from 'express-validator'
import { checkValidations, createDate }  from "../shared.controller/shared.controller"
import i18n                              from 'i18n'
import ApiError                          from '../../helpers/ApiError'


const populateQuery = [
    { path: 'product', model: 'product' },
    // { path: 'category', model: 'category' }
];

export default {

    async findAll(req, res, next) {
        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;

            let { type, numberOfSlots, localized, product } = req.query
            let query = { deleted: false }

            let date = createDate(req)
            if(date) query.createdAt = {$gte: date.startOfDate, $lte: date.endOfDate}

            if (numberOfSlots) {
                query.numberOfSlots = numberOfSlots
            }
            if (type) {
                query.type = type
            }
            if (product) query.product = product
            let advertisment = await Advertisements.find(query).sort({ updatedAt: -1 }).limit(limit).skip((page - 1) * limit).populate(populateQuery);
            
            if(localized)
                advertisment = Advertisements.schema.methods.toJSONLocalizedOnly(advertisment, req.locale);
            
            const advertismentCount = await Advertisements.count(query);
            const pageCount = Math.ceil(advertismentCount / limit);
            res.status(200).send(new ApiResponse(advertisment, page, pageCount, limit, advertismentCount, req ));
        } catch (err) {
            next(err);
        }
    },

    validateBody(update = false) {
        let validations = !update ? [
            body('numberOfSlots').not().isEmpty().withMessage(() => { return i18n.__('numberOfSlotsRequired') })
                .isInt({ max: 4, min: 1 }).withMessage(() => { return i18n.__('invalidSlot') }),
            body('type').not().isEmpty().withMessage(() => { return i18n.__('typeRequired') })
                .isIn(['HOME_PAGE', 'PRODUCT_PAGE']).withMessage(() => { return i18n.__('invalidType') }),
            body('homeAddsAfetr').optional().not().isEmpty().withMessage(() => { return i18n.__('homeAddsAfetrRequired') })
                .isIn(['PRODUCT', 'CATEGORY']).withMessage(() => { return i18n.__('invalidType') }),
            body('image.url').not().isEmpty().withMessage('image url is required'),
            body('image.id').not().isEmpty().withMessage('image id is required'),
            body('product').optional().not().isEmpty().withMessage('product is required').custom(async(val)=>{
                await checkExist(val,Product,{deleted:false})
                return true
            }),
        ] :
        [
            body('numberOfSlots').optional().not().isEmpty().withMessage(() => { return i18n.__('numberOfSlotsRequired') })
                .isInt({ max: 4, min: 1 }).withMessage(() => { return i18n.__('invalidSlot') }),
            body('type').optional().not().isEmpty().withMessage(() => { return i18n.__('typeRequired') })
                .isIn(['HOME_PAGE', 'PRODUCT_PAGE']).withMessage(() => { return i18n.__('invalidType') }),
            body('homeAddsAfetr').optional().not().isEmpty().withMessage(() => { return i18n.__('homeAddsAfetrRequired') })
                .isIn(['PRODUCT', 'CATEGORY']).withMessage(() => { return i18n.__('invalidType') }),
            body('image.url').optional().not().isEmpty().withMessage('image url is required'),
            body('image.id').optional().not().isEmpty().withMessage('image id is required'),
            body('product').optional().not().isEmpty().withMessage('product is required').custom(async(val)=>{
                await checkExist(val,Product,{deleted:false})
                return true
            }),
        ];

        return validations;
    },

    async create(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let advertisment = await Advertisements.create(validatedBody);
            advertisment = await Advertisements.populate(advertisment,populateQuery)
            res.status(200).send(advertisment);
        } catch (error) {
            next(error)
        }
    },

    async update(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let { AdvertisementId } = req.query;
            await checkExist(AdvertisementId, Advertisements, { deleted: false });
            
            let advertisment = await Advertisements.findByIdAndUpdate(AdvertisementId, validatedBody, { new: true });
            advertisment = await Advertisements.populate(advertisment,populateQuery)
            
            res.status(200).send(advertisment);
        } catch (error) {
            next(error)
        }
    },

    async findById(req, res, next) {
        try {
            let { AdvertisementId } = req.query;
            let { localized } = req.query
            let query = {deleted:false, populate:populateQuery}

            let advertisment = await checkExistThenGet(AdvertisementId, Advertisements, query);

            if(localized)
                advertisment = Advertisements.schema.methods.toJSONLocalizedOnly(advertisment, req.locale);

            res.status(200).send(advertisment);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let { AdvertisementId } = req.query;
            await Advertisements.deleteOne({_id:AdvertisementId})
            res.status(200).send("Deleted Successfully");
        }
        catch (err) {
            next(err);
        }
    }
}

