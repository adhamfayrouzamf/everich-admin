import Message                           from "../../models/message.model/message.model"
import { checkExistThenGet, checkExist } from "../../helpers/CheckMethods"
import User                              from '../../models/user.model/user.model'
import { body }                          from "express-validator"
import { checkValidations, createDate }  from '../shared.controller/shared.controller'
import ApiError                          from "../../helpers/ApiError"
import ApiResponse                       from "../../helpers/ApiResponse"
// const config = require('../../config')
// const notificationController = require('../notif.controller/notif.controller')
// const SocketEvents = require('../../socketEvents')

let populateQuery = [{ path: 'sender', model: 'user' }, { path: 'reciever.user', model: 'user' }]

let countUnseen = async (id)=>{
    try {
        let query = {
            deleted: false,
            'reciever.user': id ,
            'reciever.read': false 
        };
        const chatCount = await Message.count(query);
        chatNSP.to('room-' + id).emit(SocketEvents.NewMessageCount, { chatCount: chatCount });
    } catch (error) {
        throw error ;
    }
}

let countUnseenForAdmin = async ()=>{
    try {
        let query = {
            deleted: false,
            'reciever.user': null,
            'reciever.read': false,
            lastMessage: true
        };
        const chatCount = await Message.count(query);
        chatNSP.to('room-admin').emit(SocketEvents.NewMessageCount, {count:chatCount });
    } catch (error) {
        throw error ;
    }
}
// let handelNewMessageSocket = async (message) => {
//     try {
//         if (message.reciever && message.reciever.user && chatNSP.adapter.rooms['room-' + message.reciever.user.id]) {
//             await countUnseen(message.reciever.user.id)
//             chatNSP.to('room-' + message.reciever.user.id).emit(SocketEvents.NewMessage, { message: message });
//             if (message.reciever.user.activeChatHead == false) {
//                 let text = (message.message.text) ? message.message.text : ' رسالة جديدة ';
//                 if (message.reciever.user.language == 'ar') {
//                     await notificationController.pushNotification(message.reciever.user.id, 'MESSAGE', message.sender.id, text, config.notificationTitle.ar)
//                 } else {
//                     await notificationController.pushNotification(message.reciever.user.id, 'MESSAGE', message.sender.id, text, config.notificationTitle.en)
//                 }
//             }
//         } else if(message.reciever && message.reciever.user && !chatNSP.adapter.rooms['room-' + message.reciever.user.id]) {
//             await countUnseen(message.reciever.user.id)
//             let text = (message.message.text) ? message.message.text :' رسالة جديدة ';
//             if (message.reciever.user.language == 'ar') {
//                 await notificationController.pushNotification(message.reciever.user.id, 'MESSAGE', message.sender.id, text, config.notificationTitle.ar)
//             } else {
//                 await notificationController.pushNotification(message.reciever.user.id, 'MESSAGE', message.sender.id, text, config.notificationTitle.en)
//             }
//         }else if(!message.reciever.user){
//             chatNSP.to('room-admin').emit(SocketEvents.NewMessage, { message: message });
//             await countUnseenForAdmin();
//         }
//     } catch (error) {
//         console.log(error);
//     }
// }

let updateSeen = async (user)=>{
    try {
        await Message.updateMany({ deleted: false, 'reciever.user': user,'reciever.read':false }, { $set: { 'reciever.read': true, 'reciever.readDate': new Date() } })
        await countUnseen(user);
    } catch (error) {
        throw error ;
    }
}



export default {

    

    validate() {
        let validation = [
            body('text').optional().not().isEmpty().withMessage('message is required'),
            body('reciever').optional().not().isEmpty().withMessage('reciever is required'),
            body('chatTicket').not().isEmpty().withMessage('chat Id is required'),
            body('file').optional().not().isEmpty().withMessage('file required')
        ]
        return validation;
    },

    async create(req, res, next) {
        try {
            let user = req.user;
            let data = checkValidations(req);
            let message = { sender: user.id, message: {},chatTicket:data.chatTicket, reciever:{} };
            // if (!(data.text || req.file)) {
            //     return next(new ApiError(404, i18n.__('messageRequired')))
            // }

            if (data.text) {
                message.message.text = data.text;
            }
            if(data.file){
                switch(data.file.type){
                    case 'audio':
                        message.message.audio = data.file
                        break
                    case 'image':
                        message.message.image = data.file
                        break
                    case 'video':
                        message.message.video = data.file
                        break
                    case 'raw':
                        message.message.document = data.file
                        break
                }
            }
            if(data.reciever){
                message.reciever.user = data.reciever
            }
            // if (req.file) {
            //     let file = handleImg(req, { attributeName: 'file' });
            //     if (req.file.mimetype.includes('image/')) {
            //         message.message.image = file;
            //     } else if (req.file.mimetype.includes('video/')) {
            //         message.message.video = file;
            //     } else if (req.file.mimetype.includes('application/')) {
            //         message.message.document = file;
            //     } else {
            //         return next(new ApiError(404, i18n.__('fileTypeError')));
            //     }
            // }
            

            message.lastMessage = true;
            let createdMessage = await Message.create(message);
            createdMessage = await Message.populate(createdMessage, populateQuery);
            res.status(200).send(createdMessage);
            if (user.type == 'CLIENT' ) {
                await Message.updateMany({deleted:false , _id:{$ne:createdMessage.id} ,$or:[{sender:user.id},{'reciever.user':user.id}]},{$set:{lastMessage:false}});
            }else{
                await Message.updateMany({deleted:false , _id:{$ne:createdMessage.id} ,$or:[{sender:data.reciever},{'reciever.user':data.reciever}]},{$set:{lastMessage:false}});
            }
            
            chatNSP.to('room-'+data.chatTicket).emit('sendMessage', createdMessage);

        } catch (error) {
            next(error)
        }
    },
    
    async getById(req, res, next) {
        try {
            const {messageId} = req.query;
            let message = await checkExistThenGet(id, Message, { deleted: false });
            res.status(200).send(message);
        } catch (error) {
            next(error)
        }
    },

    async deleteForEveryOne(req, res, next) {
        try {
            const {messageId} = req.query;
            let message = await checkExistThenGet(id, Message, { deleted: false });
            message.deleted = true;
            await message.save();
            res.status(200).send('Deleted');
        } catch (error) {
            next(error)
        }
    },

    async getChatForUser(req, res, next) {
        try {
            if (req.user.type != 'ADMIN' && req.user.type != 'SUB_ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let page = +req.query.page || 1,
                limit = +req.query.limit || 20;
            let { user } = req.query;
            let query = {
                deleted: false,
                $or: [{ sender: user },
                { 'reciever.user': user }
                ]
            };
            await Message.updateMany({ deleted: false, sender: user,'reciever.read':false }, { $set: { 'reciever.read': true, 'reciever.readDate': new Date() } })
            var chats = await Message.find(query).populate(populateQuery).sort({ _id: -1 }).limit(limit).skip((page - 1) * limit)
            const chatCount = await Message.count(query);
            const pageCount = Math.ceil(chatCount / limit);
            res.send(new ApiResponse(chats, page, pageCount, limit, chatCount, req));
            await countUnseenForAdmin();
        } catch (error) {
            next(error)
        }
    },

    async getMyChat(req, res, next) {
        try {
            
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let user = req.user._id;
            const {chatId} = req.query
            let query = {
                deleted: false,
                $or: [{ sender: user },
                { 'reciever.user': user}
                ]
            };
            if(chatId)  query.chatTicket = chatId
            await Message.updateMany({ deleted: false, 'reciever.user': user ,'reciever.read':false}, { $set: { 'reciever.read': true, 'reciever.readDate': new Date() } })
            var chats = await Message.find(query).sort({ updatedAt: -1 }).limit(limit).skip((page - 1) * limit).populate(populateQuery)
            console.log('chats',chats)
            const chatCount = await Message.count(query);
            const pageCount = Math.ceil(chatCount / limit);
            
            res.send(new ApiResponse(chats, page, pageCount, limit, chatCount, req));

        } catch (error) {
            next(error)
        }
    },

    async getLastContactsForAdminSupport(req, res, next){
        try {
            
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false ,lastMessage:true};
            let messages = await Message.find(query).populate(populateQuery).sort({ updatedAt: -1 }).limit(limit).skip((page - 1) * limit);
            let messagesCount = await Message.count(query);
            const pageCount = Math.ceil(messagesCount / limit);
            res.send(new ApiResponse(messages, page, pageCount, limit, messagesCount, req));
            //adminNSP.emit(SocketEvents.NewMessageCount, {count:0 });
        } catch (error) {
            next(error);
        }
    },

    countUnseen,
    updateSeen,
    countUnseenForAdmin
}