const {authPaths, privatePaths} = require("./routes")
module.exports = {
  reactStrictMode: true,
  serverRuntimeConfig: {
    PROJECT_ROOT: __dirname
  },
  i18n:{
    locales:["ar","en"],
    defaultLocale:"ar"
  },
  images: {
    domains: ['res.cloudinary.com'],
  },
  env:{
    authPaths,
    privatePaths,
    MONGO_URI:'mongodb+srv://madloldb:madloldb@madlol.fmhuj.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
    LOCALES:["ar","en"],
    jwtSecret : 'Madlol',
    twilio:{
      AccountSID : 'ACa1e6b887f9ec81651cee3ee32f8ae971',
      AuthToken : '872cfa04954c17ec58485d8c852e9cb9',
      serviceSID:'VAadbf120f2d887b5641684dd63c4d4bf4'
    },
    sendGrid:{
      api_key:"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",
      templatesId:{
        verify:'d-a169e47849bf444b979e17e3d7d911ec',
        mailer:'d-b35a26000dcd4d9e9efe7d7826982209'
      }
    },
    cloudinaryConfig:{
      cloud_name: 'adhamfayrouz', 
      api_key: '142467767387469', 
      api_secret: '-7lZe-UrWdH-qqo7PyK5MSou0XE'
    },
    cloudinaryDirs:{
      product:'products',
      category:'categories',
      message:'messages',
      ads:'ads',
      notif:'notifications',
      user:"users"
    },
    pusher:{
      appId: "1267291",
      key: "3147d4a5ff77fea57db4",
      secret: "df8e01ce2b627b3c37b3",
      cluster: "eu",
      useTLS: true
    },
    webPush:{
      privateKey:"6qWY91h3MsaNiVygkP9kvenUkCAmAfzs2DU5hwh-ZX4",
      publicKey:"BFE8AbK8szY0Z8ppW2_coJcg7yCmQHZjph77YnVcbS0YZfvW-wjI-GYN5nK3uXGL8NTtjSeGhQnzotqlGD7wyxc"
    },
    firebase:{
      serverKey:"AAAAt9vkQiw:APA91bHQPlZYMYFqEygOll9A0MdaX4UdJK3VrGq1mNTeEYYkfkEpgxKGn_hKmCHtjcoCGNgPPGlRPzjN3_YUwVJeHWs4mMvm2FvH0eFPMLzMt1b0B4tgzw7dzAH9paxEq7sdo2teptjD"
    }
  },
  async redirects(){
    return [
      {
        source:"/",
        destination:"/dashboard",
        permanent: true,
      }
    ]
  }
}
