// import next, {NextApiRequest, NextApiResponse} from 'next'
// import i18n from 'i18n'
import nc from 'next-connect'
import mongoose from 'mongoose'
import cors from 'cors'
import errors from "../locales/errors"


const handler = ()=>{
    
    const handle = nc({
        onError:(err,req,res)=>{
            console.error(err)
            res.status(err.status || 500);
            res.json(err)
        },
        onNoMatch:(req,res)=>{
            console.log(404)
            res.status(404)
            res.json({message:"404 page not found"})
        },
    })
    handle.use(cors({
        origin:"*"
    }))
    // handle.use((req,res,next)=>{
    //     res.setHeader('Access-Control-Allow-Origin', '*')
    //     return next()
    // })
    handle.use(async(req,res,next)=>{
        if(mongoose.connections[0].readyState){
            console.log('already connected')
            return next()
        }
        console.log('connecting to db')
        await mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true })
        return next()
    })
    
    handle.use((req,res,next)=>{
        console.log('language detection')
        req.locale = req.headers['acceptlanguage'] || "ar"
        errors.setLanguage(req.headers['acceptlanguage'] || "ar")
        return next()
    })
    return handle
}

export default handler