// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getMessaging, getToken, onMessage } from "firebase/messaging"
// import firebase from "firebase"
// import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyD-YiOrosbZO896fqceMCLU67fp4udLTEI",
    authDomain: "push-notifications-62db2.firebaseapp.com",
    projectId: "push-notifications-62db2",
    storageBucket: "push-notifications-62db2.appspot.com",
    messagingSenderId: "613242954137",
    appId: "1:613242954137:web:33ba7f57522867c841f308",
    measurementId: "G-BYN0D5XYKQ"
};
const vapidKey = "BPWdCFA4WGEL-BHH0FTzSfnkGs4-lNHjeA9OFVshtdjdAJowM4vLxzFaozD35XIfDA2VfmTtIbuuxzQJ6j_WHgQ"

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const getMyToken = async()=>{
    const messaging = getMessaging(app)
    const token = await getToken(messaging,{vapidKey})
    return token
}
export const onMessageListener = ()=>{
    const messaging = getMessaging(app)
    return new Promise(resolve=>{
        onMessage(messaging,payload=>{
            resolve(payload)
        })
    })
}
// export const messaging = getMessaging(app)
// export const getMyToken = async()=>{
//     const messaging = getMessaging(app)
//     const onRecieve = onMessage(messaging,payload=>{
//         console.log(payload)
//     })
//     const token = await getToken(messaging,{vapidKey:"BPWdCFA4WGEL-BHH0FTzSfnkGs4-lNHjeA9OFVshtdjdAJowM4vLxzFaozD35XIfDA2VfmTtIbuuxzQJ6j_WHgQ"})
//     return token
// }

// const analytics = getAnalytics(app);