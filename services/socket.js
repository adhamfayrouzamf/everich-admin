import axios from "axios"
class Socket {

    constructor(ns = null){
        this.appId="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2MzIyNDUxNTAsImV4cCI6MTYzMjI0NTE1MX0.CsaE8pyXmBlkFZi7bx0xJFvecj2ubO72-cM4HEvolaY"
        this.ns = ns
    }

    async trigger(event,data,to){
        let nsRoute = this.ns ? `${this.ns}/` : ""
        await axios.post(`https://ots-sockets.herokuapp.com/${nsRoute}${event}`,{data:data,to:to || null},{headers:{appid:this.appId}})
    }
}

export default Socket