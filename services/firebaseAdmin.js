import admin from "firebase-admin"
const serviceAccount = require("../push-service.json");

try{
    admin.initializeApp({
        credential: admin.credential.cert(serviceAccount)
    });

}catch(err){
    console.log("already initialized")
}

const options = {
    priority: "high",
    timeToLive: 1,
    visibility: "public",
    importance: "high",
}

export const sendNotif = async(token)=>{
    try{
        const payload = {
            notification:{
                title:"hello from server",
                body:"welcome from my server",
                // image:"https://res.cloudinary.com/adhamfayrouz/image/upload/v1632625367/categories/back_kc5r9y.png",
                // "click_action":"/dashboard",
                // icon:"fcm_push_icon",
            }/* ,
            data:{
                message:"testing",
                title:"hello from server",
                body:"welcome from my server",
            } */
        }
        admin.messaging().sendToDevice(token,payload).then(res=>{
            console.log("sent")
            console.log(res)
        }).catch(err=>console.error(err))
    }catch(err){
        console.error(err)
    }
}