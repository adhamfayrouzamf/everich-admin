import passport     from 'passport'
import passportJwt  from 'passport-jwt'
import User         from '../models/user.model/user.model'
// const passportLocal = require('passport-local');
// var FacebookStrategy = require('passport-facebook').Strategy;
const JwtStrategy = passportJwt.Strategy;
// const LocalStrategy = passportLocal.Strategy;
const { ExtractJwt } = passportJwt;
const jwtSecret = process.env.jwtSecret;


passport.use("clientAuth",new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: jwtSecret
}, (payload, done) => {
    User.findOne({_id: payload.sub, deleted : false,activated: true })
    .then(user => {

        if (!user)
            return done(null, false);

        return done(null, user)
    }).catch(err => {
        console.log('Passport Error: ', err);
        return done(null, false);
    })
}
));

passport.use("adminAuth", new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: jwtSecret
},(payload, done)=>{
    User.findOne({_id: payload.sub, deleted : false,activated: true, $or:[{type:"ADMIN"},{type:"SUB_ADMIN"}] })
    .then(user => {

        if (!user)
            return done(null, false);

        return done(null, user)
    }).catch(err => {
        console.log('Passport Error: ', err);
        return done(null, false);
    })
}))
// passport.use(new LocalStrategy({
//     usernameField: 'email'
// }, (email, password, done) => {

//     User.findOne({ email,deleted:false }).then(user => {
//         if (!user)
//             return done(null, false);

//         // Compare Passwords 
//         user.isValidPassword(password, function (err, isMatch) {
//             if (err) return done(err);
//             if (!isMatch) return done(null, false, { error: 'Invalid Credentials' });

//             return done(null, user);
//         })

//     });
// }));

// passport.use(new FacebookStrategy({
//     clientID: '2030001723966035', //FACEBOOK_APP_ID,
//     clientSecret:'46ce3cad21a27bf4be3b95c4c85aedab', //FACEBOOK_APP_SECRET,
//     callbackURL: "https://madlol-back.herokuapp.com/auth/facebook/callback",// when the auth is ok route to this 
//     profileFields: ['id', 'picture', 'email' , 'firstname','lastname']
//   },
//   function(accessToken, refreshToken, profile, cb) {
    
//     var user = {
//         email : profile.email,
//         picture: profile.picture,
//         firstname : profile.firstname ,
//         lastname:profile.lastname
//     }
//     return cb(err, user);
//   }
// ));

export const requireAuth = passport.authenticate('clientAuth', { session: false });
export const requireAdmin = passport.authenticate('adminAuth', { session: false });
// export const requireSignIn = passport.authenticate('local', { session: false });
// export const facebookSignUp = passport.authenticate('facebook');
// export const facebookSignUp2 =  passport.authenticate('facebook',{failureRedirect:'/facebookerror'});
// export default { requireAuth, requireSignIn , facebookSignUp , facebookSignUp2 };