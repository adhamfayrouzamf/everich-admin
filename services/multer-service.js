import multer    from 'multer'
import ApiError  from '../helpers/ApiError'
import fs from "fs"

export const multerSaveTo = (folderName)=> {

    const location = __dirname + '/multer'
    if(!fs.existsSync(location))
        fs.mkdirSync(location,{recursive:true})

    const storage = multer.diskStorage({

        destination:(req,file,callback)=>{
            callback(null,location)
        },
        filename:(req,file,callback)=>{
            callback(null,file.originalname)
        },
    })
    
    return multer({
        limits:{fileSize:1024*1024*50},
        // storage:storage,
        // fileFilter:(req,file,callback)=>{
        //     if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg" || file.mimetype == "image/svg+xml") {
        //         callback(null, true);
        //     } else {
        //         callback(null, false);
        //         return callback(new Error('Only .png, .jpg and .jpeg format allowed!'));
        //     }
        // }
    })
}


