import formidable from "formidable-serverless"
import fs from "fs"
import path from "path"
export const fileUploader = async(req,res,next)=>{
    // const location = __dirname + '/multer'
    // if(!fs.existsSync(location))
    //     fs.mkdirSync(location,{recursive:true})
    const form = new formidable.IncomingForm({multiples:true})
    form.parse( req, (err, fields, files)=>{
        // req.body = fields
        if(err)
            throw err
        req.body = {...fields}
        req.files = files
        if(files && files.files){
            if(files.files.length)
                req.files.files = files.files
            else
                req.files.files = [files.files]
        }
        next();
    })
}