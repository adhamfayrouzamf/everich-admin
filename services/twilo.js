import {generateToken}  from '../utils/token'
import ApiError         from '../helpers/ApiError'
import i18n             from 'i18n'
import twilio           from 'twilio'
import sgMail           from '@sendgrid/mail'
// const reportController = require('../controllers/report.controller/report.controller')
// const ConfirmationCode = require('../models/confirmationsCodes.model/confirmationscodes.model')

const accountSid = process.env.twilio.AccountSID;
const authToken  = process.env.twilio.AuthToken;
const verifyServiceId = process.env.twilio.serviceSID;
const client = twilio(accountSid, authToken);
sgMail.setApiKey(process.env.sendGrid.api_key)


export let twilioSend = (number, ar = 'ar') => {
    try {
        client.verify.services(verifyServiceId)
            .verifications
            .create({ to: number, channel: 'sms', locale: ar })
            .then(verification => {
                console.log('Twilio verification Sent');
            }).catch(error => console.log(error));
    } catch (error) {
        console.log('error in twilio ==> ', error)
    }
}
export let twilioMail = async (email,subject,subtitle,msg)=>{
    try{
        client.verify.services(verifyServiceId).verifications.create({
            to:email,
            channel:'email',
            channelConfiguration:{
                substitutions:{
                    subject:subject || 'Souk Akfa (Email Verification)',
                    title:'Souk Akfa',
                    subtitle: subtitle || 'Email Verification',
                    msg:msg || 'Your verification code is :',
                }
            }
        }).then(verification => console.log(verification.sid)).catch(err=>{
            console.log(err)
        })
    }catch(err){
        console.log(err)
    }
}
export let twilioVerify = async(email, code) => {
    try {
        let verify = await client.verify.services(verifyServiceId)
            .verificationChecks
            .create({ to: email, code: code })
        return verify.valid
    } catch (error) {
        console.log(error)
        return false
    }
}

export let sendMail = (email,title, subtitle, msg)=>{
    console.log(email)
    sgMail.send({
        to:email,
        from:'adham20191700087@cis.asu.edu.eg',
        templateId: process.env.sendGrid.templatesId.mailer,
        dynamic_template_data:{
            subject: 'Everich',
            title: title || 'Everich',
            subtitle: subtitle || 'Email Verification',
            msg: msg || 'please verify your account',
        }
    }).then(() => {
        console.log('Email sent')

    }).catch((error) => {
        console.log(error)
    })
}
