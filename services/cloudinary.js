const  cloudinary   = require('cloudinary').v2
import  fs          from 'fs'
// const path = require('path')
const cloudinaryConfig = process.env.cloudinaryConfig
const cloudinaryDirs = process.env.cloudinaryDirs
cloudinary.config(cloudinaryConfig)


export async function uploadFile (image,dir){
    try{
        if(!cloudinaryDirs[dir]){
            throw new Error('this dir doesn\'t exist')
        }
        
        const res = await cloudinary.uploader.upload(image.path,{
            upload_preset:'ml_default',
            folder:cloudinaryDirs[dir],
            "resource_type":'auto',
            "color_space":"no_cmyk"
        })
        console.log('uploaded file',res)
        if(res){
            fs.unlink(image.path,()=>{console.log('image uploaded and removed from backend')})
        }
        return {url:res.url,id:res.public_id,ext:res.format,type:res.is_audio ? 'audio' : res.resource_type,size:res.bytes,name:res.original_filename}
    }catch(err){
        console.log({...err})
        fs.unlink(image.path,()=>{console.log('image failed to upload and  is removed from backend')})
    }
    
}
export async function deleteFile(id){
    try{
        console.log(id)
        const res = await cloudinary.uploader.destroy(id)
        return res
    }catch(err){
        console.log(err.message)
    }
}

export async function uploadManyImgs(images,dir){
    try{
        if(!cloudinaryDirs[dir]){
            throw new Error('this dir doesn\'t exist')
        }
        let imagesRes = []
        for(i in images ){
            let res = await cloudinary.uploader.upload(images[i].path,{
                upload_preset:'ml_default',
                folder:cloudinaryDirs[dir]
            })
            if(res){
                fs.unlink(images[i].path,()=>{console.log('image uploaded and removed from backend')})
            }
            imagesRes.push({url:res.url,id:res.public_id,ext:res.format,type:res.resource_type,size:res.bytes,name:res.original_filename})
        }
        return imagesRes;
    }catch(err){
        console.log(err.message)
    }
}

