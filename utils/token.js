import jwt from 'jsonwebtoken'


const jwtSecret = process.env.jwtSecret;

const generateToken = id => {
    return jwt.sign({
        sub: id,
        iss: 'App',
        iat: new Date().getTime()
    }, jwtSecret, { expiresIn: '30d' });
};

export default generateToken