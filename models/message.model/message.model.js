var mongoose = require('mongoose');
var mongoose_auto_increment = require('mongoose-auto-increment');
var Schema = mongoose.Schema;

var message = {
    _id: {
        type: Number,
        required: true
    },
    sender: {
        type: Number,
        ref: 'user'
    },
    reciever: { 
        user: { type: Number, ref: 'user' },
        delivered: { type: Boolean, default: false },
        deliverDate: { type: Date },
        read: { type: Boolean, default: false },
        readDate: { type: Date },
        deleted:{ type: Boolean, default: false }
    },
    message: {
        text: { type: String },
        image: { type: {} },
        video: { type: {} },
        document: { type: {} },
        audio: { type: {} },
        location: { lat: { type: String }, long: { type: String } }
    },
    // playedBy: [Number],
    lastMessage:{
        type:Boolean,
        default:true
    },
    //////////////////////////////////////////////////////////////
    chatTicket: {
        type: Number,
        ref: 'chatTicket'
    },
    activeChatHead: {
        type: Number,
        ref: 'user'
    },
    deleted: {
        type: Boolean,
        default: 0
    },
}

var messageSchema = new Schema(message, { timestamps: true });
messageSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
mongoose_auto_increment.initialize(mongoose.connection);
messageSchema.plugin(mongoose_auto_increment.plugin, { model: 'message', startAt: 1 });


export default mongoose.models.message || mongoose.model('message', messageSchema)