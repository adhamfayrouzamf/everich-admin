const mongoose = require("mongoose");
const { Schema } = mongoose
const autoIncrement = require('mongoose-auto-increment');
const mongooseI18n = require('mongoose-i18n-localize')
const config = process.env

const advertisementsSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    image:{
        type:{
            url:String,
            id:String
        }
    },
    numberOfSlots:{
        type:Number,
        max:4,
        min:1,
        default:2
    },
    type:{
        type:String,
        enum:['HOME_PAGE','PRODUCT_PAGE'],
        default:'HOME_PAGE'
    },
    // homeAddsAftr:{
    //     type:String,
    //     enum:['PRODUCT','CATEGORY'],
    //     default:'PRODUCT'
    // },
    product:{
        type: Number,
        ref: 'product'
    },
    deleted: {
        type: Boolean,
        default: false
    },
}, { timestamps: true });

advertisementsSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
advertisementsSchema.plugin(mongooseI18n,{locales:config.LOCALES})
advertisementsSchema.plugin(autoIncrement.plugin, { model: 'advertisement', startAt: 1 });
export default mongoose.models.advertisement || mongoose.model('advertisement', advertisementsSchema);