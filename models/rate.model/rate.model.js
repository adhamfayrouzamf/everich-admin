const mongoose =require( "mongoose");
const { Schema } = mongoose
const autoIncrement =require( 'mongoose-auto-increment');
const mongooseI18n = require('mongoose-i18n-localize')
const config = process.env
const rateSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    user: {
        type: Number,
        required: true,
        ref: 'user'
    },
    product: {
        type: Number,
        required: true,
        ref: 'product'
    },
    rate: {
        type: Number,
        required: true
    },
    deleted:{
        type: Boolean,
        default: false
    }

}, { timestamps: true });

rateSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
rateSchema.plugin(mongooseI18n,{locales:config.LOCALES})
rateSchema.plugin(autoIncrement.plugin, { model: 'rate', startAt: 1 });

export default mongoose.models.rate || mongoose.model('rate', rateSchema);