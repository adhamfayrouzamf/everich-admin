import mongoose, { Schema } from "mongoose"
import autoIncrement        from 'mongoose-auto-increment'
import mongooseI18n         from 'mongoose-i18n-localize'
const config = process.env

const shippingCompanySchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    name:{
        type:String,
        i18n:true
    },
    shippingPrice:{
        type:Number
    },
    deleted:{
        type: Boolean,
        default: false
    }

}, { timestamps: true });

shippingCompanySchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
shippingCompanySchema.plugin(mongooseI18n,{locales:config.LOCALES})
shippingCompanySchema.plugin(autoIncrement.plugin, { model: 'Shipping', startAt: 1 });

export default mongoose.models.Shipping || mongoose.model('Shipping', shippingCompanySchema);