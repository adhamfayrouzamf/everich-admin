const mongoose =require( "mongoose");
const { Schema } = mongoose
const autoIncrement =require( 'mongoose-auto-increment');
const mongooseI18n = require('mongoose-i18n-localize')
const config = process.env
const RegionSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    name:{
        type:String,
        // required:true,
        i18n: true
    },
    city:{
        type: Number,
        // required: true,
        ref:'city'
    },
    // transportPrice:{
    //     type:Number,
    //     default:150
    //     // required:true,
    // },
    deleted:{
        type:Boolean,
        default:false
    }

}, { timestamps: true });

RegionSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});

autoIncrement.initialize(mongoose.connection);
RegionSchema.plugin(mongooseI18n,{locales:config.LOCALES})
RegionSchema.plugin(autoIncrement.plugin, { model: 'region', startAt: 1 });

export default mongoose.models.region || mongoose.model('region', RegionSchema);