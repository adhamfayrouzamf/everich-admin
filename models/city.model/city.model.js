const mongoose =require( "mongoose");
const { Schema } = mongoose
const autoIncrement =require( 'mongoose-auto-increment');
const mongooseI18n = require('mongoose-i18n-localize')
const config = process.env
const CitySchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    name:{
        type:String,
        // required:true,
        i18n: true
    },
    country:{
        type: Number,
        required: true,
        ref:'country'
    },
    deleted:{
        type:Boolean,
        default:false
    }

}, { timestamps: true });

CitySchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
CitySchema.plugin(mongooseI18n,{locales:config.LOCALES})
CitySchema.plugin(autoIncrement.plugin, { model: 'city', startAt: 1 });

export default  mongoose.models.city || mongoose.model('city', CitySchema);