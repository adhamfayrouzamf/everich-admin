var mongoose = require('mongoose');
var mongoose_auto_increment = require('mongoose-auto-increment');
var Schema = mongoose.Schema;
const mongooseI18n = require('mongoose-i18n-localize')
const config = process.env
var chat = {
    _id: {
        type: Number,
        required: true
    },
    client: {
        type: Number,
        ref: 'user'
    },
    admin: { 
        type: Number,
        ref: 'user'
    },
    //////////////////////////////////////////////////////////////
    status: {
        type: String,
        enum:['WAITING','ACTIVE','CLOSED'],
        default:'WAITING',
    },
    notes:{
        type:String
    },
    reason:{
        type:String
    },
    deleted: {
        type: Boolean,
        default: 0
    },
}

var chatSchema = new Schema(chat, { timestamps: true });
chatSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
mongoose_auto_increment.initialize(mongoose.connection);
chatSchema.plugin(mongooseI18n,{locales:config.LOCALES})
chatSchema.plugin(mongoose_auto_increment.plugin, { model: 'chatTicket', startAt: 1 });


export default mongoose.models.chatTicket || mongoose.model('chatTicket', chatSchema);;