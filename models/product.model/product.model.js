import mongoose, { Schema } from "mongoose"
import autoIncrement        from 'mongoose-auto-increment'
import mongooseI18n         from 'mongoose-i18n-localize'
// const config = process.env
// let rateSchema = new Schema({
//     user: { type: Number, ref: 'user', required: true },
//     rate: { type: Number, required: true }
// });

// let colorSchema = new Schema({
//     color: { type: Number, ref: 'color' },
//     images: { type: [String], required: true },
//     sizes: {
//         type: [
//             {
//                 size: { type: Number, required: true },
//                 quantity:{type: Number,required: true}
//             }
//         ]
//     },
// })

const productSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        // required: true,
        i18n: true
    },
    // unit: {
    //     type: String,
    //     // required: true,
    //     // i18n: true
    // },
    desc: {
        type: String,
        // required: true,
        i18n: true
    },
    trademark: {
        type: Number,
        ref:'tradeMark'
    },
    category: {
        type: Number,
        ref:'category'
        // required:true
    },
    subCategory:{
        type: Number,
        ref:'category'
    },
    // priceBeforeTaxes: {
    //     type: Number,
    // },
    
    images: {
        type:[],
        default:[]
    },
    image: {
        type: {},
        required: true
    },
    favorite: {
        type: Boolean,
        default: false
    },
    favorites:{
        type:[Number],
        ref:"user",
        default:[],
        select:false
    },
    related:{
        type:[]
    },
    rateValues: {
        type: Number,
        default: 0
    },
    rateCount: {
        type: Number,
        default: 0
    },
    totalRate: {
        type: Number,
        default: 0
    },
    userRate:{
        type:Number,
        default: 0
    },
    deleted: {
        type: Boolean,
        default: false
    },
    // regions: [Number],
    // cities: [Number],
    price: {
        type:Number,
    },
    priceAfterOffer: {
        type: Number,
    },
    offer: {
        type: Number,
        default: 0
    },
    quantity: {
        type: Number,
        default: 0
    },
    maxOrder:{
        type:Number,
        default:5,
    },
    advertisement:{
        type:Number,
        ref:'advertisement'
    },
    meta:{
        type:[]
    },
    useStatus:{
        type: String,
        enum:['USED','NEW'],
        default:'NEW'
    },
    serialNumber:{
        type: String,
        default:'12345'
    },
    // taxes:{
    //     type: Number,
    //     default: 5
    // },
}, { timestamps: true });

productSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
productSchema.plugin(mongooseI18n,{locales:process.env.LOCALES})
productSchema.plugin(autoIncrement.plugin, { model: 'product', startAt: 1 });

export default mongoose.models.product || mongoose.model('product', productSchema);