const mongoose =require( "mongoose");
const { Schema } = mongoose
const autoIncrement =require( 'mongoose-auto-increment');
const mongooseI18n = require('mongoose-i18n-localize')
const config = process.env
const CountrySchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    name:{
        type:String,
        // required:true,
        i18n: true
    },
    countryCode:{
        type:String,
        default:'20'
    },
    countryKey:{
        type:String,
        default:'EG'
    },
    currency:{
        type:String,
        default:'EGP'
    },
    language:{
        type:String,
        default:'AR'
    },
    deleted:{
        type:Boolean,
        default:false
    }

}, { timestamps: true });

CountrySchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
CountrySchema.plugin(mongooseI18n,{locales:config.LOCALES})
CountrySchema.plugin(autoIncrement.plugin, { model: 'country', startAt: 1 });

export default mongoose.models.country || mongoose.model('country', CountrySchema);