import mongoose, { Schema } from "mongoose"
import bcrypt               from 'bcrypt'
import autoIncrement        from 'mongoose-auto-increment'
import mongooseI18n         from 'mongoose-i18n-localize'
import isEmail              from 'validator/lib/isEmail'
const config = process.env

const cartSchema = new Schema({
    promocode:{
        type:Number,
        ref:'promocode'
    },
    products:{
        type:[
            // {
            //     product:{type:Number, ref:'product'},
            //     quantity:{type:Number}
            // }
        ],
        default:[]
    },
    shippingCompany:{
        type:Number,
        ref:'shipping'
    },
    totalPrice:{type:Number, default:0},
    totalPriceAfterDiscount:{type:Number, default:0},
    totalPriceAfterTaxes:{type:Number, default:0},
    finalPrice:{type:Number, default:0}
})

const userSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    username: {
        type: String,
        // required: true,
    },
    password: {
        type: String
    },
    phone: {
        type: String,
        trim: true
    },
    email: {
        type: String,
        trim: true,
        validate: {
            validator: (email) => isEmail(email),
            message: 'Invalid Email Syntax'
        }
    },
    type: {
        type: String,
        enum: ['ADMIN','SUB_ADMIN','CLIENT','VISITOR'],
        required: true,
        default:'CLIENT'
    },
    deleted: {
        type: Boolean,
        default: false
    },
    tokens:{
        type:[
            {
                token:{type: String,required: true},
                type:{type:String,required: true,enum:['ios','android','web']}
            }
        ]
    },
    activated :{
        type: Boolean,
        default: true
    },
    rules:{
        type:[Number],
        ref:'assignRule'
    },
    image:{
        type: String
    },
    language:{
        type:String,
        default:'ar',
        enum:['ar','en']
    },
    notification:{
        type:Boolean,
        default:true
    },
    countryCode:{
        type:String,
        default:'20'
    },
    countryKey:{
        type:String,
        default:'EG'
    },
    socialId:{
        type: String
    },
    socialMediaType:{
        type: String,
        enum: ['NORMAL','FACEBOOK','TWITTER','INSTAGRAM','GOOGLE','APPLE'],
        default:'NORMAL'
    },
    credit:{
        type:Number,
        default:0
    },
    country:{
        type: Number,
        ref: 'country'
    },
    city:{
        type: Number,
        ref: 'city'
    },
    region:{
        type: Number,
        ref: 'region'
    },
    activeChatHead:{
        type: Boolean,
        default: false
    },
    lastCheckoutCreditId:{
        type:String
    },
    lastCheckoutCredit:{
        type:Number,
        default:0
    },
    cart: {
        type:cartSchema
    }

}, { timestamps: true });


userSchema.pre('save', function (next) {
    const account = this;
    if (!account.isModified('password')) return next();
    const salt = bcrypt.genSaltSync();
    bcrypt.hash(account.password, salt).then(hash => {
        account.password = hash;
        next();
    }).catch(err => console.log(err));
});
userSchema.methods.isValidPassword = function (newPassword, callback) {
    let user = this;
    bcrypt.compare(newPassword, user.password, function (err, isMatch) {
        if (err)
            return callback(err);
        callback(null, isMatch);
    });
};

userSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret.password;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
userSchema.plugin(mongooseI18n,{locales:config.LOCALES})
userSchema.plugin(autoIncrement.plugin, { model: 'user', startAt: 1 });
export default mongoose.models.user || mongoose.model('user', userSchema);