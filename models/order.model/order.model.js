import mongoose, {Schema} from "mongoose";
import mongooseI18n  from'mongoose-i18n-localize'
import autoIncrement from'mongoose-auto-increment'
const config = process.env

let productSchema = new Schema({ 
    product: { type: Number, required: true, ref:'product'},
    quantity: { type: Number, required: true },
});

const orderSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    deleted: {
        type: Boolean,
        default: false
    },
    user: {
        type: Number,
        ref: 'user',
        required: true
    },
    products:{
        type:[productSchema]
    },
    address: {
        type: Number,
        ref: 'address',
    },
    notes:{
        type: String
    },
    status: {
        type: String,
        enum: ['WAITING',
            'ACCEPTED',
            'REJECTED',
            'CANCELLED',
            'PREPARED',
            'ONTHEWAY',
            'SHIPPED',
            'DELIVERED'],
        default: 'WAITING'
    },
    rejectReason:{
        type: String
    },
    paymentMethod: {
        type: String,
        enum: ['CASH', 'CREDIT']
    },
    creditCard: {
        type: Number,
        ref: 'credit'
    },
    promocode:{
        type: Number,
        ref:'promocode'
    },
    
    totalPrice:{
        type: Number
    },
    totalPriceAfterDiscount:{
        type: Number
    },
    totalPriceAfterTaxes:{
        type: Number
    },
    finalPrice:{
        type: Number
    },
    type:{
        type:String,
        enum:['ONLINE','MANULAY'],
        default:'ONLINE'
    },
    replied:{
        type:Boolean
    },
    deliveredDate:{
        type: Date,
        default: new Date()
    },
    orderNumber:{
        type:String,
        default:'23443'
    },
    adminInformed:{
        type:Boolean,
        default:false
    },
    moneyReminder:{
        type:Boolean,
        default:false
    },
    discountValue:{
        type: Number
    },
    shipping:{
        type:Number,
        ref:'shipping'
    },
    ///////////////////////////////////////////////////////
    checkoutId:{
        type:String
    },
    paymentId:{
        type:String
    },
    paymentStatus:{
        type:String,
        enum:['PENDING','FAILED','SUCCESSED','REFUNDED'],
    },

}, { timestamps: true });
orderSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
orderSchema.plugin(mongooseI18n,{locales:config.LOCALES})
orderSchema.plugin(autoIncrement.plugin, { model: 'order', startAt: 1 });

export default mongoose.models.order || mongoose.model('order', orderSchema);