const mongoose =require( "mongoose");
const { Schema } = mongoose
const autoIncrement =require( 'mongoose-auto-increment');
const mongooseI18n = require('mongoose-i18n-localize')
const config = process.env

const favouritesSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    product:{
        type:Number,
        ref: 'product',
        required:true
    },
    user:{
        type: Number,
        required: true,
        ref:'user'
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

favouritesSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
favouritesSchema.plugin(mongooseI18n,{locales:config.LOCALES})
favouritesSchema.plugin(autoIncrement.plugin, { model: 'favourites', startAt: 1 });

export default mongoose.models.favourites || mongoose.model('favourites', favouritesSchema);