const mongoose =require( "mongoose");
const { Schema } = mongoose
const autoIncrement =require( 'mongoose-auto-increment');
const mongooseI18n = require('mongoose-i18n-localize')
const config = process.env
const CategorySchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        // required: true,
        i18n: true
    },
    image:{
        type: {},
        // required: true,
    },
    hasChild:{
        type: Boolean
    },
    parent:{
        type: Number,
        ref: 'category'
    },
    deleted: {
        type: Boolean,
        default: false
    },
    // type:{
    //     type: String,
    //     enum: ['FIRST_CATEGORY','SECOND_CATEGORY','THIRD_CATEGORY']
    // }
}, { timestamps: true });

CategorySchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
CategorySchema.plugin(mongooseI18n,{locales:config.LOCALES})
CategorySchema.plugin(autoIncrement.plugin, { model: 'category', startAt: 1 });

export default mongoose.models.category || mongoose.model('category', CategorySchema);