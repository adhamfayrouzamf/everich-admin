import mongoose,{ Schema} from "mongoose";
// import mongooseI18n from "mongoose-i18n-localize";
import autoIncrement from 'mongoose-auto-increment';
const imagesSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    name:{
        type:String,
        // i18n: true
    },
    category:{
        type: Number
    },
    product:{
        type:Number
    },
    image:{
        type:String,
        required:true
    },
    type:{
        type: String,
        enum: ['HOME_IMAGES','HOME_SLIDER'],
        default: 'HOME_IMAGES'
    },
    deleted:{
        type:Boolean,
        default:false
    },
    sliderType:{
        type: String,
        enum:['MOBILE','WEBSITE']
    }
}, { timestamps: true });

imagesSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
imagesSchema.plugin(autoIncrement.plugin, { model: 'images', startAt: 1 });

export default mongoose.models.images || mongoose.model('images', imagesSchema);