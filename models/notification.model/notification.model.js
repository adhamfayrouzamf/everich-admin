import mongoose, { Schema } from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
import mongooseI18nLocalize from 'mongoose-i18n-localize'

const NotifSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    resource: {
        type: Number,
        ref: 'user'
    },
    target: {
        type: Number,
        ref: 'user'
    },
    title:{
        type:String,
        i18n:true
    },
    text:{
        type:String,
        i18n:true
    },
    subject:{
        type:Number
    },
    subjectType:{
        type:String,
        enum:['USER','CONTACTUS','ADMIN','ORDER','CHANGE_ORDER_STATUS','PROMOCODE','PRODUCT']
    },
    type:{
        type:String,
        enum:['CLIENT', 'SPECIFIC', 'ALL']
    },
    read:{
        type:Boolean,
        default:false
    },
    promocode:{
        type:Number,
        ref:"promocode"
    },
    informed : {
        type:[Number],
        ref:"user"
    },
    users:{
        type: [Number],
        ref: 'user'
    },
    order:{
        type: Number,
        ref: 'order'
    },
    image:{
        type: {
            url:String,
            id:String
        }
    }
}, { timestamps: true });

NotifSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
NotifSchema.plugin(autoIncrement.plugin, { model: 'notification', startAt: 1 });
NotifSchema.plugin(mongooseI18nLocalize,{locales:process.env.LOCALES});
export default mongoose.models.notification || mongoose.model('notification', NotifSchema);