const mongoose =require( "mongoose");
const { Schema } = mongoose
const autoIncrement =require( 'mongoose-auto-increment');
const mongooseI18n = require('mongoose-i18n-localize')
const config = process.env

const commentSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    comment:{
        type:String,
        required:true
    },
    user:{
        type:Number,
        ref:'user'
    },
    userRate:{
        type:Number,
    },
    product:{
        type:Number,
        ref:'product'
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

commentSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});

autoIncrement.initialize(mongoose.connection);
commentSchema.plugin(mongooseI18n, {locales: config.LOCALES});
commentSchema.plugin(autoIncrement.plugin, { model: 'comment', startAt: 1 });

export default mongoose.models.comment || mongoose.model('comment', commentSchema);