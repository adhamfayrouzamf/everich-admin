import mongoose, {Schema} from "mongoose"
import autoIncrement      from 'mongoose-auto-increment'
import mongooseI18n       from 'mongoose-i18n-localize'
// const config = process.env
const trademarkSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    name:{
        type:String,
        i18n:true,
    },
    category:{
        type: Number,
        ref:'category'
    },
    deleted:{
        type:Boolean,  
        default:false
    }
}, { timestamps: true });


trademarkSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
trademarkSchema.plugin(mongooseI18n,{locales:process.env.LOCALES})
trademarkSchema.plugin(autoIncrement.plugin, { model: 'trademark', startAt: 1 });

export default mongoose.models.trademark || mongoose.model('trademark', trademarkSchema);